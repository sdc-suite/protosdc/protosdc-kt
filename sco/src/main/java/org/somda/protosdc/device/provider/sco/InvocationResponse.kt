package org.somda.protosdc.device.provider.sco

import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.model.biceps.AbstractSetResponse
import org.somda.protosdc.model.biceps.InvocationError
import org.somda.protosdc.model.biceps.InvocationInfo
import org.somda.protosdc.model.biceps.InvocationState
import org.somda.protosdc.model.biceps.LocalizedText
import org.somda.protosdc.model.biceps.TransactionId

/**
 * Initial response required to answer a set service request.
 *
 *
 * The object is supposed to be created by using [Context.createSuccessfulResponse]
 * and [Context.createUnsuccessfulResponse].
 */
public data class InvocationResponse constructor(
    val mdibVersion: MdibVersion,
    val transactionId: TransactionId,
    val invocationState: InvocationState,
    val invocationError: InvocationError? = null,
    val invocationErrorMessage: List<LocalizedText> = listOf()
) {
    public fun toAbstractSetResponse(): AbstractSetResponse = AbstractSetResponse(
        mdibVersionGroupAttr = this.mdibVersion.toMdibVersionGroup(),
        invocationInfo = InvocationInfo(
            transactionId = this.transactionId,
            invocationState = this.invocationState,
            invocationError = this.invocationError,
            invocationErrorMessage = this.invocationErrorMessage
        )
    )
}