package org.somda.protosdc.device.consumer.sco

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.onFailure
import kotlinx.coroutines.channels.onSuccess
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.somda.protosdc.common.DeviceId
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.model.biceps.OperationInvokedReport
import kotlin.time.Duration

/**
 * Helper class to dispatch incoming operation invoked report parts to [ScoTransaction] objects.
 */
public class OperationInvocationDispatcher constructor(
    awaitingTransactionDuration: Duration,
    instanceId: InstanceId,
    deviceId: DeviceId
) {
    private val awaitingTransactionTimeout = awaitingTransactionDuration
    private val logger by InstanceLogger(instanceId(), deviceId())
    private val pendingReports = mutableMapOf<Int, Channel<OperationInvokedReport.ReportPart>>()
    private val runningTransactions = mutableMapOf<Int, ScoTransactionImpl>()
    private val awaitingTransactions = mutableMapOf<Int, Instant>()
    private val mutex = Mutex()

    /**
     * Accepts a report and dispatches its report parts to registered SCO transactions.
     *
     * By using [OperationInvocationDispatcher.registerTransaction], SCO transactions can be registered to get notified
     * on incoming operation invoked report parts.
     *
     * If an SCO transaction is not registered by the time the first report pops up, reports are going to be buffered.
     * Buffered "dead" reports are sanitized on each incoming report.
     *
     * @param report the report to process.
     */
    public suspend fun dispatchReport(report: OperationInvokedReport) {
        report.reportPart.forEach { dispatchReport(it) }
    }

    /**
     * Registers an SCO transaction and delivers buffered reports immediately.
     *
     * Once a final report is registered, the allocated heap used for the transaction is erased.
     *
     * @param transaction the transaction to register.
     */
    public suspend fun registerTransaction(transaction: ScoTransactionImpl) {
        mutex.withLock {
            val transactionId = transaction.transactionId()
            if (runningTransactions.containsKey(transactionId)) {
                logger.warn { "Try to add transaction $transactionId twice, which is not permitted" }
                return
            }

            logger.debug { "Register transaction and apply pending reports for transaction #${transaction.transactionId()}" }
            logger.trace { "Register transaction and apply pending reports: $transaction" }
            awaitingTransactions.remove(transactionId)
            runningTransactions[transactionId] = transaction
            pendingReports[transactionId]?.let {
                addReportsToTransaction(it, transaction)
            }
        }
    }

    private suspend fun dispatchReport(reportPart: OperationInvokedReport.ReportPart) {
        sanitizeAwaitingTransactions()

        val transactionId = reportPart.invocationInfo.transactionId.unsignedInt
        mutex.withLock {
            val reportPartsQueue = pendingReports[transactionId] ?: Channel<OperationInvokedReport.ReportPart>(
                ScoTransaction.MAX_REPORTS
            ).also {
                pendingReports[transactionId] = it
                awaitingTransactions[transactionId] = Clock.System.now()
            }

            val transaction = runningTransactions[transactionId]
            if (reportPart.isFinalReport()) {
                runningTransactions.remove(transactionId)
            }

            reportPartsQueue.trySend(reportPart)
                .onSuccess {
                    transaction?.let {
                        addReportsToTransaction(reportPartsQueue, it)
                    }
                }
                .onFailure {
                    logger.warn { "Too many reports received for transaction $transactionId" }
                }
        }
    }

    private fun addReportsToTransaction(
        channel: Channel<OperationInvokedReport.ReportPart>,
        transaction: ScoTransactionImpl
    ) {
        do {
            val receivedReport = channel.tryReceive().getOrNull()?.let {
                logger.debug { "Announce incoming report: $it" }
                transaction.addReport(it)
            } != null
        } while (receivedReport)
    }

    private suspend fun sanitizeAwaitingTransactions() {
        mutex.withLock {
            awaitingTransactions.mapNotNull { (transactionId, start) ->
                val finish = Clock.System.now()
                if (finish - start > awaitingTransactionTimeout) {
                    logger.info("Transaction $transactionId timed out. Remove from awaiting transactions.")
                    transactionId
                } else {
                    null
                }
            }.forEach {
                awaitingTransactions.remove(it)
                pendingReports.remove(it)
            }
        }
    }
}