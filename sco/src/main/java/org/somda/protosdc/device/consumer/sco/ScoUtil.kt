package org.somda.protosdc.device.consumer.sco

import org.somda.protosdc.model.biceps.AbstractSetResponseOneOf
import org.somda.protosdc.model.biceps.InvocationInfo
import org.somda.protosdc.model.biceps.InvocationState
import org.somda.protosdc.model.biceps.OperationInvokedReport

public fun AbstractSetResponseOneOf.invocationInfo(): InvocationInfo = when (this) {
    is AbstractSetResponseOneOf.ChoiceAbstractSetResponse ->
        this.value.invocationInfo

    is AbstractSetResponseOneOf.ChoiceActivateResponse ->
        this.value.abstractSetResponse.invocationInfo

    is AbstractSetResponseOneOf.ChoiceSetStringResponse ->
        this.value.abstractSetResponse.invocationInfo

    is AbstractSetResponseOneOf.ChoiceSetValueResponse ->
        this.value.abstractSetResponse.invocationInfo

    is AbstractSetResponseOneOf.ChoiceSetComponentStateResponse ->
        this.value.abstractSetResponse.invocationInfo

    is AbstractSetResponseOneOf.ChoiceSetMetricStateResponse ->
        this.value.abstractSetResponse.invocationInfo

    is AbstractSetResponseOneOf.ChoiceSetContextStateResponse ->
        this.value.abstractSetResponse.invocationInfo

    is AbstractSetResponseOneOf.ChoiceSetAlertStateResponse ->
        this.value.abstractSetResponse.invocationInfo
}

public fun AbstractSetResponseOneOf.invocationState(): InvocationState.EnumType =
    this.invocationInfo().invocationState.enumType

public fun AbstractSetResponseOneOf.transactionId(): Int = when (this) {
    is AbstractSetResponseOneOf.ChoiceAbstractSetResponse ->
        this.value.invocationInfo.transactionId.unsignedInt

    is AbstractSetResponseOneOf.ChoiceActivateResponse ->
        this.value.abstractSetResponse.invocationInfo.transactionId.unsignedInt

    is AbstractSetResponseOneOf.ChoiceSetStringResponse ->
        this.value.abstractSetResponse.invocationInfo.transactionId.unsignedInt

    is AbstractSetResponseOneOf.ChoiceSetValueResponse ->
        this.value.abstractSetResponse.invocationInfo.transactionId.unsignedInt

    is AbstractSetResponseOneOf.ChoiceSetComponentStateResponse ->
        this.value.abstractSetResponse.invocationInfo.transactionId.unsignedInt

    is AbstractSetResponseOneOf.ChoiceSetMetricStateResponse ->
        this.value.abstractSetResponse.invocationInfo.transactionId.unsignedInt

    is AbstractSetResponseOneOf.ChoiceSetContextStateResponse ->
        this.value.abstractSetResponse.invocationInfo.transactionId.unsignedInt

    is AbstractSetResponseOneOf.ChoiceSetAlertStateResponse ->
        this.value.abstractSetResponse.invocationInfo.transactionId.unsignedInt
}

public fun OperationInvokedReport.ReportPart.transactionId(): Int = this.invocationInfo.transactionId.unsignedInt

public fun OperationInvokedReport.ReportPart.invocationState(): InvocationState.EnumType =
    this.invocationInfo.invocationState.enumType

public fun OperationInvokedReport.ReportPart.isFinalReport(): Boolean = when (this.invocationState()) {
    InvocationState.EnumType.Wait,
    InvocationState.EnumType.Start -> false

    InvocationState.EnumType.Cnclld,
    InvocationState.EnumType.CnclldMan,
    InvocationState.EnumType.Fin,
    InvocationState.EnumType.FinMod,
    InvocationState.EnumType.Fail -> true
}

public fun OperationInvokedReport.ReportPart.isSuccess(): Boolean = when (this.invocationState()) {
    InvocationState.EnumType.Wait,
    InvocationState.EnumType.Start,
    InvocationState.EnumType.Cnclld,
    InvocationState.EnumType.CnclldMan,
    InvocationState.EnumType.Fail -> false

    InvocationState.EnumType.Fin,
    InvocationState.EnumType.FinMod -> true
}

public fun OperationInvokedReport.ReportPart.isFailure(): Boolean = when (this.invocationState()) {
    InvocationState.EnumType.Wait,
    InvocationState.EnumType.Start -> false

    InvocationState.EnumType.Cnclld,
    InvocationState.EnumType.CnclldMan,
    InvocationState.EnumType.Fail -> true

    InvocationState.EnumType.Fin,
    InvocationState.EnumType.FinMod -> false
}

public fun OperationInvokedReport.ReportPart.isIntermediate(): Boolean = when (this.invocationState()) {
    InvocationState.EnumType.Wait,
    InvocationState.EnumType.Start -> true

    InvocationState.EnumType.Cnclld,
    InvocationState.EnumType.CnclldMan,
    InvocationState.EnumType.Fail,
    InvocationState.EnumType.Fin,
    InvocationState.EnumType.FinMod -> false
}