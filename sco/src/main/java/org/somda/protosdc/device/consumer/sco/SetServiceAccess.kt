package org.somda.protosdc.device.consumer.sco

import org.somda.protosdc.model.biceps.AbstractSetOneOf
import org.somda.protosdc.model.biceps.Activate
import org.somda.protosdc.model.biceps.SetAlertState
import org.somda.protosdc.model.biceps.SetComponentState
import org.somda.protosdc.model.biceps.SetContextState
import org.somda.protosdc.model.biceps.SetMetricState
import org.somda.protosdc.model.biceps.SetString
import org.somda.protosdc.model.biceps.SetValue

/**
 * Lean interface to get access to the BICEPS set service.
 */
public interface SetServiceAccess {
    /**
     * Invokes any operation.
     *
     * @param request the set operation request.
     * @return the set operation response.
     */
    public suspend fun invokeOperation(request: AbstractSetOneOf): ScoTransaction

    public suspend fun invokeOperation(request: Activate): ScoTransaction {
        return invokeOperation(AbstractSetOneOf.ChoiceActivate(request))
    }

    public suspend fun invokeOperation(request: SetValue): ScoTransaction {
        return invokeOperation(AbstractSetOneOf.ChoiceSetValue(request))
    }

    public suspend fun invokeOperation(request: SetString): ScoTransaction {
        return invokeOperation(AbstractSetOneOf.ChoiceSetString(request))
    }

    public suspend fun invokeOperation(request: SetAlertState): ScoTransaction {
        return invokeOperation(AbstractSetOneOf.ChoiceSetAlertState(request))
    }

    public suspend fun invokeOperation(request: SetMetricState): ScoTransaction {
        return invokeOperation(AbstractSetOneOf.ChoiceSetMetricState(request))
    }

    public suspend fun invokeOperation(request: SetContextState): ScoTransaction {
        return invokeOperation(AbstractSetOneOf.ChoiceSetContextState(request))
    }

    public suspend fun invokeOperation(request: SetComponentState): ScoTransaction {
        return invokeOperation(AbstractSetOneOf.ChoiceSetComponentState(request))
    }
}