package org.somda.protosdc.device.consumer.sco

import org.somda.protosdc.model.biceps.AbstractAlertStateOneOf
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.AbstractDeviceComponentStateOneOf
import org.somda.protosdc.model.biceps.AbstractMetricStateOneOf
import org.somda.protosdc.model.biceps.Activate
import org.somda.protosdc.model.biceps.Activate.Argument
import org.somda.protosdc.model.biceps.Extension
import org.somda.protosdc.model.biceps.HandleRef
import org.somda.protosdc.model.biceps.SetAlertState
import org.somda.protosdc.model.biceps.SetComponentState
import org.somda.protosdc.model.biceps.SetContextState
import org.somda.protosdc.model.biceps.SetMetricState
import org.somda.protosdc.model.biceps.SetString
import org.somda.protosdc.model.biceps.SetValue
import java.math.BigDecimal

public object ScoFactory {
    public fun activate(
        operationHandleRef: HandleRef,
        arguments: List<Argument> = listOf(),
        extensionElement: Extension? = null
    ): Activate = Activate(
        abstractSet = abstractSet(extensionElement, operationHandleRef),
        argument = arguments
    )

    public fun setString(
        operationHandleRef: HandleRef,
        requestedStringValue: String,
        extensionElement: Extension? = null
    ): SetString = SetString(
        abstractSet = abstractSet(extensionElement, operationHandleRef),
        requestedStringValue = requestedStringValue
    )

    public fun setValue(
        operationHandleRef: HandleRef,
        requestedNumericValue: BigDecimal,
        extensionElement: Extension? = null
    ): SetValue = SetValue(
        abstractSet = abstractSet(extensionElement, operationHandleRef),
        requestedNumericValue = requestedNumericValue
    )

    public fun setContextState(
        operationHandleRef: HandleRef,
        proposedContextStates: List<AbstractContextStateOneOf> = listOf(),
        extensionElement: Extension? = null
    ): SetContextState = SetContextState(
        abstractSet = abstractSet(extensionElement, operationHandleRef),
        proposedContextState = proposedContextStates
    )

    public fun setComponentState(
        operationHandleRef: HandleRef,
        proposedComponentStates: List<AbstractDeviceComponentStateOneOf>,
        extensionElement: Extension? = null,
    ): SetComponentState = SetComponentState(
        abstractSet = abstractSet(extensionElement, operationHandleRef),
        proposedComponentState = proposedComponentStates
    )

    public fun setMetricState(
        operationHandleRef: HandleRef,
        proposedMetricStates: List<AbstractMetricStateOneOf> = listOf(),
        extensionElement: Extension? = null
    ): SetMetricState = SetMetricState(
        abstractSet = abstractSet(extensionElement, operationHandleRef),
        proposedMetricState = proposedMetricStates
    )

    public fun setAlertState(
        operationHandleRef: HandleRef,
        proposedAlertState: AbstractAlertStateOneOf,
        extensionElement: Extension? = null,
    ): SetAlertState = SetAlertState(
        abstractSet = abstractSet(extensionElement, operationHandleRef),
        proposedAlertState = proposedAlertState
    )

    private fun abstractSet(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef
    ) = org.somda.protosdc.model.biceps.AbstractSet(
        extensionElement = extensionElement,
        operationHandleRef = operationHandleRef
    )
}