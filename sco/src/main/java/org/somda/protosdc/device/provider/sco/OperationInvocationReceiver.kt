package org.somda.protosdc.device.provider.sco

import org.somda.protosdc.model.biceps.*

public interface OperationInvocationReceiver {
    public suspend fun activate(
        operationHandle: String,
        context: Context,
        request: Activate
    ): InvocationResponse {
        throw NotImplementedError()
    }

    public suspend fun setAlertState(
        operationHandle: String,
        context: Context,
        request: SetAlertState
    ): InvocationResponse {
        throw NotImplementedError()
    }

    public suspend fun setComponentState(
        operationHandle: String,
        context: Context,
        request: SetComponentState
    ): InvocationResponse {
        throw NotImplementedError()
    }

    public suspend fun setContextState(
        operationHandle: String,
        context: Context,
        request: SetContextState
    ): InvocationResponse {
        throw NotImplementedError()
    }

    public suspend fun setMetricState(
        operationHandle: String,
        context: Context,
        request: SetMetricState
    ): InvocationResponse {
        throw NotImplementedError()
    }

    public suspend fun setString(
        operationHandle: String,
        context: Context,
        request: SetString
    ): InvocationResponse {
        throw NotImplementedError()
    }

    public suspend fun setValue(
        operationHandle: String,
        context: Context,
        request: SetValue
    ): InvocationResponse {
        throw NotImplementedError()
    }
}