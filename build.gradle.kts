import com.google.protobuf.gradle.*

plugins {
    id("org.somda.protosdc.sdc.shared")
    alias(libs.plugins.org.somda.gitlab.maven.publishing)
    alias(libs.plugins.com.google.protobuf)
    alias(libs.plugins.org.somda.append.snapshot.id)
}

dependencies {
    api(projects.biceps)
    api(projects.common)
    api(projects.crypto)
    api(projects.sco)
    api(projects.udp)

    api(libs.bundles.grpc)
    api(libs.bundles.protosdc)

    implementation(libs.kotlinx.coroutines)
    implementation(libs.kotlinx.datetime)
    implementation(libs.ktoml.core)

    testImplementation(testFixtures(projects.biceps))
    testImplementation(testFixtures(projects.common))
    testImplementation(testFixtures(projects.crypto))
    testImplementation(testFixtures(projects.udp))
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

// https://github.com/grpc/grpc-java/issues/7690
val classified: String? = when (val it = osdetector.classifier) {
    "osx-aarch_64" -> "osx-x86_64"
    else -> null
}

fun withClassified(pkg: Provider<MinimalExternalModuleDependency>, classified: String?) = when (classified) {
    null -> pkg.get().toString()
    else -> "${pkg.get()}:$classified"
}

protobuf {
    protoc {
        // The artifact spec for the Protobuf Compiler
        artifact = withClassified(libs.google.protobuf.protoc, classified)
    }
    plugins {
        id("grpc") {
            artifact = withClassified(libs.grpc.generator.java, classified)
        }
        id("grpckt") {
            artifact = withClassified(libs.grpc.generator.kotlin, "jdk8@jar")
        }
    }
    generateProtoTasks {
        ofSourceSet("testFixtures").forEach {
            it.plugins {
                // Apply the "grpc" and "grpckt" plugins whose specs are defined above, without options.
                id("grpc")
                id("grpckt")
            }
        }
    }
}

kotlin {
    explicitApi()
}