package it.org.somda.protosdc.network.udp

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.somda.protosdc.network.ip.ipVersion
import org.somda.protosdc.network.udp.SocketInfo
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.network.udp.udp
import org.somda.protosdc.test.util.DefaultWait
import org.somda.protosdc.test.util.LongWait
import org.somda.protosdc.test.util.NetworkInterfaceWithMulticastSupport
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*

class UdpMessageQueueTest {
    private lateinit var udp: Udp
    private val EXPECTED_MESSAGE = "sample".toByteArray()
    private val MESSAGE_COUNT = 100


    @BeforeEach
    fun beforeEach(): Unit = runBlocking {
        udp = udpMock {
            messageChannel(Channel(MESSAGE_COUNT))
        }.apply { start() }
    }

    @AfterEach
    fun afterEach(): Unit = runBlocking {
        udp.stop()
    }

    @Test
    fun `test sending and receiving of UDP messages`(): Unit = runBlocking {
        val channel = udp.messageQueue.subscribe()
        val range = (1..MESSAGE_COUNT)
        launch {
            range.forEach { _ ->
                udp.messageQueue.sendMulticastMessage(EXPECTED_MESSAGE)
            }
        }

        coroutineScope {
            withTimeout(DefaultWait.MILLIS) {
                range.forEach {
                    val actualMsg = channel.receive()
                    assertTrue(
                        udpMessageEqualsBytes(
                            actualMsg,
                            EXPECTED_MESSAGE
                        )
                    ) { "Message $it differed to expected message" }
                }
            }
        }
    }

    @Test
    fun `test sendMessage() overrides`(): Unit = runBlocking {
        val udpMessageQueue = udp.messageQueue

        val channel = udpMessageQueue.subscribe()
        udpMessageQueue.sendMulticastMessage(EXPECTED_MESSAGE)

        udpMessageQueue.sendMessage(EXPECTED_MESSAGE, Inet4Address.getByName("127.0.0.1"), 1)
        udpMessageQueue.sendMessage(EXPECTED_MESSAGE, Inet4Address.getByName("127.0.0.2"), 2)

        coroutineScope {
            withTimeout(DefaultWait.MILLIS) {
                val messages = mutableListOf<UdpMessage>()
                for (i in 1..3) {
                    messages.add(channel.receive())
                }

                assertTrue(udpMessageEqualsBytes(messages[0], EXPECTED_MESSAGE))
                assertEquals(1, messages[1].header.destinationPort)
                assertEquals(2, messages[2].header.destinationPort)
                assertEquals("127.0.0.1", messages[1].header.ipHeader.destinationAddress().hostAddress)
                assertEquals("127.0.0.2", messages[2].header.ipHeader.destinationAddress().hostAddress)
            }
        }
    }

    @Test
    fun `test with real udp binding`(): Unit = runBlocking {
        val createRealUdpComponent: (NetworkInterface, InetAddress) -> Udp = { networkInterface, address ->
            udp {
                binding(
                    UdpBindingConfig(
                        networkInterface,
                        address.ipVersion(),
                        SocketInfo(address, 6464)
                    )
                )
            }
        }

        val channelCapacity = 100
        val address = InetAddress.getByName("239.255.255.250")
        val networkInterface = NetworkInterfaceWithMulticastSupport.findFor(address.hostAddress).let {
            check(it != null)
            it
        }

        val receiverChannel = Channel<UdpMessage>(channelCapacity)
        val senderChannel = Channel<UdpMessage>(channelCapacity)
        val receiverComponent = createRealUdpComponent(networkInterface, address).apply { start() }
        val receiverUdpMessageQueue = receiverComponent.messageQueue
        val senderComponent = createRealUdpComponent(networkInterface, address).apply { start() }
        val senderUdpMessageQueue = senderComponent.messageQueue

        val expectedMsg = uniqueMessage()
        val subscribe = receiverUdpMessageQueue.subscribe()
        launch {
            for (i in 1..channelCapacity * 2) {
                senderUdpMessageQueue.sendMulticastMessage(expectedMsg)
            }
        }
        withTimeout(LongWait.MILLIS) {
            launch {
                for (i in 1..channelCapacity * 2) {
                    assertTrue(udpMessageEqualsBytes(subscribe.receive(), expectedMsg))
                }
            }.join()
        }

        receiverChannel.close()
        senderChannel.close()
        receiverComponent.stop()
        senderComponent.stop()
    }

    private fun uniqueMessage() = UUID.randomUUID().toString().toByteArray()

    private fun udpMessageEqualsBytes(msg: UdpMessage, bytes: ByteArray) =
        msg.data.copyOfRange(0, msg.header.length).contentEquals(bytes)
}