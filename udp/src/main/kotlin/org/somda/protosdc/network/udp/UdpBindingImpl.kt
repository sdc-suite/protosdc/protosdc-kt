package org.somda.protosdc.network.udp

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.common.awaitWithIoDispatcher
import org.somda.protosdc.network.ip.IpVersion
import org.somda.protosdc.network.ip.firstAddressFor
import org.somda.protosdc.network.ip.ipVersion
import org.somda.protosdc.network.udp.UdpBinding.MessageInterceptor
import java.io.IOException
import java.net.*
import java.time.Duration
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

internal class UdpBindingImpl @AssistedInject constructor(
    @Assisted private val receiver: Channel<UdpMessage>,
    @Assisted private val networkInterface: NetworkInterface,
    @Assisted private val ipVersion: IpVersion,
    @Assisted private val multicastSocketInfo: SocketInfo?,
    @Assisted private val messageInterceptor: MessageInterceptor,
    udpConfig: UdpConfig,
) : UdpBinding, ServiceDelegate by ServiceDelegateImpl() {
    private val maxMessageSize = udpConfig.maxMessageSize

    @AssistedFactory
    interface Factory {
        fun create(
            receiver: Channel<UdpMessage>,
            networkInterfaceAddress: NetworkInterface,
            ipVersion: IpVersion,
            multicastSocketInfo: SocketInfo?,
            messageInterceptor: MessageInterceptor = object : MessageInterceptor {}
        ): UdpBindingImpl
    }

    private companion object : Logging {
        /**
         * Defines the minimum delay for the retransmission algorithm.
         */
        val UDP_MIN_DELAY: Duration = Duration.ofMillis(50L)

        /**
         * Defines the maximum delay for the retransmission algorithm.
         */
        val UDP_MAX_DELAY: Duration = Duration.ofMillis(250L)

        /**
         * Defines the upper delay for the retransmission algorithm.
         */
        val UDP_UPPER_DELAY: Duration = Duration.ofMillis(450L)

        /**
         * Defines the retry number for unreliable UDP multicast traffic.
         */
        const val UDP_REPEAT = 1
    }

    private val closed = AtomicBoolean()

    private val random = Random()
    private val multicastAddress = when (multicastSocketInfo != null) {
        true -> InetSocketAddress(multicastSocketInfo.ipAddress, multicastSocketInfo.port)
        false -> null
    }

    private val networkInterfaceAddress: InetAddress =
        checkNotNull(networkInterface.firstAddressFor(ipVersion)) {
            "Could not retrieve network interface address for $networkInterface"
        }

    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)

    private val multicastSocketRunner = coroutineScopeBinding.async(start = CoroutineStart.LAZY) {
        while (isActive) {
            val packet = DatagramPacket(ByteArray(maxMessageSize), maxMessageSize)
            try {
                awaitWithIoDispatcher { incomingSocket.receive(packet) }.getOrThrow()
            } catch (e: IOException) {
                logger.trace("Could not process UDP packet. Discard.")
                continue
            }

            val message = UdpMessage(
                packet.data,
                UdpHeader(
                    packet.port,
                    incomingSocket.localPort,
                    packet.length,
                    ipVersion.header(packet.address, incomingSocket.localAddress)
                )
            )
            runCatching {
                receiver.send(messageInterceptor.onReceive(message))
            }.onFailure {
                logger.info(it) { "Sending of a UDP message to a receive channel failed: ${it.message}" }
            }
        }
    }

    private val unicastSocketRunner = coroutineScopeBinding.async(start = CoroutineStart.LAZY) {
        while (isActive) {
            val packet = DatagramPacket(ByteArray(maxMessageSize), maxMessageSize)
            try {
                awaitWithIoDispatcher { outgoingSocket.receive(packet) }.getOrThrow()
            } catch (e: IOException) {
                logger.trace("Could not process UDP packet. Discard.")
                continue
            }

            val message = UdpMessage(
                packet.data,
                UdpHeader(
                    packet.port,
                    incomingSocket.localPort,
                    packet.length,
                    ipVersion.header(packet.address, outgoingSocket.localAddress)
                )
            )
            runCatching {
                receiver.send(messageInterceptor.onReceive(message))
            }.onFailure {
                logger.info(it) { "Sending of a UDP message to a receive channel failed: ${it.message}" }
            }
        }
    }

    private lateinit var incomingSocket: DatagramSocket
    private var multicastSocket: MulticastSocket? = null
    private val outgoingSocket = DatagramSocket(0, networkInterfaceAddress)

    init {
        onStartUp {
            if (multicastSocketInfo != null) {
                require(multicastSocketInfo.ipAddress.ipVersion() == ipVersion) {
                    "UDP binding is $ipVersion, but multicast group is ${multicastSocketInfo.ipAddress.ipVersion()}"
                }
                require(multicastSocketInfo.ipAddress.isMulticastAddress) {
                    "Given address is not a multicast address: ${multicastSocketInfo.ipAddress}"
                }

                multicastSocket = MulticastSocket(multicastSocketInfo.port)
                logger.info { "Join UDP multicast address group $multicastAddress" }
                multicastSocket!!.joinGroup(multicastAddress, networkInterface)
                incomingSocket = multicastSocket!!
            } else {
                multicastSocket = null
                incomingSocket = DatagramSocket(0, networkInterfaceAddress)
                logger.info { "Incoming socket is open: ${incomingSocket.localSocketAddress}" }
            }

            multicastSocketRunner.start()
            unicastSocketRunner.start()

            // wait for the sockets, the IGMP join and the worker threads to be available
            // this has primarily been an issue in low performance environments such as the CI
            // todo see https://gitlab.com/sdc-suite/protosdc-kt/-/issues/20
            delay(1000)
            logger.info { "UDP binding $this is running" }

            //  make sure sockets get closed prior to shutdown of the Java virtual machine
            Runtime.getRuntime().addShutdownHook(thread(start = false) {
                runBlocking {
                    if (isRunning()) {
                        stop()
                    }
                }
            })
        }

        onShutDown {
            if (closed.compareAndSet(false, true)) {
                logger.info { "Shut down UDP binding $this" }
                if (multicastSocket != null) {
                    awaitWithIoDispatcher {
                        runCatching {
                            multicastSocket!!.leaveGroup(multicastAddress, networkInterface)
                        }.onFailure {
                            logger.warn(it) { "Error leaving multicast group: ${it.message}" }
                        }
                    }
                    logger.info { "UDP binding $this is running" }
                }
                multicastSocketRunner.cancel()
                unicastSocketRunner.cancel()
                incomingSocket.close()
                outgoingSocket.close()

                if (coroutineScopeBinding.isActive) {
                    coroutineScopeBinding.cancel()
                }

                logger.info { "UDP binding $this shut down" }
            }
        }
    }

    @Throws(UdpMaxMessageSizeExceededException::class)
    override suspend fun sendMulticastMessage(data: ByteArray) {
        checkNotNull(multicastSocketInfo) {
            "No transport data in UDP message, which is required as no multicast group " +
                    "is available. Message: $data"
        }

        sendMessage(data, multicastSocketInfo.ipAddress, multicastSocketInfo.port)
    }

    @Throws(UdpMaxMessageSizeExceededException::class)
    override suspend fun sendMessage(data: ByteArray, destinationAddress: String, destinationPort: Int) {
        val dest = awaitWithIoDispatcher { InetAddress.getByName(destinationAddress) }.getOrThrow()
        sendMessage(data, dest, destinationPort)
    }

    @Throws(UdpMaxMessageSizeExceededException::class)
    override suspend fun sendMessage(data: ByteArray, destinationAddress: InetAddress, destinationPort: Int) {
        val intercepted = messageInterceptor.onSend(
            UdpMessage(
                data,
                UdpHeader(
                    sourcePort = outgoingSocket.localPort,
                    destinationPort = destinationPort,
                    data.size,
                    ipVersion.header(
                        outgoingSocket.localAddress,
                        destinationAddress
                    )
                )
            )
        )

        if (intercepted.header.ipHeader.destinationAddress().ipVersion() != ipVersion) {
            return
        }

        if (intercepted.data.size > maxMessageSize) {
            throw UdpMaxMessageSizeExceededException(
                "In an attempt to write ${data.size} Bytes the maximum UDP message size of $maxMessageSize Bytes has been exceeded"
            )
        }

        sendMessageWithRetry(
            DatagramPacket(
                intercepted.data,
                intercepted.data.size,
                fixInetAddressScope(intercepted.header.ipHeader.destinationAddress()),
                intercepted.header.destinationPort
            )
        )
    }

    /**
     * Adds interface specific IPv6 scopes to addresses.
     *
     * @param addr address to add IPv6 scopes to if it is an IPv6 address
     * @return updated address
     */
    private suspend fun fixInetAddressScope(addr: InetAddress?) = when (addr) {
        is Inet6Address -> fixIpv6AddressScope(addr)
        else -> addr
    }

    private suspend fun fixIpv6AddressScope(addr: Inet6Address): Inet6Address {
        val fixed = awaitWithIoDispatcher {
            Inet6Address.getByAddress(addr.hostAddress, addr.address, networkInterface)
        }.getOrThrow()
        return fixed
    }

    private suspend fun sendMessageWithRetry(packet: DatagramPacket) {
        val send: suspend () -> Unit = {
            awaitWithIoDispatcher {
                outgoingSocket.send(packet)
            }.onFailure {
                logger.warn(it) { "Error sending a packet to a UDP socket: ${it.message}" }
            }
        }

        send()

        // Retransmission algorithm as defined in
        // http://docs.oasis-open.org/ws-dd/soapoverudp/1.1/os/wsdd-soapoverudp-1.1-spec-os.docx
        val udpMinDelay = UDP_MIN_DELAY.toMillis().toInt()
        val udpMaxDelay = UDP_MAX_DELAY.toMillis().toInt()
        val udpUpperDelay = UDP_UPPER_DELAY.toMillis().toInt()
        var t = random.nextInt(udpMaxDelay - udpMinDelay + 1) + udpMinDelay

        for (udpRepeat in UDP_REPEAT downTo 1) {
            try {
                delay(t.toLong())
            } catch (e: InterruptedException) {
                logger.info(e) { "Thread interrupted" }
                break
            }

            send()

            t *= 2
            if (t > udpUpperDelay) {
                t = udpUpperDelay
            }
        }
    }

    override fun toString() = makeStringRepresentation()

    private fun makeStringRepresentation(): String {
        val multicast = when (multicastSocketInfo != null) {
            true -> "w/ multicast joined at ${multicastSocketInfo.ipAddress.hostName}:${multicastSocketInfo.port}"
            false -> "w/o multicast"
        }
        return "[$networkInterfaceAddress:[${incomingSocket.localPort}|${outgoingSocket.localPort}] $multicast]"
    }
}