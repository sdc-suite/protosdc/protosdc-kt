package org.somda.protosdc.network.udp.dagger

import dagger.BindsInstance
import dagger.Component
import org.somda.protosdc.common.ComponentConfig
import org.somda.protosdc.network.udp.UdpBinding
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.UdpConfig
import org.somda.protosdc.network.udp.UdpMessageQueue
import javax.annotation.Nullable
import javax.inject.Singleton

@Singleton
@Component(modules = [UdpModule::class])
internal interface UdpComponent {
    @Component.Builder
    interface Builder {
        fun bind(@BindsInstance @ComponentConfig udpBindings: List<UdpBindingConfig>): Builder
        fun bind(@BindsInstance @ComponentConfig @Nullable udpConfig: UdpConfig?): Builder
        fun build(): UdpComponent
    }

    fun udpBindings(): List<@JvmSuppressWildcards UdpBinding>
    fun udpMessageQueue(): UdpMessageQueue
}