package org.somda.protosdc.network.udp

/**
 * Exception to be thrown when a UDP message size exceeds an internal threshold.
 */
public class UdpMaxMessageSizeExceededException constructor(message: String) : Exception(message)