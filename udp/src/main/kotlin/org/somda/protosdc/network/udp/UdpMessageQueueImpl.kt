package org.somda.protosdc.network.udp

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import java.net.InetAddress

internal class UdpMessageQueueImpl @AssistedInject constructor(
    @Assisted incomingChannel: Channel<UdpMessage>,
    @Assisted udpBindings: Collection<UdpBinding>,
    udpConfig: UdpConfig,
) : UdpMessageQueue, ServiceDelegate by ServiceDelegateImpl() {
    private val queueCapacity = udpConfig.queueCapacity

    @AssistedFactory
    interface Factory {
        fun create(
            incomingChannel: Channel<UdpMessage>,
            outgoingBindings: Collection<UdpBinding> = listOf(),
        ): UdpMessageQueueImpl
    }

    private val publisher = ChannelPublisher<UdpMessage>("UdpMessageQueuePublisher", queueCapacity)
    private val outgoingMessageQueue = Channel<OutgoingPacketInfo>(queueCapacity)
    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)

    init {
        onStartUp {
            logger.info { "Start UdpMessageQueue" }
            coroutineScopeBinding.launch(CoroutineName("Outgoing UdpMessageQueue")) {
                try {
                    while (true) {
                        when (val message = outgoingMessageQueue.receive()) {
                            is OutgoingPacketInfo.DataOnly ->
                                udpBindings.forEach { it.sendMulticastMessage(message.data) }
                            is OutgoingPacketInfo.DataAndDestinationAsInetAddress ->
                                udpBindings.forEach {
                                    it.sendMessage(
                                        message.data,
                                        message.destinationAddress,
                                        message.destinationPort
                                    )
                                }
                            is OutgoingPacketInfo.DataAndDestinationAsString ->
                                udpBindings.forEach {
                                    it.sendMessage(
                                        message.data,
                                        message.destinationAddress,
                                        message.destinationPort
                                    )
                                }
                        }
                    }
                } catch (e: CancellationException) {
                    logger.info { "Outgoing UdpMessageQueue has been cancelled" }
                } catch (e: Exception) {
                    logger.info(e) { "Outgoing UdpMessageQueue ended with exception: ${e.message}" }
                }
            }
            coroutineScopeBinding.launch(CoroutineName("Incoming UdpMessageQueue")) {
                try {
                    while (isActive) {
                        try {
                            val message = incomingChannel.receive()
                            logger.trace { "Incoming UdpMessageQueue received UDP message, posting: $message" }
                            publisher.offer(message)
                        } catch (e: CancellationException) {
                            logger.info { "Incoming UdpMessageQueue has been cancelled" }
                        } catch (e: Exception) {
                            logger.warn(e) { "Incoming UdpMessageQueue encountered an error on event dissemination" }
                        }
                    }
                } finally {
                    logger.info { "Incoming UdpMessageQueue ended" }
                }
            }
            logger.info { "UdpMessageQueue is running" }
        }

        onShutDown {
            if (coroutineScopeBinding.isActive) {
                coroutineScopeBinding.cancel()
            }
        }
    }

    override suspend fun sendMulticastMessage(data: ByteArray) =
        outgoingMessageQueue.trySend(OutgoingPacketInfo.DataOnly(data)).isSuccess

    override suspend fun sendMessage(data: ByteArray, destinationAddress: InetAddress, destinationPort: Int) =
        outgoingMessageQueue.trySend(
            OutgoingPacketInfo.DataAndDestinationAsInetAddress(
                data,
                destinationAddress,
                destinationPort
            )
        ).isSuccess

    override suspend fun sendMessage(data: ByteArray, destinationAddress: String, destinationPort: Int) =
        outgoingMessageQueue.trySend(
            OutgoingPacketInfo.DataAndDestinationAsString(
                data,
                destinationAddress,
                destinationPort
            )
        ).isSuccess

    override suspend fun subscribe() = publisher.subscribe()

    private sealed class OutgoingPacketInfo {
        data class DataOnly(val data: ByteArray) : OutgoingPacketInfo() {
            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as DataOnly

                if (!data.contentEquals(other.data)) return false

                return true
            }

            override fun hashCode(): Int {
                return data.contentHashCode()
            }
        }

        data class DataAndDestinationAsInetAddress(
            val data: ByteArray,
            val destinationAddress: InetAddress,
            val destinationPort: Int
        ) : OutgoingPacketInfo() {
            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as DataAndDestinationAsInetAddress

                if (!data.contentEquals(other.data)) return false
                if (destinationAddress != other.destinationAddress) return false
                if (destinationPort != other.destinationPort) return false

                return true
            }

            override fun hashCode(): Int {
                var result = data.contentHashCode()
                result = 31 * result + destinationAddress.hashCode()
                result = 31 * result + destinationPort
                return result
            }
        }

        data class DataAndDestinationAsString(
            val data: ByteArray,
            val destinationAddress: String,
            val destinationPort: Int
        ) : OutgoingPacketInfo() {
            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as DataAndDestinationAsString

                if (!data.contentEquals(other.data)) return false
                if (destinationAddress != other.destinationAddress) return false
                if (destinationPort != other.destinationPort) return false

                return true
            }

            override fun hashCode(): Int {
                var result = data.contentHashCode()
                result = 31 * result + destinationAddress.hashCode()
                result = 31 * result + destinationPort
                return result
            }
        }
    }

    private companion object: Logging
}