package org.somda.protosdc.network.udp

import kotlinx.serialization.Serializable

/**
 * UDP configuration.
 *
 * @property maxMessageSize Defines the maximum message size transmitted via UDP.
 * @property queueCapacity Defines the queue capacity of the [org.somda.protosdc.network.udp.UdpMessageQueue].
 */
@Serializable
public data class UdpConfig(
    val maxMessageSize: Int = 4_096,
    val queueCapacity: Int = 1_024
)
