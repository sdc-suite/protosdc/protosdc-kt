package org.somda.protosdc.network.udp

import java.net.InetAddress

/**
 * Tuple to describe an IP address plus port.
 * 
 * @param ipAddress the IP address information.
 * @param port the port number.
 */
public data class SocketInfo(
    val ipAddress: InetAddress,
    val port: Int
)
