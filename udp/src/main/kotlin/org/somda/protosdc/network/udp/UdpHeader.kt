package org.somda.protosdc.network.udp

import org.somda.protosdc.network.ip.IpHeader

/**
 * UDP header model with source port, destination port, length and enclosed IP header.
 */
public data class UdpHeader(
    val sourcePort: Int,
    val destinationPort: Int,
    val length: Int,
    val ipHeader: IpHeader
)
