package org.somda.protosdc.network.udp

import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.Service
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.network.udp.dagger.DaggerUdpComponent
import java.util.*

@DslMarker
public annotation class UdpInitDsl

public interface Udp : Service {
    public val messageQueue: UdpMessageQueue

    @UdpInitDsl
    public interface Init {
        public val instanceId: InstanceId
        public val config: UdpConfig
        public val bindings: Collection<UdpBindingConfig>

        public fun instanceId(instanceId: InstanceId)
        public fun config(config: UdpConfig)
        public fun binding(vararg binding: UdpBindingConfig)
    }
}

private class UdpImpl(init: UdpInitImpl) : Udp, ServiceDelegate by ServiceDelegateImpl() {

    private val daggerComponent = DaggerUdpComponent.builder()
        .bind(init.config)
        .bind(init.bindings.toList())
        .build()

    init {
        this.onStartUp {
            daggerComponent.udpMessageQueue().start()
            daggerComponent.udpBindings().forEach { it.start() }
        }

        this.onShutDown {
            daggerComponent.udpBindings().forEach { it.stop() }
            daggerComponent.udpMessageQueue().stop()
        }
    }

    override val messageQueue: UdpMessageQueue
        get() = daggerComponent.udpMessageQueue()
}

@UdpInitDsl
internal class UdpInitImpl : Udp.Init {
    private var _instanceId: InstanceId = InstanceId(UUID.randomUUID().toString())
    private var _config = UdpConfig()
    private val _bindings = mutableListOf<UdpBindingConfig>()

    override val instanceId: InstanceId
        get() = _instanceId

    override val config: UdpConfig
        get() = _config

    override val bindings: Collection<UdpBindingConfig>
        get() = _bindings

    override fun instanceId(instanceId: InstanceId) {
        this._instanceId = instanceId
    }

    override fun config(config: UdpConfig) {
        this._config = config
    }

    override fun binding(vararg binding: UdpBindingConfig) {
        this._bindings.addAll(binding)
    }
}

public fun udp(init: Udp.Init.() -> Unit = {}): Udp {
    return UdpImpl(UdpInitImpl().apply(init))
}

