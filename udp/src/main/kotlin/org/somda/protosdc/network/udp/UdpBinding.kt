package org.somda.protosdc.network.udp

import org.somda.protosdc.common.Service
import org.somda.protosdc.network.ip.IpVersion
import org.somda.protosdc.network.udp.UdpBinding.MessageInterceptor
import java.net.InetAddress
import java.net.NetworkInterface

public data class UdpBindingConfig(
    val networkInterface: NetworkInterface,
    val ipVersion: IpVersion,
    val multicastSocket: SocketInfo? = null,
    val messageInterceptor: MessageInterceptor = object : MessageInterceptor {}
)

/**
 * Binding to receive and send UDP messages including multicast support.
 */
public interface UdpBinding : Service {
    /**
     * Interceptor to read and/or modify incoming and outgoing messages.
     */
    public interface MessageInterceptor {
        public fun onReceive(message: UdpMessage): UdpMessage = message
        public fun onSend(message: UdpMessage): UdpMessage = message
    }

    /**
     * Multicasts a data buffer via the UDP socket bound to this service.
     *
     * The function blocks until the message is sent. As no destination information is required, this function shall
     * not be called if no multicast socket information is available as otherwise an exception is thrown.
     *
     * @param data the data to send.
     * @throws UdpMaxMessageSizeExceededException if the message exceeds the maximum message size.
     * @throws IllegalStateException if no multicast socket information is available.
     */
    @Throws(UdpMaxMessageSizeExceededException::class, IllegalStateException::class)
    public suspend fun sendMulticastMessage(data: ByteArray)

    /**
     * Sends a data buffer via the UDP socket bound to this service.
     *
     * The function blocks until the message is sent.
     *
     * @param data the data to send.
     * @param destinationAddress the destination host address.
     * @param destinationPort the destination host port.
     * @throws UdpMaxMessageSizeExceededException if the message exceeds the maximum message size.
     */
    @Throws(UdpMaxMessageSizeExceededException::class)
    public suspend fun sendMessage(data: ByteArray, destinationAddress: String, destinationPort: Int)

    /**
     * Sends a data buffer via the connected UDP socket.
     *
     * The function blocks until the message is sent.
     *
     * @param data the data to send.
     * @param destinationAddress the destination host address.
     * @param destinationPort the destination host port.
     * @throws UdpMaxMessageSizeExceededException if the message exceeds the maximum message size.
     */
    @Throws(UdpMaxMessageSizeExceededException::class)
    public suspend fun sendMessage(data: ByteArray, destinationAddress: InetAddress, destinationPort: Int)
}