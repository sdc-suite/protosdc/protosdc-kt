# protoSDC-kt Dokka conventions

This document describes conventions when writing Dokka comments for the protoSDC-kt software. Each of the guidelines below consists of a short description of the rule and an explanation plus - optionally - an example.

## Write Dokka to be read as source code

Always write Dokka comments to be best read on source code level. Only a minority of developers will ever pay attention to the Dokka output. Nevertheless, put Markdown code to the comments to facilitate well-formatted output.

## What to document

 - All methods that are visible to users of protoSDC-kt **shall** be fully defined with Dokka comments.
 - If a method is overridden in a subclass, Dokka comments **shall** only be present if it says something distinct from the original definition of the method.

## Comment layout

- Each comment starts with a brief punchline which **shall** be a single sentence followed by two new-lines. It has the responsibility of summing up the method or class to readers scanning the class or package.
- The first sentence **shall** be ended by a dot.
- The third-person form **shall** be used at the start. For example, "Gets the foo", "Sets the bar" or "Consumes the baz". Avoid the second-person form, such as "Get the foo".

```kotlin
  /**
   * This brief description is the comment's punchline.
   *
   * A detailed description follows.
   *
   * A list:
   * - the first item
   * - the second item
   * - the third item
   */
  fun foobar() // ...
```

## Use *this* to refer to an instance of the class

When referring to an instance of a class being documented, the term *this* **shall** be used to reference it. For example, "Returns a copy of this foo with the bar value updated".

## Aim for short single line sentences

- Dokka sentences **should** fit on a single line. Allow flexibility in the line length, favoring 120 characters to make this work.
- In most cases, each new sentence **should** start on a new line. This aids readability as source code and simplifies refactoring re-writes of complex Dokka comments.

```kotlin
  /**
   * This is the first paragraph, on one line.
   * 
   * This is the first sentence of the second paragraph, on one line.
   * This is the second sentence of the second paragraph, on one line.
   * This is the third sentence of the second paragraph which is a bit longer so has been
   * split onto a second line, as that makes sense.
   * This is the fourth sentence, which starts a new line, even though there is space above.
   */
  fun foobar() // ...
```

## Use brackets to cross-reference methods and classes

Many Dokka descriptions reference other methods and classes. This can be achieved most effectively by using Markdown brackets.

```kotlin
  /**
   * First paragraph.
   * 
   * Link to a class named 'Foo': [Foo].
   * Link to a method 'bar' on a class named 'Foo': [Foo.bar].
  fun foobar() ...
```

## Use @param, @return and @throws

Almost all methods take in a parameter, return a result or both. The @param and @return features specify those inputs and outputs. The @throws feature specifies the thrown exceptions.

- The @param entries **shall** be specified in the same order as the parameters.
- The @return **shall** be after the @param entries.
- The @throws **shall** be after the @return entry.

## Use one blank line before @param

There **shall** be one blank line between a comment and the first @param, @return or @throws. This aids readability in source code.

## Treat @param and @return as a phrase

The @param and @return **should** be treated as phrases rather than complete sentences. They **shall** start with a lower case letter, typically using the word *the*. They **shall not** end with a dot if be written as a single sentence. This aids readability in source code and when generated.

## Treat @throws as an if clause

The @throws feature **should** normally be followed by *if* and the rest of the phrase describing the condition. For example, `@throws IllegalArgumentException if the file could not be found`. This aids readability in source code and when generated.

## Avoid @author

The @author feature can be used to record the authors of a class. This **shall** be avoided, as it is usually out of date, and it can promote code ownership by an individual. The source control system is in a much better position to record authors.

## Full example

```kotlin
/**
 * Provides arithmetic operations.
 *
 * You can divide by using [ArithmeticOperations.divide].
 */
class ArithmeticOperations {
    /**
     * Computes the division of two values.
     *
     * A lengthy description of divisions are found here.
     *
     * @param a the dividend.
     * @param b the divisor.
     * @return  the division of a and b.
     * @throws ArithmeticException if b is 0.
     */
    fun divide(a: Int, b: Int) = a / b
}
```