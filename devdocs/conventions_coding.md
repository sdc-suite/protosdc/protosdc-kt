# protoSDC-kt coding conventions

This document describes coding conventions when writing Kotlin code for the protoSDC-kt software beyond code style formatting.

## Naming

- Naming of objects, methods and variables **shall** follow camelCase notation.
- Any object, method or variable **shall** have speaking names.
- If the scope of a variable spans less than 5 lines, names **may** be narrowed down to 3 characters.
- Loop variables **may** be expressed with 1 character (e.g., the glorious i and j loop counters).

## Immutability

Immutability allows stricter control over data, hence immutable objects **should** be used where applicable. 

## Error handling

- Errors **shall** be expressed using exceptions.
- Errors **should not** be expressed by return values.
- The format of exception messages **shall** be in compliance with the format for log messages (see below).

## Logging

### Log levels

- The DEBUG log level **shall** be used generously in order to ease debugging.
- The INFO log level **shall** be used sparingly for important log information that is not repeated at a high frequency. INFO **may** also express uncritical failures.
- The WARN log level **shall** be used only in case of a failure that hinders the application from continuing its processing for a specific function call. A WARN is always a sign of malfunction and should be investigated when observed.
- The ERROR log level **shall** be used only in case that the application detects a systematic or incurable failure.

### Format

For any line in the log output,

- any variable portion **should** be placed at the end if reasonable,
- the line **should not** comprise more than three sentences, preferably one,
- a sentence stop **shall not** be placed if the line comprises one sentence only,
- sentence stops **shall** be placed if the line comprises more than one sentence. 

### Format of toString() methods

- For data classes, the Kotlin built-in stringification **shall** be used.
- Other `toString()` outputs **should** be formatted in accordance to the following style:

- Simple class name prefix followed by essential attributes in parentheses
- Each attribute is separated by using a semicolon
- Each attribute key is separated from the attribute value by an equal character

Example: `MdibVersion(sequenceId=<VALUE>;instanceId=<VALUE>;version=<VALUE>)` for the `MdibVersion` class with attributes `instanceId`, `sequenceId` and `version`.

> See `org.somda.protosdc.common.stringify()` to generate `toString()` output that is in accordance with the aforementioned rules.

## Dependency injection

protoSDC-kt utilizes Dagger2 for dependency injection.

- In general, every class that exhibits logic **shall** be injected by using Dagger.
- Data classes **should not** be injected.
- Local helper classes **may** be used without dependency injection.
- Assisted parameters **shall** appear before any other parameters.
- Field injection **should not** be used.

### Class bindings with default modules

For each module there **shall** be a package named `dagger` that includes a default module and other Dagger related classes and annotations.

Example: the common module encloses a package named `org.somda.protosdc.common.dagger` with the default module named `CommonModule`.

## Module configuration

### Static configuration

The static configuration of a module **shall** be facilitated by a dedicated Dagger configuration module located at each `dagger` package. The configuration module **shall** provide named bindings for all module related static configuration values (values that are applied once during runtime and never change). Consider utilizing `org.somda.protosdc.common.Config`.

Example: the common module encloses the file `CommonConfigModule` derived from `org.somda.protosdc.common.Config`. It defines default values for all static configuration values related to the sub-project *common*.

### Dynamic configuration

Sometimes it’s required to dynamically inject configuration parameters at runtime (those that are generated or changed at runtime). In protoSDC-kt that type of configuration is called *Settings*. Typically, settings are interfaces that are supposed to be implemented by a module's user.

Any dynamic configuration class/interface name **shall** be appended with `Settings`.

## Directory/package structure

- Every module *shall* include a package named `dagger` (see section _Class bindings with default modules_)
- Factories and builders **shall** be provided in Dagger-style (one per class as inner interface, no separate packages or classes needed)
- Helper classes/files go into a sub-package of each package named `helper`. Helper classes/files are not intended to be used publicly.
- Event messages *shall* go into `event` packages (see section _Observer pattern_) 

## Observer pattern

protoSDC-kt uses Google Guava's EventBus to enable the observer pattern. The EventBus is a lightweight class that is capable of invoking arbitrary, annotated functions on registered observers. In order to distinguish between different function calls that accept the same payload, every event **shall** be conveyed using a strong-typed wrapper class called *message*, ending with `Message` and stored in a separate sub-package called `event`.

Example:
```kotlin
data class DeviceEnteredMessage(val payload: DiscoveredDevice)
```

### Empty observer interfaces

For every observer there **shall** be an empty interface that describes the accepted messages.

Example:

```kotlin
/**  
 * Indicates a discovery observer.
 * 
 * Supported messages:  
 * - [DeviceEnteredMessage]  
 * - [DeviceLeftMessage]  
 * - [ProbedDeviceFoundMessage]  
 * - [DeviceProbeTimeoutMessage]
 */
interface DiscoveryObserver
```

## Test code conventions

Rule of thumb: treat your test code as you treat productive code.

### Test classes

 Unit tests are performed in the *test* phase whereas integration tests are performed in a downstream *integrationTest* phase.

#### Unit test classes

Unit test class names **shall** end with a *Test* and go to the same package as the tested classes go. Unit tests are located at *src/test/kotlin*.

#### Integration test classes

Integration test class names **shall** end with an IT and go to a package that complies with the top-level package named *it* followed by the module package name plus a suitable sub-package tree. Integration tests are located at *src/integrationTest/kotlin*.

Example: `it.org.somda.protosdc.grpc` where `it` is the integration test prefix package, `org.org.somda.protosdc` is the module's base package and `grpc` is a meaningful sub-package name.
 
#### Unit test scaffold

Any test **should** outline the following points:

- Given: the input to the test
- When: the tested condition
- Then: the expected result

To ease reading a test, it is helpful to annotate a test with comments marking the given-when-then sections.

Example:

```kotlin
// Given an object Foo
val foo = Foo()

// When foo invokes bar()
// Then expect an ArithmeticException to be thrown
assertThrows<ArithmeticException> { foo.bar() }
```
