plugins {
    id("org.somda.protosdc.sdc.shared")
    alias(libs.plugins.org.somda.gitlab.maven.publishing)
}

dependencies {
    api(projects.common)
    api(libs.protosdc.biceps.model)
}

kotlin {
    explicitApi()
}