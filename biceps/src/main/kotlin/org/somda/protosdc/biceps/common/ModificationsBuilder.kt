package org.somda.protosdc.biceps.common

import com.google.common.collect.ArrayListMultimap
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.somda.protosdc.model.biceps.AbstractComplexDeviceComponentDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceBatteryDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceChannelDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceClockDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceLocationContextDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceMdsDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceMeansContextDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoicePatientContextDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceScoDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceSystemContextDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceVmdDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor
import org.somda.protosdc.model.biceps.AbstractStateOneOf
import org.somda.protosdc.model.biceps.AlertSystemDescriptor
import org.somda.protosdc.model.biceps.BatteryDescriptor
import org.somda.protosdc.model.biceps.ChannelDescriptor
import org.somda.protosdc.model.biceps.ClockDescriptor
import org.somda.protosdc.model.biceps.Mdib
import org.somda.protosdc.model.biceps.MdsDescriptor
import org.somda.protosdc.model.biceps.ScoDescriptor
import org.somda.protosdc.model.biceps.SystemContextDescriptor
import org.somda.protosdc.model.biceps.VmdDescriptor

private fun AbstractComplexDeviceComponentDescriptor.withoutChildren() = this.copy(
    alertSystem = null,
    sco = null
)


/**
 * Utility class to create an [MdibDescriptionModifications] object from an [Mdib] container.
 */
public class ModificationsBuilder(mdib: Mdib) {

    private val states = ArrayListMultimap.create<String, AbstractStateOneOf>()
    private val modifications = MdibDescriptionModifications()

    /**
     * Gets the precompiled [MdibDescriptionModifications].
     *
     * @return the built [MdibDescriptionModifications]. As the build process takes place right after object
     * construction, this method returns a pre-assembled object structure.
     */
    public fun get(): MdibDescriptionModifications {
        return modifications
    }

    private fun build(mds: MdsDescriptor) {
        val handle = mds.handleString()
        insert(
            ChoiceMdsDescriptor(
                mds.copy(
                    abstractComplexDeviceComponentDescriptor =
                    mds.abstractComplexDeviceComponentDescriptor.withoutChildren(),
                    systemContext = null,
                    vmd = listOf(),
                    clock = null,
                    battery = listOf()
                )
            )
        )
        insertChildren(mds.abstractComplexDeviceComponentDescriptor)
        mds.systemContext?.let { build(it, handle) }
        mds.battery.forEach { build(it, handle) }
        mds.clock?.let { build(it, handle) }
        mds.vmd.forEach { build(it, handle) }
    }

    private fun insert(descriptor: AbstractDescriptorOneOf, parent: String? = null) {
        modifications.insert(DescriptionItem(descriptor, states[descriptor.handleString()], parent))
    }

    private fun insertChildren(
        componentDescriptor: AbstractComplexDeviceComponentDescriptor
    ) {
        componentDescriptor.alertSystem?.let { build(it, componentDescriptor.handleString()) }
        componentDescriptor.sco?.let { build(it, componentDescriptor.handleString()) }
    }

    private fun build(vmd: VmdDescriptor, parentHandle: String) {
        insert(
            ChoiceVmdDescriptor(
                vmd.copy(
                    abstractComplexDeviceComponentDescriptor =
                    vmd.abstractComplexDeviceComponentDescriptor.withoutChildren(),
                    channel = listOf(),
                )
            ), parentHandle
        )
        insertChildren(vmd.abstractComplexDeviceComponentDescriptor)
        vmd.channel.forEach { build(it, vmd.handleString()) }
    }

    private fun build(channel: ChannelDescriptor, parentHandle: String) {
        insert(
            ChoiceChannelDescriptor(
                channel.copy(
                    metric = listOf(),
                )
            ), parentHandle
        )

        channel.handleString().also {
            channel.metric.forEach { insert(it.toAbstractDescriptorOneOf(), channel.handleString()) }
        }
    }

    private fun build(systemContext: SystemContextDescriptor, parentHandle: String) {
        insert(
            ChoiceSystemContextDescriptor(
                systemContext.copy(
                    ensembleContext = listOf(),
                    locationContext = null,
                    meansContext = listOf(),
                    operatorContext = listOf(),
                    patientContext = null,
                    workflowContext = listOf()
                )
            ), parentHandle
        )
        systemContext.ensembleContext.forEach {
            insert(ChoiceEnsembleContextDescriptor(it), systemContext.handleString())
        }
        systemContext.locationContext?.let {
            insert(ChoiceLocationContextDescriptor(it), systemContext.handleString())
        }
        systemContext.meansContext.forEach {
            insert(ChoiceMeansContextDescriptor(it), systemContext.handleString())
        }
        systemContext.operatorContext.forEach {
            insert(ChoiceOperatorContextDescriptor(it), systemContext.handleString())
        }
        systemContext.patientContext?.let {
            insert(ChoicePatientContextDescriptor(it), systemContext.handleString())
        }
        systemContext.workflowContext.forEach {
            insert(ChoiceWorkflowContextDescriptor(it), systemContext.handleString())
        }
    }

    private fun build(battery: BatteryDescriptor, parentHandle: String) {
        insert(
            ChoiceBatteryDescriptor(
                battery
            ), parentHandle
        )
    }

    private fun build(clock: ClockDescriptor, parentHandle: String) {
        insert(
            ChoiceClockDescriptor(
                clock
            ), parentHandle
        )
    }

    private fun build(alertSystem: AlertSystemDescriptor, parentHandle: String) {
        insert(
            ChoiceAlertSystemDescriptor(
                alertSystem.copy(
                    alertCondition = listOf(),
                    alertSignal = listOf()
                )
            ), parentHandle
        )

        alertSystem.alertCondition.forEach {
            insert(it.toAbstractDescriptorOneOf(), alertSystem.handleString())
        }
        alertSystem.alertSignal.forEach {
            insert(ChoiceAlertSignalDescriptor(it), alertSystem.handleString())
        }
    }

    private fun build(sco: ScoDescriptor, parentHandle: String) {
        insert(
            ChoiceScoDescriptor(
                sco.copy(
                    operation = listOf()
                )
            ), parentHandle
        )

        sco.operation.forEach {
            insert(it.toAbstractDescriptorOneOf(), sco.handleString())
        }
    }

    init {
        mdib.mdState?.let { mdState ->
            mdState.state.forEach { state ->
                states.put(state.descriptorHandleString(), state)
            }
        }

        mdib.mdDescription?.let { mdDescription ->
            mdDescription.mds.forEach { mds -> this.build(mds) }
        }
    }
}