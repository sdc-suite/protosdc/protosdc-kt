package org.somda.protosdc.biceps.common.helper

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc.biceps.common.WrittenMdibDescriptionModifications
import org.somda.protosdc.biceps.common.WrittenMdibStateModifications
import org.somda.protosdc.biceps.common.access.MdibAccess
import org.somda.protosdc.biceps.common.access.MdibAccessEvent
import org.somda.protosdc.common.ChannelPublisher

internal class EventDistributor(private val publisher: ChannelPublisher<MdibAccessEvent>) {

    private companion object : Logging

    /**
     * Creates an [MdibAccessEvent.DescriptionModification] and sends it to all subscribers.
     *
     * @param mdibAccess the MDIB access to be published as part of the event payload.
     * @param entities all written entities.
     */
    suspend fun sendDescriptionModificationEvent(
        mdibAccess: MdibAccess,
        entities: WrittenMdibDescriptionModifications
    ) {
        publisher.send(MdibAccessEvent.DescriptionModification(mdibAccess, entities))
    }

    /**
     * Creates a state-specific [MdibAccessEvent] based on the *states* modifications sends it to all subscribers.
     *
     * @param mdibAccess the MDIB access to be published as part of the event payload.
     * @param states all updated states.
     */
    suspend fun sendStateModificationEvent(mdibAccess: MdibAccess, states: WrittenMdibStateModifications) {
        when (states) {
            is WrittenMdibStateModifications.Alert ->
                publisher.send(MdibAccessEvent.AlertStateModification(mdibAccess, states.alertStates))

            is WrittenMdibStateModifications.Component ->
                publisher.send(MdibAccessEvent.ComponentStateModification(mdibAccess, states.componentStates))

            is WrittenMdibStateModifications.Context ->
                publisher.send(MdibAccessEvent.ContextStateModification(mdibAccess, states.contextStates))

            is WrittenMdibStateModifications.Metric ->
                publisher.send(MdibAccessEvent.MetricStateModification(mdibAccess, states.metricStates))

            is WrittenMdibStateModifications.Operation ->
                publisher.send(MdibAccessEvent.OperationStateModification(mdibAccess, states.operationStates))

            is WrittenMdibStateModifications.Waveform ->
                publisher.send(
                    MdibAccessEvent.WaveformModification(
                        mdibAccess,
                        states.waveformStates
                    )
                )
        }
    }
}