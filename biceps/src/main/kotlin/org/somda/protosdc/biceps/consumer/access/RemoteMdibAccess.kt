package org.somda.protosdc.biceps.consumer.access

import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.access.ObservableMdibAccess
import org.somda.protosdc.biceps.common.access.ReadTransaction
import org.somda.protosdc.biceps.common.access.WriteDescriptionResult
import org.somda.protosdc.biceps.common.access.WriteStateResult
import org.somda.protosdc.biceps.common.storage.PreprocessingException

/**
 * Thread-safe MDIB read and write access for the BICEPS consumer side.
 */
public interface RemoteMdibAccess : ObservableMdibAccess, ReadTransaction {
    public interface Factory {
        public fun create(initialMdibVersion: MdibVersion): RemoteMdibAccess
    }

    /**
     * Processes the description modifications object, stores the data internally and trigger an event.
     *
     * Data is passed through without any checks to the MDIB storage.
     *
     * @param descriptionModifications a set of insertions, updates and deletes.
     * @param mdibVersion the MDIB version to set for this write operation.
     * @return a write result including inserted, updates and deleted entities.
     * @throws PreprocessingException if something goes wrong during preprocessing, i.e., the consistency of the MDIB
     * would be violated.
     */
    @Throws(PreprocessingException::class)
    public suspend fun writeDescription(
        descriptionModifications: MdibDescriptionModifications,
        mdibVersion: Long
    ): WriteDescriptionResult

    /**
     * Processes the state modifications object, stores the data internally and triggers an event.
     *
     * Data is passed through without any checks to the MDIB storage.
     *
     * @param stateModifications a set of state updates.
     * @param mdibVersion the MDIB version to set for this write operation.
     * @return a write result including the updated entities.
     * @throws PreprocessingException if something goes wrong during preprocessing, i.e., the consistency of the MDIB
     * would be violated.
     */
    @Throws(PreprocessingException::class)
    public suspend fun writeStates(
        stateModifications: MdibStateModifications,
        mdibVersion: Long
    ): WriteStateResult
}