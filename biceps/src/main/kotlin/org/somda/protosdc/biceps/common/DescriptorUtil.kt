package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.*

/*
 * This file contains extension functions for all significant BICEPS descriptor types to retrieve
 *
 * - handle name
 * - descriptor version number.
 */

// --- descriptor handle

public fun AbstractDescriptor.handleString(): String = this.handleAttr.string

public fun AbstractDeviceComponentDescriptor.handleString(): String = this.abstractDescriptor.handleString()

public fun AbstractComplexDeviceComponentDescriptor.handleString(): String = this.abstractDeviceComponentDescriptor.handleString()

public fun AbstractOperationDescriptor.handleString(): String = this.abstractDescriptor.handleString()

public fun AbstractSetStateOperationDescriptor.handleString(): String = this.abstractOperationDescriptor.handleString()

public fun AbstractAlertDescriptor.handleString(): String = this.abstractDescriptor.handleString()

public fun AbstractMetricDescriptor.handleString(): String = this.abstractDescriptor.handleString()

public fun AbstractContextDescriptor.handleString(): String = this.abstractDescriptor.handleString()

public fun ActivateOperationDescriptor.handleString(): String = this.abstractSetStateOperationDescriptor.handleString()

public fun AlertConditionDescriptor.handleString(): String = this.abstractAlertDescriptor.handleString()

public fun AlertSignalDescriptor.handleString(): String = this.abstractAlertDescriptor.handleString()

public fun AlertSystemDescriptor.handleString(): String = this.abstractAlertDescriptor.handleString()

public fun BatteryDescriptor.handleString(): String = this.abstractDeviceComponentDescriptor.handleString()

public fun ChannelDescriptor.handleString(): String = this.abstractDeviceComponentDescriptor.handleString()

public fun ClockDescriptor.handleString(): String = this.abstractDeviceComponentDescriptor.handleString()

public fun DistributionSampleArrayMetricDescriptor.handleString(): String = this.abstractMetricDescriptor.handleString()

public fun EnsembleContextDescriptor.handleString(): String = this.abstractContextDescriptor.handleString()

public fun StringMetricDescriptor.handleString(): String = this.abstractMetricDescriptor.handleString()

public fun EnumStringMetricDescriptor.handleString(): String = this.stringMetricDescriptor.handleString()

public fun LimitAlertConditionDescriptor.handleString(): String = this.alertConditionDescriptor.handleString()

public fun LocationContextDescriptor.handleString(): String = this.abstractContextDescriptor.handleString()

public fun MdsDescriptor.handleString(): String = this.abstractComplexDeviceComponentDescriptor.handleString()

public fun MeansContextDescriptor.handleString(): String = this.abstractContextDescriptor.handleString()

public fun NumericMetricDescriptor.handleString(): String = this.abstractMetricDescriptor.handleString()

public fun OperatorContextDescriptor.handleString(): String = this.abstractContextDescriptor.handleString()

public fun PatientContextDescriptor.handleString(): String = this.abstractContextDescriptor.handleString()

public fun RealTimeSampleArrayMetricDescriptor.handleString(): String = this.abstractMetricDescriptor.handleString()

public fun ScoDescriptor.handleString(): String = this.abstractDeviceComponentDescriptor.handleString()

public fun SetAlertStateOperationDescriptor.handleString(): String = this.abstractSetStateOperationDescriptor.handleString()

public fun SetComponentStateOperationDescriptor.handleString(): String = this.abstractSetStateOperationDescriptor.handleString()

public fun SetContextStateOperationDescriptor.handleString(): String = this.abstractSetStateOperationDescriptor.handleString()

public fun SetMetricStateOperationDescriptor.handleString(): String = this.abstractSetStateOperationDescriptor.handleString()

public fun SetStringOperationDescriptor.handleString(): String = this.abstractOperationDescriptor.handleString()

public fun SetValueOperationDescriptor.handleString(): String = this.abstractOperationDescriptor.handleString()

public fun SystemContextDescriptor.handleString(): String = this.abstractDeviceComponentDescriptor.handleString()

public fun VmdDescriptor.handleString(): String = this.abstractComplexDeviceComponentDescriptor.handleString()

public fun WorkflowContextDescriptor.handleString(): String = this.abstractContextDescriptor.handleString()



public fun AbstractDescriptor.handle(): Handle = this.handleAttr

public fun AbstractDeviceComponentDescriptor.handle(): Handle = this.abstractDescriptor.handle()

public fun AbstractComplexDeviceComponentDescriptor.handle(): Handle = this.abstractDeviceComponentDescriptor.handle()

public fun AbstractOperationDescriptor.handle(): Handle = this.abstractDescriptor.handle()

public fun AbstractSetStateOperationDescriptor.handle(): Handle = this.abstractOperationDescriptor.handle()

public fun AbstractAlertDescriptor.handle(): Handle = this.abstractDescriptor.handle()

public fun AbstractMetricDescriptor.handle(): Handle = this.abstractDescriptor.handle()

public fun AbstractContextDescriptor.handle(): Handle = this.abstractDescriptor.handle()

public fun ActivateOperationDescriptor.handle(): Handle = this.abstractSetStateOperationDescriptor.handle()

public fun AlertConditionDescriptor.handle(): Handle = this.abstractAlertDescriptor.handle()

public fun AlertSignalDescriptor.handle(): Handle = this.abstractAlertDescriptor.handle()

public fun AlertSystemDescriptor.handle(): Handle = this.abstractAlertDescriptor.handle()

public fun BatteryDescriptor.handle(): Handle = this.abstractDeviceComponentDescriptor.handle()

public fun ChannelDescriptor.handle(): Handle = this.abstractDeviceComponentDescriptor.handle()

public fun ClockDescriptor.handle(): Handle = this.abstractDeviceComponentDescriptor.handle()

public fun DistributionSampleArrayMetricDescriptor.handle(): Handle = this.abstractMetricDescriptor.handle()

public fun EnsembleContextDescriptor.handle(): Handle = this.abstractContextDescriptor.handle()

public fun StringMetricDescriptor.handle(): Handle = this.abstractMetricDescriptor.handle()

public fun EnumStringMetricDescriptor.handle(): Handle = this.stringMetricDescriptor.handle()

public fun LimitAlertConditionDescriptor.handle(): Handle = this.alertConditionDescriptor.handle()

public fun LocationContextDescriptor.handle(): Handle = this.abstractContextDescriptor.handle()

public fun MdsDescriptor.handle(): Handle = this.abstractComplexDeviceComponentDescriptor.handle()

public fun MeansContextDescriptor.handle(): Handle = this.abstractContextDescriptor.handle()

public fun NumericMetricDescriptor.handle(): Handle = this.abstractMetricDescriptor.handle()

public fun OperatorContextDescriptor.handle(): Handle = this.abstractContextDescriptor.handle()

public fun PatientContextDescriptor.handle(): Handle = this.abstractContextDescriptor.handle()

public fun RealTimeSampleArrayMetricDescriptor.handle(): Handle = this.abstractMetricDescriptor.handle()

public fun ScoDescriptor.handle(): Handle = this.abstractDeviceComponentDescriptor.handle()

public fun SetAlertStateOperationDescriptor.handle(): Handle = this.abstractSetStateOperationDescriptor.handle()

public fun SetComponentStateOperationDescriptor.handle(): Handle = this.abstractSetStateOperationDescriptor.handle()

public fun SetContextStateOperationDescriptor.handle(): Handle = this.abstractSetStateOperationDescriptor.handle()

public fun SetMetricStateOperationDescriptor.handle(): Handle = this.abstractSetStateOperationDescriptor.handle()

public fun SetStringOperationDescriptor.handle(): Handle = this.abstractOperationDescriptor.handle()

public fun SetValueOperationDescriptor.handle(): Handle = this.abstractOperationDescriptor.handle()

public fun SystemContextDescriptor.handle(): Handle = this.abstractDeviceComponentDescriptor.handle()

public fun VmdDescriptor.handle(): Handle = this.abstractComplexDeviceComponentDescriptor.handle()

public fun WorkflowContextDescriptor.handle(): Handle = this.abstractContextDescriptor.handle()

// --- descriptor version

public fun AbstractDescriptor.descriptorVersion(): Long = this.descriptorVersionAttr?.unsignedLong ?: 0L

public fun AbstractDeviceComponentDescriptor.descriptorVersion(): Long = this.abstractDescriptor.descriptorVersion()

public fun AbstractComplexDeviceComponentDescriptor.descriptorVersion(): Long =
    this.abstractDeviceComponentDescriptor.descriptorVersion()

public fun AbstractOperationDescriptor.descriptorVersion(): Long = this.abstractDescriptor.descriptorVersion()

public fun AbstractSetStateOperationDescriptor.descriptorVersion(): Long = this.abstractOperationDescriptor.descriptorVersion()

public fun AbstractAlertDescriptor.descriptorVersion(): Long = this.abstractDescriptor.descriptorVersion()

public fun AbstractMetricDescriptor.descriptorVersion(): Long = this.abstractDescriptor.descriptorVersion()

public fun AbstractContextDescriptor.descriptorVersion(): Long = this.abstractDescriptor.descriptorVersion()

public fun ActivateOperationDescriptor.descriptorVersion(): Long = this.abstractSetStateOperationDescriptor.descriptorVersion()

public fun AlertConditionDescriptor.descriptorVersion(): Long = this.abstractAlertDescriptor.descriptorVersion()

public fun AlertSignalDescriptor.descriptorVersion(): Long = this.abstractAlertDescriptor.descriptorVersion()

public fun AlertSystemDescriptor.descriptorVersion(): Long = this.abstractAlertDescriptor.descriptorVersion()

public fun BatteryDescriptor.descriptorVersion(): Long = this.abstractDeviceComponentDescriptor.descriptorVersion()

public fun ChannelDescriptor.descriptorVersion(): Long = this.abstractDeviceComponentDescriptor.descriptorVersion()

public fun ClockDescriptor.descriptorVersion(): Long = this.abstractDeviceComponentDescriptor.descriptorVersion()

public fun DistributionSampleArrayMetricDescriptor.descriptorVersion(): Long = this.abstractMetricDescriptor.descriptorVersion()

public fun EnsembleContextDescriptor.descriptorVersion(): Long = this.abstractContextDescriptor.descriptorVersion()

public fun StringMetricDescriptor.descriptorVersion(): Long = this.abstractMetricDescriptor.descriptorVersion()

public fun EnumStringMetricDescriptor.descriptorVersion(): Long = this.stringMetricDescriptor.descriptorVersion()

public fun LimitAlertConditionDescriptor.descriptorVersion(): Long = this.alertConditionDescriptor.descriptorVersion()

public fun LocationContextDescriptor.descriptorVersion(): Long = this.abstractContextDescriptor.descriptorVersion()

public fun MdsDescriptor.descriptorVersion(): Long = this.abstractComplexDeviceComponentDescriptor.descriptorVersion()

public fun MeansContextDescriptor.descriptorVersion(): Long = this.abstractContextDescriptor.descriptorVersion()

public fun NumericMetricDescriptor.descriptorVersion(): Long = this.abstractMetricDescriptor.descriptorVersion()

public fun OperatorContextDescriptor.descriptorVersion(): Long = this.abstractContextDescriptor.descriptorVersion()

public fun PatientContextDescriptor.descriptorVersion(): Long = this.abstractContextDescriptor.descriptorVersion()

public fun RealTimeSampleArrayMetricDescriptor.descriptorVersion(): Long = this.abstractMetricDescriptor.descriptorVersion()

public fun ScoDescriptor.descriptorVersion(): Long = this.abstractDeviceComponentDescriptor.descriptorVersion()

public fun SetAlertStateOperationDescriptor.descriptorVersion(): Long = this.abstractSetStateOperationDescriptor.descriptorVersion()

public fun SetComponentStateOperationDescriptor.descriptorVersion(): Long =
    this.abstractSetStateOperationDescriptor.descriptorVersion()

public fun SetContextStateOperationDescriptor.descriptorVersion(): Long =
    this.abstractSetStateOperationDescriptor.descriptorVersion()

public fun SetMetricStateOperationDescriptor.descriptorVersion(): Long = this.abstractSetStateOperationDescriptor.descriptorVersion()

public fun SetStringOperationDescriptor.descriptorVersion(): Long = this.abstractOperationDescriptor.descriptorVersion()

public fun SetValueOperationDescriptor.descriptorVersion(): Long = this.abstractOperationDescriptor.descriptorVersion()

public fun SystemContextDescriptor.descriptorVersion(): Long = this.abstractDeviceComponentDescriptor.descriptorVersion()

public fun VmdDescriptor.descriptorVersion(): Long = this.abstractComplexDeviceComponentDescriptor.descriptorVersion()

public fun WorkflowContextDescriptor.descriptorVersion(): Long = this.abstractContextDescriptor.descriptorVersion()

// --- version replacement

public fun AbstractDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AbstractDescriptor =
    this.copy(descriptorVersionAttr = newDescriptorVersion?.let { MdibUtil.versionCounter(it) }
        ?: this.descriptorVersionAttr)

public fun AbstractAlertDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AbstractAlertDescriptor =
    this.copy(abstractDescriptor = abstractDescriptor.replaceInCopy(newDescriptorVersion))

public fun AbstractDeviceComponentDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AbstractDeviceComponentDescriptor =
    this.copy(abstractDescriptor = abstractDescriptor.replaceInCopy(newDescriptorVersion))

public fun AbstractComplexDeviceComponentDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AbstractComplexDeviceComponentDescriptor =
    this.copy(abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor.replaceInCopy(newDescriptorVersion))

public fun AbstractOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AbstractOperationDescriptor =
    this.copy(abstractDescriptor = abstractDescriptor.replaceInCopy(newDescriptorVersion))

public fun AbstractSetStateOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AbstractSetStateOperationDescriptor =
    this.copy(abstractOperationDescriptor = abstractOperationDescriptor.replaceInCopy(newDescriptorVersion))

public fun AbstractMetricDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AbstractMetricDescriptor =
    this.copy(abstractDescriptor = abstractDescriptor.replaceInCopy(newDescriptorVersion))

public fun AbstractContextDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AbstractContextDescriptor =
    this.copy(abstractDescriptor = abstractDescriptor.replaceInCopy(newDescriptorVersion))

public fun AlertConditionDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AlertConditionDescriptor =
    this.copy(abstractAlertDescriptor = abstractAlertDescriptor.replaceInCopy(newDescriptorVersion))

public fun LimitAlertConditionDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): LimitAlertConditionDescriptor =
    this.copy(alertConditionDescriptor = alertConditionDescriptor.replaceInCopy(newDescriptorVersion))

public fun AlertSignalDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AlertSignalDescriptor =
    this.copy(abstractAlertDescriptor = abstractAlertDescriptor.replaceInCopy(newDescriptorVersion))

public fun AlertSystemDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): AlertSystemDescriptor =
    this.copy(abstractAlertDescriptor = abstractAlertDescriptor.replaceInCopy(newDescriptorVersion))

public fun ClockDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): ClockDescriptor =
    this.copy(abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor.replaceInCopy(newDescriptorVersion))

public fun BatteryDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): BatteryDescriptor =
    this.copy(abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor.replaceInCopy(newDescriptorVersion))

public fun ChannelDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): ChannelDescriptor =
    this.copy(abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor.replaceInCopy(newDescriptorVersion))

public fun ScoDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): ScoDescriptor =
    this.copy(abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor.replaceInCopy(newDescriptorVersion))

public fun SystemContextDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): SystemContextDescriptor =
    this.copy(abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor.replaceInCopy(newDescriptorVersion))

public fun VmdDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): VmdDescriptor = this.copy(
    abstractComplexDeviceComponentDescriptor = abstractComplexDeviceComponentDescriptor.replaceInCopy(
        newDescriptorVersion
    )
)

public fun MdsDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): MdsDescriptor = this.copy(
    abstractComplexDeviceComponentDescriptor = abstractComplexDeviceComponentDescriptor.replaceInCopy(
        newDescriptorVersion
    )
)

public fun SetStringOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): SetStringOperationDescriptor =
    this.copy(abstractOperationDescriptor = abstractOperationDescriptor.replaceInCopy(newDescriptorVersion))

public fun SetValueOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): SetValueOperationDescriptor =
    this.copy(abstractOperationDescriptor = abstractOperationDescriptor.replaceInCopy(newDescriptorVersion))

public fun SetComponentStateOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): SetComponentStateOperationDescriptor = this.copy(
    abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor.replaceInCopy(
        newDescriptorVersion
    )
)

public fun SetAlertStateOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): SetAlertStateOperationDescriptor = this.copy(
    abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor.replaceInCopy(
        newDescriptorVersion
    )
)

public fun SetMetricStateOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): SetMetricStateOperationDescriptor = this.copy(
    abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor.replaceInCopy(
        newDescriptorVersion
    )
)

public fun SetContextStateOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): SetContextStateOperationDescriptor = this.copy(
    abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor.replaceInCopy(
        newDescriptorVersion
    )
)

public fun ActivateOperationDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): ActivateOperationDescriptor = this.copy(
    abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor.replaceInCopy(
        newDescriptorVersion
    )
)

public fun NumericMetricDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): NumericMetricDescriptor =
    this.copy(abstractMetricDescriptor = abstractMetricDescriptor.replaceInCopy(newDescriptorVersion))

public fun DistributionSampleArrayMetricDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): DistributionSampleArrayMetricDescriptor =
    this.copy(abstractMetricDescriptor = abstractMetricDescriptor.replaceInCopy(newDescriptorVersion))

public fun RealTimeSampleArrayMetricDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): RealTimeSampleArrayMetricDescriptor =
    this.copy(abstractMetricDescriptor = abstractMetricDescriptor.replaceInCopy(newDescriptorVersion))

public fun StringMetricDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): StringMetricDescriptor =
    this.copy(abstractMetricDescriptor = abstractMetricDescriptor.replaceInCopy(newDescriptorVersion))

public fun EnumStringMetricDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): EnumStringMetricDescriptor =
    this.copy(stringMetricDescriptor = stringMetricDescriptor.replaceInCopy(newDescriptorVersion))

public fun MeansContextDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): MeansContextDescriptor =
    this.copy(abstractContextDescriptor = abstractContextDescriptor.replaceInCopy(newDescriptorVersion))

public fun PatientContextDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): PatientContextDescriptor =
    this.copy(abstractContextDescriptor = abstractContextDescriptor.replaceInCopy(newDescriptorVersion))

public fun OperatorContextDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): OperatorContextDescriptor =
    this.copy(abstractContextDescriptor = abstractContextDescriptor.replaceInCopy(newDescriptorVersion))

public fun EnsembleContextDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): EnsembleContextDescriptor =
    this.copy(abstractContextDescriptor = abstractContextDescriptor.replaceInCopy(newDescriptorVersion))

public fun LocationContextDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): LocationContextDescriptor =
    this.copy(abstractContextDescriptor = abstractContextDescriptor.replaceInCopy(newDescriptorVersion))

public fun WorkflowContextDescriptor.replaceInCopy(newDescriptorVersion: Long? = null): WorkflowContextDescriptor =
    this.copy(abstractContextDescriptor = abstractContextDescriptor.replaceInCopy(newDescriptorVersion))