package org.somda.protosdc.biceps.common.storage

import org.somda.protosdc.biceps.common.MdibStateModifications

/**
 * A segment that is applied during state modifications.
 */
internal interface StatePreprocessingSegment {
    /**
     * In a sequence of modifications this function processes one modification.
     *
     * @param modifications the modifications as submitted by the user or modified by the previous element in the chain.
     * @param storage the MDIB storage for access.
     */
    fun process(
        modifications: MdibStateModifications,
        storage: MdibStorage
    ): MdibStateModifications
}