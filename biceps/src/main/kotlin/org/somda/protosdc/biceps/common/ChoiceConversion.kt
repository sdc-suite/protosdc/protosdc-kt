package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.*

// --- from AbstractStateOneOf

public fun AbstractStateOneOf.toAbstractOperationStateOneOf(): AbstractOperationStateOneOf? = when (this) {
    is AbstractStateOneOf.ChoiceSetContextStateOperationState ->
        AbstractOperationStateOneOf.ChoiceSetContextStateOperationState(this.value)
    is AbstractStateOneOf.ChoiceSetValueOperationState ->
        AbstractOperationStateOneOf.ChoiceSetValueOperationState(this.value)
    is AbstractStateOneOf.ChoiceSetComponentStateOperationState ->
        AbstractOperationStateOneOf.ChoiceSetComponentStateOperationState(this.value)
    is AbstractStateOneOf.ChoiceSetMetricStateOperationState ->
        AbstractOperationStateOneOf.ChoiceSetMetricStateOperationState(this.value)
    is AbstractStateOneOf.ChoiceSetAlertStateOperationState ->
        AbstractOperationStateOneOf.ChoiceSetAlertStateOperationState(this.value)
    is AbstractStateOneOf.ChoiceSetStringOperationState ->
        AbstractOperationStateOneOf.ChoiceSetStringOperationState(this.value)
    is AbstractStateOneOf.ChoiceActivateOperationState ->
        AbstractOperationStateOneOf.ChoiceActivateOperationState(this.value)
    else -> null
}

public fun AbstractStateOneOf.toAbstractComponentStateOneOf(): AbstractDeviceComponentStateOneOf? = when (this) {
    is AbstractStateOneOf.ChoiceClockState ->
        AbstractDeviceComponentStateOneOf.ChoiceClockState(this.value)
    is AbstractStateOneOf.ChoiceChannelState ->
        AbstractDeviceComponentStateOneOf.ChoiceChannelState(this.value)
    is AbstractStateOneOf.ChoiceSystemContextState ->
        AbstractDeviceComponentStateOneOf.ChoiceSystemContextState(this.value)
    is AbstractStateOneOf.ChoiceVmdState ->
        AbstractDeviceComponentStateOneOf.ChoiceVmdState(this.value)
    is AbstractStateOneOf.ChoiceMdsState ->
        AbstractDeviceComponentStateOneOf.ChoiceMdsState(this.value)
    is AbstractStateOneOf.ChoiceBatteryState ->
        AbstractDeviceComponentStateOneOf.ChoiceBatteryState(this.value)
    is AbstractStateOneOf.ChoiceScoState ->
        AbstractDeviceComponentStateOneOf.ChoiceScoState(this.value)
    else -> null
}

public fun AbstractStateOneOf.toAbstractMetricStateOneOf(): AbstractMetricStateOneOf? = when (this) {
    is AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState ->
        AbstractMetricStateOneOf.ChoiceRealTimeSampleArrayMetricState(this.value)
    is AbstractStateOneOf.ChoiceNumericMetricState ->
        AbstractMetricStateOneOf.ChoiceNumericMetricState(this.value)
    is AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState ->
        AbstractMetricStateOneOf.ChoiceDistributionSampleArrayMetricState(this.value)
    is AbstractStateOneOf.ChoiceStringMetricState ->
        AbstractMetricStateOneOf.ChoiceStringMetricState(this.value)
    is AbstractStateOneOf.ChoiceEnumStringMetricState ->
        AbstractMetricStateOneOf.ChoiceEnumStringMetricState(this.value)
    else -> null
}

public fun AbstractStateOneOf.toAbstractAlertStateOneOf(): AbstractAlertStateOneOf? = when (this) {
    is AbstractStateOneOf.ChoiceAlertConditionState ->
        AbstractAlertStateOneOf.ChoiceAlertConditionState(this.value)
    is AbstractStateOneOf.ChoiceLimitAlertConditionState ->
        AbstractAlertStateOneOf.ChoiceLimitAlertConditionState(this.value)
    is AbstractStateOneOf.ChoiceAlertSystemState ->
        AbstractAlertStateOneOf.ChoiceAlertSystemState(this.value)
    is AbstractStateOneOf.ChoiceAlertSignalState ->
        AbstractAlertStateOneOf.ChoiceAlertSignalState(this.value)
    else -> null
}

public fun AbstractStateOneOf.toAbstractContextStateOneOf(): AbstractContextStateOneOf? = when (this) {
    is AbstractStateOneOf.ChoiceEnsembleContextState ->
        AbstractContextStateOneOf.ChoiceEnsembleContextState(this.value)
    is AbstractStateOneOf.ChoiceMeansContextState ->
        AbstractContextStateOneOf.ChoiceMeansContextState(this.value)
    is AbstractStateOneOf.ChoiceLocationContextState ->
        AbstractContextStateOneOf.ChoiceLocationContextState(this.value)
    is AbstractStateOneOf.ChoiceOperatorContextState ->
        AbstractContextStateOneOf.ChoiceOperatorContextState(this.value)
    is AbstractStateOneOf.ChoicePatientContextState ->
        AbstractContextStateOneOf.ChoicePatientContextState(this.value)
    is AbstractStateOneOf.ChoiceWorkflowContextState ->
        AbstractContextStateOneOf.ChoiceWorkflowContextState(this.value)
    else -> null
}

// --- from AbstractDescriptorOneOf

public fun AbstractDescriptorOneOf.toAbstractContextDescriptorOneOf(): AbstractContextDescriptorOneOf? = when (this) {
    is AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor ->
        AbstractContextDescriptorOneOf.ChoiceEnsembleContextDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceMeansContextDescriptor ->
        AbstractContextDescriptorOneOf.ChoiceMeansContextDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceLocationContextDescriptor ->
        AbstractContextDescriptorOneOf.ChoiceLocationContextDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor ->
        AbstractContextDescriptorOneOf.ChoiceOperatorContextDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoicePatientContextDescriptor ->
        AbstractContextDescriptorOneOf.ChoicePatientContextDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor ->
        AbstractContextDescriptorOneOf.ChoiceWorkflowContextDescriptor(this.value)
    else -> null
}

public fun AbstractDescriptorOneOf.toAbstractOperationDescriptorOneOf(): AbstractOperationDescriptorOneOf? = when (this) {
    is AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor ->
        AbstractOperationDescriptorOneOf.ChoiceSetContextStateOperationDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor ->
        AbstractOperationDescriptorOneOf.ChoiceSetValueOperationDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor ->
        AbstractOperationDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor ->
        AbstractOperationDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor ->
        AbstractOperationDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor ->
        AbstractOperationDescriptorOneOf.ChoiceSetStringOperationDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor ->
        AbstractOperationDescriptorOneOf.ChoiceActivateOperationDescriptor(this.value)
    else -> null
}

public fun AbstractDescriptorOneOf.toAbstractComponentDescriptorOneOf(): AbstractDeviceComponentDescriptorOneOf? =
    when (this) {
        is AbstractDescriptorOneOf.ChoiceClockDescriptor ->
            AbstractDeviceComponentDescriptorOneOf.ChoiceClockDescriptor(this.value)
        is AbstractDescriptorOneOf.ChoiceChannelDescriptor ->
            AbstractDeviceComponentDescriptorOneOf.ChoiceChannelDescriptor(this.value)
        is AbstractDescriptorOneOf.ChoiceSystemContextDescriptor ->
            AbstractDeviceComponentDescriptorOneOf.ChoiceSystemContextDescriptor(this.value)
        is AbstractDescriptorOneOf.ChoiceVmdDescriptor ->
            AbstractDeviceComponentDescriptorOneOf.ChoiceVmdDescriptor(this.value)
        is AbstractDescriptorOneOf.ChoiceMdsDescriptor ->
            AbstractDeviceComponentDescriptorOneOf.ChoiceMdsDescriptor(this.value)
        is AbstractDescriptorOneOf.ChoiceBatteryDescriptor ->
            AbstractDeviceComponentDescriptorOneOf.ChoiceBatteryDescriptor(this.value)
        is AbstractDescriptorOneOf.ChoiceScoDescriptor ->
            AbstractDeviceComponentDescriptorOneOf.ChoiceScoDescriptor(this.value)
        else -> null
    }

public fun AbstractDescriptorOneOf.toAbstractMetricDescriptorOneOf(): AbstractMetricDescriptorOneOf? = when (this) {
    is AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor ->
        AbstractMetricDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor ->
        AbstractMetricDescriptorOneOf.ChoiceNumericMetricDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor ->
        AbstractMetricDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceStringMetricDescriptor ->
        AbstractMetricDescriptorOneOf.ChoiceStringMetricDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor ->
        AbstractMetricDescriptorOneOf.ChoiceEnumStringMetricDescriptor(this.value)
    else -> null
}

public fun AbstractDescriptorOneOf.toAbstractAlertDescriptorOneOf(): AbstractAlertDescriptorOneOf? = when (this) {
    is AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor ->
        AbstractAlertDescriptorOneOf.ChoiceAlertConditionDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor ->
        AbstractAlertDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor ->
        AbstractAlertDescriptorOneOf.ChoiceAlertSystemDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor ->
        AbstractAlertDescriptorOneOf.ChoiceAlertSignalDescriptor(this.value)
    else -> null
}

public fun AbstractDescriptorOneOf.toAlertConditionDescriptorOneOf(): AlertConditionDescriptorOneOf? = when(this) {
    is AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor ->
        AlertConditionDescriptorOneOf.ChoiceAlertConditionDescriptor(this.value)
    is AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor ->
        AlertConditionDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(this.value)
    else -> null
}

// --- to AbstractStateOneOf

public fun AbstractContextStateOneOf.toAbstractStateOneOf(): AbstractStateOneOf = when (this) {
    is AbstractContextStateOneOf.ChoiceEnsembleContextState ->
        AbstractStateOneOf.ChoiceEnsembleContextState(this.value)
    is AbstractContextStateOneOf.ChoiceMeansContextState ->
        AbstractStateOneOf.ChoiceMeansContextState(this.value)
    is AbstractContextStateOneOf.ChoiceLocationContextState ->
        AbstractStateOneOf.ChoiceLocationContextState(this.value)
    is AbstractContextStateOneOf.ChoiceOperatorContextState ->
        AbstractStateOneOf.ChoiceOperatorContextState(this.value)
    is AbstractContextStateOneOf.ChoicePatientContextState ->
        AbstractStateOneOf.ChoicePatientContextState(this.value)
    is AbstractContextStateOneOf.ChoiceWorkflowContextState ->
        AbstractStateOneOf.ChoiceWorkflowContextState(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractOperationStateOneOf.toAbstractStateOneOf(): AbstractStateOneOf = when (this) {
    is AbstractOperationStateOneOf.ChoiceSetContextStateOperationState ->
        AbstractStateOneOf.ChoiceSetContextStateOperationState(this.value)
    is AbstractOperationStateOneOf.ChoiceSetValueOperationState ->
        AbstractStateOneOf.ChoiceSetValueOperationState(this.value)
    is AbstractOperationStateOneOf.ChoiceSetComponentStateOperationState ->
        AbstractStateOneOf.ChoiceSetComponentStateOperationState(this.value)
    is AbstractOperationStateOneOf.ChoiceSetMetricStateOperationState ->
        AbstractStateOneOf.ChoiceSetMetricStateOperationState(this.value)
    is AbstractOperationStateOneOf.ChoiceSetAlertStateOperationState ->
        AbstractStateOneOf.ChoiceSetAlertStateOperationState(this.value)
    is AbstractOperationStateOneOf.ChoiceSetStringOperationState ->
        AbstractStateOneOf.ChoiceSetStringOperationState(this.value)
    is AbstractOperationStateOneOf.ChoiceActivateOperationState ->
        AbstractStateOneOf.ChoiceActivateOperationState(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractDeviceComponentStateOneOf.toAbstractStateOneOf(): AbstractStateOneOf = when (this) {
    is AbstractDeviceComponentStateOneOf.ChoiceClockState ->
        AbstractStateOneOf.ChoiceClockState(this.value)
    is AbstractDeviceComponentStateOneOf.ChoiceChannelState ->
        AbstractStateOneOf.ChoiceChannelState(this.value)
    is AbstractDeviceComponentStateOneOf.ChoiceSystemContextState ->
        AbstractStateOneOf.ChoiceSystemContextState(this.value)
    is AbstractDeviceComponentStateOneOf.ChoiceVmdState ->
        AbstractStateOneOf.ChoiceVmdState(this.value)
    is AbstractDeviceComponentStateOneOf.ChoiceMdsState ->
        AbstractStateOneOf.ChoiceMdsState(this.value)
    is AbstractDeviceComponentStateOneOf.ChoiceBatteryState ->
        AbstractStateOneOf.ChoiceBatteryState(this.value)
    is AbstractDeviceComponentStateOneOf.ChoiceScoState ->
        AbstractStateOneOf.ChoiceScoState(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractMetricStateOneOf.toAbstractStateOneOf(): AbstractStateOneOf = when (this) {
    is AbstractMetricStateOneOf.ChoiceRealTimeSampleArrayMetricState ->
        AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState(this.value)
    is AbstractMetricStateOneOf.ChoiceNumericMetricState ->
        AbstractStateOneOf.ChoiceNumericMetricState(this.value)
    is AbstractMetricStateOneOf.ChoiceDistributionSampleArrayMetricState ->
        AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState(this.value)
    is AbstractMetricStateOneOf.ChoiceStringMetricState ->
        AbstractStateOneOf.ChoiceStringMetricState(this.value)
    is AbstractMetricStateOneOf.ChoiceEnumStringMetricState ->
        AbstractStateOneOf.ChoiceEnumStringMetricState(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractAlertStateOneOf.toAbstractStateOneOf(): AbstractStateOneOf = when (this) {
    is AbstractAlertStateOneOf.ChoiceAlertConditionState ->
        AbstractStateOneOf.ChoiceAlertConditionState(this.value)
    is AbstractAlertStateOneOf.ChoiceLimitAlertConditionState ->
        AbstractStateOneOf.ChoiceLimitAlertConditionState(this.value)
    is AbstractAlertStateOneOf.ChoiceAlertSystemState ->
        AbstractStateOneOf.ChoiceAlertSystemState(this.value)
    is AbstractAlertStateOneOf.ChoiceAlertSignalState ->
        AbstractStateOneOf.ChoiceAlertSignalState(this.value)
    else -> throw InvalidWhenBranchException(this)
}

// --- to AbstractDescriptorOneOf

public fun AbstractContextDescriptorOneOf.toAbstractDescriptorOneOf(): AbstractDescriptorOneOf = when (this) {
    is AbstractContextDescriptorOneOf.ChoiceEnsembleContextDescriptor ->
        AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor(this.value)
    is AbstractContextDescriptorOneOf.ChoiceMeansContextDescriptor ->
        AbstractDescriptorOneOf.ChoiceMeansContextDescriptor(this.value)
    is AbstractContextDescriptorOneOf.ChoiceLocationContextDescriptor ->
        AbstractDescriptorOneOf.ChoiceLocationContextDescriptor(this.value)
    is AbstractContextDescriptorOneOf.ChoiceOperatorContextDescriptor ->
        AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor(this.value)
    is AbstractContextDescriptorOneOf.ChoicePatientContextDescriptor ->
        AbstractDescriptorOneOf.ChoicePatientContextDescriptor(this.value)
    is AbstractContextDescriptorOneOf.ChoiceWorkflowContextDescriptor ->
        AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractOperationDescriptorOneOf.toAbstractDescriptorOneOf(): AbstractDescriptorOneOf = when (this) {
    is AbstractOperationDescriptorOneOf.ChoiceSetContextStateOperationDescriptor ->
        AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor(this.value)
    is AbstractOperationDescriptorOneOf.ChoiceSetValueOperationDescriptor ->
        AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor(this.value)
    is AbstractOperationDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor ->
        AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor(this.value)
    is AbstractOperationDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor ->
        AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor(this.value)
    is AbstractOperationDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor ->
        AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor(this.value)
    is AbstractOperationDescriptorOneOf.ChoiceSetStringOperationDescriptor ->
        AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor(this.value)
    is AbstractOperationDescriptorOneOf.ChoiceActivateOperationDescriptor ->
        AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractDeviceComponentDescriptorOneOf.toAbstractDescriptorOneOf(): AbstractDescriptorOneOf = when (this) {
    is AbstractDeviceComponentDescriptorOneOf.ChoiceClockDescriptor ->
        AbstractDescriptorOneOf.ChoiceClockDescriptor(this.value)
    is AbstractDeviceComponentDescriptorOneOf.ChoiceChannelDescriptor ->
        AbstractDescriptorOneOf.ChoiceChannelDescriptor(this.value)
    is AbstractDeviceComponentDescriptorOneOf.ChoiceSystemContextDescriptor ->
        AbstractDescriptorOneOf.ChoiceSystemContextDescriptor(this.value)
    is AbstractDeviceComponentDescriptorOneOf.ChoiceVmdDescriptor ->
        AbstractDescriptorOneOf.ChoiceVmdDescriptor(this.value)
    is AbstractDeviceComponentDescriptorOneOf.ChoiceMdsDescriptor ->
        AbstractDescriptorOneOf.ChoiceMdsDescriptor(this.value)
    is AbstractDeviceComponentDescriptorOneOf.ChoiceBatteryDescriptor ->
        AbstractDescriptorOneOf.ChoiceBatteryDescriptor(this.value)
    is AbstractDeviceComponentDescriptorOneOf.ChoiceScoDescriptor ->
        AbstractDescriptorOneOf.ChoiceScoDescriptor(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractMetricDescriptorOneOf.toAbstractDescriptorOneOf(): AbstractDescriptorOneOf = when (this) {
    is AbstractMetricDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor ->
        AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor(this.value)
    is AbstractMetricDescriptorOneOf.ChoiceNumericMetricDescriptor ->
        AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor(this.value)
    is AbstractMetricDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor ->
        AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor(this.value)
    is AbstractMetricDescriptorOneOf.ChoiceStringMetricDescriptor ->
        AbstractDescriptorOneOf.ChoiceStringMetricDescriptor(this.value)
    is AbstractMetricDescriptorOneOf.ChoiceEnumStringMetricDescriptor ->
        AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractAlertDescriptorOneOf.toAbstractDescriptorOneOf(): AbstractDescriptorOneOf = when (this) {
    is AbstractAlertDescriptorOneOf.ChoiceAlertConditionDescriptor ->
        AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor(this.value)
    is AbstractAlertDescriptorOneOf.ChoiceLimitAlertConditionDescriptor ->
        AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(this.value)
    is AbstractAlertDescriptorOneOf.ChoiceAlertSystemDescriptor ->
        AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor(this.value)
    is AbstractAlertDescriptorOneOf.ChoiceAlertSignalDescriptor ->
        AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor(this.value)
    else -> throw InvalidWhenBranchException(this)
}

public fun AlertConditionDescriptorOneOf.toAbstractDescriptorOneOf(): AbstractDescriptorOneOf = when (this) {
    is AlertConditionDescriptorOneOf.ChoiceAlertConditionDescriptor ->
        AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor(this.value)
    is AlertConditionDescriptorOneOf.ChoiceLimitAlertConditionDescriptor ->
        AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(this.value)
    else -> throw InvalidWhenBranchException(this)
}