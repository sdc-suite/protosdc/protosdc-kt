package org.somda.protosdc.biceps.common

import org.somda.protosdc.common.UriUtil
import org.somda.protosdc.model.biceps.MdibVersionGroup
import org.somda.protosdc.model.biceps.VersionCounter

/**
 * Gets the version of the version group.
 *
 * @return the version considering the default value.
 */
public fun MdibVersionGroup.version(): Long = mdibVersionAttr?.unsignedLong ?: 0L

/**
 * Gets the instance id of the version group.
 *
 * @return the instance id number or 0 if none is set.
 *
 *         *This is not in accordance with the latest BICEPS version in which no default value is defined. However, it's
 *         the only logical behavior as a none value cannot be compared against an actual number.*
 */
public fun MdibVersionGroup.instanceId(): Long = instanceIdAttr ?: 0L

/**
 * Gets the sequence id of the version group.
 *
 * @return the sequence id.
 */
public fun MdibVersionGroup.sequenceId(): String = sequenceIdAttr

/**
 * Converts an [MdibVersionGroup] instance to an [MdibVersion] instance.
 *
 * @return the MDIB version derived from this MDIB version group.
 */
public fun MdibVersionGroup.toMdibVersion(): MdibVersion = MdibVersion(sequenceId(), instanceId(), version())

/**
 * Container for MDIB version attributes.
 *
 * This class models the MDIB version attributes that lack a separate container in the XML Schema.
 * It models sequence id, instance id and version number, enclosed in one class.
 * [MdibVersion] is an immutable class and provides means to
 *
 *  - create versions with random UUID by [.create]
 *  - increment versions by [.increment]
 *  - set the version counter of an [MdibVersion] by [.setVersionCounter]
 *  - compare versions (see [.equals])
 *
 * @param sequenceId the sequence id to set.
 * @param version the version counter to set.
 * @param instanceId the instance id to set.
 */
public data class MdibVersion(val sequenceId: String, val instanceId: Long, val version: Long) {
    public companion object {
        /**
         * Creates a new instance with a random sequence id.
         *
         * @return a new instance.
         */
        public fun create(): MdibVersion = MdibVersion(UriUtil.randomUuid().toString())
    }

    /**
     * Constructor that sets a given sequence id.
     *
     * Instance id and version counter are initialized with 0.
     *
     * @param sequenceId the sequence id to set.
     */
    public constructor(sequenceId: String) : this(sequenceId, 0, 0)

    /**
     * Constructor that sets a given sequence id and version counter.
     *
     * Instance id is initialized with 0.
     *
     * @param sequenceId the sequence id to set.
     * @param version the version counter to set.
     */
    public constructor(sequenceId: String, version: Long) : this(sequenceId, version, 0)

    /**
     * Compares two MDIB versions.
     *
     *
     * As the MDIB version is a triple consisting of at least one non-trivially comparable data type, it cannot be
     * compared the classical way.
     * Hence, there is a set of rules that define what to expect depending on the data of that triple.
     * For that reason this compare function introduces a fourth result state, which is null,
     * in case that the non-trivially comparable sequence ids are not equal.
     *
     * @param rhs right hand side to compare.
     * @return depending on the fields of the MDIB version triple:
     *
     *  - null if sequence ids differ from each other
     *  - -1 if `lhs.instanceId < rhs.instanceId  || lhs.instanceId == rhs.instanceId && lhs.version < rhs.version
     *  - 0 if `lhs.instanceId == rhs.instanceId` && `lhs.version == rhs.version`
     *  - 1 if `lhs.instanceId > rhs.instanceId || lhs.instanceId == rhs.instanceId && lhs.version > rhs.version`
     */
    public fun compareTo(rhs: MdibVersion): Int? = when {
        sequenceId != rhs.sequenceId -> null
        instanceId < rhs.instanceId -> -1
        instanceId == rhs.instanceId -> version.compareTo(rhs.version)
        instanceId > rhs.instanceId -> 1
        else -> null
    }

    /**
     * Converts this to an [MdibVersionGroup].
     *
     * @return the converted [MdibVersionGroup] instance.
     */
    public fun toMdibVersionGroup(): MdibVersionGroup = MdibVersionGroup(
        mdibVersionAttr = VersionCounter(version),
        sequenceIdAttr = sequenceId,
        instanceIdAttr = instanceId
    )

    /**
     * Increases the version counter of this object by one.
     *
     * @return a new [MdibVersion] instance with `version = version + 1`.
     */
    public operator fun inc(): MdibVersion = copy(version = this.version.inc())
}