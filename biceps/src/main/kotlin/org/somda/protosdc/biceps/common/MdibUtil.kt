package org.somda.protosdc.biceps.common

import org.somda.protosdc.common.Oid
import org.somda.protosdc.common.toOid
import org.somda.protosdc.model.biceps.AbstractContextState
import org.somda.protosdc.model.biceps.AbstractDeviceComponentState
import org.somda.protosdc.model.biceps.AbstractMetricState
import org.somda.protosdc.model.biceps.AbstractMetricValue
import org.somda.protosdc.model.biceps.AbstractOperationDescriptor
import org.somda.protosdc.model.biceps.ActivateOperationDescriptor
import org.somda.protosdc.model.biceps.AlertConditionDescriptor
import org.somda.protosdc.model.biceps.AlertConditionState
import org.somda.protosdc.model.biceps.AlertSignalDescriptor
import org.somda.protosdc.model.biceps.AlertSignalPresence
import org.somda.protosdc.model.biceps.AlertSignalPrimaryLocation
import org.somda.protosdc.model.biceps.AlertSignalState
import org.somda.protosdc.model.biceps.CalibrationInfo
import org.somda.protosdc.model.biceps.CalibrationType
import org.somda.protosdc.model.biceps.ClinicalInfo
import org.somda.protosdc.model.biceps.ClockState
import org.somda.protosdc.model.biceps.CodedValue
import org.somda.protosdc.model.biceps.ComponentActivation
import org.somda.protosdc.model.biceps.ContextAssociation
import org.somda.protosdc.model.biceps.DistributionSampleArrayMetricState
import org.somda.protosdc.model.biceps.EnsembleContextState
import org.somda.protosdc.model.biceps.EnumStringMetricState
import org.somda.protosdc.model.biceps.GenerationMode
import org.somda.protosdc.model.biceps.Handle
import org.somda.protosdc.model.biceps.HandleRef
import org.somda.protosdc.model.biceps.InstanceIdentifier
import org.somda.protosdc.model.biceps.LimitAlertConditionDescriptor
import org.somda.protosdc.model.biceps.LocationContextState
import org.somda.protosdc.model.biceps.MdsOperatingMode
import org.somda.protosdc.model.biceps.MdsState
import org.somda.protosdc.model.biceps.MeansContextState
import org.somda.protosdc.model.biceps.NumericMetricState
import org.somda.protosdc.model.biceps.NumericMetricValue
import org.somda.protosdc.model.biceps.OperatorContextState
import org.somda.protosdc.model.biceps.PatientContextState
import org.somda.protosdc.model.biceps.QualityIndicator
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricState
import org.somda.protosdc.model.biceps.ReferencedVersion
import org.somda.protosdc.model.biceps.SampleArrayValue
import org.somda.protosdc.model.biceps.SetAlertStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetComponentStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetContextStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetMetricStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetStringOperationDescriptor
import org.somda.protosdc.model.biceps.SetValueOperationDescriptor
import org.somda.protosdc.model.biceps.StringMetricState
import org.somda.protosdc.model.biceps.StringMetricValue
import org.somda.protosdc.model.biceps.VersionCounter
import org.somda.protosdc.model.biceps.WorkflowContextState
import java.math.BigDecimal
import java.net.URI
import java.time.Duration

public fun Handle.ref(): HandleRef = HandleRef(this.string)

public fun VersionCounter.ref(): ReferencedVersion = ReferencedVersion(this)

public fun Long.toVersionCounter(): VersionCounter = VersionCounter(this)

public fun Long.toReferencedVersion(): ReferencedVersion = ReferencedVersion(toVersionCounter())

public object MdibUtil {
    public fun referencedVersion(value: Long): ReferencedVersion = ReferencedVersion(VersionCounter(value))
    public fun versionCounter(value: Long): VersionCounter = VersionCounter(value)
    public fun handle(handle: String): Handle = Handle(handle)
    public fun handleRef(handleRef: String): HandleRef = HandleRef(handleRef)
}

public fun AlertConditionState.presence(): Boolean = presenceAttr ?: false

public fun AlertSignalState.presence(): AlertSignalPresence =
    presenceAttr ?: AlertSignalPresence(AlertSignalPresence.EnumType.Off)

/**
 * Resolves the coding system of this coded value.
 *
 * @return the coding system. If none is specified explicitly, this function returns *urn:oid:1.3.111.2.11073.10101.1*
 * which refers to the IEEE 11073-10101 OID sub-arc. Unlike the implied value that is defined in the BICEPS standard,
 * this OID reflects the official reference to the IEEE 11073-10101 coding system.
 *
 * See https://sourceforge.net/p/opensdc/ieee11073-10207/265/
 */
public fun CodedValue.codingSystem(): String =
    codingSystemAttr ?: ieeeNomenclatureOid(IeeeVersion.VERSION_1).toUri().toString()

public fun CodedValue.codingSystemUri(): URI = URI(codingSystem())

public fun CodedValue.codingSystemOid(): Oid = codingSystemUri().toOid()

public fun CodedValue.allCodes(): MutableList<CodedValue> = this.translation.fold(mutableListOf(this)) { acc, cv ->
    acc.apply {
        add(
            CodedValue(
                codeAttr = cv.codeAttr,
                codingSystemAttr = cv.codingSystemAttr,
                codingSystemVersionAttr = cv.codingSystemVersionAttr
            )
        )
    }
}

public fun InstanceIdentifier.root(): Any = rootAttr ?: "biceps.uri.unk"

public fun CalibrationInfo.calibrationType(): CalibrationType =
    typeAttr ?: CalibrationType(CalibrationType.EnumType.Unspec)

public fun AbstractDeviceComponentState.activationState(): ComponentActivation =
    activationStateAttr ?: ComponentActivation(ComponentActivation.EnumType.On)

public fun MdsState.operationMode(): MdsOperatingMode =
    operatingModeAttr ?: MdsOperatingMode(MdsOperatingMode.EnumType.Nml)

public fun AlertConditionDescriptor.defaultConditionGenerationDelay(): Comparable<Nothing> =
    defaultConditionGenerationDelayAttr ?: Duration.ofMillis(0)

public fun LimitAlertConditionDescriptor.defaultConditionGenerationDelay(): Comparable<Nothing> =
    alertConditionDescriptor.defaultConditionGenerationDelay()

public fun LimitAlertConditionDescriptor.autoLimitSupported(): Boolean = autoLimitSupportedAttr ?: false

public fun AlertSignalDescriptor.defaultSignalGenerationDelay(): Comparable<Nothing> =
    defaultSignalGenerationDelayAttr ?: Duration.ofMillis(0)

public fun AlertSignalDescriptor.acknowledgementSupported(): Boolean = acknowledgementSupportedAttr ?: false

public fun AlertSignalState.location(): AlertSignalPrimaryLocation =
    locationAttr ?: AlertSignalPrimaryLocation(AlertSignalPrimaryLocation.EnumType.Loc)

public fun AbstractMetricValue.generationMode(): GenerationMode =
    metricQuality.modeAttr ?: GenerationMode(GenerationMode.EnumType.Real)

public fun AbstractMetricValue.qualityIndicator(): QualityIndicator =
    metricQuality.qiAttr ?: QualityIndicator(BigDecimal.ONE)

public fun NumericMetricValue.generationMode(): GenerationMode = abstractMetricValue.generationMode()

public fun NumericMetricValue.qualityIndicator(): QualityIndicator = abstractMetricValue.qualityIndicator()

public fun SampleArrayValue.generationMode(): GenerationMode = abstractMetricValue.generationMode()

public fun SampleArrayValue.qualityIndicator(): QualityIndicator = abstractMetricValue.qualityIndicator()

public fun StringMetricValue.generationMode(): GenerationMode = abstractMetricValue.generationMode()

public fun StringMetricValue.qualityIndicator(): QualityIndicator = abstractMetricValue.qualityIndicator()

public fun AbstractMetricState.activationState(): ComponentActivation =
    activationStateAttr ?: ComponentActivation(ComponentActivation.EnumType.On)

public fun StringMetricState.activationState(): ComponentActivation = abstractMetricState.activationState()

public fun NumericMetricState.activationState(): ComponentActivation = abstractMetricState.activationState()

public fun EnumStringMetricState.activationState(): ComponentActivation = stringMetricState.activationState()

public fun RealTimeSampleArrayMetricState.activationState(): ComponentActivation = abstractMetricState.activationState()

public fun DistributionSampleArrayMetricState.activationState(): ComponentActivation =
    abstractMetricState.activationState()

public fun AbstractOperationDescriptor.retriggerable(): Boolean = retriggerableAttr ?: true

public fun AbstractOperationDescriptor.accessLevel(): AbstractOperationDescriptor.AccessLevelAttr =
    accessLevelAttr ?: AbstractOperationDescriptor.AccessLevelAttr(
        AbstractOperationDescriptor.AccessLevelAttr.EnumType.Usr
    )

public fun SetStringOperationDescriptor.retriggerable(): Boolean = abstractOperationDescriptor.retriggerable()

public fun SetValueOperationDescriptor.retriggerable(): Boolean = abstractOperationDescriptor.retriggerable()

public fun ActivateOperationDescriptor.retriggerable(): Boolean =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.retriggerable()

public fun SetMetricStateOperationDescriptor.retriggerable(): Boolean =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.retriggerable()

public fun SetComponentStateOperationDescriptor.retriggerable(): Boolean =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.retriggerable()

public fun SetAlertStateOperationDescriptor.retriggerable(): Boolean =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.retriggerable()

public fun SetContextStateOperationDescriptor.retriggerable(): Boolean =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.retriggerable()

public fun SetStringOperationDescriptor.accessLevel(): AbstractOperationDescriptor.AccessLevelAttr =
    abstractOperationDescriptor.accessLevel()

public fun SetValueOperationDescriptor.accessLevel(): AbstractOperationDescriptor.AccessLevelAttr =
    abstractOperationDescriptor.accessLevel()

public fun ActivateOperationDescriptor.accessLevel(): AbstractOperationDescriptor.AccessLevelAttr =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.accessLevel()

public fun SetMetricStateOperationDescriptor.accessLevel(): AbstractOperationDescriptor.AccessLevelAttr =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.accessLevel()

public fun SetComponentStateOperationDescriptor.accessLevel(): AbstractOperationDescriptor.AccessLevelAttr =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.accessLevel()

public fun SetAlertStateOperationDescriptor.accessLevel(): AbstractOperationDescriptor.AccessLevelAttr =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.accessLevel()

public fun SetContextStateOperationDescriptor.accessLevel(): AbstractOperationDescriptor.AccessLevelAttr =
    abstractSetStateOperationDescriptor.abstractOperationDescriptor.accessLevel()

public fun ClockState.criticalUse(): Boolean = criticalUseAttr ?: false

public fun AbstractContextState.association(): ContextAssociation.EnumType =
    contextAssociationAttr?.enumType ?: ContextAssociation.EnumType.No

public fun EnsembleContextState.association(): ContextAssociation.EnumType = abstractContextState.association()

public fun MeansContextState.association(): ContextAssociation.EnumType = abstractContextState.association()

public fun LocationContextState.association(): ContextAssociation.EnumType = abstractContextState.association()

public fun OperatorContextState.association(): ContextAssociation.EnumType = abstractContextState.association()

public fun PatientContextState.association(): ContextAssociation.EnumType = abstractContextState.association()

public fun WorkflowContextState.association(): ContextAssociation.EnumType = abstractContextState.association()

public fun ClinicalInfo.criticality(): ClinicalInfo.Criticality =
    criticality ?: ClinicalInfo.Criticality(ClinicalInfo.Criticality.EnumType.Lo)