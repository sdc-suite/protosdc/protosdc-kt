package org.somda.protosdc.biceps.common.storage

import org.somda.protosdc.biceps.common.MdibDescriptionModifications

/**
 * A segment that is applied during description modifications.
 */
internal interface DescriptionPreprocessingSegment {
    /**
     * In a sequence of modifications this function processes one modification.
     *
     * @param modifications the modifications as passed from the previous preprocessing segment.
     * @param storage the MDIB storage for access.
     * @return the modifications to be passed to the next preprocessing segment
     */
    fun process(
        modifications: MdibDescriptionModifications,
        storage: MdibStorage
    ): MdibDescriptionModifications
}