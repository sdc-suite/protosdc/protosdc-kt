package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.*

/*
 * This file contains extension functions for all significant BICEPS state types to retrieve
 * 
 * - descriptor handle name
 * - descriptor version number
 * - state handle name (for context states)
 * - state version number
 */

// --- descriptor handle

public fun AbstractState.descriptorHandleString(): String = this.descriptorHandleAttr.string

public fun AbstractDeviceComponentState.descriptorHandleString(): String = this.abstractState.descriptorHandleString()

public fun AbstractComplexDeviceComponentState.descriptorHandleString(): String = this.abstractDeviceComponentState.descriptorHandleString()

public fun AbstractOperationState.descriptorHandleString(): String = this.abstractState.descriptorHandleString()

public fun AbstractAlertState.descriptorHandleString(): String = this.abstractState.descriptorHandleString()

public fun AbstractMetricState.descriptorHandleString(): String = this.abstractState.descriptorHandleString()

public fun AbstractMultiState.descriptorHandleString(): String = this.abstractState.descriptorHandleString()

public fun AbstractContextState.descriptorHandleString(): String = this.abstractMultiState.descriptorHandleString()

public fun ActivateOperationState.descriptorHandleString(): String = this.abstractOperationState.descriptorHandleString()

public fun AlertConditionState.descriptorHandleString(): String = this.abstractAlertState.descriptorHandleString()

public fun AlertSignalState.descriptorHandleString(): String = this.abstractAlertState.descriptorHandleString()

public fun AlertSystemState.descriptorHandleString(): String = this.abstractAlertState.descriptorHandleString()

public fun BatteryState.descriptorHandleString(): String = this.abstractDeviceComponentState.descriptorHandleString()

public fun ChannelState.descriptorHandleString(): String = this.abstractDeviceComponentState.descriptorHandleString()

public fun ClockState.descriptorHandleString(): String = this.abstractDeviceComponentState.descriptorHandleString()

public fun DistributionSampleArrayMetricState.descriptorHandleString(): String = this.abstractMetricState.descriptorHandleString()

public fun EnsembleContextState.descriptorHandleString(): String = this.abstractContextState.descriptorHandleString()

public fun StringMetricState.descriptorHandleString(): String = this.abstractMetricState.descriptorHandleString()

public fun EnumStringMetricState.descriptorHandleString(): String = this.stringMetricState.descriptorHandleString()

public fun LimitAlertConditionState.descriptorHandleString(): String = this.alertConditionState.descriptorHandleString()

public fun LocationContextState.descriptorHandleString(): String = this.abstractContextState.descriptorHandleString()

public fun MdsState.descriptorHandleString(): String = this.abstractComplexDeviceComponentState.descriptorHandleString()

public fun MeansContextState.descriptorHandleString(): String = this.abstractContextState.descriptorHandleString()

public fun NumericMetricState.descriptorHandleString(): String = this.abstractMetricState.descriptorHandleString()

public fun OperatorContextState.descriptorHandleString(): String = this.abstractContextState.descriptorHandleString()

public fun PatientContextState.descriptorHandleString(): String = this.abstractContextState.descriptorHandleString()

public fun RealTimeSampleArrayMetricState.descriptorHandleString(): String = this.abstractMetricState.descriptorHandleString()

public fun ScoState.descriptorHandleString(): String = this.abstractDeviceComponentState.descriptorHandleString()

public fun SetAlertStateOperationState.descriptorHandleString(): String = this.abstractOperationState.descriptorHandleString()

public fun SetComponentStateOperationState.descriptorHandleString(): String = this.abstractOperationState.descriptorHandleString()

public fun SetContextStateOperationState.descriptorHandleString(): String = this.abstractOperationState.descriptorHandleString()

public fun SetMetricStateOperationState.descriptorHandleString(): String = this.abstractOperationState.descriptorHandleString()

public fun SetStringOperationState.descriptorHandleString(): String = this.abstractOperationState.descriptorHandleString()

public fun SetValueOperationState.descriptorHandleString(): String = this.abstractOperationState.descriptorHandleString()

public fun SystemContextState.descriptorHandleString(): String = this.abstractDeviceComponentState.descriptorHandleString()

public fun VmdState.descriptorHandleString(): String = this.abstractComplexDeviceComponentState.descriptorHandleString()

public fun WorkflowContextState.descriptorHandleString(): String = this.abstractContextState.descriptorHandleString()



public fun AbstractState.descriptorHandle(): HandleRef = this.descriptorHandleAttr

public fun AbstractDeviceComponentState.descriptorHandle(): HandleRef = this.abstractState.descriptorHandle()

public fun AbstractComplexDeviceComponentState.descriptorHandle(): HandleRef = this.abstractDeviceComponentState.descriptorHandle()

public fun AbstractOperationState.descriptorHandle(): HandleRef = this.abstractState.descriptorHandle()

public fun AbstractAlertState.descriptorHandle(): HandleRef = this.abstractState.descriptorHandle()

public fun AbstractMetricState.descriptorHandle(): HandleRef = this.abstractState.descriptorHandle()

public fun AbstractMultiState.descriptorHandle(): HandleRef = this.abstractState.descriptorHandle()

public fun AbstractContextState.descriptorHandle(): HandleRef = this.abstractMultiState.descriptorHandle()

public fun ActivateOperationState.descriptorHandle(): HandleRef = this.abstractOperationState.descriptorHandle()

public fun AlertConditionState.descriptorHandle(): HandleRef = this.abstractAlertState.descriptorHandle()

public fun AlertSignalState.descriptorHandle(): HandleRef = this.abstractAlertState.descriptorHandle()

public fun AlertSystemState.descriptorHandle(): HandleRef = this.abstractAlertState.descriptorHandle()

public fun BatteryState.descriptorHandle(): HandleRef = this.abstractDeviceComponentState.descriptorHandle()

public fun ChannelState.descriptorHandle(): HandleRef = this.abstractDeviceComponentState.descriptorHandle()

public fun ClockState.descriptorHandle(): HandleRef = this.abstractDeviceComponentState.descriptorHandle()

public fun DistributionSampleArrayMetricState.descriptorHandle(): HandleRef = this.abstractMetricState.descriptorHandle()

public fun EnsembleContextState.descriptorHandle(): HandleRef = this.abstractContextState.descriptorHandle()

public fun StringMetricState.descriptorHandle(): HandleRef = this.abstractMetricState.descriptorHandle()

public fun EnumStringMetricState.descriptorHandle(): HandleRef = this.stringMetricState.descriptorHandle()

public fun LimitAlertConditionState.descriptorHandle(): HandleRef = this.alertConditionState.descriptorHandle()

public fun LocationContextState.descriptorHandle(): HandleRef = this.abstractContextState.descriptorHandle()

public fun MdsState.descriptorHandle(): HandleRef = this.abstractComplexDeviceComponentState.descriptorHandle()

public fun MeansContextState.descriptorHandle(): HandleRef = this.abstractContextState.descriptorHandle()

public fun NumericMetricState.descriptorHandle(): HandleRef = this.abstractMetricState.descriptorHandle()

public fun OperatorContextState.descriptorHandle(): HandleRef = this.abstractContextState.descriptorHandle()

public fun PatientContextState.descriptorHandle(): HandleRef = this.abstractContextState.descriptorHandle()

public fun RealTimeSampleArrayMetricState.descriptorHandle(): HandleRef = this.abstractMetricState.descriptorHandle()

public fun ScoState.descriptorHandle(): HandleRef = this.abstractDeviceComponentState.descriptorHandle()

public fun SetAlertStateOperationState.descriptorHandle(): HandleRef = this.abstractOperationState.descriptorHandle()

public fun SetComponentStateOperationState.descriptorHandle(): HandleRef = this.abstractOperationState.descriptorHandle()

public fun SetContextStateOperationState.descriptorHandle(): HandleRef = this.abstractOperationState.descriptorHandle()

public fun SetMetricStateOperationState.descriptorHandle(): HandleRef = this.abstractOperationState.descriptorHandle()

public fun SetStringOperationState.descriptorHandle(): HandleRef = this.abstractOperationState.descriptorHandle()

public fun SetValueOperationState.descriptorHandle(): HandleRef = this.abstractOperationState.descriptorHandle()

public fun SystemContextState.descriptorHandle(): HandleRef = this.abstractDeviceComponentState.descriptorHandle()

public fun VmdState.descriptorHandle(): HandleRef = this.abstractComplexDeviceComponentState.descriptorHandle()

public fun WorkflowContextState.descriptorHandle(): HandleRef = this.abstractContextState.descriptorHandle()

// --- descriptor version

public fun AbstractState.descriptorVersion(): Long = this.descriptorVersionAttr?.versionCounter?.unsignedLong ?: 0L

public fun AbstractDeviceComponentState.descriptorVersion(): Long = this.abstractState.descriptorVersion()

public fun AbstractComplexDeviceComponentState.descriptorVersion(): Long = this.abstractDeviceComponentState.descriptorVersion()

public fun AbstractOperationState.descriptorVersion(): Long = this.abstractState.descriptorVersion()

public fun AbstractAlertState.descriptorVersion(): Long = this.abstractState.descriptorVersion()

public fun AbstractMetricState.descriptorVersion(): Long = this.abstractState.descriptorVersion()

public fun AbstractMultiState.descriptorVersion(): Long = this.abstractState.descriptorVersion()

public fun AbstractContextState.descriptorVersion(): Long = this.abstractMultiState.descriptorVersion()

public fun ActivateOperationState.descriptorVersion(): Long = this.abstractOperationState.descriptorVersion()

public fun AlertConditionState.descriptorVersion(): Long = this.abstractAlertState.descriptorVersion()

public fun AlertSignalState.descriptorVersion(): Long = this.abstractAlertState.descriptorVersion()

public fun AlertSystemState.descriptorVersion(): Long = this.abstractAlertState.descriptorVersion()

public fun BatteryState.descriptorVersion(): Long = this.abstractDeviceComponentState.descriptorVersion()

public fun ChannelState.descriptorVersion(): Long = this.abstractDeviceComponentState.descriptorVersion()

public fun ClockState.descriptorVersion(): Long = this.abstractDeviceComponentState.descriptorVersion()

public fun DistributionSampleArrayMetricState.descriptorVersion(): Long = this.abstractMetricState.descriptorVersion()

public fun EnsembleContextState.descriptorVersion(): Long = this.abstractContextState.descriptorVersion()

public fun StringMetricState.descriptorVersion(): Long = this.abstractMetricState.descriptorVersion()

public fun EnumStringMetricState.descriptorVersion(): Long = this.stringMetricState.descriptorVersion()

public fun LimitAlertConditionState.descriptorVersion(): Long = this.alertConditionState.descriptorVersion()

public fun LocationContextState.descriptorVersion(): Long = this.abstractContextState.descriptorVersion()

public fun MdsState.descriptorVersion(): Long = this.abstractComplexDeviceComponentState.descriptorVersion()

public fun MeansContextState.descriptorVersion(): Long = this.abstractContextState.descriptorVersion()

public fun NumericMetricState.descriptorVersion(): Long = this.abstractMetricState.descriptorVersion()

public fun OperatorContextState.descriptorVersion(): Long = this.abstractContextState.descriptorVersion()

public fun PatientContextState.descriptorVersion(): Long = this.abstractContextState.descriptorVersion()

public fun RealTimeSampleArrayMetricState.descriptorVersion(): Long = this.abstractMetricState.descriptorVersion()

public fun ScoState.descriptorVersion(): Long = this.abstractDeviceComponentState.descriptorVersion()

public fun SetAlertStateOperationState.descriptorVersion(): Long = this.abstractOperationState.descriptorVersion()

public fun SetComponentStateOperationState.descriptorVersion(): Long = this.abstractOperationState.descriptorVersion()

public fun SetContextStateOperationState.descriptorVersion(): Long = this.abstractOperationState.descriptorVersion()

public fun SetMetricStateOperationState.descriptorVersion(): Long = this.abstractOperationState.descriptorVersion()

public fun SetStringOperationState.descriptorVersion(): Long = this.abstractOperationState.descriptorVersion()

public fun SetValueOperationState.descriptorVersion(): Long = this.abstractOperationState.descriptorVersion()

public fun SystemContextState.descriptorVersion(): Long = this.abstractDeviceComponentState.descriptorVersion()

public fun VmdState.descriptorVersion(): Long = this.abstractComplexDeviceComponentState.descriptorVersion()

public fun WorkflowContextState.descriptorVersion(): Long = this.abstractContextState.descriptorVersion()

// --- state version

public fun AbstractState.stateVersion(): Long = this.stateVersionAttr?.unsignedLong ?: 0L

public fun AbstractDeviceComponentState.stateVersion(): Long = this.abstractState.stateVersion()

public fun AbstractComplexDeviceComponentState.stateVersion(): Long = this.abstractDeviceComponentState.stateVersion()

public fun AbstractOperationState.stateVersion(): Long = this.abstractState.stateVersion()

public fun AbstractAlertState.stateVersion(): Long = this.abstractState.stateVersion()

public fun AbstractMetricState.stateVersion(): Long = this.abstractState.stateVersion()

public fun AbstractMultiState.stateVersion(): Long = this.abstractState.stateVersion()

public fun AbstractContextState.stateVersion(): Long = this.abstractMultiState.stateVersion()

public fun ActivateOperationState.stateVersion(): Long = this.abstractOperationState.stateVersion()

public fun AlertConditionState.stateVersion(): Long = this.abstractAlertState.stateVersion()

public fun AlertSignalState.stateVersion(): Long = this.abstractAlertState.stateVersion()

public fun AlertSystemState.stateVersion(): Long = this.abstractAlertState.stateVersion()

public fun BatteryState.stateVersion(): Long = this.abstractDeviceComponentState.stateVersion()

public fun ChannelState.stateVersion(): Long = this.abstractDeviceComponentState.stateVersion()

public fun ClockState.stateVersion(): Long = this.abstractDeviceComponentState.stateVersion()

public fun DistributionSampleArrayMetricState.stateVersion(): Long = this.abstractMetricState.stateVersion()

public fun EnsembleContextState.stateVersion(): Long = this.abstractContextState.stateVersion()

public fun StringMetricState.stateVersion(): Long = this.abstractMetricState.stateVersion()

public fun EnumStringMetricState.stateVersion(): Long = this.stringMetricState.stateVersion()

public fun LimitAlertConditionState.stateVersion(): Long = this.alertConditionState.stateVersion()

public fun LocationContextState.stateVersion(): Long = this.abstractContextState.stateVersion()

public fun MdsState.stateVersion(): Long = this.abstractComplexDeviceComponentState.stateVersion()

public fun MeansContextState.stateVersion(): Long = this.abstractContextState.stateVersion()

public fun NumericMetricState.stateVersion(): Long = this.abstractMetricState.stateVersion()

public fun OperatorContextState.stateVersion(): Long = this.abstractContextState.stateVersion()

public fun PatientContextState.stateVersion(): Long = this.abstractContextState.stateVersion()

public fun RealTimeSampleArrayMetricState.stateVersion(): Long = this.abstractMetricState.stateVersion()

public fun ScoState.stateVersion(): Long = this.abstractDeviceComponentState.stateVersion()

public fun SetAlertStateOperationState.stateVersion(): Long = this.abstractOperationState.stateVersion()

public fun SetComponentStateOperationState.stateVersion(): Long = this.abstractOperationState.stateVersion()

public fun SetContextStateOperationState.stateVersion(): Long = this.abstractOperationState.stateVersion()

public fun SetMetricStateOperationState.stateVersion(): Long = this.abstractOperationState.stateVersion()

public fun SetStringOperationState.stateVersion(): Long = this.abstractOperationState.stateVersion()

public fun SetValueOperationState.stateVersion(): Long = this.abstractOperationState.stateVersion()

public fun SystemContextState.stateVersion(): Long = this.abstractDeviceComponentState.stateVersion()

public fun VmdState.stateVersion(): Long = this.abstractComplexDeviceComponentState.stateVersion()

public fun WorkflowContextState.stateVersion(): Long = this.abstractContextState.stateVersion()

// --- state handle

public fun AbstractContextState.handleString(): String = this.abstractMultiState.handleAttr.string

public fun EnsembleContextState.handleString(): String = this.abstractContextState.handleString()

public fun LocationContextState.handleString(): String = this.abstractContextState.handleString()

public fun MeansContextState.handleString(): String = this.abstractContextState.handleString()

public fun OperatorContextState.handleString(): String = this.abstractContextState.handleString()

public fun PatientContextState.handleString(): String = this.abstractContextState.handleString()

public fun WorkflowContextState.handleString(): String = this.abstractContextState.handleString()

// --- version replacement

public fun AbstractState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractState =
    this.copy(
        descriptorVersionAttr = newDescriptorVersion?.let { MdibUtil.referencedVersion(it) }
            ?: this.descriptorVersionAttr,
        stateVersionAttr = newStateVersion?.let { MdibUtil.versionCounter(it) } ?: this.stateVersionAttr,
        descriptorHandleAttr = newDescriptorHandle?.let { MdibUtil.handleRef(newDescriptorHandle) }
            ?: this.descriptorHandleAttr
    )

public fun AbstractOperationState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractOperationState =
    this.copy(
        abstractState = this.abstractState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AbstractAlertState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractAlertState =
    this.copy(
        abstractState = this.abstractState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AbstractMultiState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractMultiState =
    this.copy(
        abstractState = this.abstractState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AbstractContextState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractContextState =
    this.copy(
        abstractMultiState = this.abstractMultiState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AbstractMetricState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractMetricState =
    this.copy(
        abstractState = this.abstractState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AbstractDeviceComponentState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractDeviceComponentState =
    this.copy(
        abstractState = this.abstractState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AbstractComplexDeviceComponentState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null, newDescriptorHandle: String? = null
): AbstractComplexDeviceComponentState = this.copy(
    abstractDeviceComponentState = this.abstractDeviceComponentState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

public fun SetContextStateOperationState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): SetContextStateOperationState =
    this.copy(
        abstractOperationState = this.abstractOperationState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun SetValueOperationState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): SetValueOperationState =
    this.copy(
        abstractOperationState = this.abstractOperationState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun SetComponentStateOperationState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): SetComponentStateOperationState =
    this.copy(
        abstractOperationState = this.abstractOperationState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun SetMetricStateOperationState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): SetMetricStateOperationState =
    this.copy(
        abstractOperationState = this.abstractOperationState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun SetAlertStateOperationState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): SetAlertStateOperationState =
    this.copy(
        abstractOperationState = this.abstractOperationState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun SetStringOperationState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): SetStringOperationState =
    this.copy(
        abstractOperationState = this.abstractOperationState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun ActivateOperationState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): ActivateOperationState =
    this.copy(
        abstractOperationState = this.abstractOperationState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AlertConditionState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AlertConditionState =
    this.copy(
        abstractAlertState = this.abstractAlertState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun LimitAlertConditionState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): LimitAlertConditionState =
    this.copy(
        alertConditionState = this.alertConditionState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AlertSystemState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AlertSystemState =
    this.copy(
        abstractAlertState = this.abstractAlertState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun AlertSignalState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AlertSignalState =
    this.copy(
        abstractAlertState = this.abstractAlertState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun OperatorContextState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): OperatorContextState =
    this.copy(
        abstractContextState = this.abstractContextState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun EnsembleContextState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): EnsembleContextState =
    this.copy(
        abstractContextState = this.abstractContextState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun WorkflowContextState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): WorkflowContextState =
    this.copy(
        abstractContextState = this.abstractContextState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun PatientContextState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): PatientContextState =
    this.copy(
        abstractContextState = this.abstractContextState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun LocationContextState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): LocationContextState =
    this.copy(
        abstractContextState = this.abstractContextState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun MeansContextState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): MeansContextState =
    this.copy(
        abstractContextState = this.abstractContextState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun RealTimeSampleArrayMetricState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): RealTimeSampleArrayMetricState =
    this.copy(
        abstractMetricState = this.abstractMetricState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun NumericMetricState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): NumericMetricState =
    this.copy(
        abstractMetricState = this.abstractMetricState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun DistributionSampleArrayMetricState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null, newDescriptorHandle: String? = null
): DistributionSampleArrayMetricState = this.copy(
    abstractMetricState = this.abstractMetricState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

public fun StringMetricState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): StringMetricState =
    this.copy(
        abstractMetricState = this.abstractMetricState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun EnumStringMetricState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): EnumStringMetricState =
    this.copy(
        stringMetricState = this.stringMetricState.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

public fun ClockState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): ClockState = this.copy(
    abstractDeviceComponentState = this.abstractDeviceComponentState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

public fun ChannelState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): ChannelState = this.copy(
    abstractDeviceComponentState = this.abstractDeviceComponentState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

public fun SystemContextState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): SystemContextState = this.copy(
    abstractDeviceComponentState = this.abstractDeviceComponentState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

public fun VmdState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): VmdState = this.copy(
    abstractComplexDeviceComponentState = this.abstractComplexDeviceComponentState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

public fun MdsState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): MdsState = this.copy(
    abstractComplexDeviceComponentState = this.abstractComplexDeviceComponentState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

public fun BatteryState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): BatteryState = this.copy(
    abstractDeviceComponentState = this.abstractDeviceComponentState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

public fun ScoState.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): ScoState = this.copy(
    abstractDeviceComponentState = this.abstractDeviceComponentState.replaceInCopy(
        newDescriptorVersion,
        newStateVersion,
        newDescriptorHandle
    )
)

// conversion functions from state to one-ofs

public fun AlertConditionState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceAlertConditionState(this)
public fun AlertConditionState.toAlertOneOf(): AbstractAlertStateOneOf = AbstractAlertStateOneOf.ChoiceAlertConditionState(this)

public fun LimitAlertConditionState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceLimitAlertConditionState(this)
public fun LimitAlertConditionState.toAlertOneOf(): AbstractAlertStateOneOf = AbstractAlertStateOneOf.ChoiceLimitAlertConditionState(this)

public fun AlertSignalState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceAlertSignalState(this)
public fun AlertSignalState.toAlertOneOf(): AbstractAlertStateOneOf = AbstractAlertStateOneOf.ChoiceAlertSignalState(this)

public fun AlertSystemState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceAlertSystemState(this)
public fun AlertSystemState.toAlertOneOf(): AbstractAlertStateOneOf = AbstractAlertStateOneOf.ChoiceAlertSystemState(this)

public fun ClockState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceClockState(this)
public fun ClockState.toComponentOneOf(): AbstractDeviceComponentStateOneOf = AbstractDeviceComponentStateOneOf.ChoiceClockState(this)

public fun BatteryState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceBatteryState(this)
public fun BatteryState.toComponentOneOf(): AbstractDeviceComponentStateOneOf = AbstractDeviceComponentStateOneOf.ChoiceBatteryState(this)

public fun ChannelState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceChannelState(this)
public fun ChannelState.toComponentOneOf(): AbstractDeviceComponentStateOneOf = AbstractDeviceComponentStateOneOf.ChoiceChannelState(this)

public fun ScoState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceScoState(this)
public fun ScoState.toComponentOneOf(): AbstractDeviceComponentStateOneOf = AbstractDeviceComponentStateOneOf.ChoiceScoState(this)

public fun SystemContextState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceSystemContextState(this)
public fun SystemContextState.toComponentOneOf(): AbstractDeviceComponentStateOneOf = AbstractDeviceComponentStateOneOf.ChoiceSystemContextState(this)

public fun VmdState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceVmdState(this)
public fun VmdState.toComponentOneOf(): AbstractDeviceComponentStateOneOf = AbstractDeviceComponentStateOneOf.ChoiceVmdState(this)

public fun MdsState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceMdsState(this)
public fun MdsState.toComponentOneOf(): AbstractDeviceComponentStateOneOf = AbstractDeviceComponentStateOneOf.ChoiceMdsState(this)

public fun SetStringOperationState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceSetStringOperationState(this)
public fun SetStringOperationState.toOperationOneOf(): AbstractOperationStateOneOf = AbstractOperationStateOneOf.ChoiceSetStringOperationState(this)

public fun SetComponentStateOperationState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceSetComponentStateOperationState(this)
public fun SetComponentStateOperationState.toOperationOneOf(): AbstractOperationStateOneOf = AbstractOperationStateOneOf.ChoiceSetComponentStateOperationState(this)

public fun SetAlertStateOperationState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceSetAlertStateOperationState(this)
public fun SetAlertStateOperationState.toOperationOneOf(): AbstractOperationStateOneOf = AbstractOperationStateOneOf.ChoiceSetAlertStateOperationState(this)

public fun SetMetricStateOperationState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceSetMetricStateOperationState(this)
public fun SetMetricStateOperationState.toOperationOneOf(): AbstractOperationStateOneOf = AbstractOperationStateOneOf.ChoiceSetMetricStateOperationState(this)

public fun SetContextStateOperationState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceSetContextStateOperationState(this)
public fun SetContextStateOperationState.toOperationOneOf(): AbstractOperationStateOneOf = AbstractOperationStateOneOf.ChoiceSetContextStateOperationState(this)

public fun ActivateOperationState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceActivateOperationState(this)
public fun ActivateOperationState.toOperationOneOf(): AbstractOperationStateOneOf = AbstractOperationStateOneOf.ChoiceActivateOperationState(this)

public fun SetValueOperationState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceSetValueOperationState(this)
public fun SetValueOperationState.toOperationOneOf(): AbstractOperationStateOneOf = AbstractOperationStateOneOf.ChoiceSetValueOperationState(this)

public fun NumericMetricState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceNumericMetricState(this)
public fun NumericMetricState.toMetricOneOf(): AbstractMetricStateOneOf = AbstractMetricStateOneOf.ChoiceNumericMetricState(this)

public fun DistributionSampleArrayMetricState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState(this)
public fun DistributionSampleArrayMetricState.toMetricOneOf(): AbstractMetricStateOneOf = AbstractMetricStateOneOf.ChoiceDistributionSampleArrayMetricState(this)

public fun RealTimeSampleArrayMetricState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState(this)
public fun RealTimeSampleArrayMetricState.toMetricOneOf(): AbstractMetricStateOneOf = AbstractMetricStateOneOf.ChoiceRealTimeSampleArrayMetricState(this)

public fun StringMetricState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceStringMetricState(this)
public fun StringMetricState.toMetricOneOf(): AbstractMetricStateOneOf = AbstractMetricStateOneOf.ChoiceStringMetricState(this)

public fun EnumStringMetricState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceEnumStringMetricState(this)
public fun EnumStringMetricState.toMetricOneOf(): AbstractMetricStateOneOf = AbstractMetricStateOneOf.ChoiceEnumStringMetricState(this)

public fun MeansContextState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceMeansContextState(this)
public fun MeansContextState.toContextOneOf(): AbstractContextStateOneOf = AbstractContextStateOneOf.ChoiceMeansContextState(this)

public fun WorkflowContextState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceWorkflowContextState(this)
public fun WorkflowContextState.toContextOneOf(): AbstractContextStateOneOf = AbstractContextStateOneOf.ChoiceWorkflowContextState(this)

public fun PatientContextState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoicePatientContextState(this)
public fun PatientContextState.toContextOneOf(): AbstractContextStateOneOf = AbstractContextStateOneOf.ChoicePatientContextState(this)

public fun OperatorContextState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceOperatorContextState(this)
public fun OperatorContextState.toContextOneOf(): AbstractContextStateOneOf = AbstractContextStateOneOf.ChoiceOperatorContextState(this)

public fun EnsembleContextState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceEnsembleContextState(this)
public fun EnsembleContextState.toContextOneOf(): AbstractContextStateOneOf = AbstractContextStateOneOf.ChoiceEnsembleContextState(this)

public fun LocationContextState.toOneOf(): AbstractStateOneOf = AbstractStateOneOf.ChoiceLocationContextState(this)
public fun LocationContextState.toContextOneOf(): AbstractContextStateOneOf = AbstractContextStateOneOf.ChoiceLocationContextState(this)
