package org.somda.protosdc.biceps.common

import org.apache.logging.log4j.kotlin.Logging

/**
 * Container to collect changes supposed to be applied on an MDIB.
 *
 * The [MdibDescriptionModifications] is a fluent interface.
 */
public class MdibDescriptionModifications {
    private companion object : Logging

    private val modifications = mutableListOf<MdibDescriptionModification>()
    private val usedHandles = mutableListOf<String>()

    public fun add(modification: MdibDescriptionModification): MdibDescriptionModifications = modification.let {
        duplicateDetection(modification)
        modifications.add(modification)
    }.let { this }

    public fun addAll(modifications: Collection<MdibDescriptionModification>): MdibDescriptionModifications =
        modifications
            .also {
                require(mutableSetOf<String>()
                    .also { set -> set.addAll(modifications.map { it.descriptor.handleString() }) }
                    .also { set -> set.addAll(this.modifications.map { it.descriptor.handleString() }) }
                    .size == modifications.size + this.modifications.size) {
                    "Duplicated handle detected while adding modifications to MdibDescriptionModifications"
                }
            }
            .onEach { add(it) }
            .let { this }

    public fun insert(modification: DescriptionItem): MdibDescriptionModifications = add(MdibDescriptionModification.Insert(modification))

    public fun insertAll(modifications: Collection<DescriptionItem>): MdibDescriptionModifications =
        addAll(modifications.map { MdibDescriptionModification.Insert(it) })

    public fun update(modification: DescriptionItem): MdibDescriptionModifications = add(MdibDescriptionModification.Update(modification))

    public fun updateAll(modifications: Collection<DescriptionItem>): MdibDescriptionModifications =
        addAll(modifications.map { MdibDescriptionModification.Update(it) })

    public fun delete(handle: String): MdibDescriptionModifications = add(MdibDescriptionModification.Delete(handle))

    public fun deleteAll(handles: Collection<String>): MdibDescriptionModifications =
        addAll(handles.map { MdibDescriptionModification.Delete(it) })

    private fun duplicateDetection(modification: MdibDescriptionModification) {
        modification.descriptor.handleString().also {
            require(!usedHandles.contains(it)) {
                "Handle '$it' has already been inserted into description change set"
            }
            usedHandles.add(it)
        }
    }

    public fun asList(): List<MdibDescriptionModification> = modifications
}