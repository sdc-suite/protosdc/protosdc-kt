package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.*

/*
 * This file contains convenience extension functions to OneOf data classes, like retrieving
 *
 * - handles (descriptors and states)
 * - descriptor version
 * - state version
 * - context association
 */

public fun AbstractDescriptorOneOf.handleString(): String = when (this) {
    is AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceBatteryDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceChannelDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceClockDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceLocationContextDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceMdsDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceMeansContextDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoicePatientContextDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceScoDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceStringMetricDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceSystemContextDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceVmdDescriptor ->
        this.value.handleString()

    is AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor ->
        this.value.handleString()
    // in case entities are deleted, this branch becomes valid
    is AbstractDescriptorOneOf.ChoiceAbstractDescriptor -> this.value.handleString()
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractDescriptorOneOf.descriptorVersion(): Long = when (this) {
    is AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceBatteryDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceChannelDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceClockDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceLocationContextDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceMdsDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceMeansContextDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoicePatientContextDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceScoDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceStringMetricDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceSystemContextDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceVmdDescriptor ->
        this.value.descriptorVersion()

    is AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor ->
        this.value.descriptorVersion()
    // in case entities are deleted, this branch becomes valid
    is AbstractDescriptorOneOf.ChoiceAbstractDescriptor -> this.value.descriptorVersion()
    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractStateOneOf.descriptorHandleString(): String = when (this) {
    is AbstractStateOneOf.ChoiceActivateOperationState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceAlertConditionState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceAlertSignalState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceAlertSystemState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceBatteryState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceChannelState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceClockState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceEnsembleContextState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceEnumStringMetricState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceLimitAlertConditionState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceLocationContextState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceMdsState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceMeansContextState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceNumericMetricState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceOperatorContextState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoicePatientContextState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceScoState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceSetAlertStateOperationState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceSetComponentStateOperationState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceSetContextStateOperationState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceSetMetricStateOperationState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceSetStringOperationState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceSetValueOperationState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceStringMetricState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceSystemContextState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceVmdState ->
        this.value.descriptorHandleString()

    is AbstractStateOneOf.ChoiceWorkflowContextState ->
        this.value.descriptorHandleString()

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractStateOneOf.descriptorVersion(): Long = when (this) {
    is AbstractStateOneOf.ChoiceActivateOperationState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceAlertConditionState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceAlertSignalState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceAlertSystemState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceBatteryState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceChannelState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceClockState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceEnsembleContextState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceEnumStringMetricState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceLimitAlertConditionState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceLocationContextState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceMdsState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceMeansContextState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceNumericMetricState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceOperatorContextState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoicePatientContextState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceScoState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceSetAlertStateOperationState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceSetComponentStateOperationState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceSetContextStateOperationState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceSetMetricStateOperationState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceSetStringOperationState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceSetValueOperationState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceStringMetricState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceSystemContextState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceVmdState ->
        this.value.descriptorVersion()

    is AbstractStateOneOf.ChoiceWorkflowContextState ->
        this.value.descriptorVersion()

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractStateOneOf.stateVersion(): Long = when (this) {
    is AbstractStateOneOf.ChoiceActivateOperationState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceAlertConditionState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceAlertSignalState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceAlertSystemState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceBatteryState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceChannelState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceClockState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceEnsembleContextState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceEnumStringMetricState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceLimitAlertConditionState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceLocationContextState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceMdsState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceMeansContextState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceNumericMetricState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceOperatorContextState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoicePatientContextState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceScoState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceSetAlertStateOperationState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceSetComponentStateOperationState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceSetContextStateOperationState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceSetMetricStateOperationState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceSetStringOperationState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceSetValueOperationState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceStringMetricState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceSystemContextState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceVmdState ->
        this.value.stateVersion()

    is AbstractStateOneOf.ChoiceWorkflowContextState ->
        this.value.stateVersion()

    else -> throw InvalidWhenBranchException(this)
}

public fun EnsembleContextState.associationType(): ContextAssociation.EnumType =
    abstractContextState.contextAssociationAttr?.enumType ?: ContextAssociation.EnumType.No

public fun MeansContextState.associationType(): ContextAssociation.EnumType =
    abstractContextState.contextAssociationAttr?.enumType ?: ContextAssociation.EnumType.No

public fun LocationContextState.associationType(): ContextAssociation.EnumType =
    abstractContextState.contextAssociationAttr?.enumType ?: ContextAssociation.EnumType.No

public fun OperatorContextState.associationType(): ContextAssociation.EnumType =
    abstractContextState.contextAssociationAttr?.enumType ?: ContextAssociation.EnumType.No

public fun PatientContextState.associationType(): ContextAssociation.EnumType =
    abstractContextState.contextAssociationAttr?.enumType ?: ContextAssociation.EnumType.No

public fun WorkflowContextState.associationType(): ContextAssociation.EnumType =
    abstractContextState.contextAssociationAttr?.enumType ?: ContextAssociation.EnumType.No

public fun resolveContextAssociationType(state: AbstractStateOneOf): ContextAssociation.EnumType? = when (state) {
    is AbstractStateOneOf.ChoiceEnsembleContextState ->
        state.value.associationType()

    is AbstractStateOneOf.ChoiceMeansContextState ->
        state.value.associationType()

    is AbstractStateOneOf.ChoiceLocationContextState ->
        state.value.associationType()

    is AbstractStateOneOf.ChoiceOperatorContextState ->
        state.value.associationType()

    is AbstractStateOneOf.ChoicePatientContextState ->
        state.value.associationType()

    is AbstractStateOneOf.ChoiceWorkflowContextState ->
        state.value.associationType()

    else -> null
}

public fun AbstractContextStateOneOf.contextAssociation(): ContextAssociation.EnumType = when (this) {
    is AbstractContextStateOneOf.ChoiceEnsembleContextState ->
        this.value.associationType()

    is AbstractContextStateOneOf.ChoiceMeansContextState ->
        this.value.associationType()

    is AbstractContextStateOneOf.ChoiceLocationContextState ->
        this.value.associationType()

    is AbstractContextStateOneOf.ChoiceOperatorContextState ->
        this.value.associationType()

    is AbstractContextStateOneOf.ChoicePatientContextState ->
        this.value.associationType()

    is AbstractContextStateOneOf.ChoiceWorkflowContextState ->
        this.value.associationType()

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractContextStateOneOf.handleString(): String = when (this) {
    is AbstractContextStateOneOf.ChoiceEnsembleContextState ->
        this.value.handleString()

    is AbstractContextStateOneOf.ChoiceMeansContextState ->
        this.value.handleString()

    is AbstractContextStateOneOf.ChoiceLocationContextState ->
        this.value.handleString()

    is AbstractContextStateOneOf.ChoiceOperatorContextState ->
        this.value.handleString()

    is AbstractContextStateOneOf.ChoicePatientContextState ->
        this.value.handleString()

    is AbstractContextStateOneOf.ChoiceWorkflowContextState ->
        this.value.handleString()

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractContextStateOneOf.descriptorHandleString(): String = when (this) {
    is AbstractContextStateOneOf.ChoiceEnsembleContextState ->
        this.value.descriptorHandleString()

    is AbstractContextStateOneOf.ChoiceMeansContextState ->
        this.value.descriptorHandleString()

    is AbstractContextStateOneOf.ChoiceLocationContextState ->
        this.value.descriptorHandleString()

    is AbstractContextStateOneOf.ChoiceOperatorContextState ->
        this.value.descriptorHandleString()

    is AbstractContextStateOneOf.ChoicePatientContextState ->
        this.value.descriptorHandleString()

    is AbstractContextStateOneOf.ChoiceWorkflowContextState ->
        this.value.descriptorHandleString()

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractContextStateOneOf.descriptorVersion(): Long = when (this) {
    is AbstractContextStateOneOf.ChoiceEnsembleContextState ->
        this.value.descriptorVersion()

    is AbstractContextStateOneOf.ChoiceMeansContextState ->
        this.value.descriptorVersion()

    is AbstractContextStateOneOf.ChoiceLocationContextState ->
        this.value.descriptorVersion()

    is AbstractContextStateOneOf.ChoiceOperatorContextState ->
        this.value.descriptorVersion()

    is AbstractContextStateOneOf.ChoicePatientContextState ->
        this.value.descriptorVersion()

    is AbstractContextStateOneOf.ChoiceWorkflowContextState ->
        this.value.descriptorVersion()

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractContextStateOneOf.stateVersion(): Long = when (this) {
    is AbstractContextStateOneOf.ChoiceEnsembleContextState ->
        this.value.stateVersion()

    is AbstractContextStateOneOf.ChoiceMeansContextState ->
        this.value.stateVersion()

    is AbstractContextStateOneOf.ChoiceLocationContextState ->
        this.value.stateVersion()

    is AbstractContextStateOneOf.ChoiceOperatorContextState ->
        this.value.stateVersion()

    is AbstractContextStateOneOf.ChoicePatientContextState ->
        this.value.stateVersion()

    is AbstractContextStateOneOf.ChoiceWorkflowContextState ->
        this.value.stateVersion()

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractContextStateOneOf.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractContextStateOneOf = when (this) {
    is AbstractContextStateOneOf.ChoiceOperatorContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractContextStateOneOf.ChoiceEnsembleContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractContextStateOneOf.ChoiceWorkflowContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractContextStateOneOf.ChoicePatientContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractContextStateOneOf.ChoiceLocationContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractContextStateOneOf.ChoiceMeansContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractStateOneOf.replaceInCopy(
    newDescriptorVersion: Long? = null,
    newStateVersion: Long? = null,
    newDescriptorHandle: String? = null
): AbstractStateOneOf = when (this) {
    is AbstractStateOneOf.ChoiceSetContextStateOperationState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceSetValueOperationState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceSetComponentStateOperationState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceSetMetricStateOperationState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceSetAlertStateOperationState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceSetStringOperationState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceActivateOperationState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceAlertConditionState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceLimitAlertConditionState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceAlertSystemState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceAlertSignalState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceOperatorContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceEnsembleContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceWorkflowContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoicePatientContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceLocationContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceMeansContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceNumericMetricState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceStringMetricState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceEnumStringMetricState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceClockState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceChannelState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceSystemContextState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceVmdState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceMdsState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceBatteryState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    is AbstractStateOneOf.ChoiceScoState -> copy(
        value = value.replaceInCopy(
            newDescriptorVersion,
            newStateVersion,
            newDescriptorHandle
        )
    )

    else -> throw InvalidWhenBranchException(this)
}

public fun AbstractDescriptorOneOf.replaceInCopy(newDescriptorVersion: Long? = null): AbstractDescriptorOneOf =
    when (this) {
        is AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceClockDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceBatteryDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceChannelDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceScoDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceSystemContextDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceVmdDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceMdsDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor -> copy(
            value = value.replaceInCopy(
                newDescriptorVersion
            )
        )

        is AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceStringMetricDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceMeansContextDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoicePatientContextDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        is AbstractDescriptorOneOf.ChoiceLocationContextDescriptor -> copy(
            value = value.replaceInCopy(newDescriptorVersion)
        )

        else -> throw InvalidWhenBranchException(this)
    }