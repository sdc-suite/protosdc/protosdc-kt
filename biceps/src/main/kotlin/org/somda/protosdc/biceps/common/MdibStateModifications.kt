package org.somda.protosdc.biceps.common

import org.somda.protosdc.biceps.common.MdibStateModifications.Alert
import org.somda.protosdc.biceps.common.MdibStateModifications.Component
import org.somda.protosdc.biceps.common.MdibStateModifications.Context
import org.somda.protosdc.biceps.common.MdibStateModifications.Metric
import org.somda.protosdc.biceps.common.MdibStateModifications.Operation
import org.somda.protosdc.biceps.common.MdibStateModifications.Waveform
import org.somda.protosdc.model.biceps.AbstractAlertStateOneOf
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.AbstractDeviceComponentStateOneOf
import org.somda.protosdc.model.biceps.AbstractMetricStateOneOf
import org.somda.protosdc.model.biceps.AbstractOperationStateOneOf
import org.somda.protosdc.model.biceps.AbstractStateOneOf
import org.somda.protosdc.model.biceps.AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState
import org.somda.protosdc.model.biceps.ActivateOperationState
import org.somda.protosdc.model.biceps.AlertConditionState
import org.somda.protosdc.model.biceps.AlertSignalState
import org.somda.protosdc.model.biceps.AlertSystemState
import org.somda.protosdc.model.biceps.BatteryState
import org.somda.protosdc.model.biceps.ChannelState
import org.somda.protosdc.model.biceps.ClockState
import org.somda.protosdc.model.biceps.DistributionSampleArrayMetricState
import org.somda.protosdc.model.biceps.EnsembleContextState
import org.somda.protosdc.model.biceps.EnumStringMetricState
import org.somda.protosdc.model.biceps.LimitAlertConditionState
import org.somda.protosdc.model.biceps.LocationContextState
import org.somda.protosdc.model.biceps.MdsState
import org.somda.protosdc.model.biceps.MeansContextState
import org.somda.protosdc.model.biceps.NumericMetricState
import org.somda.protosdc.model.biceps.OperatorContextState
import org.somda.protosdc.model.biceps.PatientContextState
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricState
import org.somda.protosdc.model.biceps.ScoState
import org.somda.protosdc.model.biceps.SetAlertStateOperationState
import org.somda.protosdc.model.biceps.SetComponentStateOperationState
import org.somda.protosdc.model.biceps.SetContextStateOperationState
import org.somda.protosdc.model.biceps.SetMetricStateOperationState
import org.somda.protosdc.model.biceps.SetStringOperationState
import org.somda.protosdc.model.biceps.SetValueOperationState
import org.somda.protosdc.model.biceps.StringMetricState
import org.somda.protosdc.model.biceps.SystemContextState
import org.somda.protosdc.model.biceps.VmdState
import org.somda.protosdc.model.biceps.WorkflowContextState

public sealed class StateModification(public val state: AbstractStateOneOf) {
    public data class Alert(
        val alertState: AbstractAlertStateOneOf,
    ) : StateModification(alertState.toAbstractStateOneOf())

    public data class Component(
        val componentState: AbstractDeviceComponentStateOneOf
    ) : StateModification(componentState.toAbstractStateOneOf())

    public data class Metric(
        val metricState: AbstractMetricStateOneOf,
    ) : StateModification(metricState.toAbstractStateOneOf())

    public data class Context(
        val contextState: AbstractContextStateOneOf
    ) : StateModification(contextState.toAbstractStateOneOf())

    public data class Operation(
        val operationState: AbstractOperationStateOneOf
    ) : StateModification(operationState.toAbstractStateOneOf())

    public data class Waveform(
        val waveformState: RealTimeSampleArrayMetricState
    ) : StateModification(ChoiceRealTimeSampleArrayMetricState(waveformState))
}

internal fun StateModification.copyFor(state: AbstractStateOneOf): StateModification {
    return when (this) {
        is StateModification.Alert -> requireNotNull(state.toAbstractAlertStateOneOf()) {
            "${this::class.java} copyFor failed as a non-supported state was found: ${state::class.java}"
        }.let { copy(alertState = it) }

        is StateModification.Component -> requireNotNull(state.toAbstractComponentStateOneOf()) {
            "${this::class.java} copyFor failed as a non-supported state was found: ${state::class.java}"
        }.let { copy(componentState = it) }

        is StateModification.Context -> requireNotNull(state.toAbstractContextStateOneOf()) {
            "${this::class.java} copyFor failed as a non-supported state was found: ${state::class.java}"
        }.let { copy(contextState = it) }

        is StateModification.Metric -> requireNotNull(state.toAbstractMetricStateOneOf()) {
            "${this::class.java} copyFor failed as a non-supported state was found: ${state::class.java}"
        }.let { copy(metricState = it) }

        is StateModification.Operation -> requireNotNull(state.toAbstractOperationStateOneOf()) {
            "${this::class.java} copyFor failed as a non-supported state was found: ${state::class.java}"
        }.let { copy(operationState = it) }

        is StateModification.Waveform -> requireNotNull(state as? ChoiceRealTimeSampleArrayMetricState) {
            "${this::class.java} copyFor failed as a non-supported state was found: ${state::class.java}"
        }.let { copy(waveformState = it.value) }
    }
}

/**
 * Container to collect state updates supposed to be applied on an MDIB.
 */
public sealed interface MdibStateModifications {
    public val states: List<StateModification>

    /**
     * Collects alert states.
     *
     * @param alertStates to be modified.
     */
    public data class Alert(val alertStates: List<StateModification.Alert>) : MdibStateModifications {
        public constructor(alertStates: AlertStates) : this(alertStates.alertStates)

        override val states: List<StateModification> = alertStates
    }

    /**
     * Collects component states.
     *
     * @param componentStates to be modified.
     */
    public data class Component(val componentStates: List<StateModification.Component>) : MdibStateModifications {
        public constructor(componentStates: ComponentStates) : this(componentStates.componentStates)

        override val states: List<StateModification> = componentStates
    }

    /**
     * Collects context states.
     *
     * @param contextStates to be modified.
     */
    public data class Context(val contextStates: List<StateModification.Context>) : MdibStateModifications {
        public constructor(contextStates: ContextStates) : this(contextStates.contextStates)

        override val states: List<StateModification> = contextStates
    }

    /**
     * Collects metric states.
     *
     * @param metricStates to be modified.
     */
    public data class Metric(val metricStates: List<StateModification.Metric>) : MdibStateModifications {
        public constructor(metricStates: MetricStates) : this(metricStates.metricStates)

        override val states: List<StateModification> = metricStates
    }

    /**
     * Collects operation states.
     *
     * @param operationStates to be modified.
     */
    public data class Operation(val operationStates: List<StateModification.Operation>) : MdibStateModifications {
        public constructor(operationStates: OperationStates) : this(operationStates.operationStates)

        override val states: List<StateModification> = operationStates
    }

    /**
     * Collects real-time sample array metric states to be used for waveform updates.
     *
     * @param waveformStates to be modified.
     */
    public data class Waveform(val waveformStates: List<StateModification.Waveform>) : MdibStateModifications {
        public constructor(waveformStates: WaveformStates) : this(waveformStates.waveformStates)

        override val states: List<StateModification> = waveformStates
    }

    public fun size(): Int = states.size
}

internal fun MdibStateModifications.copyFor(states: List<StateModification>) = when (this) {
    is Alert -> Alert(states.filterIsInstance<StateModification.Alert>())
    is Component -> Component(states.filterIsInstance<StateModification.Component>())
    is Context -> Context(states.filterIsInstance<StateModification.Context>())
    is Metric -> Metric(states.filterIsInstance<StateModification.Metric>())
    is Operation -> Operation(states.filterIsInstance<StateModification.Operation>())
    is Waveform -> Waveform(states.filterIsInstance<StateModification.Waveform>())
}

public interface States {
    public val states: List<StateModification>
}

public data class AlertStates(
    val alertStates: MutableList<StateModification.Alert> = mutableListOf()
) : States {

    override val states: List<StateModification> = alertStates
    public fun add(state: AlertConditionState): AlertStates =
        add(AbstractAlertStateOneOf.ChoiceAlertConditionState(state))

    public fun add(state: AlertSignalState): AlertStates =
        add(AbstractAlertStateOneOf.ChoiceAlertSignalState(state))

    public fun add(state: AlertSystemState): AlertStates =
        add(AbstractAlertStateOneOf.ChoiceAlertSystemState(state))

    public fun add(state: LimitAlertConditionState): AlertStates =
        add(AbstractAlertStateOneOf.ChoiceLimitAlertConditionState(state))

    public fun add(state: AbstractAlertStateOneOf): AlertStates = apply {
        alertStates.add(
            StateModification.Alert(state)
        )
    }

    public fun addAll(states: Collection<AbstractAlertStateOneOf>): AlertStates = apply {
        states.forEach {
            add(it)
        }
    }
}

public data class ComponentStates(
    val componentStates: MutableList<StateModification.Component> = mutableListOf()
) : States {

    override val states: List<StateModification> = componentStates

    public fun add(state: BatteryState): ComponentStates =
        add(AbstractDeviceComponentStateOneOf.ChoiceBatteryState(state))

    public fun add(state: ChannelState): ComponentStates =
        add(AbstractDeviceComponentStateOneOf.ChoiceChannelState(state))

    public fun add(state: ClockState): ComponentStates =
        add(AbstractDeviceComponentStateOneOf.ChoiceClockState(state))

    public fun add(state: MdsState): ComponentStates =
        add(AbstractDeviceComponentStateOneOf.ChoiceMdsState(state))

    public fun add(state: ScoState): ComponentStates =
        add(AbstractDeviceComponentStateOneOf.ChoiceScoState(state))

    public fun add(state: SystemContextState): ComponentStates =
        add(AbstractDeviceComponentStateOneOf.ChoiceSystemContextState(state))

    public fun add(state: VmdState): ComponentStates =
        add(AbstractDeviceComponentStateOneOf.ChoiceVmdState(state))

    public fun add(state: AbstractDeviceComponentStateOneOf): ComponentStates = apply {
        componentStates.add(StateModification.Component(state))
    }

    public fun addAll(states: Collection<AbstractDeviceComponentStateOneOf>): ComponentStates = apply {
        states.forEach {
            add(it)
        }
    }
}

public data class MetricStates(
    val metricStates: MutableList<StateModification.Metric> = mutableListOf()
) : States {
    override val states: List<StateModification> = metricStates

    public fun add(state: DistributionSampleArrayMetricState): MetricStates =
        add(AbstractMetricStateOneOf.ChoiceDistributionSampleArrayMetricState(state))

    public fun add(state: EnumStringMetricState): MetricStates =
        add(AbstractMetricStateOneOf.ChoiceEnumStringMetricState(state))

    public fun add(state: NumericMetricState): MetricStates =
        add(AbstractMetricStateOneOf.ChoiceNumericMetricState(state))

    public fun add(state: RealTimeSampleArrayMetricState): MetricStates =
        add(AbstractMetricStateOneOf.ChoiceRealTimeSampleArrayMetricState(state))

    public fun add(state: StringMetricState): MetricStates =
        add(AbstractMetricStateOneOf.ChoiceStringMetricState(state))

    public fun add(state: AbstractMetricStateOneOf): MetricStates =
        apply { metricStates.add(StateModification.Metric(state)) }

    public fun addAll(states: Collection<AbstractMetricStateOneOf>): MetricStates = apply {
        states.forEach {
            add(it)
        }
    }
}

public data class ContextStates(
    val contextStates: MutableList<StateModification.Context> = mutableListOf()
) : States {

    override val states: List<StateModification> = contextStates

    public fun add(state: EnsembleContextState): ContextStates =
        add(AbstractContextStateOneOf.ChoiceEnsembleContextState(state))

    public fun add(state: LocationContextState): ContextStates =
        add(AbstractContextStateOneOf.ChoiceLocationContextState(state))

    public fun add(state: MeansContextState): ContextStates =
        add(AbstractContextStateOneOf.ChoiceMeansContextState(state))

    public fun add(state: OperatorContextState): ContextStates =
        add(AbstractContextStateOneOf.ChoiceOperatorContextState(state))

    public fun add(state: PatientContextState): ContextStates =
        add(AbstractContextStateOneOf.ChoicePatientContextState(state))

    public fun add(state: WorkflowContextState): ContextStates =
        add(AbstractContextStateOneOf.ChoiceWorkflowContextState(state))

    public fun add(state: AbstractContextStateOneOf): ContextStates =
        apply { contextStates.add(StateModification.Context(state)) }.let { this }

    public fun addAll(states: Collection<AbstractContextStateOneOf>): ContextStates = apply {
        states.forEach {
            add(it)
        }
    }
}

public data class OperationStates(
    val operationStates: MutableList<StateModification.Operation> = mutableListOf()
) : States {

    override val states: List<StateModification> = operationStates

    public fun add(state: ActivateOperationState): OperationStates =
        add(AbstractOperationStateOneOf.ChoiceActivateOperationState(state))

    public fun add(state: SetAlertStateOperationState): OperationStates =
        add(AbstractOperationStateOneOf.ChoiceSetAlertStateOperationState(state))

    public fun add(state: SetComponentStateOperationState): OperationStates =
        add(AbstractOperationStateOneOf.ChoiceSetComponentStateOperationState(state))

    public fun add(state: SetContextStateOperationState): OperationStates =
        add(AbstractOperationStateOneOf.ChoiceSetContextStateOperationState(state))

    public fun add(state: SetMetricStateOperationState): OperationStates =
        add(AbstractOperationStateOneOf.ChoiceSetMetricStateOperationState(state))

    public fun add(state: SetStringOperationState): OperationStates =
        add(AbstractOperationStateOneOf.ChoiceSetStringOperationState(state))

    public fun add(state: SetValueOperationState): OperationStates =
        add(AbstractOperationStateOneOf.ChoiceSetValueOperationState(state))

    public fun add(state: AbstractOperationStateOneOf): OperationStates = apply {
        operationStates.add(
            StateModification.Operation(state)
        )
    }.let { this }

    public fun addAll(states: Collection<AbstractOperationStateOneOf>): OperationStates = apply {
        states.forEach {
            add(it)
        }
    }
}

public data class WaveformStates(
    val waveformStates: MutableList<StateModification.Waveform> = mutableListOf()
) : States {

    override val states: List<StateModification> = waveformStates

    public fun add(state: RealTimeSampleArrayMetricState): Boolean =
        waveformStates.add(StateModification.Waveform(state))

    public fun addAll(states: Collection<RealTimeSampleArrayMetricState>): WaveformStates = apply {
        states.forEach {
            add(it)
        }
    }
}