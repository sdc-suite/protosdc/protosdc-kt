package org.somda.protosdc.biceps.provider.preprocessing

import org.somda.protosdc.biceps.common.*
import org.somda.protosdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.protosdc.biceps.common.storage.MdibStorage
import org.somda.protosdc.biceps.common.storage.StatePreprocessingSegment
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf
import org.somda.protosdc.model.biceps.AbstractStateOneOf
import org.somda.protosdc.model.biceps.ContextAssociation
import javax.inject.Inject

/**
 * Preprocessing segment that manages BICEPS versioning.
 *
 * The segment takes care of incrementing descriptor and state versions.
 *
 * If an exception ensues during processing, the MDIB storage is very likely to be inconsistent, i.e. there is no
 * transactional behavior that triggers a rollback in case of an error.
 */
internal class VersionHandler @Inject constructor() : DescriptionPreprocessingSegment, StatePreprocessingSegment {
    // temporary buffer per description write to remember already updated parents which
    // prevents incrementing versions more than once per write request
    private val updatedParents = mutableSetOf<String>()
    private val modifiedDescriptorHandles = mutableSetOf<String>()
    private val resultingModifications = mutableListOf<MdibDescriptionModification>()

    // BICEPS does not allow to decrement version counters within an MDIB sequence, hence versions of
    // deleted entities and context states need to be memorized to continue incrementation
    // if the entities or context state is reinserted at a later time
    private val versionsOfDeletedArtifacts = mutableMapOf<String, VersionPair>()


    override fun process(
        modifications: MdibDescriptionModifications,
        storage: MdibStorage
    ): MdibDescriptionModifications {
        modifiedDescriptorHandles.also { handles ->
            handles.clear()
            handles.addAll(modifications.asList().map { it.descriptor.handleString() })
        }
        updatedParents.clear()
        resultingModifications.clear()
        modifications.asList().forEach { modification ->
            // depending on the modification type, perform version handling for insert, update or delete
            when (modification) {
                is MdibDescriptionModification.Insert -> processInsert(modification, storage)
                is MdibDescriptionModification.Update -> processUpdate(modification, storage)
                is MdibDescriptionModification.Delete -> processDelete(modification, storage)
            }
        }

        return MdibDescriptionModifications().addAll(resultingModifications)
    }

    override fun process(modifications: MdibStateModifications, storage: MdibStorage) = modifications.copyFor(
        modifications.states.map { modification ->
            // distinguish between single state and multi state (context state) updates
            val versionPair = when (val contextState = modification.state.toAbstractContextStateOneOf()) {
                // single state: get the latest state version from storage and increment
                null -> getVersionPair(storage, modification.state)
                    ?.let { it.copy(stateVersion = it.stateVersion + 1) }
                    ?: throw Exception("Mdib inconsistency, missing versions for state update")

                // context state: get the latest state version or startup with a new version if the context state didn't exist
                else -> {
                    // first try to get existing context state version
                    // fallback to descriptor version information and a reset state version number
                    val versionPair = getVersionPair(storage, contextState)
                        ?: getVersionPair(storage, modification.state)?.copy(stateVersion = -1)
                        ?: throw Exception("Multi-state descriptor was missing during multi state update")

                    versionPair.copy(stateVersion = versionPair.stateVersion + 1).let {
                        // remember state version in case a context state is deleted (association == not-associated)
                        if (contextState.contextAssociation() == ContextAssociation.EnumType.No) {
                            versionsOfDeletedArtifacts[contextState.handleString()] = it
                        }
                        it
                    }
                }
            }

            // write updated state back to modification set
            modification.copyFor(
                modification.state.replaceInCopy(
                    newDescriptorVersion = versionPair.descriptorVersion,
                    newStateVersion = versionPair.stateVersion
                )
            )
        }
    )

    private fun processDelete(
        modification: MdibDescriptionModification.Delete,
        storage: MdibStorage
    ) {
        // look for entity; exit when not existing
        val entityToDelete = storage.entity(modification.descriptor.handleString()).getOrNull()
        require(entityToDelete != null) { "Deletion of an entity requires an existing entity in the MDIB storage" }

        // remember descriptor and state versions
        versionsOfDeletedArtifacts[entityToDelete.handle] = VersionPair.fromEntity(entityToDelete)
        entityToDelete.resolveContextStates().forEach {
            versionsOfDeletedArtifacts[it.handleString()] = VersionPair.fromContext(it)
        }

        resultingModifications.add(
            MdibDescriptionModification.Delete(
                DescriptionItem(
                    entityToDelete.descriptorOneOf,
                    entityToDelete.resolveStates(),
                    entityToDelete.resolveParent()
                )
            )
        )

        //
        // in the following, take care of incrementing the parent version
        //

        val parentHandle = entityToDelete.resolveParent()
        if (entityToDelete.descriptorOneOf is AbstractDescriptorOneOf.ChoiceMdsDescriptor ||
            parentHandle?.let { modifiedDescriptorHandles.contains(it) } == true
        ) {
            // no need to update parent in case of an MDS or if the parent is going to be updated anyway
            return
        }

        require(parentHandle != null) {
            "Parent handle expected entity with handle '${modification.descriptor.handleString()}', but is missing"
        }

        val parentEntity = storage.entity(parentHandle).getOrNull()
        check(parentEntity != null) {
            "MDIB storage inconsistency: parent entity with handle '$parentHandle' is missing"
        }

        // start writing
        val update = MdibDescriptionModification.Update(
            DescriptionItem(
                descriptor = parentEntity.descriptorOneOf,
                states = parentEntity.resolveStates(),
                parentHandle = parentEntity.resolveParent()
            )
        )

        processUpdate(update, storage)
    }

    private fun processUpdate(
        modification: MdibDescriptionModification.Update,
        storage: MdibStorage
    ) {
        if (isUpdatedAlready(modification.descriptor)) {
            return
        }

        if (modification.descriptor.toAbstractContextDescriptorOneOf() != null) {
            processUpdateWithMultiState(modification, storage)
        } else {
            processUpdateWithSingleState(modification, storage)
        }

        setUpdated(modification.descriptor)
    }

    private fun processUpdateWithMultiState(
        modification: MdibDescriptionModification.Update,
        storage: MdibStorage
    ) {
        val version = getVersion(storage, modification)
        require(version != null) { "Expected existing version on update, but none found" }
        val updatedDescriptor = modification.descriptor.replaceInCopy(
            newDescriptorVersion = version.number + 1
        )

        val statesFromStorage = storage.contextStates()
            .filter { it.descriptorHandleString() == modification.descriptor.handleString() }
            .map { it.handleString() to it }
            .toMap().toMutableMap()

        val replaceVersions: (AbstractContextStateOneOf) -> AbstractContextStateOneOf = { contextState ->
            contextState.replaceInCopy(
                newDescriptorVersion = updatedDescriptor.descriptorVersion(),
                newStateVersion = (getVersion(storage, contextState) ?: Version()).number + 1
            )
        }

        // increment versions for all states from change set
        val updatedStates = modification.states.map { state ->
            state.toAbstractContextStateOneOf()
                ?.let {
                    statesFromStorage.remove(it.handleString())
                    replaceVersions(it).toAbstractStateOneOf()
                }
                ?: throw Exception("Expected multi state, but single state found")
        }.toMutableList()

        // increment versions from states in MDIB storage that are not in change set
        updatedStates.addAll(statesFromStorage.map {
            replaceVersions(it.value).toAbstractStateOneOf()
        })

        resultingModifications.add(
            MdibDescriptionModification.Update(
                DescriptionItem(
                    updatedDescriptor,
                    updatedStates,
                    storage.entity(modification.descriptor.handleString()).getOrNull()?.resolveParent()
                )
            )
        )
    }

    private fun processUpdateWithSingleState(
        modification: MdibDescriptionModification.Update,
        storage: MdibStorage
    ) {
        val versionPair = getVersionPair(storage, modification)
        check(versionPair != null) { "Expected existing version on update, but none found" }

        val updatedDescriptor = modification.descriptor.replaceInCopy(
            newDescriptorVersion = versionPair.descriptorVersion + 1
        )

        val stateToUpdate = when (modification.states.isEmpty()) {
            true -> storage.entity(modification.descriptor.handleString()).getOrNull()?.resolveStates()?.firstOrNull()
                ?: throw Exception("Expected existing state to be updated, but none found")

            false -> modification.states.first()
        }
        val updatedState = stateToUpdate.replaceInCopy(
            newDescriptorVersion = updatedDescriptor.descriptorVersion(),
            newStateVersion = versionPair.stateVersion + 1
        )

        resultingModifications.add(
            MdibDescriptionModification.Update(
                DescriptionItem(
                    updatedDescriptor,
                    listOf(updatedState),
                    storage.entity(modification.descriptor.handleString()).getOrNull()?.resolveParent()
                )
            )
        )
    }

    private fun processInsert(
        modification: MdibDescriptionModification.Insert,
        storage: MdibStorage
    ) {
        val updatedInsertion = if (modification.descriptor.toAbstractContextDescriptorOneOf() != null) {
            processInsertWithMultiState(modification, storage)
        } else {
            processInsertWithSingleState(modification, storage)
        }

        setUpdated(modification.descriptor)
        modification.parentHandle?.let { parentHandle ->
            storage.entity(parentHandle).getOrNull()?.let { entity ->
                processUpdate(
                    MdibDescriptionModification.Update(DescriptionItem(entity.descriptorOneOf, entity.resolveStates())),
                    storage
                )
            } ?: check(updatedParents.contains(parentHandle)) { "Missing parent to be inserted before child" }
        }

        resultingModifications.add(updatedInsertion)
    }

    private fun processInsertWithSingleState(
        modification: MdibDescriptionModification.Insert,
        storage: MdibStorage
    ): MdibDescriptionModification {
        require(modification.states.isNotEmpty()) { "State is missing to complete the insert operation" }

        val state = modification.states.first()
        val versionPair = getVersionPair(storage, modification) ?: VersionPair()
        val updatedDescriptor = modification.descriptor.replaceInCopy(
            newDescriptorVersion = versionPair.descriptorVersion + 1
        )
        val updatedState = state.replaceInCopy(
            newDescriptorVersion = updatedDescriptor.descriptorVersion(),
            newStateVersion = versionPair.stateVersion + 1
        )

        return modification.copy(
            descriptor = updatedDescriptor,
            states = listOf(updatedState)
        )
    }

    private fun processInsertWithMultiState(
        modification: MdibDescriptionModification.Insert,
        storage: MdibStorage
    ): MdibDescriptionModification {
        val updatedDescriptor = modification.descriptor.replaceInCopy(
            newDescriptorVersion = (getVersion(storage, modification) ?: Version()).number + 1
        )

        val updatedStates = modification.states.map { state ->
            state.toAbstractContextStateOneOf()?.let {
                return@map it.replaceInCopy(
                    newDescriptorVersion = updatedDescriptor.descriptorVersion(),
                    newStateVersion = (getVersion(storage, it) ?: Version()).number + 1
                ).toAbstractStateOneOf()
            } ?: throw Exception("Expected multi-state, but single state found")
        }

        return modification.copy(
            descriptor = updatedDescriptor,
            states = updatedStates
        )
    }

    private fun getVersionPair(storage: MdibStorage, modification: MdibDescriptionModification) =
        storage.entity(modification.descriptor.handleString()).getOrNull()?.let { VersionPair.fromEntity(it) }
            ?: versionsOfDeletedArtifacts[modification.descriptor.handleString()]

    private fun getVersionPair(storage: MdibStorage, modification: AbstractStateOneOf) =
        storage.entity(modification.descriptorHandleString()).getOrNull()?.let { VersionPair.fromEntity(it) }
            ?: versionsOfDeletedArtifacts[modification.descriptorHandleString()]

    private fun getVersionPair(storage: MdibStorage, modification: AbstractContextStateOneOf) =
        storage.contextState(modification.handleString()).getOrNull()?.let { VersionPair.fromContext(it) }
            ?: versionsOfDeletedArtifacts[modification.handleString()]

    private fun getVersion(storage: MdibStorage, modification: MdibDescriptionModification) =
        storage.entity(modification.descriptor.handleString()).getOrNull()?.let { Version.fromEntity(it) }
            ?: versionsOfDeletedArtifacts[modification.descriptor.handleString()]?.let { Version(it.descriptorVersion) }

    private fun getVersion(storage: MdibStorage, state: AbstractContextStateOneOf) =
        storage.contextState(state.handleString()).getOrNull()?.let { Version.fromContext(it) }
            ?: versionsOfDeletedArtifacts[state.handleString()]?.let { Version(it.stateVersion) }

    private fun isUpdatedAlready(descriptor: AbstractDescriptorOneOf) =
        updatedParents.contains(descriptor.handleString())

    private fun setUpdated(descriptor: AbstractDescriptorOneOf) {
        updatedParents.add(descriptor.handleString())
    }

    override fun toString(): String {
        return this.javaClass.simpleName
    }

    private data class VersionPair(val descriptorVersion: Long = -1, val stateVersion: Long = -1) {
        companion object {
            fun fromEntity(entity: MdibEntity) = VersionPair(
                descriptorVersion = entity.descriptorOneOf.descriptorVersion(),
                stateVersion = entity.resolveSingleState()?.stateVersion() ?: -1
            )

            fun fromContext(context: AbstractContextStateOneOf) = VersionPair(
                descriptorVersion = context.descriptorVersion(),
                stateVersion = context.stateVersion()
            )
        }
    }

    private data class Version(val number: Long = -1) {
        companion object {
            fun fromEntity(entity: MdibEntity) = Version(entity.descriptorOneOf.descriptorVersion())

            fun fromContext(context: AbstractContextStateOneOf) = Version(context.stateVersion())
        }
    }
}