package org.somda.protosdc.biceps.consumer.access

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.access.*
import org.somda.protosdc.biceps.common.helper.EventDistributor
import org.somda.protosdc.biceps.common.helper.WriteUtil
import org.somda.protosdc.biceps.common.preprocessing.DescriptorChildRemover
import org.somda.protosdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.protosdc.biceps.common.storage.MdibStorageImpl
import org.somda.protosdc.biceps.common.storage.MdibStoragePreprocessingChainImpl
import org.somda.protosdc.biceps.common.storage.StatePreprocessingSegment
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.Handle
import kotlin.reflect.KClass

internal class RemoteMdibAccessImpl constructor(
    mdibVersion: MdibVersion,
    descriptionPreprocessingSegments: List<DescriptionPreprocessingSegment>,
    statePreprocessingSegments: List<StatePreprocessingSegment>,
    mdibStorageFactory: MdibStorageImpl.Factory,
    preProcessingChainFactory: MdibStoragePreprocessingChainImpl.Factory,
    instanceId: InstanceId
) : RemoteMdibAccess {

    @AssistedInject
    constructor(
        @Assisted mdibVersion: MdibVersion,
        mdibStorageFactory: MdibStorageImpl.Factory,
        preProcessingChainFactory: MdibStoragePreprocessingChainImpl.Factory,
        descriptorChildRemover: DescriptorChildRemover,
        instanceId: InstanceId
    ) : this(
        mdibVersion,
        listOf(descriptorChildRemover),
        listOf(),
        mdibStorageFactory,
        preProcessingChainFactory,
        instanceId
    )

    @AssistedFactory
    interface Factory {
        fun create(@Assisted initialMdibVersion: MdibVersion): RemoteMdibAccessImpl
    }

    companion object {
        private const val mdDescriptionVersion = 0L
        private const val mdStateVersion = 0L
    }

    private val readMutex = Mutex()

    private val publisher = ChannelPublisher<MdibAccessEvent>("$instanceId@RemoteMdib")

    private val eventDistributor = EventDistributor(publisher)

    private val mdibStorage = mdibStorageFactory.create(mdibVersion, mdDescriptionVersion, mdStateVersion)

    private val writeUtil = WriteUtil(
        instanceId,
        eventDistributor,
        preProcessingChainFactory.create(mdibStorage, descriptionPreprocessingSegments, statePreprocessingSegments),
        readMutex,
        this
    )

    override suspend fun writeDescription(
        descriptionModifications: MdibDescriptionModifications,
        mdibVersion: Long
    ): WriteDescriptionResult =
        writeUtil.writeDescription(descriptionModifications) {
            mdibStorage.apply(mdibVersion, mdDescriptionVersion, mdStateVersion, it)
        }

    override suspend fun writeStates(
        stateModifications: MdibStateModifications,
        mdibVersion: Long
    ): WriteStateResult =
        writeUtil.writeStates(stateModifications) {
            mdibStorage.apply(mdibVersion, mdStateVersion, it)
        }

    override suspend fun <T> withReadLock(action: suspend (MdibAccess) -> T): T = readMutex.withLock {
        action(AsyncStorageFacade(mdibStorage))
    }

    override suspend fun subscribe() = publisher.subscribe()

    override suspend fun unsubscribeAll() = publisher.unsubscribeAll()

    override suspend fun mdibVersion() = withReadLock { it.mdibVersion() }

    override suspend fun mdDescriptionVersion() = withReadLock { it.mdDescriptionVersion() }

    override suspend fun mdStateVersion() = withReadLock { it.mdStateVersion() }

    override suspend fun anyEntity(handle: String) = withReadLock { it.anyEntity(handle) }

    override suspend fun <T : MdibEntity> entity(handle: String, type: KClass<out T>) = withReadLock {
        it.entity(handle, type)
    }

    override suspend fun rootEntities() = withReadLock { it.rootEntities() }

    override suspend fun abstractContextState(handle: String) = withReadLock { it.abstractContextState(handle) }

    override suspend fun <T : AbstractContextStateOneOf> contextState(handle: String, type: KClass<out T>) =
        withReadLock {
            it.contextState(handle, type)
        }

    override suspend fun contextStates() = withReadLock { it.contextStates() }

    override suspend fun <T : MdibEntity> entitiesByType(type: KClass<out T>) = withReadLock { it.entitiesByType(type) }

    override suspend fun <T : MdibEntity> childrenByType(handle: String, type: KClass<out T>) = withReadLock {
        it.childrenByType(handle, type)
    }

    override suspend fun abstractContextState(handle: Handle) = abstractContextState(handle.string)

    override suspend fun anyEntity(handle: Handle) = anyEntity(handle.string)

    override suspend fun <T : MdibEntity> entity(handle: Handle, type: KClass<out T>) = entity(handle.string, type)

    override suspend fun <T : AbstractContextStateOneOf> contextState(handle: Handle, type: KClass<out T>) =
        contextState(handle.string, type)

    override suspend fun <T : MdibEntity> childrenByType(handle: Handle, type: KClass<out T>) =
        childrenByType(handle.string, type)
}