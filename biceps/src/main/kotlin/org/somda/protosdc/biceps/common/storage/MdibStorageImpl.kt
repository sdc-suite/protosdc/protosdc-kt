package org.somda.protosdc.biceps.common.storage


import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.somda.protosdc.biceps.BicepsConfig
import org.somda.protosdc.biceps.common.*
import org.somda.protosdc.biceps.common.MdibStateModifications.Alert
import org.somda.protosdc.biceps.common.MdibStateModifications.Component
import org.somda.protosdc.biceps.common.MdibStateModifications.Context
import org.somda.protosdc.biceps.common.MdibStateModifications.Metric
import org.somda.protosdc.biceps.common.MdibStateModifications.Operation
import org.somda.protosdc.biceps.common.MdibStateModifications.Waveform
import org.somda.protosdc.biceps.common.access.WriteDescriptionResult
import org.somda.protosdc.biceps.common.access.WriteStateResult
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.model.biceps.*
import kotlin.reflect.KClass
import kotlin.reflect.cast
import kotlin.reflect.full.isSubclassOf

/**
 * Default implementation of [MdibStorage].
 */
internal open class MdibStorageImpl @AssistedInject internal constructor(
    @Assisted initialMdibVersion: MdibVersion,
    @Assisted("initialMdDescriptionVersion") initialMdDescriptionVersion: Long,
    @Assisted("initialMdStateVersion") initialMdStateVersion: Long,
    private val mdibEntityFactory: MdibEntityFactory,
    instanceId: InstanceId,
    bicepsConfig: BicepsConfig
) : MdibStorage {
    private val storeNotAssociatedContextStates = bicepsConfig.storeNotAssociatedContextStates

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted initialMdibVersion: MdibVersion = MdibVersion.create(),
            @Assisted("initialMdDescriptionVersion") initialMdDescriptionVersion: Long = 0L,
            @Assisted("initialMdStateVersion") initialMdStateVersion: Long = 0L,
        ): MdibStorageImpl
    }

    private val instanceLogger by InstanceLogger(instanceId())

    /**
     * MDIB version tracked by [MdibStorage.apply] calls.
     */
    var mdibVersion = initialMdibVersion
        private set

    /**
     * Description version tracked by [MdibStorage.apply] calls.
     */
    var mdDescriptionVersion = initialMdDescriptionVersion
        private set

    /**
     * State version tracked by [MdibStorage.apply] calls.
     */
    var mdStateVersion = initialMdStateVersion
        private set

    private val entities = mutableMapOf<String, MdibEntity>()

    private var rootEntities = mutableListOf<MdibEntity.Mds>()

    private val contextStates = mutableMapOf<String, AbstractContextStateOneOf>()

    override fun mdibVersion() = mdibVersion

    override fun mdDescriptionVersion() = mdDescriptionVersion

    override fun mdStateVersion() = mdStateVersion

    override fun entity(handle: String): Result<MdibEntity> = entities[handle]
        ?.let { Result.success(it) }
        ?: Result.failure(Exception("Entity {$handle} not found in MDIB storage"))

    override fun <T : MdibEntity> entity(handle: String, type: KClass<out T>): Result<T> =
        entity(handle).let { result ->
            result.onFailure {
                return@let Result.failure(it)
            }
            result.getOrThrow().let {
                when (it::class.isSubclassOf(type)) {
                    true -> Result.success(type.cast(it))
                    else -> Result.failure(Exception("Found entity type (${it::class} differs from requested type ($type)"))
                }
            }
        }

    override fun rootEntities() = rootEntities.toList()

    override fun contextState(handle: String) = contextStates[handle]
        ?.let { Result.success(it) }
        ?: Result.failure(Exception("Context state with handle $handle not found in MDIB storage"))

    override fun <T : AbstractContextStateOneOf> contextState(handle: String, type: KClass<out T>): Result<T> =
        contextState(handle).let { result ->
            result.onFailure {
                return@let Result.failure(it)
            }
            result.getOrThrow().let {
                when (it::class.isSubclassOf(type)) {
                    true -> Result.success(type.cast(it))
                    else -> Result.failure(Exception("Found context state type (${it::class} differs from requested type ($type)"))
                }
            }
        }

    override fun contextStates(): Collection<AbstractContextStateOneOf> = contextStates.map { it.value }

    override fun <T : MdibEntity> entitiesByType(type: KClass<out T>): Collection<T> =
        entities.filter { it.value::class.isSubclassOf(type) }.map { type.cast(it.value) }.toList()

    override fun <T : MdibEntity> childrenByType(handle: String, type: KClass<out T>): List<T> =
        entities[handle]?.let {
            return@let it.resolveChildren()
                .mapNotNull { childHandle -> entities[childHandle] }
                .filter { childEntity -> childEntity::class.isSubclassOf(type) }
                .map { childEntity -> type.cast(childEntity) }
        } ?: listOf()

    @Throws(IllegalArgumentException::class)
    override fun apply(
        mdibVersion: Long,
        mdDescriptionVersion: Long?,
        mdStateVersion: Long?,
        descriptionModifications: MdibDescriptionModifications
    ): WriteDescriptionResult {
        this.mdibVersion = this.mdibVersion.copy(version = mdibVersion)
        mdDescriptionVersion?.let { this.mdDescriptionVersion = it }
        mdStateVersion?.let { this.mdStateVersion = it }

        val insertedEntities = mutableListOf<MdibEntity>()
        val updatedEntities = mutableListOf<MdibEntity>()
        val deletedEntities = mutableListOf<MdibEntity>()
        val updatedParentEntitiesDueToInsertOrDelete = mutableListOf<String>()

        for (modification in descriptionModifications.asList()) {
            val sanitizedStates = removeNotAssociatedContextStates(modification.states)
            when (modification) {
                is MdibDescriptionModification.Insert -> insertEntity(
                    modification,
                    sanitizedStates,
                    insertedEntities,
                    updatedParentEntitiesDueToInsertOrDelete
                )

                is MdibDescriptionModification.Update -> updateEntity(
                    modification,
                    sanitizedStates,
                    updatedEntities
                )

                is MdibDescriptionModification.Delete -> deleteEntity(
                    modification,
                    deletedEntities,
                    updatedParentEntitiesDueToInsertOrDelete
                )
            }
        }

        // Add updated parent entities to updatedEntities in case they are not part of any existing result lists
        for (handle in updatedParentEntitiesDueToInsertOrDelete) {
            if (updatedEntities.none { handle == it.handle }
                && insertedEntities.none { handle == it.handle }
                && deletedEntities.none { handle == it.handle }) {
                val entity = entities[handle]
                require(entity != null) { "Expect $handle to be existing in the entities but it is not" }
                updatedEntities.add(entity)
            }
        }

        return WriteDescriptionResult(
            this.mdibVersion, WrittenMdibDescriptionModifications(
                insertedEntities, updatedEntities, deletedEntities
            )
        )
    }

    private fun removeNotAssociatedContextStates(states: List<AbstractStateOneOf>): List<AbstractStateOneOf> {
        val result = states.toMutableList() // copy as the input might be immutable
        if (storeNotAssociatedContextStates) {
            // also return a copy in this case to have the same behavior afterwards
            return result
        }
        result.removeIf { resolveContextAssociationType(it) == ContextAssociation.EnumType.No }
        return result
    }

    private fun deleteEntity(
        modification: MdibDescriptionModification,
        deletedEntities: MutableList<MdibEntity>,
        updatedParentEntities: MutableList<String>
    ) {
        val handle = modification.descriptor.handleString()
        val entityToDelete = entities[handle]
        require(entityToDelete != null) { "Inconsistency detected. Entity to delete was not found: $handle" }

        instanceLogger.debug { "Delete entity with handle: $handle" }

        entityToDelete.resolveParent()?.also { parentHandle ->
            entities[parentHandle]?.also { parentEntity ->
                val updatedParentEntity = mdibEntityFactory.replaceChildren(
                    parentEntity,
                    parentEntity.resolveChildren().filter { it != entityToDelete.handle })
                entities[parentEntity.handle] = updatedParentEntity
                updatedParentEntities.add(updatedParentEntity.handle)
            }
        }

        rootEntities.removeIf { it.handle == handle }
        entities.remove(handle)
        contextStates.entries.removeIf { it.value.descriptorHandleString() == handle }
        deletedEntities.add(entityToDelete)
    }

    private fun updateEntity(
        modification: MdibDescriptionModification,
        sanitizedStates: List<AbstractStateOneOf>,
        updatedEntities: MutableList<MdibEntity>
    ) {
        val handle = modification.descriptor.handleString()
        val entityToUpdate = entities[handle]
        require(entityToUpdate != null) { "Inconsistency detected. Entity to update was not found: $handle" }

        instanceLogger.debug { "Update entity: ${entityToUpdate.handle}" }
        instanceLogger.trace { "Update entity: $entityToUpdate" }
        addOrReplace(
            mdibEntityFactory.replaceDescriptorAndStates(
                entityToUpdate,
                modification.descriptor,
                sanitizedStates
            )
        )
        updatedEntities.add(
            mdibEntityFactory.replaceDescriptorAndStates(
                entityToUpdate,
                modification.descriptor,
                modification.states
            )
        )
        updateContextStatesMap(modification.states)

    }

    private fun updateContextStatesMap(states: List<AbstractStateOneOf>) {
        for (state in states) {
            state.toAbstractContextStateOneOf()?.let {
                if (!storeNotAssociatedContextStates && it.contextAssociation() == ContextAssociation.EnumType.No) {
                    contextStates.remove(it.handleString())
                } else {
                    contextStates[it.handleString()] = it
                }
            }
        }
    }

    private fun insertEntity(
        modification: MdibDescriptionModification,
        sanitizedStates: List<AbstractStateOneOf>,
        insertedEntities: MutableList<MdibEntity>,
        updatedEntityHandles: MutableList<String>
    ) {
        val parentEntity = entities[modification.parentHandle]
        if (parentEntity == null) {
            require(modification.descriptor is AbstractDescriptorOneOf.ChoiceMdsDescriptor) {
                "Tried to insert a non-MDS entity for which there is no parent in the storage: ${modification.handle}"
            }
        }

        val sourceMds = parentEntity?.sourceMds ?: modification.handle

        val mdibEntityForStorage = mdibEntityFactory.create(
            modification.descriptor,
            sanitizedStates,
            mdibVersion,
            modification.parentHandle,
            sourceMds
        )
        val mdibEntityForResultSet = mdibEntityFactory.create(
            modification.descriptor,
            modification.states,
            mdibVersion,
            modification.parentHandle,
            sourceMds
        )

        // Either add entity as child of a parent or expect it to be a root entity
        when (modification.parentHandle) {
            null -> {
                require(mdibEntityForStorage is MdibEntity.Mds) {
                    "An inserted entity without parent needs to be an " +
                            "MDS, but is ${mdibEntityForStorage::class}"
                }
                addOrReplace(mdibEntityForStorage)
            }

            else -> {
                require(parentEntity != null) {
                    "A parent handle is required for non-root MDIB entities, but parent with handle " +
                            "'${modification.parentHandle}' was not found for '${modification.descriptor.handleString()}'"
                }
                val children = parentEntity.resolveChildren().toMutableList()
                children.add(mdibEntityForStorage.handle)
                addOrReplace(mdibEntityFactory.replaceChildren(parentEntity, children))
                updatedEntityHandles.add(parentEntity.handle)
            }
        }

        // Add to entities list
        entities[mdibEntityForStorage.handle] = mdibEntityForStorage
        instanceLogger.debug { "Insert entity with handle: ${mdibEntityForStorage.handle}" }
        instanceLogger.trace { "Insert entity: $mdibEntityForStorage" }

        // Add to context states if context entity
        sanitizedStates.forEach {
            it.toAbstractContextStateOneOf()?.let { contextState ->
                contextStates[contextState.handleString()] = contextState
            }
        }

        insertedEntities.add(mdibEntityForResultSet)
    }

    @Throws(IllegalArgumentException::class)
    override fun apply(
        mdibVersion: Long,
        mdStateVersion: Long?,
        stateModifications: MdibStateModifications
    ): WriteStateResult {
        this.mdibVersion = this.mdibVersion.copy(version = mdibVersion)
        mdStateVersion?.let { this.mdStateVersion = it }


        val modifiedStates = when (stateModifications) {
            is Alert -> mutableListOf<WrittenStateModification.Alert>().let {
                applyTyped(stateModifications.states, it)
                WrittenMdibStateModifications.Alert(it)
            }

            is Component -> mutableListOf<WrittenStateModification.Component>().let {
                applyTyped(stateModifications.states, it)
                WrittenMdibStateModifications.Component(it)
            }

            is Context -> mutableListOf<WrittenStateModification.Context>().let {
                applyTyped(stateModifications.states, it)
                WrittenMdibStateModifications.Context(it)
            }

            is Metric -> mutableListOf<WrittenStateModification.Metric>().let {
                applyTyped(stateModifications.states, it)
                WrittenMdibStateModifications.Metric(it)
            }

            is Operation -> mutableListOf<WrittenStateModification.Operation>().let {
                applyTyped(stateModifications.states, it)
                WrittenMdibStateModifications.Operation(it)
            }

            is Waveform -> mutableListOf<WrittenStateModification.Waveform>().let {
                applyTyped(stateModifications.states, it)
                WrittenMdibStateModifications.Waveform(it)
            }
        }

        return WriteStateResult(this.mdibVersion, modifiedStates)
    }

    private inline fun <reified V : WrittenStateModification> applyTyped(
        stateModificationsInput: List<StateModification>,
        stateModificationsOutput: MutableList<V>
    ) {
        for (modification in stateModificationsInput) {
            instanceLogger.debug { "Update state with descriptor handle: ${modification.state.descriptorHandleString()}" }
            instanceLogger.debug { "Update state: $modification" }

            val modificationAsContextState = modification.state.toAbstractContextStateOneOf()
            val mdibEntity = entities[modification.state.descriptorHandleString()]
            require(mdibEntity != null) {
                "No entity for handle '${modification.state.descriptorHandleString()}' found, but required to update state"
            }

            if (modificationAsContextState == null) {
                addOrReplace(mdibEntityFactory.replaceStates(mdibEntity, listOf(modification.state)))
            } else {
                val newStates = mutableListOf<AbstractStateOneOf>()
                val multiStateHandle = modificationAsContextState.handleString()
                val contextStatesFromEntity = mdibEntity.resolveStates()
                newStates.addAll(contextStatesFromEntity.filter { s ->
                    s.toAbstractContextStateOneOf()?.let { cs -> cs.handleString() != multiStateHandle } ?: false
                })

                val doStore = storeNotAssociatedContextStates
                        || modificationAsContextState.contextAssociation() != ContextAssociation.EnumType.No
                if (doStore) {
                    instanceLogger.debug {
                        val nonExisting = contextStatesFromEntity.none { s ->
                            s.toAbstractContextStateOneOf()?.let { cs -> cs.handleString() == multiStateHandle } == true
                        }
                        if (nonExisting) {
                            "Adding new multi-state $multiStateHandle"
                        } else {
                            "Replacing already present multi-state: $multiStateHandle"
                        }
                    }
                    newStates.add(modification.state)
                } else {
                    instanceLogger.debug {
                        "Found context state $multiStateHandle with association=not-associated; refuse storage in MDIB"
                    }
                }

                addOrReplace(mdibEntityFactory.replaceStates(mdibEntity, newStates))
                mdibEntity.resolveContextStates().forEach { contextStates.remove(it.handleString()) }
                newStates.forEach { s ->
                    s.toAbstractContextStateOneOf()?.let { cs ->
                        contextStates[cs.handleString()] = cs
                    }
                }
            }

            stateModificationsOutput.add(
                when (modification) {
                    is StateModification.Alert -> WrittenStateModification.Alert(
                        modification.alertState,
                        mdibEntity.sourceMds
                    ) as V

                    is StateModification.Component -> WrittenStateModification.Component(
                        modification.componentState,
                        mdibEntity.sourceMds
                    ) as V

                    is StateModification.Context -> WrittenStateModification.Context(
                        modification.contextState,
                        mdibEntity.sourceMds
                    ) as V

                    is StateModification.Metric -> WrittenStateModification.Metric(
                        modification.metricState,
                        mdibEntity.sourceMds
                    ) as V

                    is StateModification.Operation -> WrittenStateModification.Operation(
                        modification.operationState,
                        mdibEntity.sourceMds
                    ) as V

                    is StateModification.Waveform -> WrittenStateModification.Waveform(
                        modification.waveformState,
                        mdibEntity.sourceMds
                    ) as V
                }
            )
        }
    }

    private fun addOrReplace(entity: MdibEntity) {
        entities[entity.handle] = entity
        if (entity is MdibEntity.Mds) {
            rootEntities
                .mapIndexed { index, mds -> Pair(index, mds) }
                .firstOrNull { it.second.handle == entity.handle }
                ?.also { rootEntities[it.first] = entity }
                ?: rootEntities.add(entity)
        }
    }
}