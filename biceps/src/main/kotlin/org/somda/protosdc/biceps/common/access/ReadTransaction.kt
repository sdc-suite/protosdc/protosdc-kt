package org.somda.protosdc.biceps.common.access

/**
 * Defines an interface to declare a class capable of executing read transactions.
 */
public interface ReadTransaction {
    /**
     * Starts a read transaction.
     *
     * @param action code to be performed with locked MDIB access (read-only).
     *               While the action hasn't finished execution, it is not possible to initiate any other write or read
     *               operation. Hence, make sure to keep the read section as short as possible to reduce lock times.
     *
     *               Attention: make sure to not use any MDIB access reference other than the one that is passed to
     *               *action* as otherwise a function call to this MDIB access reference can cause a deadlock.
     */
    public suspend fun <T> withReadLock(action: suspend (MdibAccess) -> T): T
}