package org.somda.protosdc.biceps.provider.preprocessing

import org.somda.protosdc.biceps.common.MdibDescriptionModification
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.protosdc.biceps.common.storage.MdibStorage
import javax.inject.Inject

/**
 * Preprocessing segment that checks for handle duplicates on inserted entities during description modifications.
 */
internal class DuplicateChecker @Inject internal constructor() : DescriptionPreprocessingSegment {
    override fun process(
        modifications: MdibDescriptionModifications,
        storage: MdibStorage
    ) = modifications.also {
        modifications.asList()
            .filterIsInstance<MdibDescriptionModification.Insert>()
            .firstOrNull { storage.entity(it.descriptor.handleString()).isSuccess }
            ?.let {
                throw IllegalArgumentException(
                    "Inserted handle is a duplicate: ${it.descriptor.handleString()}"
                )
            }
    }

    override fun toString(): String = javaClass.simpleName
}