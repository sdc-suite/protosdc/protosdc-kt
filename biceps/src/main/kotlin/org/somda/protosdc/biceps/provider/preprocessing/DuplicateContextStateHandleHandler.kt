package org.somda.protosdc.biceps.provider.preprocessing

import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.descriptorHandleString
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.biceps.common.storage.MdibStorage
import org.somda.protosdc.biceps.common.storage.StatePreprocessingSegment
import org.somda.protosdc.biceps.common.toAbstractContextStateOneOf
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import javax.inject.Inject

/**
 * Preprocessing segment that throws an exception if two context state have the same handle but different
 * descriptor handles. This can happen, e.g. if a context state handle is used multiple times or if a context
 * state was assigned to a new descriptor.
 */
internal class DuplicateContextStateHandleHandler @Inject constructor() : StatePreprocessingSegment {
    private var allContextStates = mutableMapOf<String, AbstractContextStateOneOf>()

    override fun process(modifications: MdibStateModifications, storage: MdibStorage): MdibStateModifications {
        allContextStates = storage.contextStates().map { it.handleString() to it }.toMap().toMutableMap()
        modifications.states.forEach { modification ->
            modification.state.toAbstractContextStateOneOf()?.let { contextState ->
                allContextStates[contextState.handleString()]?.let {
                    require(it.descriptorHandleString() == contextState.descriptorHandleString()) {
                        "Two different descriptors: ${it.descriptorHandleString()} and ${contextState.descriptorHandleString()} " +
                                "cannot have the same context state: ${contextState.handleString()}"
                    }
                }
                allContextStates[contextState.handleString()] = contextState
            }
        }
        return modifications
    }

    override fun toString(): String {
        return this.javaClass.simpleName
    }
}