package org.somda.protosdc.biceps.common.access

import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.storage.MdibStorage
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.Handle
import kotlin.reflect.KClass

/**
 * Facade that makes [MdibStorage] a suspendable [MdibAccess] version.
 */
internal class AsyncStorageFacade(private val mdibStorage: MdibStorage) : MdibAccess {
    override suspend fun mdibVersion() = mdibStorage.mdibVersion()

    override suspend fun mdDescriptionVersion() = mdibStorage.mdDescriptionVersion()

    override suspend fun mdStateVersion() = mdibStorage.mdStateVersion()

    override suspend fun anyEntity(handle: String) = mdibStorage.entity(handle)

    override suspend fun <T : MdibEntity> entity(handle: String, type: KClass<out T>) = mdibStorage.entity(handle, type)

    override suspend fun rootEntities() = mdibStorage.rootEntities()

    override suspend fun abstractContextState(handle: String) = mdibStorage.contextState(handle)

    override suspend fun <T : AbstractContextStateOneOf> contextState(handle: String, type: KClass<out T>) =
        mdibStorage.contextState(handle, type)

    override suspend fun contextStates() = mdibStorage.contextStates()

    override suspend fun <T : MdibEntity> entitiesByType(type: KClass<out T>) = mdibStorage.entitiesByType(type)

    override suspend fun <T : MdibEntity> childrenByType(handle: String, type: KClass<out T>) =
        mdibStorage.childrenByType(handle, type)

    override suspend fun abstractContextState(handle: Handle) = abstractContextState(handle.string)

    override suspend fun anyEntity(handle: Handle) = anyEntity(handle.string)

    override suspend fun <T : MdibEntity> entity(handle: Handle, type: KClass<out T>) = entity(handle.string, type)

    override suspend fun <T : AbstractContextStateOneOf> contextState(handle: Handle, type: KClass<out T>) =
        contextState(handle.string, type)

    override suspend fun <T : MdibEntity> childrenByType(handle: Handle, type: KClass<out T>) =
        childrenByType(handle.string, type)
}