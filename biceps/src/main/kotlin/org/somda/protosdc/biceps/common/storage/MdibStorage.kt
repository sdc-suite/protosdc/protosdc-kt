package org.somda.protosdc.biceps.common.storage

import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.access.WriteDescriptionResult
import org.somda.protosdc.biceps.common.access.WriteStateResult
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import kotlin.reflect.KClass

/**
 * Map-based access to [MdibEntity] instances.
 *
 * The interface providers read and write access.
 * Read access is realized by a variety of query functions.
 * Write access is realized by two functions named [MdibStorage.apply].
 * Apply can be used either to write description items including descriptor and states (insert, update, delete) or
 * to write state updates only.
 *
 * Note that [MdibStorage]
 *
 * - is not meant to be thread-safe
 * - does not necessarily perform MDIB consistency checks
 */
internal interface MdibStorage {
    /**
     * The latest known MDIB version.
     *
     * @return the latest known MDIB version which in case of remote access may not necessarily reflect the MDIB version
     *         of the whole MDIB (e.g. if only a subset of all available reports is subscribed). In case of local access
     *         the returned version corresponds to the latest state.
     */
    fun mdibVersion(): MdibVersion

    /**
     * The latest known medical device description version.
     *
     * @return the latest known medial device description version which in case of remote access may be outdated. In
     *         case of local access the returned version corresponds to the latest description.
     */
    fun mdDescriptionVersion(): Long

    /**
     * The latest known medical device state version.
     *
     * @return the latest known medial device state version which in case of remote access may be outdated. In
     *         case of local access the returned version corresponds to the latest state.
     */
    fun mdStateVersion(): Long

    /**
     * Finds an entity for the given handle.
     *
     * @param handle the descriptor handle to seek.
     * @return a result representing the found [MdibEntity], [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    fun entity(handle: String): Result<MdibEntity>

    /**
     * Finds an entity for the given handle of the given type.
     *
     * @param handle the descriptor handle to seek.
     * @param type type of the entity to seek.
     * @return a result representing the found [MdibEntity], [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    fun <T : MdibEntity> entity(handle: String, type: KClass<out T>): Result<T>

    /**
     * Gets a copy of all root entities.
     *
     * @return a list of all available [MdibEntity.Mds] instances.
     */
    fun rootEntities(): List<MdibEntity.Mds>

    /**
     * Finds a context state for a given handle.
     *
     * @param handle the state handle to seek.
     * @return the corresponding context state, [NotFoundException] if the state was not found
     * or another exception expressing a failure otherwise.
     */
    fun contextState(handle: String): Result<AbstractContextStateOneOf>

    /**
     * Finds a context state for a given handle of a given type.
     *
     * @param handle the state handle to seek.
     * @param type type of the entity to seek.
     * @return the corresponding context state, [NotFoundException] if the state was not found
     * or another exception expressing a failure otherwise.
     */
    fun <T : AbstractContextStateOneOf> contextState(handle: String, type: KClass<out T>): Result<T>

    /**
     * Gets a copy of all context states.
     *
     * @return a collection of all available context states.
     */
    fun contextStates(): Collection<AbstractContextStateOneOf>

    /**
     * Filters entities by type.
     *
     * @param type the type to filter for.
     * @return a collection that contains all entities of the given *type*.
     */
    fun <T: MdibEntity> entitiesByType(type: KClass<out T>): Collection<T>

    /**
     * Filters child entities by type.
     *
     * @param handle the parent handle for which children are filtered.
     * @param type the type to filter for.
     * @return a list that contains all children for *handle* of the given *type*.
     */
    fun <T: MdibEntity> childrenByType(handle: String, type: KClass<out T>): List<T>
    
    /**
     * Applies description modifications on this object with almost no consistency checks.
     *
     * - Versions are applied without being verified
     * - Consistency checks must be performed beforehand
     *
     * @param mdibVersion the MDIB version to apply.
     * @param mdDescriptionVersion the MD description version to apply. Value null leaves version as is.
     * @param mdStateVersion the MD state version to apply. Value null leaves version as is.
     * @param descriptionModifications the modifications to apply.
     * @return a result set with inserted, updated and deleted entities.
     * @throws IllegalArgumentException when a handle expected to be existing is not found in the storage.
     */
    @Throws(IllegalArgumentException::class)
    fun apply(
        mdibVersion: Long,
        mdDescriptionVersion: Long?,
        mdStateVersion: Long?,
        descriptionModifications: MdibDescriptionModifications
    ): WriteDescriptionResult

    /**
     * Applies state modifications on this object with almost no consistency checks.
     *
     * - Versions are applied without being verified
     * - Consistency checks must be performed beforehand
     *
     * @param mdibVersion the MDIB version to apply.
     * @param mdStateVersion the MD state version to apply. Value null leaves version as is.
     * @param stateModifications the modifications to apply.
     * @return a result set with updated states.
     * @throws IllegalArgumentException when there is an entity missing from this storage for which a state update
     *                                  is applied.
     */
    @Throws(IllegalArgumentException::class)
    fun apply(
        mdibVersion: Long,
        mdStateVersion: Long?,
        stateModifications: MdibStateModifications
    ): WriteStateResult
}