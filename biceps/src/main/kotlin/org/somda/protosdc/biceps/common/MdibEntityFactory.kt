package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.*

/**
 * Convenience factory to create [MdibEntity] instances.
 */
public class MdibEntityFactory {
    /**
     * Creates an MDIB entity.
     *
     * @param descriptor the descriptor from which the resulting MDIB entity data class type is derived.
     * @param states a list of states that corresponds to the descriptor type.
     * @param mdibVersion the MDIB version to set.
     * @param parent the parent handle or null if the MDIB entity is going to be an [MdibEntity.Mds].
     * @param sourceMds the parent MDS of the entity to create. Is null if an MDS is created.
     * @param children a list of child handles or an empty list of the resulting [MdibEntity] does not contain children.
     * @throws IllegalArgumentException if preconditions for instantiation of the MDIB entity are not fulfilled:
     *
     *                                  - parent information is missing
     *                                  - number of states don't match
     *                                  - state types don't match
     */
    @Throws(IllegalArgumentException::class)
    public fun create(
        descriptor: AbstractDescriptorOneOf,
        states: List<AbstractStateOneOf>,
        mdibVersion: MdibVersion,
        parent: String? = null,
        sourceMds: String? = null,
        children: List<String> = listOf()
    ): MdibEntity =
        when (descriptor) {
            is AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceActivateOperationState, ActivateOperationState>(
                    states,
                    parent
                ) {
                    it.value
                }
                MdibEntity.ActivateOperation(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceAlertConditionState, AlertConditionState>(
                    states,
                    parent
                ) {
                    it.value
                }
                MdibEntity.AlertCondition(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceAlertSignalState, AlertSignalState>(states, parent) {
                        it.value
                    }
                MdibEntity.AlertSignal(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceAlertSystemState, AlertSystemState>(states, parent) {
                        it.value
                    }
                MdibEntity.AlertSystem(mdibVersion, descriptor.value, state, parent!!, sourceMds!!, children)
            }
            is AbstractDescriptorOneOf.ChoiceBatteryDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceBatteryState, BatteryState>(states, parent) {
                    it.value
                }
                MdibEntity.Battery(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceChannelDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceChannelState, ChannelState>(states, parent) {
                    it.value
                }
                MdibEntity.Channel(mdibVersion, descriptor.value, state, parent!!, sourceMds!!, children)
            }
            is AbstractDescriptorOneOf.ChoiceClockDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceClockState, ClockState>(states, parent) {
                    it.value
                }
                MdibEntity.Clock(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState, DistributionSampleArrayMetricState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.DistributionSampleArrayMetric(
                    mdibVersion,
                    descriptor.value,
                    state,
                    parent!!, sourceMds!!
                )
            }
            is AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor -> {
                val preparedStates =
                    prepareContextState<AbstractStateOneOf.ChoiceEnsembleContextState, EnsembleContextState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.EnsembleContext(mdibVersion, descriptor.value, preparedStates, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceEnumStringMetricState, EnumStringMetricState>(
                    states,
                    parent
                ) {
                    it.value
                }
                MdibEntity.EnumStringMetric(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceLimitAlertConditionState, LimitAlertConditionState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.LimitAlertCondition(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceLocationContextDescriptor -> {
                val preparedStates =
                    prepareContextState<AbstractStateOneOf.ChoiceLocationContextState, LocationContextState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.LocationContext(mdibVersion, descriptor.value, preparedStates, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceMdsDescriptor -> {
                MdibEntity.Mds(mdibVersion, descriptor.value, prepareMds(states), children)
            }
            is AbstractDescriptorOneOf.ChoiceMeansContextDescriptor -> {
                val preparedStates =
                    prepareContextState<AbstractStateOneOf.ChoiceMeansContextState, MeansContextState>(states, parent) {
                        it.value
                    }
                MdibEntity.MeansContext(mdibVersion, descriptor.value, preparedStates, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceNumericMetricState, NumericMetricState>(
                    states,
                    parent
                ) {
                    it.value
                }
                MdibEntity.NumericMetric(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor -> {
                val preparedStates =
                    prepareContextState<AbstractStateOneOf.ChoiceOperatorContextState, OperatorContextState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.OperatorContext(mdibVersion, descriptor.value, preparedStates, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoicePatientContextDescriptor -> {
                val preparedStates =
                    prepareContextState<AbstractStateOneOf.ChoicePatientContextState, PatientContextState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.PatientContext(mdibVersion, descriptor.value, preparedStates, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState, RealTimeSampleArrayMetricState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.RealTimeSampleArrayMetric(
                    mdibVersion,
                    descriptor.value,
                    state,
                    parent!!, sourceMds!!
                )
            }
            is AbstractDescriptorOneOf.ChoiceScoDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceScoState, ScoState>(states, parent) {
                    it.value
                }
                MdibEntity.Sco(mdibVersion, descriptor.value, state, parent!!, sourceMds!!, children)
            }
            is AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceSetAlertStateOperationState, SetAlertStateOperationState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.SetAlertStateOperation(
                    mdibVersion,
                    descriptor.value,
                    state,
                    parent!!, sourceMds!!
                )
            }
            is AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceSetComponentStateOperationState, SetComponentStateOperationState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.SetComponentStateOperation(
                    mdibVersion,
                    descriptor.value,
                    state,
                    parent!!, sourceMds!!
                )
            }
            is AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceSetContextStateOperationState, SetContextStateOperationState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.SetContextStateOperation(
                    mdibVersion,
                    descriptor.value,
                    state,
                    parent!!, sourceMds!!
                )
            }
            is AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceSetMetricStateOperationState, SetMetricStateOperationState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.SetMetricStateOperation(
                    mdibVersion,
                    descriptor.value,
                    state,
                    parent!!, sourceMds!!
                )
            }
            is AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceSetStringOperationState, SetStringOperationState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.SetStringOperation(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceSetValueOperationState, SetValueOperationState>(
                    states,
                    parent
                ) {
                    it.value
                }
                MdibEntity.SetValueOperation(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceStringMetricDescriptor -> {
                val state =
                    prepareSingleState<AbstractStateOneOf.ChoiceStringMetricState, StringMetricState>(states, parent) {
                        it.value
                    }
                MdibEntity.StringMetric(mdibVersion, descriptor.value, state, parent!!, sourceMds!!)
            }
            is AbstractDescriptorOneOf.ChoiceSystemContextDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceSystemContextState, SystemContextState>(
                    states,
                    parent
                ) {
                    it.value
                }
                MdibEntity.SystemContext(mdibVersion, descriptor.value, state, parent!!, sourceMds!!, children)
            }
            is AbstractDescriptorOneOf.ChoiceVmdDescriptor -> {
                val state = prepareSingleState<AbstractStateOneOf.ChoiceVmdState, VmdState>(states, parent) {
                    it.value
                }
                MdibEntity.Vmd(mdibVersion, descriptor.value, state, parent!!, sourceMds!!, children)
            }
            is AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor -> {
                val preparedStates =
                    prepareContextState<AbstractStateOneOf.ChoiceWorkflowContextState, WorkflowContextState>(
                        states,
                        parent
                    ) {
                        it.value
                    }
                MdibEntity.WorkflowContext(mdibVersion, descriptor.value, preparedStates, parent!!, sourceMds!!)
            }
            else -> throw throw InvalidWhenBranchException(descriptor)
        }


    private inline fun <reified T, reified V> prepareSingleState(
        states: List<AbstractStateOneOf>,
        parent: String?,
        mapState: (T) -> V
    ): V {
        require(parent != null) { "${T::class} requires a parent, but none found" }
        require(states.size == 1) { "${T::class} requires exactly one state, but found: ${states.size}" }
        val state = states.first()
        require(state is T) { "Expected ${T::class}, but found ${state::class}" }
        return mapState(state)
    }

    private inline fun <reified T, reified V> prepareContextState(
        states: List<AbstractStateOneOf>,
        parent: String?,
        mapState: (T) -> V
    ): List<V> {
        require(parent != null) { "${T::class} requires a parent, but none found" }
        return states.map {
            require(it is T) { "Expected ${T::class}, but found ${it::class}" }
            mapState(it)
        }
    }

    private fun prepareMds(states: List<AbstractStateOneOf>): MdsState {
        require(states.size == 1) { "An MDS requires exactly one state, but found: ${states.size}" }
        val state = states.first()
        require(state is AbstractStateOneOf.ChoiceMdsState) {
            "Expected ${AbstractStateOneOf.ChoiceMdsState::class}, but found ${state::class}"
        }
        return state.value
    }

    public fun replaceChildren(mdibEntity: MdibEntity, newChildren: List<String>): MdibEntity = when (mdibEntity) {
        is MdibEntity.AlertSystem -> mdibEntity.copy(children = newChildren)
        is MdibEntity.Mds -> mdibEntity.copy(children = newChildren)
        is MdibEntity.Sco -> mdibEntity.copy(children = newChildren)
        is MdibEntity.SystemContext -> mdibEntity.copy(children = newChildren)
        is MdibEntity.Vmd -> mdibEntity.copy(children = newChildren)
        is MdibEntity.Channel -> mdibEntity.copy(children = newChildren)
        else -> mdibEntity
    }

    public fun replaceDescriptorAndStates(
        mdibEntity: MdibEntity,
        newDescriptor: AbstractDescriptorOneOf,
        newStates: List<AbstractStateOneOf>
    ): MdibEntity = when (mdibEntity) {
        is MdibEntity.ActivateOperation -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor, ActivateOperationDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceActivateOperationState, ActivateOperationState>(newStates) { it.value }
            )
        }
        is MdibEntity.AlertCondition -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor, AlertConditionDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceAlertConditionState, AlertConditionState>(newStates) { it.value }
            )
        }

        is MdibEntity.AlertSignal -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor, AlertSignalDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceAlertSignalState, AlertSignalState>(newStates) { it.value }
            )
        }
        is MdibEntity.AlertSystem -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor, AlertSystemDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceAlertSystemState, AlertSystemState>(newStates) { it.value }
            )
        }
        is MdibEntity.Battery -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceBatteryDescriptor, BatteryDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceBatteryState, BatteryState>(newStates) { it.value }
            )
        }
        is MdibEntity.Channel -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceChannelDescriptor, ChannelDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceChannelState, ChannelState>(newStates) { it.value }
            )
        }
        is MdibEntity.Clock -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceClockDescriptor, ClockDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceClockState, ClockState>(newStates) { it.value }
            )
        }
        is MdibEntity.DistributionSampleArrayMetric -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor, DistributionSampleArrayMetricDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState, DistributionSampleArrayMetricState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.EnsembleContext -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor, EnsembleContextDescriptor>(
                    newDescriptor
                ) { it.value },
                states = copyStates<AbstractStateOneOf.ChoiceEnsembleContextState, EnsembleContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.EnumStringMetric -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor, EnumStringMetricDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceEnumStringMetricState, EnumStringMetricState>(newStates) { it.value }
            )
        }
        is MdibEntity.LimitAlertCondition -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor, LimitAlertConditionDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceLimitAlertConditionState, LimitAlertConditionState>(newStates) { it.value }
            )
        }
        is MdibEntity.LocationContext -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceLocationContextDescriptor, LocationContextDescriptor>(
                    newDescriptor
                ) { it.value },
                states = copyStates<AbstractStateOneOf.ChoiceLocationContextState, LocationContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.Mds -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceMdsDescriptor, MdsDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceMdsState, MdsState>(newStates) { it.value }
            )
        }
        is MdibEntity.MeansContext -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceMeansContextDescriptor, MeansContextDescriptor>(
                    newDescriptor
                ) { it.value },
                states = copyStates<AbstractStateOneOf.ChoiceMeansContextState, MeansContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.NumericMetric -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor, NumericMetricDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceNumericMetricState, NumericMetricState>(newStates) { it.value }
            )
        }
        is MdibEntity.OperatorContext -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor, OperatorContextDescriptor>(
                    newDescriptor
                ) { it.value },
                states = copyStates<AbstractStateOneOf.ChoiceOperatorContextState, OperatorContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.PatientContext -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoicePatientContextDescriptor, PatientContextDescriptor>(
                    newDescriptor
                ) { it.value },
                states = copyStates<AbstractStateOneOf.ChoicePatientContextState, PatientContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.RealTimeSampleArrayMetric -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor, RealTimeSampleArrayMetricDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState, RealTimeSampleArrayMetricState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.Sco -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceScoDescriptor, ScoDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceScoState, ScoState>(newStates) { it.value }
            )
        }
        is MdibEntity.SetAlertStateOperation -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor, SetAlertStateOperationDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceSetAlertStateOperationState, SetAlertStateOperationState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.SetComponentStateOperation -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor, SetComponentStateOperationDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceSetComponentStateOperationState, SetComponentStateOperationState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.SetContextStateOperation -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor, SetContextStateOperationDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceSetContextStateOperationState, SetContextStateOperationState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.SetMetricStateOperation -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor, SetMetricStateOperationDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceSetMetricStateOperationState, SetMetricStateOperationState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.SetStringOperation -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor, SetStringOperationDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceSetStringOperationState, SetStringOperationState>(newStates) { it.value }
            )
        }
        is MdibEntity.SetValueOperation -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor, SetValueOperationDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceSetValueOperationState, SetValueOperationState>(newStates) { it.value }
            )
        }
        is MdibEntity.StringMetric -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceStringMetricDescriptor, StringMetricDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceStringMetricState, StringMetricState>(newStates) { it.value }
            )
        }
        is MdibEntity.SystemContext -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceSystemContextDescriptor, SystemContextDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceSystemContextState, SystemContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.Vmd -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceVmdDescriptor, VmdDescriptor>(
                    newDescriptor
                ) { it.value },
                state = copyState<AbstractStateOneOf.ChoiceVmdState, VmdState>(newStates) { it.value }
            )
        }
        is MdibEntity.WorkflowContext -> {
            mdibEntity.copy(
                descriptor = copyDescr<AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor, WorkflowContextDescriptor>(
                    newDescriptor
                ) { it.value },
                states = copyStates<AbstractStateOneOf.ChoiceWorkflowContextState, WorkflowContextState>(newStates) { it.value }
            )
        }
    }

    public fun replaceStates(
        mdibEntity: MdibEntity,
        newStates: List<AbstractStateOneOf>
    ): MdibEntity = when (mdibEntity) {
        is MdibEntity.ActivateOperation -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceActivateOperationState, ActivateOperationState>(newStates) { it.value }
            )
        }
        is MdibEntity.AlertCondition -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceAlertConditionState, AlertConditionState>(newStates) { it.value }
            )
        }

        is MdibEntity.AlertSignal -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceAlertSignalState, AlertSignalState>(newStates) { it.value }
            )
        }
        is MdibEntity.AlertSystem -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceAlertSystemState, AlertSystemState>(newStates) { it.value }
            )
        }
        is MdibEntity.Battery -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceBatteryState, BatteryState>(newStates) { it.value }
            )
        }
        is MdibEntity.Channel -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceChannelState, ChannelState>(newStates) { it.value }
            )
        }
        is MdibEntity.Clock -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceClockState, ClockState>(newStates) { it.value }
            )
        }
        is MdibEntity.DistributionSampleArrayMetric -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState, DistributionSampleArrayMetricState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.EnsembleContext -> {
            mdibEntity.copy(
                states = copyStates<AbstractStateOneOf.ChoiceEnsembleContextState, EnsembleContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.EnumStringMetric -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceEnumStringMetricState, EnumStringMetricState>(newStates) { it.value }
            )
        }
        is MdibEntity.LimitAlertCondition -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceLimitAlertConditionState, LimitAlertConditionState>(newStates) { it.value }
            )
        }
        is MdibEntity.LocationContext -> {
            mdibEntity.copy(
                states = copyStates<AbstractStateOneOf.ChoiceLocationContextState, LocationContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.Mds -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceMdsState, MdsState>(newStates) { it.value }
            )
        }
        is MdibEntity.MeansContext -> {
            mdibEntity.copy(
                states = copyStates<AbstractStateOneOf.ChoiceMeansContextState, MeansContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.NumericMetric -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceNumericMetricState, NumericMetricState>(newStates) { it.value }
            )
        }
        is MdibEntity.OperatorContext -> {
            mdibEntity.copy(
                states = copyStates<AbstractStateOneOf.ChoiceOperatorContextState, OperatorContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.PatientContext -> {
            mdibEntity.copy(
                states = copyStates<AbstractStateOneOf.ChoicePatientContextState, PatientContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.RealTimeSampleArrayMetric -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState, RealTimeSampleArrayMetricState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.Sco -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceScoState, ScoState>(newStates) { it.value }
            )
        }
        is MdibEntity.SetAlertStateOperation -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceSetAlertStateOperationState, SetAlertStateOperationState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.SetComponentStateOperation -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceSetComponentStateOperationState, SetComponentStateOperationState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.SetContextStateOperation -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceSetContextStateOperationState, SetContextStateOperationState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.SetMetricStateOperation -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceSetMetricStateOperationState, SetMetricStateOperationState>(
                    newStates
                ) { it.value }
            )
        }
        is MdibEntity.SetStringOperation -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceSetStringOperationState, SetStringOperationState>(newStates) { it.value }
            )
        }
        is MdibEntity.SetValueOperation -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceSetValueOperationState, SetValueOperationState>(newStates) { it.value }
            )
        }
        is MdibEntity.StringMetric -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceStringMetricState, StringMetricState>(newStates) { it.value }
            )
        }
        is MdibEntity.SystemContext -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceSystemContextState, SystemContextState>(newStates) { it.value }
            )
        }
        is MdibEntity.Vmd -> {
            mdibEntity.copy(
                state = copyState<AbstractStateOneOf.ChoiceVmdState, VmdState>(newStates) { it.value }
            )
        }
        is MdibEntity.WorkflowContext -> {
            mdibEntity.copy(
                states = copyStates<AbstractStateOneOf.ChoiceWorkflowContextState, WorkflowContextState>(newStates) { it.value }
            )
        }
    }

    private inline fun <reified T, reified V> copyDescr(descr: AbstractDescriptorOneOf, map: (T) -> V): V = descr.let {
        require(descr is T) { "Expected descriptor to be of type ${T::class}, but was ${descr::class}" }
        map(it as T)
    }

    private inline fun <reified T, reified V> copyState(states: List<AbstractStateOneOf>, map: (T) -> V): V =
        states.first().let {
            require(it is T) { "Expected state to be of type ${T::class}, but was ${it::class}" }
            map(it as T)
        }

    private inline fun <reified T, reified V> copyStates(states: List<AbstractStateOneOf>, map: (T) -> V): List<V> =
        states.map {
            require(it is T) { "Expected state to be of type ${T::class}, but was ${it::class}" }
            map(it as T)
        }

}