package org.somda.protosdc.biceps.provider.preprocessing

import org.somda.protosdc.biceps.common.MdibDescriptionModification
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.protosdc.biceps.common.storage.MdibStorage
import org.somda.protosdc.biceps.provider.preprocessing.helper.MdibTreeValidator
import javax.inject.Inject

/**
 * Preprocessing segment that ensures correctness of child-parent type relationships.
 */
internal class TypeConsistencyChecker @Inject constructor() : DescriptionPreprocessingSegment {
    private val treeValidator = MdibTreeValidator()

    override fun process(
        modifications: MdibDescriptionModifications,
        storage: MdibStorage
    ) = modifications.also {
        modifications.asList()
            .filterIsInstance<MdibDescriptionModification.Insert>() // only insert is affected
            .filter { it.parentHandle != null } // No parent means: MDS, which may occur many times
            .forEach { currentModification ->
                val parentHandle = currentModification.parentHandle!!
                val descriptor = currentModification.descriptor

                val parentDescr = storage.entity(parentHandle).getOrNull()?.descriptorOneOf
                    ?: modifications.asList()
                        .filterIsInstance<MdibDescriptionModification.Insert>()
                        .firstOrNull { it.descriptor.handleString() == parentHandle }?.descriptor
                    ?: throw IllegalArgumentException(
                        "Insertion requires a parent for non-MDS entities, " +
                                "but none found for handle '${descriptor.handleString()}' with parent handle '${parentHandle}'"
                    )

                require(treeValidator.isValidParent(parentDescr, descriptor)) {
                    "Parent descriptor of ${descriptor::class} is invalid: ${parentDescr::class}. " +
                            "Valid parents: [${treeValidator.allowedParents(descriptor).joinToString(", ")}]."
                }
            }
    }

    override fun toString(): String = this.javaClass.simpleName
}