package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.AbstractAlertStateOneOf
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.AbstractDeviceComponentStateOneOf
import org.somda.protosdc.model.biceps.AbstractMetricStateOneOf
import org.somda.protosdc.model.biceps.AbstractOperationStateOneOf
import org.somda.protosdc.model.biceps.AbstractStateOneOf
import org.somda.protosdc.model.biceps.AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricState

/**
 * A state modification written by [org.somda.protosdc.biceps.common.storage.MdibStorage].
 */
public sealed class WrittenStateModification(public val state: AbstractStateOneOf) {
    /**
     * Source MDS the written state belongs to.
     */
    public abstract val sourceMds: String

    public data class Alert(
        val alertState: AbstractAlertStateOneOf,
        override val sourceMds: String
    ) : WrittenStateModification(alertState.toAbstractStateOneOf())

    public data class Component(
        val componentState: AbstractDeviceComponentStateOneOf,
        override val sourceMds: String
    ) : WrittenStateModification(componentState.toAbstractStateOneOf())

    public data class Metric(
        val metricState: AbstractMetricStateOneOf,
        override val sourceMds: String
    ) : WrittenStateModification(metricState.toAbstractStateOneOf())

    public data class Context(
        val contextState: AbstractContextStateOneOf,
        override val sourceMds: String
    ) : WrittenStateModification(contextState.toAbstractStateOneOf())

    public data class Operation(
        val operationState: AbstractOperationStateOneOf,
        override val sourceMds: String
    ) : WrittenStateModification(operationState.toAbstractStateOneOf())

    public data class Waveform(
        val waveformState: RealTimeSampleArrayMetricState,
        override val sourceMds: String
    ) : WrittenStateModification(ChoiceRealTimeSampleArrayMetricState(waveformState))
}

/**
 * Container to collect state updates supposed to be applied on an MDIB.
 */
public sealed interface WrittenMdibStateModifications {
    /**
     * Generic access to the written states.
     */
    public val states: List<WrittenStateModification>

    /**
     * Collects alert states.
     *
     * @param alertStates to be modified.
     */
    public data class Alert(val alertStates: List<WrittenStateModification.Alert>) : WrittenMdibStateModifications {
        override val states: List<WrittenStateModification.Alert> = alertStates
    }

    /**
     * Collects component states.
     *
     * @param componentStates to be modified.
     */
    public data class Component(val componentStates: List<WrittenStateModification.Component>) :
        WrittenMdibStateModifications {
        override val states: List<WrittenStateModification.Component> = componentStates
    }

    /**
     * Collects context states.
     *
     * @param contextStates to be modified.
     */
    public data class Context(val contextStates: List<WrittenStateModification.Context>) : WrittenMdibStateModifications {
        override val states: List<WrittenStateModification.Context> = contextStates
    }

    /**
     * Collects metric states.
     *
     * @param metricStates to be modified.
     */
    public data class Metric(val metricStates: List<WrittenStateModification.Metric>) : WrittenMdibStateModifications {
        override val states: List<WrittenStateModification.Metric> = metricStates
    }

    /**
     * Collects operation states.
     *
     * @param operationStates to be modified.
     */
    public data class Operation(val operationStates: List<WrittenStateModification.Operation>) :
        WrittenMdibStateModifications {
        override val states: List<WrittenStateModification.Operation> = operationStates
    }

    /**
     * Collects real-time sample array metric states to be used for waveform updates.
     *
     * @param waveformStates to be modified.
     */
    public data class Waveform(val waveformStates: List<WrittenStateModification.Waveform>) : WrittenMdibStateModifications {
        override val states: List<WrittenStateModification.Waveform> = waveformStates
    }
}