package org.somda.protosdc.biceps.common

/**
 * Entities written to [org.somda.protosdc.biceps.common.storage.MdibStorage].
 */
public data class WrittenMdibDescriptionModifications(
    val inserted: List<MdibEntity>,
    val updated: List<MdibEntity>,
    val deleted: List<MdibEntity>
)
