package org.somda.protosdc.biceps.provider.preprocessing

import org.somda.protosdc.biceps.common.MdibDescriptionModification
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.protosdc.biceps.common.storage.MdibStorage
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf
import org.somda.protosdc.biceps.provider.preprocessing.helper.MdibTreeValidator
import javax.inject.Inject

/**
 * Preprocessing segment that verifies correctness of child cardinality.
 *
 * In BICEPS descriptors of a certain type can appear one or many times as a child of another descriptor.
 * This checker guarantees that no child is inserted twice if the maximum allowed number of children is one.
 */
internal class CardinalityChecker @Inject constructor() : DescriptionPreprocessingSegment {
    private val treeValidator = MdibTreeValidator()

    override fun process(
        modifications: MdibDescriptionModifications,
        storage: MdibStorage
    ) = modifications.asList()
        .filterIsInstance<MdibDescriptionModification.Insert>() // only insert is affected
        .filter { it.parentHandle != null } // No parent means: MDS, which may occur many times
        .forEach { currentModification ->
            val parentHandle = currentModification.parentHandle!!
            val descriptor = currentModification.descriptor

            // Require that there is no other child of the same type to be inserted below the parent for
            // single child constraints
            require(
                !isSameTypeInModifications(modifications.asList(), descriptor, parentHandle)
                        || treeValidator.isManyAllowed(descriptor)
            ) {
                exceptionText(descriptor)
            }

            storage.entity(parentHandle).let {
                // Require that there is at least one child of given type, but multiple children of that
                // type are not allowed
                require(
                    storage.childrenByType(parentHandle, MdibEntity.typeFromDescriptorOneOf(descriptor)).isEmpty()
                            || treeValidator.isManyAllowed(descriptor)
                ) {
                    exceptionText(descriptor)
                }
            }
        }.let { modifications } // Every condition passed, insertion is ok

    private fun isSameTypeInModifications(
        modifications: List<MdibDescriptionModification>,
        descriptor: AbstractDescriptorOneOf,
        parentHandle: String
    ) = modifications
        .filterIsInstance<MdibDescriptionModification.Insert>()
        .filter { it.descriptor.handleString() != descriptor.handleString() }
        .filter { it.parentHandle != null && it.parentHandle == parentHandle }
        .any { it.descriptor.javaClass == descriptor.javaClass }

    private fun exceptionText(descriptor: AbstractDescriptorOneOf) =
        "Type ${descriptor.javaClass} is not allowed to appear more than once as a child (see handle '${descriptor.handleString()}')"

    override fun toString(): String = this.javaClass.simpleName
}