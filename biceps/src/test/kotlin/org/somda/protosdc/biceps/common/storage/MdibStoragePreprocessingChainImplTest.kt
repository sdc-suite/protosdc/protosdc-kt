package org.somda.protosdc.biceps.common.storage

import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.somda.protosdc.biceps.common.*

internal class MdibStoragePreprocessingChainImplTest {
    private fun createPreprocessingChain(
        mdibStorage: MdibStorageImpl,
        descriptionChainSegments: List<DescriptionPreprocessingSegment> = listOf(),
        stateChainSegments: List<StatePreprocessingSegment> = listOf()
    ) = MdibStoragePreprocessingChainImpl(mdibStorage, descriptionChainSegments, stateChainSegments)

    private val instances = DefaultInstantiation()

    @Test
    fun `process description modifications`(): Unit = runBlocking {
        // Given a preprocessing chain with 3 segments
        val mockStorage = mock<MdibStorageImpl> { }
        val segment1 = mock<DescriptionPreprocessingSegment> {}
        val segment2 = mock<DescriptionPreprocessingSegment> {}
        val segment3 = mock<DescriptionPreprocessingSegment> {}
        val segments = listOf(segment1, segment2, segment3)
        val chain = createPreprocessingChain(mockStorage, segments)
        val expectedHandle = "foobarHandle"
        var modifications = MdibDescriptionModifications().add(
            MdibDescriptionModification.Insert(DescriptionItem(instances.defaultMdsDescriptor(expectedHandle)))
        )

        run {
            for (segment in segments) {
                whenever(segment.process(any(), any())).thenReturn(modifications)
            }

            // When there is a regular call to process description modifications
            modifications = chain.processDescriptionModifications(modifications)

            // Then expect every segment to be processed once
            for (segment in segments) {
                verify(segment, times(1)).process(eq(modifications), eq(mockStorage))
            }
        }

        run {

            // When there is a call that causes an exception during processing of segment2
            val expectedErrorMessage = "foobarMessage"
            doThrow(RuntimeException(expectedErrorMessage)).`when`(segment2)
                .process(eq(modifications), eq(mockStorage))

            // Then expect a PreprocessingException to be thrown
            val thrownException =
                assertThrows<PreprocessingException> { modifications = chain.processDescriptionModifications(modifications) }
            assertTrue(thrownException.message?.contains(expectedErrorMessage) ?: false)

            // Then expect segment3 not to be processed
            verify(segment3, times(1)) // still one interaction only
                .process(eq(modifications), eq(mockStorage))
        }
    }

    @Test
    fun `process state modifications`(): Unit = runBlocking {
        // Given a preprocessing chain with 3 segments
        val mockStorage = mock<MdibStorageImpl> { }
        val segment1 =
            mock<StatePreprocessingSegment> { }
        val segment2 =
            mock<StatePreprocessingSegment> { }
        val segment3 =
            mock<StatePreprocessingSegment> { }
        val segments = listOf(segment1, segment2, segment3)
        val chain: MdibStoragePreprocessingChainImpl = createPreprocessingChain(
            mdibStorage = mockStorage,
            stateChainSegments = segments
        )
        val expectedHandle = "foobarHandle"
        val modifications = MdibStateModifications.Metric(
            MetricStates().add(instances.defaultNumericMetricState(expectedHandle))
        )

        run {
            for (segment in segments) {
                whenever(segment.process(any(), any())).thenReturn(modifications)
            }

            // When there is a regular call to process state modifications
            chain.processStateModifications(modifications)

            // Then expect every segment to be processed once
            for (segment in segments) {
                verify(segment, times(1))
                    .process(eq(modifications), eq(mockStorage))
            }
        }
        run {

            // When there is a call that causes an exception during processing of segment2
            val expectedErrorMessage = "foobarMessage"
            doThrow(RuntimeException(expectedErrorMessage)).`when`(segment2)
                .process(eq(modifications), eq(mockStorage))

            // Then expect a PreprocessingException to be thrown
            try {
                chain.processStateModifications(modifications)
                fail<Any>("segment2 did not throw an exception")
            } catch (e: PreprocessingException) {
                assertTrue(e.message!!.contains(expectedErrorMessage))
            }

            // Then expect segment3 not to be processed
            verify(segment3, times(1)) // still one interaction only
                .process(eq(modifications), eq(mockStorage))
        }
    }
}