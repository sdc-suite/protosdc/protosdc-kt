package org.somda.protosdc.biceps.common.storage

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.somda.protosdc.biceps.BicepsConfig
import org.somda.protosdc.biceps.common.*
import org.somda.protosdc.biceps.common.access.WriteDescriptionResult
import org.somda.protosdc.model.biceps.ContextAssociation
import org.somda.protosdc.biceps.test.Handles
import org.somda.protosdc.common.InstanceId

class MdibStorageImplTest {
    private val instances = DefaultInstantiation()
    private var mdibStorage = createMdibStorage(
        storeNotAssociatedContextStates = false
    )
    private val expectedModificationCount = 9

    @Test
    fun `test insertion of description from scratch`() = runBlocking {
        run {
            mdibStorage = createMdibStorage(
                storeNotAssociatedContextStates = false // note that not-associated context states are not stored
            )

            assertNull(mdibStorage.entity(Handles.MDS_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.SYSTEMCONTEXT_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.VMD_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.CHANNEL_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.CHANNEL_1).getOrNull())
            assertNull(mdibStorage.entity(Handles.CONTEXTDESCRIPTOR_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.CONTEXTDESCRIPTOR_1).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_0).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_1).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_2).getOrNull())

            assertNull(mdibStorage.entity(Handles.MDS_1).getOrNull())
            assertNull(mdibStorage.entity(Handles.VMD_1).getOrNull())

            val result = applyDescription(mdibStorage)
            assertEquals(expectedModificationCount, result.entities.inserted.size)

            assertEquals(Handles.MDS_0, result.entities.inserted[0].handle)
            assertEquals(Handles.MDS_0, result.entities.inserted[0].sourceMds)
            assertEquals(Handles.SYSTEMCONTEXT_0, result.entities.inserted[1].handle)
            assertEquals(Handles.MDS_0, result.entities.inserted[1].sourceMds)
            assertEquals(Handles.VMD_0, result.entities.inserted[2].handle)
            assertEquals(Handles.MDS_0, result.entities.inserted[2].sourceMds)
            assertEquals(Handles.CHANNEL_0, result.entities.inserted[3].handle)
            assertEquals(Handles.MDS_0, result.entities.inserted[3].sourceMds)
            assertEquals(Handles.CHANNEL_1, result.entities.inserted[4].handle)
            assertEquals(Handles.MDS_0, result.entities.inserted[4].sourceMds)
            assertEquals(Handles.CONTEXTDESCRIPTOR_0, result.entities.inserted[5].handle)
            assertEquals(Handles.MDS_0, result.entities.inserted[5].sourceMds)
            assertEquals(Handles.CONTEXTDESCRIPTOR_1, result.entities.inserted[6].handle)
            assertEquals(Handles.MDS_0, result.entities.inserted[6].sourceMds)


            assertEquals(Handles.MDS_1, result.entities.inserted[7].handle)
            assertEquals(Handles.MDS_1, result.entities.inserted[7].sourceMds)
            assertEquals(Handles.VMD_1, result.entities.inserted[8].handle)
            assertEquals(Handles.MDS_1, result.entities.inserted[8].sourceMds)

            assertNotNull(mdibStorage.entity(Handles.MDS_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.SYSTEMCONTEXT_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.VMD_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.CHANNEL_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.CHANNEL_1).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.MDS_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.CONTEXTDESCRIPTOR_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.CONTEXTDESCRIPTOR_1).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_0).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_1).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_2).getOrNull())
        }
        run {
            mdibStorage = createMdibStorage(
                storeNotAssociatedContextStates = true // note that not-associated context states are stored
            )
            assertNull(mdibStorage.entity(Handles.MDS_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.SYSTEMCONTEXT_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.VMD_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.CHANNEL_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.CHANNEL_1).getOrNull())
            assertNull(mdibStorage.entity(Handles.CONTEXTDESCRIPTOR_0).getOrNull())
            assertNull(mdibStorage.entity(Handles.CONTEXTDESCRIPTOR_1).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_0).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_1).getOrNull())
            assertNull(mdibStorage.contextState(Handles.CONTEXT_2).getOrNull())

            val result = applyDescription(mdibStorage)
            assertEquals(expectedModificationCount, result.entities.inserted.size)

            assertEquals(Handles.MDS_0, result.entities.inserted[0].handle)
            assertEquals(Handles.SYSTEMCONTEXT_0, result.entities.inserted[1].handle)
            assertEquals(Handles.VMD_0, result.entities.inserted[2].handle)
            assertEquals(Handles.CHANNEL_0, result.entities.inserted[3].handle)
            assertEquals(Handles.CHANNEL_1, result.entities.inserted[4].handle)
            assertEquals(Handles.CONTEXTDESCRIPTOR_0, result.entities.inserted[5].handle)
            assertEquals(Handles.CONTEXTDESCRIPTOR_1, result.entities.inserted[6].handle)

            assertNotNull(mdibStorage.entity(Handles.MDS_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.SYSTEMCONTEXT_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.VMD_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.CHANNEL_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.CHANNEL_1).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.MDS_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.CONTEXTDESCRIPTOR_0).getOrNull())
            assertNotNull(mdibStorage.entity(Handles.CONTEXTDESCRIPTOR_1).getOrNull())
            assertNotNull(mdibStorage.contextState(Handles.CONTEXT_0).getOrNull())
            assertNotNull(mdibStorage.contextState(Handles.CONTEXT_1).getOrNull())
            assertNotNull(mdibStorage.contextState(Handles.CONTEXT_2).getOrNull())
        }
    }

    @Test
    fun `do not allow states without descriptors`() {
        mdibStorage = createMdibStorage(
            storeNotAssociatedContextStates = true
        )

        run {
            val componentStates = ComponentStates()
                .add(instances.defaultMdsState(Handles.MDS_0))
                .add(instances.defaultSystemContextState(Handles.SYSTEMCONTEXT_0))


            assertThrows(IllegalArgumentException::class.java) {
                mdibStorage.apply(
                    mdibStorage.mdibVersion.inc().version,
                    mdibStorage.mdStateVersion.inc(),
                    MdibStateModifications.Component(componentStates)
                )
            }
        }

        run {
            val contextStates = ContextStates()
                .add(instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_0))
                .add(instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1))
                .add(instances.defaultEnsembleContextState(Handles.CONTEXTDESCRIPTOR_1, Handles.CONTEXT_2))

            assertThrows(IllegalArgumentException::class.java) {
                mdibStorage.apply(
                    mdibStorage.mdibVersion.inc().version,
                    mdibStorage.mdStateVersion.inc(),
                    MdibStateModifications.Context(contextStates)
                )
            }
        }
    }

    private fun applyDescription(storage: MdibStorageImpl): WriteDescriptionResult {
        val modifications = MdibDescriptionModifications()
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultMdsDescriptor(Handles.MDS_0),
                    instances.defaultMdsState(Handles.MDS_0)
                )
            )
        )
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultSystemContextDescriptor(Handles.SYSTEMCONTEXT_0),
                    instances.defaultSystemContextState(Handles.SYSTEMCONTEXT_0), Handles.MDS_0
                )
            )
        )
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultVmdDescriptor(Handles.VMD_0),
                    instances.defaultVmdState(Handles.VMD_0), Handles.MDS_0
                )
            )
        )
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultChannelDescriptor(Handles.CHANNEL_0),
                    instances.defaultChannelState(Handles.CHANNEL_0), Handles.VMD_0
                )
            )
        )
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultChannelDescriptor(Handles.CHANNEL_1),
                    instances.defaultChannelState(Handles.CHANNEL_1), Handles.VMD_0
                )
            )
        )
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultPatientContextDescriptor(Handles.CONTEXTDESCRIPTOR_0),
                    listOf(
                        instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_0)
                            .let {
                                it.copy(
                                    abstractContextState = it.abstractContextState.copy(
                                        contextAssociationAttr = ContextAssociation(
                                            ContextAssociation.EnumType.No
                                        )
                                    )
                                )
                            },
                        instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1)
                            .let {
                                it.copy(
                                    abstractContextState = it.abstractContextState.copy(
                                        contextAssociationAttr = ContextAssociation(
                                            ContextAssociation.EnumType.No
                                        )
                                    )
                                )
                            }
                    ), Handles.SYSTEMCONTEXT_0
                )
            )
        )
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultLocationContextDescriptor(Handles.CONTEXTDESCRIPTOR_1),
                    listOf(instances.defaultLocationContextState(Handles.CONTEXTDESCRIPTOR_1, Handles.CONTEXT_2)
                        .let {
                            it.copy(
                                abstractContextState = it.abstractContextState.copy(
                                    contextAssociationAttr = ContextAssociation(
                                        ContextAssociation.EnumType.No
                                    )
                                )
                            )
                        }),
                    Handles.SYSTEMCONTEXT_0
                )
            )
        )
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultMdsDescriptor(Handles.MDS_1),
                    instances.defaultMdsState(Handles.MDS_1)
                )
            )
        )
        modifications.add(
            MdibDescriptionModification.Insert(
                DescriptionItem(
                    instances.defaultVmdDescriptor(Handles.VMD_1),
                    instances.defaultVmdState(Handles.VMD_1), Handles.MDS_1
                )
            )
        )

        return storage.apply(
            storage.mdibVersion.inc().version,
            storage.mdDescriptionVersion.inc(),
            storage.mdStateVersion.inc(),
            modifications
        )
    }

    private fun createMdibStorage(storeNotAssociatedContextStates: Boolean) =
        MdibStorageImpl(
            initialMdibVersion = MdibVersion.create(),
            initialMdDescriptionVersion = 0L,
            initialMdStateVersion = 0L,
            mdibEntityFactory = MdibEntityFactory(),
            instanceId = InstanceId("test-instance"),
            bicepsConfig = BicepsConfig(storeNotAssociatedContextStates),
        )
}