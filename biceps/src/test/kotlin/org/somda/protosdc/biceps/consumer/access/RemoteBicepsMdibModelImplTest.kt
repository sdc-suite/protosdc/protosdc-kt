package org.somda.protosdc.biceps.consumer.access

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test
import org.somda.protosdc.biceps.common.ComponentStates
import org.somda.protosdc.biceps.common.ContextStates
import org.somda.protosdc.biceps.common.DescriptionItem
import org.somda.protosdc.biceps.common.MdibDescriptionModification
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibModificationsPresets
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.MetricStates
import org.somda.protosdc.biceps.common.WrittenMdibStateModifications
import org.somda.protosdc.biceps.common.access.MdibAccess
import org.somda.protosdc.biceps.common.access.MdibAccessEvent
import org.somda.protosdc.biceps.common.contextAssociation
import org.somda.protosdc.biceps.common.descriptorHandleString
import org.somda.protosdc.biceps.common.descriptorVersion
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.biceps.common.stateVersion
import org.somda.protosdc.biceps.common.storage.DefaultInstantiation
import org.somda.protosdc.biceps.common.storage.PreprocessingException
import org.somda.protosdc.biceps.common.toAbstractContextStateOneOf
import org.somda.protosdc.biceps.mdibAccessFactory
import org.somda.protosdc.biceps.test.Handles
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.AbstractStateOneOf
import org.somda.protosdc.model.biceps.ContextAssociation
import kotlin.time.Duration.Companion.hours

class RemoteBicepsMdibModelImplTest {
    private val testComponent = mdibAccessFactory()
    private val instances = DefaultInstantiation()
    private val modPresets = MdibModificationsPresets()

    private inline fun <reified T> checkDescriptionWrite(
        expectedList: List<MdibDescriptionModification>,
        actualList: List<MdibEntity>,
        expectedVersion: Long
    ) {
        assertEquals(expectedList.size, actualList.size)
        for (i in expectedList.indices) {
            val expected = expectedList[i]
            val actual = actualList[i]
            check(expected is T)
            assertEquals(expected.parentHandle, actual.resolveParent())
            assertEquals(expected.descriptor.handleString(), actual.descriptorOneOf.handleString())
            assertEquals(expectedVersion, actual.descriptorOneOf.descriptorVersion())
            assertEquals(expected.descriptor.javaClass, actual.descriptorOneOf.javaClass)

            val states = actual.resolveStates()
            if (actual.isContext()) {
                assertEquals(expected.states.size, states.size)
                for (j in expected.states.indices) {
                    assertEquals(expectedVersion, states[j].descriptorVersion())
                    assertEquals(expectedVersion, states[j].stateVersion())
                    assertEquals(
                        expected.states[j].toAbstractContextStateOneOf()!!.handleString(),
                        states[j].toAbstractContextStateOneOf()!!.handleString()
                    )
                }
            } else {
                assertEquals(1, states.size)
                assertEquals(expected.states.size, states.size)
                assertEquals(expectedVersion, states.first().descriptorVersion())
                assertEquals(expectedVersion, states.first().stateVersion())
            }
        }
    }

    private suspend fun checkMdibAccess(
        mdibAccess: MdibAccess,
        expectedList: List<MdibDescriptionModification>,
        expectedVersion: Long
    ) {
        expectedList.forEach { mod ->
            val entity = mdibAccess.anyEntity(mod.descriptor.handleString()).getOrNull()
            check(entity != null)
            assertEquals(expectedVersion, entity.descriptorOneOf.descriptorVersion())
            entity.resolveStates().forEach {
                assertEquals(expectedVersion, it.descriptorVersion())
                assertEquals(expectedVersion, it.stateVersion())
            }
        }
    }

    @Test
    fun `test initial description insertion of full mdib tree`(): Unit = runBlocking {
        val mdibVersion = MdibVersion.create()
        val mdibAccess = testComponent.createRemoteMdibAccess(mdibVersion)
        val subscription = mdibAccess.subscribe()
        val tree = modPresets.fullTree()
        val deferredEvent = async { subscription.receive() }
        val result = mdibAccess.writeDescription(tree, 0L)
        val event = deferredEvent.await()
        mdibAccess.unsubscribeAll()
        check(event is MdibAccessEvent.DescriptionModification)

        assertEquals(tree.asList().size, result.entities.inserted.size)
        checkDescriptionWrite<MdibDescriptionModification.Insert>(tree.asList(), result.entities.inserted, 0L)
        checkDescriptionWrite<MdibDescriptionModification.Insert>(tree.asList(), event.entities.inserted, 0L)
        checkMdibAccess(mdibAccess, tree.asList(), 0L)
    }

    @Test
    fun `test description update of existing entities`(): Unit = runBlocking {
        val mdibVersion = MdibVersion.create()
        val mdibAccess = testComponent.createRemoteMdibAccess(mdibVersion)
        val tree = modPresets.fullTree()
        mdibAccess.writeDescription(tree, 0L)

        val subscription = mdibAccess.subscribe()

        // update some single states entities
        val updates = MdibDescriptionModifications().updateAll(
            listOf(
                DescriptionItem(
                    instances.defaultMdsDescriptor(Handles.MDS_0),
                    instances.defaultMdsState(Handles.MDS_0)
                ),
                DescriptionItem(
                    instances.defaultNumericMetricDescriptor(Handles.METRIC_0),
                    instances.defaultNumericMetricState(Handles.METRIC_0),
                    Handles.CHANNEL_0
                ),
                DescriptionItem(
                    instances.defaultPatientContextDescriptor(Handles.CONTEXTDESCRIPTOR_0),
                    listOf(
                        instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_0),
                        instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1)
                    ),
                    Handles.SYSTEMCONTEXT_0
                )
            )
        )

        val expectedUpdates = updates.asList()
        val deferredEvent = async { subscription.receive() }
        val result = mdibAccess.writeDescription(updates, 0L)
        val event = deferredEvent.await()
        mdibAccess.unsubscribeAll()
        check(event is MdibAccessEvent.DescriptionModification)

        checkDescriptionWrite<MdibDescriptionModification.Update>(expectedUpdates, result.entities.updated, 0L)
        checkDescriptionWrite<MdibDescriptionModification.Update>(expectedUpdates, event.entities.updated, 0L)
        checkMdibAccess(mdibAccess, expectedUpdates, 0L)
    }

    @Test
    fun `test description deletion`(): Unit = runBlocking {
        val mdibVersion = MdibVersion.create()
        val mdibAccess = testComponent.createRemoteMdibAccess(mdibVersion)
        val tree = modPresets.fullTree()
        mdibAccess.writeDescription(tree, 0L)

        val subscription = mdibAccess.subscribe()

        // delete in reversed order as otherwise an exception is thrown due to existing children in deleted parents
        val listOfHandles =
            tree.asList().reversed().map { MdibDescriptionModification.Delete(it.descriptor.handleString()) }
        val deletions = MdibDescriptionModifications().addAll(listOfHandles)

        val deferredEvent = async { subscription.receive() }
        val result = mdibAccess.writeDescription(deletions, 0L)
        val event = deferredEvent.await()
        mdibAccess.unsubscribeAll()
        check(event is MdibAccessEvent.DescriptionModification)

        val actualHandlesFromResult = result.entities.deleted.map { it.descriptorOneOf.handleString() }
        val actualHandlesFromEvent = event.entities.deleted.map { it.descriptorOneOf.handleString() }
        for (value in listOfHandles) {
            actualHandlesFromResult.contains(value.handle)
            actualHandlesFromEvent.contains(value.handle)
            assertNull(mdibAccess.anyEntity(value.handle).getOrNull())
        }
    }

    @Test
    fun `test state update of existing entities`(): Unit = runBlocking {
        val mdibVersion = MdibVersion.create()
        val mdibAccess = testComponent.createRemoteMdibAccess(mdibVersion)
        val tree = modPresets.fullTree()
        mdibAccess.writeDescription(tree, 0L)

        val subscription = mdibAccess.subscribe()

        val expectedMds0Lang = "de"
        val expectedMds1Lang = "de"
        val expectedAvgPeriod = 1.hours
        val expectedAssociation = ContextAssociation(ContextAssociation.EnumType.Dis)

        val expectedDescriptorVersion = 0L
        val expectedStateVersion = 0L

        val componentUpdates = MdibStateModifications.Component(
            ComponentStates()
                .add(instances.defaultMdsState(Handles.MDS_0).copy(langAttr = expectedMds0Lang))
                .add(instances.defaultMdsState(Handles.MDS_1).copy(langAttr = expectedMds1Lang))
        )
        val metricUpdates = MdibStateModifications.Metric(
            MetricStates()
                .add(
                    instances.defaultNumericMetricState(Handles.METRIC_0)
                        .copy(activeAveragingPeriodAttr = expectedAvgPeriod)
                )
        )
        val contextUpdates = MdibStateModifications.Context(
            ContextStates()
                .add(
                    instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1).let {
                        it.copy(
                            abstractContextState = it.abstractContextState.copy(
                                contextAssociationAttr = expectedAssociation
                            )
                        )
                    }
                )
        )

        val deferredEvents = async {
            mutableListOf<MdibAccessEvent>().let { list ->
                repeat(3) {
                    list.add(subscription.receive())
                }
                list
            }
        }

        val componentUpdateResult = mdibAccess.writeStates(componentUpdates, 0L)
        val metricUpdateResult = mdibAccess.writeStates(metricUpdates, 0L)
        val contextUpdateResult = mdibAccess.writeStates(contextUpdates, 0L)
        val events = deferredEvents.await()
        mdibAccess.unsubscribeAll()

        val componentUpdateEvent = events[0]
        check(componentUpdateEvent is MdibAccessEvent.ComponentStateModification)
        val metricUpdateEvent = events[1]
        check(metricUpdateEvent is MdibAccessEvent.MetricStateModification)
        val contextUpdateEvent = events[2]
        check(contextUpdateEvent is MdibAccessEvent.ContextStateModification)

        val checkStates: (List<AbstractStateOneOf>, List<AbstractStateOneOf>, List<AbstractStateOneOf>) -> Unit =
            { componentStates, metricStates, contextStates ->
                val componentState1 = componentStates[0]
                check(componentState1 is AbstractStateOneOf.ChoiceMdsState)
                assertEquals(Handles.MDS_0, componentState1.descriptorHandleString())
                assertEquals(expectedMds0Lang, componentState1.value.langAttr)
                assertEquals(expectedDescriptorVersion, componentState1.descriptorVersion())
                assertEquals(expectedStateVersion, componentState1.stateVersion())

                val componentState2 = componentStates[1]
                check(componentState2 is AbstractStateOneOf.ChoiceMdsState)
                assertEquals(Handles.MDS_1, componentState2.descriptorHandleString())
                assertEquals(expectedMds1Lang, componentState2.value.langAttr)
                assertEquals(expectedDescriptorVersion, componentState2.descriptorVersion())
                assertEquals(expectedStateVersion, componentState2.stateVersion())

                val metricState = metricStates.first()
                check(metricState is AbstractStateOneOf.ChoiceNumericMetricState)
                assertEquals(Handles.METRIC_0, metricState.descriptorHandleString())
                assertEquals(expectedAvgPeriod, metricState.value.activeAveragingPeriodAttr)
                assertEquals(expectedDescriptorVersion, metricState.descriptorVersion())
                assertEquals(expectedStateVersion, metricState.stateVersion())

                val contextState = contextStates.first()
                check(contextState is AbstractStateOneOf.ChoicePatientContextState)
                assertEquals(Handles.CONTEXT_1, contextState.toAbstractContextStateOneOf()!!.handleString())
                assertEquals(expectedDescriptorVersion, contextState.descriptorVersion())
                assertEquals(expectedStateVersion, contextState.stateVersion())
                assertEquals(
                    expectedAssociation,
                    contextState.value.abstractContextState.contextAssociationAttr
                )
            }

        checkStates(
            componentUpdateResult.states.states.map { it.state },
            metricUpdateResult.states.states.map { it.state },
            contextUpdateResult.states.states.map { it.state }
        )
        checkStates(
            componentUpdateEvent.modifiedStates.map { it.state },
            metricUpdateEvent.modifiedStates.map { it.state },
            contextUpdateEvent.modifiedStates.map { it.state }
        )

        val mdsEntity1 = mdibAccess.anyEntity(Handles.MDS_0).getOrNull()
        check(mdsEntity1 is MdibEntity.Mds)
        assertEquals(Handles.MDS_0, mdsEntity1.state.descriptorHandleString())
        assertEquals(expectedMds0Lang, mdsEntity1.state.langAttr)
        assertEquals(expectedDescriptorVersion, mdsEntity1.descriptor.descriptorVersion())
        assertEquals(expectedDescriptorVersion, mdsEntity1.state.descriptorVersion())
        assertEquals(expectedStateVersion, mdsEntity1.state.stateVersion())

        val mdsEntity2 = mdibAccess.anyEntity(Handles.MDS_1).getOrNull()
        check(mdsEntity2 is MdibEntity.Mds)
        assertEquals(Handles.MDS_1, mdsEntity2.state.descriptorHandleString())
        assertEquals(expectedMds1Lang, mdsEntity2.state.langAttr)
        assertEquals(expectedDescriptorVersion, mdsEntity2.descriptor.descriptorVersion())
        assertEquals(expectedDescriptorVersion, mdsEntity2.state.descriptorVersion())
        assertEquals(expectedStateVersion, mdsEntity2.state.stateVersion())


        val metricEntity = mdibAccess.anyEntity(Handles.METRIC_0).getOrNull()
        check(metricEntity is MdibEntity.NumericMetric)
        assertEquals(Handles.METRIC_0, metricEntity.state.descriptorHandleString())
        assertEquals(1.hours, metricEntity.state.activeAveragingPeriodAttr)
        assertEquals(expectedDescriptorVersion, metricEntity.descriptor.descriptorVersion())
        assertEquals(expectedDescriptorVersion, metricEntity.state.descriptorVersion())
        assertEquals(expectedStateVersion, metricEntity.state.stateVersion())

        val contextEntity = mdibAccess.anyEntity(Handles.CONTEXTDESCRIPTOR_0).getOrNull()
        check(contextEntity is MdibEntity.PatientContext)
        val contextStateFromEntity = contextEntity.states.first { it.handleString() == Handles.CONTEXT_1 }
        assertEquals(expectedDescriptorVersion, contextEntity.descriptor.descriptorVersion())
        assertEquals(expectedDescriptorVersion, contextStateFromEntity.descriptorVersion())
        assertEquals(expectedStateVersion, contextStateFromEntity.stateVersion())
        assertEquals(
            expectedAssociation,
            contextStateFromEntity.abstractContextState.contextAssociationAttr
        )

        val contextState = mdibAccess.abstractContextState(Handles.CONTEXT_1).getOrNull()
        check(contextState is AbstractContextStateOneOf.ChoicePatientContextState)

        assertEquals(expectedDescriptorVersion, contextState.descriptorVersion())
        assertEquals(expectedStateVersion, contextState.stateVersion())
        assertEquals(
            expectedAssociation,
            contextState.value.abstractContextState.contextAssociationAttr
        )
    }

    @Test
    fun `test reinsert of entities`(): Unit = runBlocking {
        val mdibVersion = MdibVersion.create()
        val mdibAccess = testComponent.createRemoteMdibAccess(mdibVersion)
        val tree = modPresets.fullTree()
        mdibAccess.writeDescription(tree, 0L)

        mdibAccess.writeDescription(
            MdibDescriptionModifications()
                .add(MdibDescriptionModification.Delete(Handles.METRIC_0))
                .add(MdibDescriptionModification.Delete(Handles.CONTEXTDESCRIPTOR_0)), 0L
        )

        val subscription = mdibAccess.subscribe()
        val deferredEvent = async { subscription.receive() }
        val result = mdibAccess.writeDescription(
            MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        DescriptionItem(
                            instances.defaultNumericMetricDescriptor(Handles.METRIC_0),
                            instances.defaultNumericMetricState(Handles.METRIC_0),
                            Handles.CHANNEL_0
                        )
                    )
                )
                .add(
                    MdibDescriptionModification.Insert(
                        DescriptionItem(
                            instances.defaultPatientContextDescriptor(Handles.CONTEXTDESCRIPTOR_0),
                            listOf(
                                instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1)
                            ),
                            Handles.SYSTEMCONTEXT_0
                        )
                    )
                ), 0L
        )

        val event = deferredEvent.await()
        mdibAccess.unsubscribeAll()
        check(event is MdibAccessEvent.DescriptionModification)

        // check reinserted single state entity
        run {
            val entityFromResult = result.entities.inserted[0]
            check(entityFromResult is MdibEntity.NumericMetric)
            assertEquals(0L, entityFromResult.descriptor.descriptorVersion())
            assertEquals(0L, entityFromResult.state.descriptorVersion())
            assertEquals(0L, entityFromResult.state.stateVersion())

            val entityFromEvent = event.entities.inserted[0]
            check(entityFromEvent is MdibEntity.NumericMetric)
            assertEquals(0L, entityFromEvent.descriptor.descriptorVersion())
            assertEquals(0L, entityFromEvent.state.descriptorVersion())
            assertEquals(0L, entityFromEvent.state.stateVersion())

            val entityFromMdibAccess = mdibAccess.anyEntity(Handles.METRIC_0).getOrNull()
            check(entityFromMdibAccess is MdibEntity.NumericMetric)
            assertEquals(0L, entityFromMdibAccess.descriptor.descriptorVersion())
            assertEquals(0L, entityFromMdibAccess.state.descriptorVersion())
            assertEquals(0L, entityFromMdibAccess.state.stateVersion())
        }

        // check reinserted context entity
        run {
            val entityFromResult = result.entities.inserted[1]
            check(entityFromResult is MdibEntity.PatientContext)
            assertEquals(0L, entityFromResult.descriptor.descriptorVersion())
            assertEquals(1, entityFromResult.states.size)
            assertEquals(0L, entityFromResult.states.first().descriptorVersion())
            assertEquals(0L, entityFromResult.states.first().stateVersion())

            val entityFromEvent = event.entities.inserted[1]
            check(entityFromEvent is MdibEntity.PatientContext)
            assertEquals(0L, entityFromEvent.descriptor.descriptorVersion())
            assertEquals(1, entityFromEvent.states.size)
            assertEquals(0L, entityFromEvent.states.first().descriptorVersion())
            assertEquals(0L, entityFromEvent.states.first().stateVersion())

            val entityFromMdibAccess = mdibAccess.anyEntity(Handles.CONTEXTDESCRIPTOR_0).getOrNull()
            check(entityFromMdibAccess is MdibEntity.PatientContext)
            assertEquals(0L, entityFromMdibAccess.descriptor.descriptorVersion())
            assertEquals(1, entityFromMdibAccess.states.size)
            assertEquals(0L, entityFromMdibAccess.states.first().descriptorVersion())
            assertEquals(0L, entityFromMdibAccess.states.first().stateVersion())
        }
    }

    @Test
    fun `test update of only a subset of states of a context entity`(): Unit = runBlocking {
        val mdibVersion = MdibVersion.create()
        val mdibAccess = testComponent.createRemoteMdibAccess(mdibVersion)
        val tree = modPresets.fullTree()
        mdibAccess.writeDescription(tree, 0L)

        val subscription = mdibAccess.subscribe()
        val deferredEvent = async { subscription.receive() }
        val result = mdibAccess.writeStates(
            MdibStateModifications.Context(
                ContextStates().add(
                    instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1)
                )
            ), 0L
        )

        val event = deferredEvent.await()
        mdibAccess.unsubscribeAll()
        check(event is MdibAccessEvent.ContextStateModification)

        val statesFromResult = result.states
        check(statesFromResult is WrittenMdibStateModifications.Context)
        assertEquals(1, statesFromResult.states.size)
        assertEquals(0L, statesFromResult.states.first().state.descriptorVersion())
        assertEquals(0L, statesFromResult.states.first().state.stateVersion())

        val statesFromEvent = event.modifiedContextStates
        assertEquals(1, statesFromEvent.size)
        assertEquals(0L, statesFromEvent.first().state.descriptorVersion())
        assertEquals(0L, statesFromEvent.first().state.stateVersion())

        val statesFromMdibAccess = mdibAccess.anyEntity(Handles.CONTEXTDESCRIPTOR_0).getOrNull()
        check(statesFromMdibAccess is MdibEntity.PatientContext)
        assertEquals(0L, statesFromMdibAccess.descriptor.descriptorVersion())
        assertEquals(2, statesFromMdibAccess.states.size)
        assertEquals(0L, statesFromMdibAccess.states[0].descriptorVersion())
        assertEquals(0L, statesFromMdibAccess.states[0].stateVersion())
        assertEquals(0L, statesFromMdibAccess.states[1].descriptorVersion())
        assertEquals(0L, statesFromMdibAccess.states[1].stateVersion())
    }

    @Test
    fun `test deletion and reinsertion of context state`(): Unit = runBlocking {
        val mdibVersion = MdibVersion.create()
        val mdibAccess = testComponent.createRemoteMdibAccess(mdibVersion)
        val tree = modPresets.fullTree()
        mdibAccess.writeDescription(tree, 0L)

        val subscription = mdibAccess.subscribe()

        // first run: delete
        run {
            val deferredEvent = async { subscription.receive() }

            val result = mdibAccess.writeStates(
                MdibStateModifications.Context(
                    ContextStates().add(
                        instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1).let {
                            it.copy(
                                abstractContextState = it.abstractContextState.copy(
                                    contextAssociationAttr = ContextAssociation(
                                        ContextAssociation.EnumType.No
                                    )
                                )
                            )
                        }
                    )
                ), 0L)

            val statesFromResult = result.states
            check(statesFromResult is WrittenMdibStateModifications.Context)
            assertEquals(1, statesFromResult.states.size)
            assertEquals(0L, statesFromResult.states.first().state.descriptorVersion())
            assertEquals(0L, statesFromResult.states.first().state.stateVersion())
            assertEquals(
                ContextAssociation.EnumType.No,
                statesFromResult.contextStates.first().contextState.contextAssociation()
            )

            val event = deferredEvent.await()
            check(event is MdibAccessEvent.ContextStateModification)
            val statesFromEvent = event.modifiedContextStates
            assertEquals(1, statesFromEvent.size)
            assertEquals(0L, statesFromEvent.first().state.descriptorVersion())
            assertEquals(0L, statesFromEvent.first().state.stateVersion())
            assertEquals(
                ContextAssociation.EnumType.No,
                statesFromResult.contextStates.first().contextState.contextAssociation()
            )

            // check that state has been removed from storage
            assertNull(mdibAccess.abstractContextState(Handles.CONTEXT_1).getOrNull())
            val contextStates = mdibAccess.anyEntity(Handles.CONTEXTDESCRIPTOR_0).getOrNull()?.resolveContextStates()
                ?: fail("Context not found")
            assertEquals(1, contextStates.size)
            assertEquals(Handles.CONTEXT_0, contextStates.first().handleString())
        }

        // second run: insert again
        run {
            val deferredEvent = async { subscription.receive() }

            val result = mdibAccess.writeStates(
                MdibStateModifications.Context(
                    ContextStates().add(
                        instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1).let {
                            it.copy(
                                abstractContextState = it.abstractContextState.copy(
                                    contextAssociationAttr = ContextAssociation(
                                        ContextAssociation.EnumType.Pre
                                    )
                                )
                            )
                        }
                    )
                ), 0L)

            val statesFromResult = result.states
            check(statesFromResult is WrittenMdibStateModifications.Context)
            assertEquals(1, statesFromResult.states.size)
            assertEquals(0L, statesFromResult.states.first().state.descriptorVersion())
            assertEquals(0L, statesFromResult.states.first().state.stateVersion())
            assertEquals(
                ContextAssociation.EnumType.Pre,
                statesFromResult.contextStates.first().contextState.contextAssociation()
            )

            val event = deferredEvent.await()
            check(event is MdibAccessEvent.ContextStateModification)
            val statesFromEvent = event.modifiedContextStates
            assertEquals(1, statesFromEvent.size)
            assertEquals(0L, statesFromEvent.first().state.descriptorVersion())
            assertEquals(0L, statesFromEvent.first().state.stateVersion())
            assertEquals(
                ContextAssociation.EnumType.Pre,
                statesFromResult.contextStates.first().contextState.contextAssociation()
            )

            // check that state has been added to storage
            val addedState = mdibAccess.abstractContextState(Handles.CONTEXT_1).getOrNull()
            check(addedState is AbstractContextStateOneOf.ChoicePatientContextState)
            assertEquals(0L, addedState.descriptorVersion())
            assertEquals(0L, addedState.stateVersion())
            assertEquals(
                ContextAssociation.EnumType.Pre,
                addedState.contextAssociation()
            )

            val contextStates = mdibAccess.anyEntity(Handles.CONTEXTDESCRIPTOR_0).getOrNull()?.resolveContextStates()
                ?: fail("Context not found")
            assertEquals(2, contextStates.size)
            assertEquals(Handles.CONTEXT_0, contextStates[0].handleString())
            assertEquals(Handles.CONTEXT_1, contextStates[1].handleString())
        }
    }

    @Test
    fun `fail on insertion of existing handles`(): Unit = runBlocking {
        run {
            val mdibVersion = MdibVersion.create()
            val mdibAccess =
                testComponent.createRemoteMdibAccess(mdibVersion)
            val tree = modPresets.fullTree()
            mdibAccess.writeDescription(tree, 0L)

            try {
                val metrics = MdibDescriptionModifications().add(
                    MdibDescriptionModification.Insert(
                        DescriptionItem(
                            instances.defaultNumericMetricDescriptor(Handles.METRIC_0),
                            instances.defaultNumericMetricState(Handles.METRIC_0),
                            Handles.CHANNEL_0
                        )
                    )
                )
                mdibAccess.writeDescription(metrics, 0L)
            } catch (e: PreprocessingException) {
            }
        }
        run {
            val mdibVersion = MdibVersion.create()
            val mdibAccess =
                testComponent.createRemoteMdibAccess(mdibVersion)
            val tree = modPresets.fullTree()
            mdibAccess.writeDescription(tree, 0L)

            try {
                val contexts = MdibDescriptionModifications().add(
                    MdibDescriptionModification.Insert(
                        DescriptionItem(
                            instances.defaultPatientContextDescriptor(Handles.CONTEXTDESCRIPTOR_0),
                            listOf(),
                            Handles.SYSTEMCONTEXT_0
                        )
                    )
                )
                mdibAccess.writeDescription(contexts, 0L)
            } catch (_: PreprocessingException) {
            }
        }
    }

    @Test
    fun `fail on update of unknown entity`(): Unit = runBlocking {
        run {
            val mdibVersion = MdibVersion.create()
            val mdibAccess =
                testComponent.createRemoteMdibAccess(mdibVersion)
            val tree = modPresets.fullTree()
            mdibAccess.writeDescription(tree, 0L)

            assertTrue(runCatching {
                val metrics = MdibDescriptionModifications().add(
                    MdibDescriptionModification.Update(
                        DescriptionItem(
                            instances.defaultNumericMetricDescriptor(Handles.METRIC_3),
                            instances.defaultNumericMetricState(Handles.METRIC_3),
                            Handles.CHANNEL_0
                        )
                    )
                )
                mdibAccess.writeDescription(metrics, 0L)
            }.isFailure)
        }
        run {
            val mdibVersion = MdibVersion.create()
            val mdibAccess =
                testComponent.createRemoteMdibAccess(mdibVersion)
            val tree = modPresets.fullTree()
            mdibAccess.writeDescription(tree, 0L)

            assertTrue(runCatching {
                val contexts = MdibDescriptionModifications().add(
                    MdibDescriptionModification.Update(
                        DescriptionItem(
                            instances.defaultWorkflowContextDescriptor(Handles.CONTEXTDESCRIPTOR_3),
                            listOf(),
                            Handles.SYSTEMCONTEXT_0
                        )
                    )
                )
                mdibAccess.writeDescription(contexts, 0L)
            }.isFailure)
        }
    }

    @Test
    fun `fail on wrong insertion order`(): Unit = runBlocking {
        val mdibVersion = MdibVersion.create()
        val mdibAccess = testComponent.createRemoteMdibAccess(mdibVersion)

        assertTrue(runCatching {
            val modifications = MdibDescriptionModifications().add(
                MdibDescriptionModification.Insert(
                    DescriptionItem(
                        instances.defaultVmdDescriptor(Handles.VMD_0),
                        instances.defaultVmdState(Handles.VMD_0),
                        Handles.MDS_0
                    )
                )
            ).add(
                MdibDescriptionModification.Insert(
                    DescriptionItem(
                        instances.defaultMdsDescriptor(Handles.MDS_0),
                        instances.defaultMdsState(Handles.MDS_0)
                    )
                )
            )
            mdibAccess.writeDescription(modifications, 0L)
        }.isFailure)
    }

    @Test
    fun `fail on handle duplication`(): Unit = runBlocking {
        try {
            val mdibVersion = MdibVersion.create()
            val mdibAccess =
                testComponent.createRemoteMdibAccess(mdibVersion)
            val modifications = MdibDescriptionModifications().add(
                MdibDescriptionModification.Insert(
                    DescriptionItem(
                        instances.defaultMdsDescriptor(Handles.MDS_0),
                        instances.defaultMdsState(Handles.MDS_0)
                    )
                )
            ).add(
                MdibDescriptionModification.Insert(
                    DescriptionItem(
                        instances.defaultMdsDescriptor(Handles.MDS_0),
                        instances.defaultMdsState(Handles.MDS_0)
                    )
                )
            )
            mdibAccess.writeDescription(modifications, 0L)
        } catch (e: IllegalArgumentException) {
            // illegal argument as error is caught during modification set insertion already
        }

        try {
            val mdibVersion = MdibVersion.create()
            val mdibAccess =
                testComponent.createRemoteMdibAccess(mdibVersion)
            val modifications1 = MdibDescriptionModifications().add(
                MdibDescriptionModification.Insert(
                    DescriptionItem(
                        instances.defaultMdsDescriptor(Handles.MDS_0),
                        instances.defaultMdsState(Handles.MDS_0)
                    )
                )
            )
            mdibAccess.writeDescription(modifications1, 0L)
            val modifications2 = MdibDescriptionModifications().add(
                MdibDescriptionModification.Insert(
                    DescriptionItem(
                        instances.defaultMdsDescriptor(Handles.MDS_0),
                        instances.defaultMdsState(Handles.MDS_0)
                    )
                )
            )
            mdibAccess.writeDescription(modifications2, 0L)
        } catch (e: PreprocessingException) {
        }
    }
}