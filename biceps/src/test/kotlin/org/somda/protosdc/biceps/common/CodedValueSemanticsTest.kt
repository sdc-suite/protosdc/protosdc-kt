package org.somda.protosdc.biceps.common

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.somda.protosdc.model.biceps.CodeIdentifier
import org.somda.protosdc.model.biceps.CodedValue
import java.lang.NumberFormatException

class CodedValueSemanticsTest {
    @Test
    fun `test failure for non-ieee codes`() {
        val unknownCode = MdibFactory.codedValue("unknown", "http://unknown")
        assertFalse(unknownCode.isSemanticallyEqual(unknownCode))
    }

    @Test
    fun `test failure for ieee codes`() {
        val code = "12345"
        val semanticallyUnequalIeeeCodes = listOf(
            Pair(cv(code, null, "1"), cv(code, null, "1")),
            Pair(cv(code), cv(code, null, "1")),
            Pair(cv(code, null, "1"), cv(code)),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(1).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(2).toUri().toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(2).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(1).toUri().toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(2).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString(), "1"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString(), "1"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString(), "2")
            ),
        )

        semanticallyUnequalIeeeCodes.forEach {
            assertFalse(it.first.isSemanticallyEqual(it.second)) {
                "$it"
            }
        }

        val invalidCode = "1234a"
        Pair(
            cv(invalidCode, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(1).toUri().toString()),
            cv(invalidCode, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(1).toUri().toString())
        ).let {
            assertThrows<NumberFormatException> {
                it.first.isSemanticallyEqual(it.second)
            }
        }
    }

    @Test
    fun `test success for ieee codes`() {
        val code = "12345"
        val semanticallyEqualIeeeCodes = listOf(
            Pair(cv(code), cv(code)),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(1).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(1).toUri().toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_HL7_OID.appendArc(2).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_HL7_OID.appendArc(2).toUri().toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_LEGACY_OID.appendArc(2).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_LEGACY_OID.appendArc(2).toUri().toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_LEGACY_OID.appendArc(2).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_HL7_OID.appendArc(2).toUri().toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(2).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_HL7_OID.appendArc(2).toUri().toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.appendArc(2).toUri().toString()),
                cv(code, NomenclatureConstants.IEEE_11073_10101_LEGACY_OID.appendArc(2).toUri().toString())
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_LEGACY_OID.toUri().toString(), "2"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_HL7_OID.toUri().toString(), "2")
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.toUri().toString(), "2"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_HL7_OID.toUri().toString(), "2")
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.toUri().toString(), "2"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_LEGACY_OID.toUri().toString(), "2")
            ),
            Pair(
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString(), "1"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_URN.toString(), "1")
            ),
            Pair(
                cv(code, "urn:oid:1.2.840.10004.1.1.1.0.0", "1"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_LEGACY_OID.toUri().toString(), "1"),
            ),
            Pair(
                cv(code, "urn:oid:2.16.840.1.113883.6.24.1.1.1.0.0", "1"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_HL7_OID.toUri().toString(), "1"),
            ),
            Pair(
                cv(code, "urn:oid:1.3.111.2.11073.10101", "1"),
                cv(code, NomenclatureConstants.IEEE_11073_10101_OID.toUri().toString(), "1"),
            )
        )

        semanticallyEqualIeeeCodes.forEach {
            assertTrue(it.first.isSemanticallyEqual(it.second)) {
                "$it"
            }
        }
    }

    @Test
    fun `test translations`() {
        val translations1 = listOf(
            CodedValue.Translation(codeAttr = CodeIdentifier("12345")),
            CodedValue.Translation(codeAttr = CodeIdentifier("67890"))
        )

        val translations2 = listOf(
            CodedValue.Translation(codeAttr = CodeIdentifier("12345")),
            CodedValue.Translation(codeAttr = CodeIdentifier("23456"))
        )

        val translations3 = listOf(
            CodedValue.Translation(codeAttr = CodeIdentifier("34567")),
            CodedValue.Translation(codeAttr = CodeIdentifier("45678"))
        )

        val code1 = CodedValue(
            codeAttr = CodeIdentifier("unknown"),
            codingSystemAttr = "http://unknown",
            translation = translations1
        )

        val code2 = CodedValue(
            codeAttr = CodeIdentifier("unknown"),
            codingSystemAttr = "http://unknown",
            translation = translations2
        )

        val code3 = CodedValue(
            codeAttr = CodeIdentifier("unknown"),
            codingSystemAttr = "http://unknown",
            translation = translations3
        )

        assertTrue(code1.isSemanticallyEqual(code2))
        assertFalse(code1.isSemanticallyEqual(code3))
    }

    private fun cv(code: String, codingSystem: String? = null, codingSystemVersion: String? = null) =
        MdibFactory.codedValue(code, codingSystem, codingSystemVersion)
}