package org.somda.protosdc.biceps.common

import org.somda.protosdc.biceps.common.storage.DefaultInstantiation
import org.somda.protosdc.biceps.test.Handles

class MdibModificationsPresets {

    companion object {
        const val MDS_0_SOURCE_LAST_INDEX = 22
        const val MDS_1_SOURCE_LAST_INDEX = 24

        val instances = DefaultInstantiation()
    }

    /*
        MDS_0
          ALERTSYSTEM_0
            ALERTCONDITION_0
            ALERTCONDITION_1
            ALERTSIGNAL_0
            ALERTSIGNAL_1
          VMD_0
            ALERTSYSTEM_1
            CHANNEL_0
              METRIC_0
              METRIC_1
            CHANNEL_1
          VMD_1
          SYSTEMCONTEXT_0
            CONTEXTDESCRIPTOR_0
              CONTEXT_0
              CONTEXT_1
            CONTEXTDESCRIPTOR_1
              CONTEXT_2
            CONTEXTDESCRIPTOR_2
              CONTEXT_3
          SCO_0
            OPERATION_0
            OPERATION_1
          CLOCK_0
          BATTERY_0
          BATTERY_1
        MDS_1
     */
    fun fullTree() = MdibDescriptionModifications().insertAll(descriptionItems())

    private fun descriptionItems() = listOf(
        DescriptionItem(
            instances.defaultMdsDescriptor(Handles.MDS_0),
            instances.defaultMdsState(Handles.MDS_0)
        ),
        DescriptionItem(
            instances.defaultAlertSystemDescriptor(Handles.ALERTSYSTEM_0),
            instances.defaultAlertSystemState(Handles.ALERTSYSTEM_0), Handles.MDS_0
        ),
        DescriptionItem(
            instances.defaultAlertConditionDescriptor(Handles.ALERTCONDITION_0),
            instances.defaultAlertConditionState(Handles.ALERTCONDITION_0), Handles.ALERTSYSTEM_0
        ),
        DescriptionItem(
            instances.defaultAlertConditionDescriptor(Handles.ALERTCONDITION_1),
            instances.defaultAlertConditionState(Handles.ALERTCONDITION_1), Handles.ALERTSYSTEM_0
        ),
        DescriptionItem(
            instances.defaultAlertSignalDescriptor(Handles.ALERTSIGNAL_0),
            instances.defaultAlertSignalState(Handles.ALERTSIGNAL_0), Handles.ALERTSYSTEM_0
        ),
        DescriptionItem(
            instances.defaultAlertSignalDescriptor(Handles.ALERTSIGNAL_1),
            instances.defaultAlertSignalState(Handles.ALERTSIGNAL_1), Handles.ALERTSYSTEM_0
        ),
        DescriptionItem(
            instances.defaultVmdDescriptor(Handles.VMD_0),
            instances.defaultVmdState(Handles.VMD_0), Handles.MDS_0
        ),
        DescriptionItem(
            instances.defaultAlertSystemDescriptor(Handles.ALERTSYSTEM_1),
            instances.defaultAlertSystemState(Handles.ALERTSYSTEM_1), Handles.VMD_0
        ),
        DescriptionItem(
            instances.defaultChannelDescriptor(Handles.CHANNEL_0),
            instances.defaultChannelState(Handles.CHANNEL_0), Handles.VMD_0
        ),
        DescriptionItem(
            instances.defaultNumericMetricDescriptor(Handles.METRIC_0),
            instances.defaultNumericMetricState(Handles.METRIC_0), Handles.CHANNEL_0
        ),
        DescriptionItem(
            instances.defaultStringMetricDescriptor(Handles.METRIC_1),
            instances.defaultStringMetricState(Handles.METRIC_1), Handles.CHANNEL_0
        ),
        DescriptionItem(
            instances.defaultChannelDescriptor(Handles.CHANNEL_1),
            instances.defaultChannelState(Handles.CHANNEL_1), Handles.VMD_0
        ),
        DescriptionItem(
            instances.defaultVmdDescriptor(Handles.VMD_1),
            instances.defaultVmdState(Handles.VMD_1), Handles.MDS_0
        ),
        DescriptionItem(
            instances.defaultSystemContextDescriptor(Handles.SYSTEMCONTEXT_0),
            instances.defaultSystemContextState(Handles.SYSTEMCONTEXT_0), Handles.MDS_0
        ),
        DescriptionItem(
            instances.defaultPatientContextDescriptor(Handles.CONTEXTDESCRIPTOR_0),
            listOf(
                instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_0),
                instances.defaultPatientContextState(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_1)
            ), Handles.SYSTEMCONTEXT_0
        ),
        DescriptionItem(
            instances.defaultEnsembleContextDescriptor(Handles.CONTEXTDESCRIPTOR_1),
            listOf(
                instances.defaultEnsembleContextState(Handles.CONTEXTDESCRIPTOR_1, Handles.CONTEXT_2)
            ), Handles.SYSTEMCONTEXT_0
        ),
        DescriptionItem(
            instances.defaultEnsembleContextDescriptor(Handles.CONTEXTDESCRIPTOR_2),
            listOf(
                instances.defaultEnsembleContextState(Handles.CONTEXTDESCRIPTOR_2, Handles.CONTEXT_3)
            ), Handles.SYSTEMCONTEXT_0
        ),
        DescriptionItem(
            instances.defaultScoDescriptor(Handles.SCO_0),
            instances.defaultScoState(Handles.SCO_0), Handles.MDS_0
        ),
        DescriptionItem(
            instances.defaultActivateOperationDescriptor(Handles.OPERATION_0),
            instances.defaultActivateOperationState(Handles.OPERATION_0), Handles.SCO_0
        ),
        DescriptionItem(
            instances.defaultSetStringOperationDescriptor(Handles.OPERATION_1),
            instances.defaultSetStringOperationState(Handles.OPERATION_1), Handles.SCO_0
        ),
        DescriptionItem(
            instances.defaultClockDescriptor(Handles.CLOCK_0),
            instances.defaultClockState(Handles.CLOCK_0), Handles.MDS_0
        ),
        DescriptionItem(
            instances.defaultBatteryDescriptor(Handles.BATTERY_0),
            instances.defaultBatteryState(Handles.BATTERY_0), Handles.MDS_0
        ),
        DescriptionItem(
            instances.defaultClockDescriptor(Handles.BATTERY_1),
            instances.defaultClockState(Handles.BATTERY_1), Handles.MDS_0
        ),
        DescriptionItem(
            instances.defaultMdsDescriptor(Handles.MDS_1),
            instances.defaultMdsState(Handles.MDS_1)
        ),
        DescriptionItem(
            instances.defaultVmdDescriptor(Handles.VMD_2),
            instances.defaultVmdState(Handles.VMD_2), Handles.MDS_1
        )
    )
}