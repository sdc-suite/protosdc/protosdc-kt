package org.somda.protosdc.common

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class ConfigTest {
    private val testMap = mapOf(
        "foo" to "test",
        "bar" to 100
    )

    @Test
    fun `test default values when configuration map is empty`() {
        val config = Config()
        assertEquals("test", config("foo") { "test" })
        assertEquals(100, config("bar") { 100 })
    }

    @Test
    fun `test values when configuration map is not empty`() {
        val config = Config(testMap)
        assertEquals("test", config("foo") { "test" })
        assertEquals(100, config("bar") { 100 })
        assertEquals(300.0, config("non-existent") { 300.0 })
    }

    @Test
    fun `test invalid type`() {
        val config = Config(testMap)
        assertEquals("test", config("foo") { "test" })

        assertThrows<IllegalArgumentException> {
            // as config() is an inline function, it's required to add some boilerplate code to not end up with
            // an unused expression warning; @SuppressWarnings("unused") didn't work here
            val configValue = config("foo") { 100 }

            // this should never happen unless the test didn't pass
            println(configValue)
        }
    }
}