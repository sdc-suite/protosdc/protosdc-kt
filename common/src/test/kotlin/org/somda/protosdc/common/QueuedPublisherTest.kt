package org.somda.protosdc.common

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class QueuedPublisherTest {
    private val expectedData = (1..10).map { "$it" }.toCollection(mutableListOf())

    @Test
    fun `test publishing to one subscriber`() {
        val publisher = create()
        val subscription = publisher.subscribe()
        expectedData.forEach(publisher::publish)
        publisher.unsubscribeAll()
        assertEquals(expectedData, subscription.toCollection(mutableListOf()))
    }

    @Test
    fun `test publishing to many subscribers`() {
        val publisher = create()

        val subscriptions = (1..5).map { publisher.subscribe() }.toCollection(mutableListOf())

        expectedData.forEach(publisher::publish)
        publisher.unsubscribeAll()

        subscriptions.forEach {
            assertEquals(expectedData, it.toCollection(mutableListOf()))
        }
    }

    @Test
    fun `test queue overflow`() {
        val queueSize = expectedData.size / 2
        val publisher = create(queueSize)

        val subscription = publisher.subscribe()
        expectedData.forEach(publisher::publish)

        assertThrows<IllegalStateException> {
            subscription.toCollection(mutableListOf())
        }
    }

    @Test
    fun `test queue subscriber end`() {
        val publisher = create()

        val subscription = publisher.subscribeWithId()
        expectedData.forEach(publisher::publish)

        val updatesUntilUnsubscribe = expectedData.size / 2

        val iterator = subscription.second.iterator()
        var receivedUpdates = 0
        for (i in 1..expectedData.size) {
            if (iterator.hasNext()) {
                iterator.next()
                receivedUpdates++
                if (i == updatesUntilUnsubscribe) {
                    publisher.unsubscribe(subscription.first)
                }
            } else {
                break
            }
        }

        // check that the unsubscribe has been applied immediately (such that no more queued items were delivered after
        // the unsubscribe call)
        assertEquals(updatesUntilUnsubscribe, receivedUpdates)

        // check that publishing further data does not end up as incoming data at the existing sequence
        publisher.publish("foobar")
        assertFalse(iterator.hasNext())
    }

    private fun create(size: Int = 1000) = QueuedPublisher("test-publisher", String::class.java, size)
}