package org.somda.protosdc.common

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.toCollection
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class ChannelPublisherTest {
    private val expectedData = (1..10).map { "$it" }.toCollection(mutableListOf())

    private val channelSizeForOffer = 1000

    @Test
    fun `test publishing to one subscriber by using offer`(): Unit = runBlocking {
        val publisher = create(channelSizeForOffer)
        val subscription = publisher.subscribe()
        expectedData.forEach { publisher.offer(it) }
        publisher.unsubscribeAll()
        assertEquals(expectedData, subscription.receiveAsFlow().toCollection(mutableListOf()))
    }

    @Test
    fun `test publishing to many subscribers by using offer`(): Unit = runBlocking {
        val publisher = create(channelSizeForOffer)

        val subscriptions = (1..5).map { publisher.subscribe() }.toCollection(mutableListOf())

        expectedData.forEach { publisher.offer(it) }
        publisher.unsubscribeAll()


        subscriptions.forEach {
            assertEquals(expectedData, it.receiveAsFlow().toCollection(mutableListOf()))
        }
    }

    @Test
    fun `test publishing to one subscriber by using send`(): Unit = runBlocking {
        val publisher = create(Channel.RENDEZVOUS)
        val subscription = publisher.subscribe()
        val senderJob = launch {
            expectedData.forEach { publisher.send(it) }
            publisher.unsubscribeAll()
        }

        val receiverJob = launch {
            assertEquals(expectedData, subscription.receiveAsFlow().toCollection(mutableListOf()))
        }

        senderJob.join()
        receiverJob.join()
    }

    @Test
    fun `test publishing to many subscribers by using send`(): Unit = runBlocking {
        val publisher = create(Channel.RENDEZVOUS)

        val subscriptions = (1..5).map { publisher.subscribe() }

        val senderJob = launch {
            expectedData.forEach { publisher.send(it) }
            publisher.unsubscribeAll()
        }

        val receiverJobs = subscriptions.map {
            launch {
                assertEquals(expectedData, it.receiveAsFlow().toCollection(mutableListOf()))
            }
        }

        senderJob.join()
        receiverJobs.forEach { it.join() }
    }

    @Test
    fun `test queue overflow`(): Unit = runBlocking {
        val queueSize = expectedData.size / 2
        val publisher = create(queueSize)

        val subscription = publisher.subscribe()
        expectedData.forEach { publisher.offer(it) }


        assertThrows<ChannelPublisher.OverflowException> {
            subscription.receiveAsFlow().toCollection(mutableListOf())
        }
    }

    @Test
    fun `test queue subscriber end by using offer`(): Unit = runBlocking {
        val publisher = create(channelSizeForOffer)

        val subscription = publisher.subscribe()
        expectedData.forEach { publisher.offer(it) }

        val updatesUntilUnsubscribe = expectedData.size / 2

        assertEquals(1, publisher.allSubscriptions().size)

        var receivedUpdates = 0
        val iterator = subscription.iterator()
        try {
            for (i in 1..expectedData.size) {
                if (iterator.hasNext()) {
                    iterator.next()
                    receivedUpdates++
                    if (i == updatesUntilUnsubscribe) {
                        subscription.cancel()
                    }
                } else {
                    break
                }
            }
        } catch (e: CancellationException) {
            // will be thrown once the subscription is cancelled and iterator.hasNext() is called
        }

        // check that the unsubscribe has been applied immediately (such that no more queued items were delivered after
        // the unsubscribe call)
        assertEquals(updatesUntilUnsubscribe, receivedUpdates)

        // check that publishing further data does not end up as incoming data to the existing sequence
        publisher.offer("foobar")
        assertThrows<CancellationException> { iterator.hasNext() }
        assertTrue(publisher.allSubscriptions().isEmpty())
    }

    @Test
    fun `test queue subscriber end by using send`(): Unit = runBlocking {
        val publisher = create(Channel.RENDEZVOUS)

        val subscription = publisher.subscribe()
        assertEquals(1, publisher.allSubscriptions().size)

        val senderJob = launch {
            expectedData.forEach { publisher.send(it) }
            publisher.unsubscribeAll()
        }

        val updatesUntilUnsubscribe = expectedData.size / 2

        var receivedUpdates = 0
        val receiverJob = launch {
            val iterator = subscription.iterator()
            try {
                for (i in 1..expectedData.size) {
                    if (iterator.hasNext()) {
                        iterator.next()
                        receivedUpdates++
                        if (i == updatesUntilUnsubscribe) {
                            subscription.cancel()
                        }
                    } else {
                        break
                    }
                }
            } catch (e: CancellationException) {
                // will be thrown once the subscription is cancelled and iterator.hasNext() is called
            }

            // check that publishing further data does not end up as incoming data to the existing sequence
            val subJob = launch {
                publisher.offer("foobar")
            }
            assertThrows<CancellationException> { iterator.hasNext() }
            subJob.join()
        }

        senderJob.join()
        receiverJob.join()

        // check that the unsubscribe has been applied immediately (such that no more queued items were delivered after
        // the unsubscribe call)
        assertEquals(updatesUntilUnsubscribe, receivedUpdates)
        assertTrue(publisher.allSubscriptions().isEmpty())
    }

    private fun create(size: Int) = ChannelPublisher<String>("test-publisher", size)
}