package org.somda.protosdc.common

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

class ServiceDelegateTest {
    @Test
    fun `test normal lifecycle`(): Unit = runBlocking {
        val delegate = ServiceDelegateImpl()
        var startUpCalled = false
        var shutDownCalled = false

        delegate.onStartUp {
            startUpCalled = true
            assertFalse(delegate.isRunning())
            assertEquals(Service.State.STARTUP, delegate.state())
        }
        delegate.onShutDown {
            shutDownCalled = true
            assertFalse(delegate.isRunning())
            assertEquals(Service.State.SHUTDOWN, delegate.state())
        }

        assertFalse(delegate.isRunning())
        assertEquals(Service.State.NEW, delegate.state())
        assertFalse(startUpCalled)
        delegate.start()
        assertTrue(startUpCalled)
        assertTrue(delegate.isRunning())

        assertEquals(Service.State.RUNNING, delegate.state())

        assertFalse(shutDownCalled)
        delegate.stop()
        assertFalse(delegate.isRunning())
        assertTrue(shutDownCalled)

        assertEquals(Service.State.TERMINATED, delegate.state())
    }

    @Test
    fun `test failed lifecycle`(): Unit = runBlocking {
        val delegate = ServiceDelegateImpl()

        val msg = UUID.randomUUID().toString()

        delegate.onStartUp {
            throw IllegalStateException(msg)
        }

        try {
            delegate.start()
            fail()
        } catch (e: IllegalStateException) {
            assertEquals(msg, e.message)
            assertFalse(delegate.isRunning())
            assertEquals(Service.State.FAILED, delegate.state())
        }
    }
}