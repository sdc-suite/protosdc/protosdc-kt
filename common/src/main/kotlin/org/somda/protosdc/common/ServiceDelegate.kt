package org.somda.protosdc.common

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

/**
 * Used to implement a service delegate for which start-up and shut-down code can be specified by the delegating class.
 */
public interface ServiceDelegate : Service {
    public fun onStartUp(doStartUp: suspend () -> Unit)

    public fun onShutDown(doShutDown: suspend () -> Unit)
}

/**
 * Default service implementation to be used as a delegate in service classes.
 *
 * Intended use:
 *
 * ```
 * class AService: ServiceDelegate by ServiceDelegateImpl() {
 *     init {
 *         onStartUp {
 *             // ...
 *         }
 *
 *         onShutDown {
 *             // ...
 *         }
 *     }
 * }
 * ```
 */
public open class ServiceDelegateImpl : ServiceDelegate {
    private var state = Service.State.NEW
    private var startUp: suspend () -> Unit = {}
    private var shutDown: suspend () -> Unit = {}
    private val mutex = Mutex()

    private var startUpSet = false
    private var shutDownSet = false

    override fun onStartUp(doStartUp: suspend () -> Unit) {
        if (!startUpSet) {
            startUpSet = true
            startUp = doStartUp
        } else {
            throw Exception("Called ServiceDelegate::onStartUp twice")
        }
    }

    override fun onShutDown(doShutDown: suspend () -> Unit) {
        if (!shutDownSet) {
            shutDownSet = true
            shutDown = doShutDown
        } else {
            throw Exception("Called ServiceDelegate::onShutDown twice")
        }
    }

    final override suspend fun start() {
        mutex.withLock {
            check(state == Service.State.NEW) {
                "Service can only be started once but start() has been called multiple times"
            }
            state = Service.State.STARTUP
        }

        try {
            startUp()
        } catch (e: Exception) {
            mutex.withLock {
                state = Service.State.FAILED
            }
            throw e
        }

        mutex.withLock {
            state = Service.State.RUNNING
        }
    }

    final override suspend fun stop() {
        mutex.withLock {
            check(state == Service.State.RUNNING) {
                "Service is not running but can only be stopped if running"
            }
            state = Service.State.SHUTDOWN
        }

        try {
            shutDown()
        } catch (e: Exception) {
            mutex.withLock {
                state = Service.State.FAILED
            }
            throw e
        }

        mutex.withLock {
            state = Service.State.TERMINATED
        }
    }

    final override suspend fun isRunning(): Boolean = mutex.withLock { state == Service.State.RUNNING }

    final override suspend fun state(): Service.State = mutex.withLock { state }
}