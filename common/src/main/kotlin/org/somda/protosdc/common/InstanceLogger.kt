package org.somda.protosdc.common

import org.apache.logging.log4j.CloseableThreadContext
import org.apache.logging.log4j.kotlin.KotlinLogger
import org.apache.logging.log4j.kotlin.loggerOf
import org.apache.logging.log4j.spi.ExtendedLogger
import java.lang.reflect.InvocationHandler
import java.lang.reflect.Method
import java.lang.reflect.Proxy
import kotlin.reflect.KProperty

/**
 * Logger which adds instance specific information to every log message using the CloseableThreadContext.
 *
 * @param loggerVariables Additional information that will be printed to the log output. Note that the key of the pairs
 * must match a custom identifier in the log format pattern.
 */
public class InstanceLogger(vararg loggerVariables: Pair<String, String>) {
    private val loggerVariables = loggerVariables.associate { it.first to it.second }

    public companion object {
        /**
         * Wraps a logger into a proxy which adds context information to all messages.
         *
         * @param logger The logger to wrap.
         * @param loggerVariables The custom variables that are made available to the logger.
         * @return Wrapped logger instance.
         */
        public fun wrap(logger: KotlinLogger, loggerVariables: Map<String, String>): KotlinLogger = KotlinLogger(
            Proxy.newProxyInstance(
                InstanceLogger::class.java.classLoader, arrayOf<Class<*>>(ExtendedLogger::class.java),
                InstanceLoggerInvocationHandler(logger.delegate, loggerVariables)
            ) as ExtendedLogger
        )

        private class InstanceLoggerInvocationHandler(
            private val logger: ExtendedLogger,
            private val loggerVariables: Map<String, String>
        ) : InvocationHandler {
            @Throws(Throwable::class)
            override fun invoke(proxy: Any, method: Method, args: Array<Any>): Any? {
                CloseableThreadContext
                    .putAll(loggerVariables.entries.associate { it.key to it.value })
                    .use {
                        return method.invoke(logger, *args)
                    }
            }
        }
    }

    /**
     * Property to initialize an instance-id featured logger.
     *
     * Use as follows:
     *
     * ```kotlin
     * class Foo {
     *     private val logger by InstanceLogger("my-instance-id")
     * }
     * ```
     */
    public operator fun getValue(thisRef: Any, property: KProperty<*>): KotlinLogger =
        wrap(loggerOf(thisRef.javaClass), loggerVariables)

}