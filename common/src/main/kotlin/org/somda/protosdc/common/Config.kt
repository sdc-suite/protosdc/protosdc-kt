package org.somda.protosdc.common

import javax.inject.Qualifier

/**
 * Annotation to be used by Dagger to identify component configuration.
 */
@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
public annotation class ComponentConfig

/**
 * Provision of configuration values.
 *
 * @param configurationMap a map to get configuration values from. Configuration values can be of any type.
 */
public class Config constructor(configurationMap: Map<String, Any>?) {
    public val configurationMap: Map<String, Any> = configurationMap ?: mapOf()

    /**
     * Instantiation with an empty configuration.
     *
     * An empty configuration always falls back to default values.
     */
    public constructor() : this(null)

    /**
     * A derived class can call this function to retrieve a configuration value of a specific type.
     *
     * @param key          the configuration key.
     * @param defaultValue the reified default value. Default values are mandatory to provide type information and
     *                     erase the need for a populated configuration map.
     * @return the configuration value if a value exists for *key*, otherwise *defaultValue*.
     * @throws IllegalArgumentException if the value found for *key* does not match the type of *defaultValue*.
     */
    @Throws(IllegalArgumentException::class)
    public inline operator fun <reified T> invoke(key: String, defaultValue: () -> T): T = defaultValue().let { defaultVal ->
        return@let configurationMap[key]?.let { value ->
            when (value is T) {
                true -> value
                false -> throw IllegalArgumentException(
                    "Type for key '%s' unexpected. Expected '%s', but found '%s'.".format(
                        key,
                        defaultVal?.let { it::class.java.toString() } ?: "<unknown>",
                        value::class::qualifiedName.get()
                    )
                )
            }
        } ?: defaultVal
    }
}