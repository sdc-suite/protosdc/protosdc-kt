package org.somda.protosdc.common

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withTimeout
import org.apache.logging.log4j.kotlin.Logging
import java.util.concurrent.CancellationException
import java.util.concurrent.atomic.AtomicInteger

/**
 * Publish-subscribe implementation with coroutine support based on [Channel] instances.
 *
 * The [ChannelPublisher] allows to distribute data to multiple subscribers.
 * Subscriptions are implemented as [Channel] objects.
 *
 * [ChannelPublisher] is implemented for an async environment, utilizing coroutine mutexes against concurrent
 * subscriptions modifications.
 *
 *
 * @param name that facilitates identification of this instance.
 * @param capacity either a positive channel capacity or one of the constants defined in [Channel.Factory]. Applied on
 *                 each channel that is created by this instance.
 * @param timeout Timeout after which an exception is thrown during a suspending send call.
 *                If null, a send call will only resume if all suspended channels resume.
 */
public class ChannelPublisher<T : Any>(
    @Stringified
    private val name: String,
    private val capacity: Int = Channel.RENDEZVOUS,
    private val timeout: Long? = null
) {
    private companion object : Logging {
        var subscriptionCount = AtomicInteger()
    }

    private val subscriptions = mutableSetOf<Channel<T>>()
    private val subscriptionsLock = Mutex()

    /**
     * Establishes a subscription to this channel publisher.
     *
     * @return a [Channel] instance representing the subscription. To unsubscribe, invoke
     *
     *         - [Channel.cancel] to remove the subscription from running subscriptions and immediately stop receiving
     *           updates
     *         - [Channel.close] to remove the subscription from running subscriptions and receive remaining items from
     *           the channel buffer
     *
     *         If there is a channel queue overflow, the channel is cancelled by this object using
     *         [Channel.cancel] with an [OverflowException].
     */
    public suspend fun subscribe(name: String = "#${subscriptionCount.getAndIncrement()}"): ReceiveChannel<T> =
        PubChannel<T>(name, capacity).also {
            subscriptionsLock.withLock { subscriptions.add(it) }
            logger.info { "$this: new subscription is running: $it" }
        }

    /**
     * Publishes a data item to all subscribers by immediately adding the data to the receiver channels.
     *
     * If a subscription exceeds its maximum amount of data items that can be held by the underlying channel,
     * the [ChannelPublisher] terminates the affected subscription by cancelling the channel.
     *
     * This function should only be used if the underlying [Channel] instances are not rendezvous channels.
     * Rendezvous channels are supposed to be used if an event needs to be fully processed before continuing with
     * distribution of subsequent data objects (see [ChannelPublisher.send]).
     *
     * @param data to distribute.
     */
    public suspend fun offer(data: T) {
        subscriptionsLock.withLock { subscriptions.toSet() }.forEach {
            logger.debug { "$this: offering new data to $it" }
            logger.trace { "$this: offering new data to $it: $data" }
            try {
                when (it.trySend(data).isSuccess) {
                    false -> {
                        logger.warn("$this: closing subscription due to a queue overflow: $it")
                        subscriptionsLock.withLock { subscriptions.remove(it) }
                        it.cancel(OverflowException(this, it))
                    }

                    else -> Unit
                }
            } catch (e: SubscriptionEndedException) {
                logger.trace { "$this: exit send() iteration for $it as publisher unsubscribed all" }
            } catch (e: CancellationException) {
                subscriptionsLock.withLock { subscriptions.remove(it) }
                logger.debug("$this: subscription $it has ended already; removed from running subscriptions")
            }
        }
    }

    /**
     * Publishes a data item to all subscribers by suspending at each receiver.
     *
     * If a subscription exceeds its maximum amount of data items that can be held by the underlying channel,
     * the [ChannelPublisher] waits until the affected subscription is cancelled or the next data item is pulled.
     *
     * This function is best suited if the [ChannelPublisher] instantiates rendezvous channels under the hood. The
     * slowest link in the chain sets the pace.
     *
     * @param data to distribute.
     */
    public suspend fun send(data: T) {
        subscriptionsLock.withLock { subscriptions.toSet() }.forEach {
            logger.debug { "$this: sending new data to $it" }
            logger.trace { "$this: sending new data to $it: $data" }
            try {
                checkTimeout {
                    it.send(data)
                }
            } catch (e: SubscriptionEndedException) {
                logger.trace { "$this: exit send() iteration for $it as publisher unsubscribed all" }
            } catch (e: Exception) {
                logger.debug { "$this: error at subscription $it; remove from running subscriptions: ${e.message}" }
                subscriptionsLock.withLock { subscriptions.remove(it) }
            }
        }
    }

    /**
     * Finishes all running subscriptions by cancelling all channels.
     */
    public suspend fun unsubscribeAll() {
        logger.info { "$this: finishing all running subscriptions" }

        val subscriptionsCopy = subscriptionsLock.withLock { subscriptions.toSet().also { subscriptions.clear() } }
        subscriptionsCopy.forEach {
            // only cancel if closing failed (e.g. due to a suspending send call or congestion)
            if (!it.close()) {
                it.cancel(SubscriptionEndedException(this, it))
            }
        }
    }

    public override fun toString(): String = stringify()

    internal suspend fun allSubscriptions() = subscriptionsLock.withLock { subscriptions.toSet() }

    /**
     * Thrown when subscription has ended due to queue overflow, i.e. data wasn't processed by the receiver fast enough
     * while distributed via [ChannelPublisher.offer].
     */
    public class OverflowException(publisher: ChannelPublisher<*>, channel: Channel<*>) :
        CancellationException("$publisher: overflow on subscription: $channel")

    /**
     * Thrown when subscription has ended due to an [ChannelPublisher.unsubscribeAll].
     */
    public class SubscriptionEndedException(publisher: ChannelPublisher<*>, channel: Channel<*>) :
        CancellationException("$publisher: subscription ended for $channel")

    private suspend fun checkTimeout(block: suspend () -> Unit) {
        timeout?.let {
            withTimeout(it) {
                block()
            }
        } ?: block()
    }

    private class PubChannel<T>(private val name: String, capacity: Int) : Channel<T> by Channel(capacity) {
        override fun toString() = "Subscription($name)"
    }
}
