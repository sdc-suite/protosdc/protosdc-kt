package org.somda.protosdc.common

/**
 * A mutable list delegator that prevents adding or removing elements.
 *
 * @param list the list for which adding and removing shall be hidden.
 */
public class ConstSizeMutableList<T>(private val list: MutableList<T>) : List<T> {
    override val size: Int
        get() = list.size

    public override fun contains(element: T): Boolean = list.contains(element)

    public override fun containsAll(elements: Collection<T>): Boolean = list.containsAll(elements)

    public override operator fun get(index: Int): T = list[index]

    public override fun indexOf(element: T): Int = list.indexOf(element)

    public override fun isEmpty(): Boolean = list.isEmpty()

    public override fun iterator(): Iterator<T> = list.iterator()

    public override fun lastIndexOf(element: T): Int = list.lastIndexOf(element)

    public override fun listIterator(): ListIterator<T> = list.listIterator()

    public override fun listIterator(index: Int): ListIterator<T> = list.listIterator(index)

    public operator fun set(index: Int, element: T): T = list.set(index, element)

    public override fun subList(fromIndex: Int, toIndex: Int): ConstSizeMutableList<T> =
        ConstSizeMutableList(list.subList(fromIndex, toIndex))
}