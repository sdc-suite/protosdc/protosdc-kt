package org.somda.protosdc.common

import org.apache.logging.log4j.kotlin.Logging
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

/**
 * Publish-subscribe implementation with sequence support.
 *
 * The [QueuedPublisher] allows to distribute data to multiple subscribers.
 * Subscriptions are implemented as [Sequence] objects.
 *
 * *The [QueuedPublisher] class is not thread-safe!*
 *
 * @param name that facilitates identification of this instance.
 * @param type of the data that this instance distributes.
 * @param queueSize the maximum amount of data items not taken by a subscriber
 *                  until an [IllegalStateException] is thrown.
 */
public class QueuedPublisher<T>(
    @Stringified
    private val name: String,
    @Stringified
    private val type: Class<T>,
    @Stringified
    private val queueSize: Int
): QueuedObservable<T> {
    /**
     * Strong-typed subscription identifier.
     *
     * @param value a subscription value, represented by a UUID - leave empty to generate a random one.
     */
    public data class SubscriptionId(val value: UUID = UUID.randomUUID())

    private companion object : Logging

    // buffer string representation
    private val stringified = this.stringify()

    // internal representation of a queue item to allow for shutdown and error transmission
    private sealed class QueueItem {
        data class Next<V>(val data: V) : QueueItem()
        object Overflow : QueueItem()
        object End : QueueItem()
    }

    private val subscriptions = mutableMapOf<SubscriptionId, BlockingQueue<QueueItem>>()

    override fun subscribe(): Sequence<T> {
        return subscribeWithId().second
    }

    override fun subscribeWithId(): Pair<SubscriptionId, Sequence<T>> {
        val subscriptionId = SubscriptionId()
        val queue = LinkedBlockingQueue<QueueItem>(queueSize)
        subscriptions[subscriptionId] = queue

        logger.debug { "New QueuedPublisher subscription with ID '$subscriptionId' is running on $this" }

        return Pair(subscriptionId, sequence {
            while (true) {
                when (val item = queue.take()) {
                    is QueueItem.Next<*> ->
                        yield(type.cast(item.data))
                    is QueueItem.Overflow -> throw IllegalStateException("Queue overflow at ${queue.size} elements")
                    is QueueItem.End -> break
                }
            }
            logger.debug { "QueuedPublisher subscription with ID '$subscriptionId' has ended" }
        })
    }

    /**
     * Publishes a data item to all subscribers.
     *
     * If a subscription exceeds its maximum amount of data items that can be held by the underlying subscription queue,
     * the [QueuedPublisher] terminates the affected subscription. The overflow becomes perceivable at the consuming
     * sequence in form of an [IllegalStateException].
     *
     * @param data to distribute.
     */
    public fun publish(data: T) {
        logger.debug { "$this: publishing new data: $data" }
        subscriptions.filter {
            when (it.value.offer(QueueItem.Next(data))) {
                false -> {
                    logger.warn("$this: close subscription for ID '${it.key}' due to a queue overflow")
                    true
                }
                else -> false
            }
        }.map { it.key }.toCollection(mutableListOf()).forEach(::shutdownSubscriptionToOverflow)
    }

    /**
     * Gracefully finishes all running subscriptions, i.e. ends the sequences.
     */
    public fun unsubscribeAll() {
        logger.info("$this: finishing all running subscriptions")

        // clear before offering the end item to make sure nothing else will be published in the meantime
        val subscriptionsCopy = subscriptions.toMap()
        subscriptions.clear()
        subscriptionsCopy.forEach {
            it.value.offer(QueueItem.End)
        }
    }

    override fun unsubscribe(subscriptionId: SubscriptionId) {
        subscriptions.remove(subscriptionId)?.let {
            logger.info("$this: finishing subscription for ID '$subscriptionId'")
            it.clear()
            it.offer(QueueItem.End)
        }
    }

    private fun shutdownSubscriptionToOverflow(subscriptionId: SubscriptionId) {
        val queue = subscriptions[subscriptionId]
        require(queue != null) { "$this: try to shutdown non-existing subscription for ID '$subscriptionId'" }
        logger.debug { "$this: shutdown subscription for ID '$subscriptionId' as queue exceeded its maximum capacity" }
        subscriptions.remove(subscriptionId)
        queue.clear()
        queue.offer(QueueItem.Overflow)
    }

    override fun toString(): String = stringified
}
