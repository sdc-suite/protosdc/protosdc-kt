package org.somda.protosdc.common

public data class DeviceId(val value: String) {
    public companion object {
        public const val KEY: String = "deviceId"
        public operator fun get(value: String): Pair<String, String> = Pair(KEY, value)
    }

    public operator fun invoke(): Pair<String, String> = Pair(KEY, value)
}

public data class InstanceId(val value: String) {
    public companion object {
        public const val KEY: String = "instanceId"
        public operator fun get(value: String): Pair<String, String> = Pair(KEY, value)
    }

    public operator fun invoke(): Pair<String, String> = Pair(KEY, value)
}