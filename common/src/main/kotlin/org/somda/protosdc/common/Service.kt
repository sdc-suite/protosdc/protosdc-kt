package org.somda.protosdc.common

/**
 * Simple service interface to allow for starting and stopping of async classes.
 */
public interface Service {
    /**
     * State of a service.
     */
    public enum class State {
        /**
         * Initial state after service instantiation.
         */
        NEW,

        /**
         * State when [Service.start] was called and the startup phase has not finished yet.
         */
        STARTUP,

        /**
         * State after startup phase ended and before shutdown is initiated. The service is up and running.
         */
        RUNNING,

        /**
         * State when [Service.stop] was called and the shutdown phased has not finished yet
         */
        SHUTDOWN,

        /**
         * State when shutdown phase ended. End of lifecycle.
         */
        TERMINATED,

        /**
         * State when an exception is thrown while starting or stopping of the service. End of lifecycle.
         */
        FAILED
    }

    public suspend fun start()
    public suspend fun stop()
    public suspend fun isRunning(): Boolean
    public suspend fun state(): State
}

/**
 * Checks if this service is running.
 *
 * Silently passes if the service is running.
 *
 * @throws IllegalStateException if the service is not running.
 */
public suspend fun Service.checkRunning() {
    check(isRunning()) { "Requested function at $this is not running. Current state: ${state()}" }
}

/**
 * Checks if this service is running while accepting an action to be performed if the service is running.
 *
 * @throws IllegalStateException if the service is not running.
 */
public suspend inline fun <T> Service.checkRunning(action: () -> T): T {
    checkRunning()
    return action()
}