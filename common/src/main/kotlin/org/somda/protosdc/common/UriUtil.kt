package org.somda.protosdc.common

import java.net.URI
import java.util.*
import kotlin.text.RegexOption.IGNORE_CASE

public fun String.toUri(): URI = URI(this)

private val regex =
    """^uuid:([0-9a-f]{8}\b-[0-9a-f]{4}\b-[0-9a-f]{4}\b-[0-9a-f]{4}\b-[0-9a-f]{12})$""".toRegex(IGNORE_CASE)

public fun URI.toUuid(): UUID? {
    if (this.scheme.lowercase() != "urn") {
        return null
    }

    return regex.find(this.schemeSpecificPart)?.let { match ->
        runCatching {
            match.groupValues[1]
        }.getOrNull()?.let {
            UUID.fromString(it)
        }
    }
}


/**
 * Utility functions to build URIs.
 */
public object UriUtil {
    /**
     * Creates a random UUID as URI.
     *
     * @return the generated URI.
     */
    public fun randomUuid(): URI = URI.create("urn:uuid:${UUID.randomUUID()}")
}
