package org.somda.protosdc.common

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.withTimeout
import kotlin.time.Duration

/**
 * Simple condition-like suspendable signalling class using a [Channel] internally.
 */
public class WakeUpSignal {
    private val channel = Channel<Unit>(1)

    /**
     * Waits *waitTime* for the signal to be triggered.
     *
     * @param waitTime time to wait until [kotlinx.coroutines.TimeoutCancellationException] is thrown.
     */
    public suspend fun awaitSignal(waitTime: Duration) {
        withTimeout(waitTime.inWholeMilliseconds) { channel.receive() }
    }

    /**
     * Sends a signal such that [awaitSignal] returns from suspension.
     */
    public fun sendSignal() {
        channel.trySend(Unit)
    }
}