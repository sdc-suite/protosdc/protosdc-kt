package org.somda.protosdc.network.ip

import java.net.InetAddress

/**
 * Enumeration of available IP versions.
 */
public enum class IpVersion {
    IP_V4,
    IP_V6;

    /**
     * Creates an [IpHeader] instance.
     *
     * The function does not validate the address formats.
     *
     * @param sourceAddress the source address of the IP header.
     * @param destinationAddress the destination address of the IP header.
     * @return the IP header in the right IP version depending on the IP address of this object.
     */
    public fun header(sourceAddress: InetAddress, destinationAddress: InetAddress): IpHeader = when (this) {
        IP_V4 -> IpHeader.IpV4(sourceAddress, destinationAddress)
        IP_V6 -> IpHeader.IpV6(sourceAddress, destinationAddress)
    }

}