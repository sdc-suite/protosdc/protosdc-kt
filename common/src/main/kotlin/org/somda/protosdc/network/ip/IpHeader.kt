package org.somda.protosdc.network.ip

import java.net.InetAddress

/**
 * Representation of an IP header including source and destination address.
 */
public sealed interface IpHeader {
    public fun sourceAddress(): InetAddress
    public fun destinationAddress(): InetAddress

    public data class IpV4(val sourceAddress: InetAddress, val destinationAddress: InetAddress) : IpHeader {
        override fun sourceAddress(): InetAddress = sourceAddress
        override fun destinationAddress(): InetAddress = destinationAddress
    }

    public data class IpV6(val sourceAddress: InetAddress, val destinationAddress: InetAddress) : IpHeader {
        override fun sourceAddress(): InetAddress = sourceAddress
        override fun destinationAddress(): InetAddress = destinationAddress
    }
}