package org.somda.protosdc.network.ip

import java.net.InetAddress
import java.net.NetworkInterface

/**
 * Finds the first [InetAddress] of a given IP version.
 *
 * @param ipVersion the IP version to seek.
 * @return first address found on this adapter for the given *ipVersion* or null if none exists.
 */
public fun NetworkInterface.firstAddressFor(ipVersion: IpVersion): InetAddress? {
    val inetAddressIterator = this.inetAddresses.iterator()
    while (inetAddressIterator.hasNext()) {
        val nextAddress = inetAddressIterator.next()
        if (nextAddress.ipVersion() == ipVersion) {
            return nextAddress
        }
    }
    return null
}