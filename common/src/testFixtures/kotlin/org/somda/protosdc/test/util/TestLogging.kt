package org.somda.protosdc.test.util

import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.Filter
import org.apache.logging.log4j.core.appender.ConsoleAppender
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration

/**
 * Utility class with static logging configuration for tests.
 */
object TestLogging {
    private const val CONTEXT_INSTANCE_ID = "instanceId"
    private const val DEVICE_ID = "deviceId"
    private const val CUSTOM_PATTERN = ("%d{HH:mm:ss.SSS}"
            + " [%thread]" // only include the space if we have a variable
            + " %notEmpty{[$CONTEXT_INSTANCE_ID=%.4X{$CONTEXT_INSTANCE_ID}] }"
            + " %notEmpty{[$DEVICE_ID=%.4X{$DEVICE_ID}] }"
            + "%-5level"
            + " %logger{36}"
            + " - %msg%n")

    // reduce log noise for the loggers listed here
    private val CHATTY_LOGGERS = listOf<String>()

    /**
     * Configures a default logging valid for all tests.
     */
    fun configure() {
        if (CiDetector.isRunningInCi) {
            Configurator.initialize(ciConfiguration())
        } else {
            // no file appender when not running in ci
            Configurator.initialize(localConfig(Level.DEBUG))
        }
    }

    private fun ciConfiguration(): BuiltConfiguration {
        val builder = ConfigurationBuilderFactory.newConfigurationBuilder()
        builder.setStatusLevel(Level.ERROR)
        builder.setConfigurationName("CiLogging")
        val layoutBuilder = builder
            .newLayout("PatternLayout")
            .addAttribute("pattern", CUSTOM_PATTERN)
        val rootLogger = builder.newRootLogger(Level.TRACE)
        run {
            // create a console appender
            val appenderBuilder = builder
                .newAppender("console_logger", ConsoleAppender.PLUGIN_NAME)
                .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
            appenderBuilder.add(layoutBuilder)
            // only log WARN or worse to console
            appenderBuilder.addComponent(
                builder.newFilter("ThresholdFilter", Filter.Result.ACCEPT, Filter.Result.DENY)
                    .addAttribute("level", Level.WARN)
            )
            builder.add(appenderBuilder)
            rootLogger.add(builder.newAppenderRef(appenderBuilder.name))
        }
        run {
            // create a file appender
            val appenderBuilder = builder.newAppender("file", "File")
                .addAttribute("fileName", "target/test.log")
                .addAttribute("append", true)
                .add(layoutBuilder)
            builder.add(appenderBuilder)
            rootLogger.add(builder.newAppenderRef(appenderBuilder.name))
        }
        run {
            // quiet down chatty loggers
            CHATTY_LOGGERS.forEach { logger ->
                builder.add(
                    builder.newLogger(logger, Level.INFO)
                        .addAttribute("additivity", true)
                )
            }
        }
        builder.add(rootLogger)
        return builder.build()
    }

    private fun localConfig(consoleLevel: Level): BuiltConfiguration {
        val builder = ConfigurationBuilderFactory.newConfigurationBuilder()
        builder.setStatusLevel(Level.ERROR)
        builder.setConfigurationName("LocalLogging")
        val layoutBuilder = builder
            .newLayout("PatternLayout")
            .addAttribute("pattern", CUSTOM_PATTERN)
        val rootLogger = builder.newRootLogger(Level.DEBUG)
        run {

            // create a console appender
            val appenderBuilder = builder
                .newAppender("console_logger", ConsoleAppender.PLUGIN_NAME)
                .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
            appenderBuilder.add(layoutBuilder)
            // only log WARN or worse to console
            appenderBuilder.addComponent(
                builder.newFilter("ThresholdFilter", Filter.Result.ACCEPT, Filter.Result.DENY)
                    .addAttribute("level", consoleLevel)
            )
            builder.add(appenderBuilder)
            rootLogger.add(builder.newAppenderRef(appenderBuilder.name))
        }
        run {
            // quiet down chatty loggers
            CHATTY_LOGGERS.forEach { logger ->
                builder.add(
                    builder.newLogger(logger, Level.INFO)
                        .addAttribute("additivity", true)
                )
            }
        }
        builder.add(rootLogger)
        return builder.build()
    }
}