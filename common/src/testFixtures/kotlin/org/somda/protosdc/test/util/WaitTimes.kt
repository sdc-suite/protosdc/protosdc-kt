package org.somda.protosdc.test.util

import java.time.Duration

/**
 * Default wait time for those tests that should pass "quickly".
 */
object DefaultWait {
    val DURATION: Duration = Duration.ofSeconds(10)
    val SECONDS = DURATION.toSeconds()
    val MILLIS = DURATION.toMillis()
}

/**
 * Long wait time for those tests that take longer due to, e.g. UDP multicast socket ramp up (esp. in CIs).
 */
object LongWait {
    val DURATION: Duration = Duration.ofMinutes(5)
    val SECONDS = DURATION.toSeconds()
    val MILLIS = DURATION.toMillis()
}