package org.somda.protosdc.test.util

/**
 * Determines if a software test is running in the continuous integration job or under other conditions (typically locally).
 */
object CiDetector {
    val isRunningInCi: Boolean
        get() = System.getenv().containsKey("CI") || System.getenv().containsKey("GITLAB_CI")
}