package org.somda.protosdc.test.util

import org.apache.logging.log4j.kotlin.Logging
import java.io.IOException
import java.net.*
import java.util.stream.Collectors

/**
 * Utility class to find network interfaces that support multicast.
 */
object NetworkInterfaceWithMulticastSupport : Logging {
    private const val PORT = 50000
    private const val SEND_PORT = 0

    /**
     * Seeks a network interface given an IPv4 or IPv6 address.
     *
     * @param address the address of the network interface to search for multicast support.
     * @return a network interface with multicast support or false if none found.
     */
    fun findFor(address: String): NetworkInterface? {
        logger.info { "Try to find network interface that is capable of sending multicast traffic for on $address" }
        val interfaces = NetworkInterface.networkInterfaces().collect(Collectors.toList())
        logger.info { "Checking interfaces $interfaces"}
        for (ni in NetworkInterface.networkInterfaces()) {
            try {
                if (!ni.supportsMulticast()) {
                    // don't try at all if the JVM claims there is no multicast
                    logger.info { "Network interface $ni does not claim multicast support - try next" }
                    continue
                }
                if (!ni.isUp) {
                    logger.info { "Network interface $ni not up - try next" }
                    continue
                }
                if (ni.isPointToPoint) {
                    continue
                }
            } catch (e: IOException) {
                logger.info { "Network interface $ni not suitable to send multicast on $address - try next" }
                continue
            }

            if (ni.inetAddresses.toList().isEmpty()) {
                logger.info { "Network interface $ni does not have IP addresses - try next" }
                continue
            }

            var inetAddress = InetAddress.getByName(address)
            val adapterAddress = ni.inetAddresses.toList().firstOrNull { it.javaClass == inetAddress.javaClass }
            if (adapterAddress == null) {
                logger.info { "Network interface $ni does not have a matching IP address version - try next" }
                continue
            }

            // add ipv6 scopes
            if (inetAddress is Inet6Address) {
                inetAddress = Inet6Address.getByAddress(inetAddress.hostAddress, inetAddress.address, ni)
            }

            val socketAddr = InetSocketAddress(inetAddress, PORT)

            val sendSocket = DatagramSocket(InetSocketAddress(adapterAddress, SEND_PORT))
            sendSocket.reuseAddress = true
            val multicastSocket = MulticastSocket(PORT)
            multicastSocket.networkInterface = ni
            try {
                multicastSocket.joinGroup(socketAddr, ni)
                logger.info { "Joined UDP multicast address group $socketAddr; try to send data" }

                val data = "network-interface-test".toByteArray()
                sendSocket.send(DatagramPacket(data, data.size, inetAddress, PORT))
                logger.info { "Found working network interface: $ni" }
                return ni
            } catch (e: IOException) {
                logger.info(e) { "Network interface $ni not suitable to send multicast on $address - try next" }
            } finally {
                multicastSocket.leaveGroup(socketAddr, ni)
            }
        }
        return null
    }
}