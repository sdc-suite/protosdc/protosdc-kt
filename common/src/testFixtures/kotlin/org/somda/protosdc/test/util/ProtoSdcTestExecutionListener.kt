package org.somda.protosdc.test.util

import org.apache.logging.log4j.kotlin.Logging
import org.junit.platform.engine.TestExecutionResult
import org.junit.platform.launcher.TestExecutionListener
import org.junit.platform.launcher.TestIdentifier

/**
 * Listener to enable logging for tests and log test start and stop events.
 */
class ProtoSdcTestExecutionListener : TestExecutionListener {
    companion object : Logging {
        init {
            TestLogging.configure()
        }
    }

    override fun executionSkipped(testIdentifier: TestIdentifier?, reason: String?) {
        onTest(testIdentifier) { testId ->
            reason?.let { reason ->
                logger.info { "" }
                logger.info { "Skip test ${testId.uniqueId}: $reason" }
                logger.info { "" }
            }
        }
    }

    override fun executionStarted(testIdentifier: TestIdentifier?) {
        onTest(testIdentifier) { testId ->
            logger.info { "" }
            logger.info { "Run test ${testId.uniqueId}" }
            logger.info { "" }
        }
    }

    override fun executionFinished(testIdentifier: TestIdentifier?, testExecutionResult: TestExecutionResult?) {
        onTest(testIdentifier) { testId ->
            testExecutionResult?.let { executionResult ->
                logger.info { "" }
                logger.info {
                    "Test %s has %s".format(
                        testId.uniqueId, when (executionResult.status) {
                            TestExecutionResult.Status.SUCCESSFUL -> "been successful"
                            TestExecutionResult.Status.ABORTED -> "has been aborted"
                            TestExecutionResult.Status.FAILED -> "has failed"
                            null -> "unknown result as status is null"
                        }
                    )
                }
                logger.info { "" }
            }
        }
    }

    private fun onTest(testIdentifier: TestIdentifier?, call: (testIdentifier: TestIdentifier) -> Unit) {
        testIdentifier?.let {
            if (it.isTest) {
                call(it)
            }
        }
    }
}