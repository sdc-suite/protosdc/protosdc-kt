package org.somda.protosdc.crypto

import org.apache.logging.log4j.kotlin.Logging
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo
import java.io.*
import java.security.*
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.TrustManager

/**
 * Loads keys and trust stores from [CryptoStore].
 */
public object CryptoUtil : Logging {

    public fun loadKeyStore(cryptoStore: CryptoStore?): KeyManagerFactory? =
        when (cryptoStore == null) {
            true -> null
            false -> {
                val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
                keyStore.load(
                    ByteArrayInputStream(cryptoStore.keyStoreBytes),
                    cryptoStore.trustStorePassword.toCharArray()
                )
                KeyManagerFactory.getInstance("SunX509").apply {
                    init(keyStore, cryptoStore.keyStorePassword.toCharArray())
                }
            }
        }

    public fun loadTrustStore(cryptoStore: CryptoStore?): TrustManager? =
        when (cryptoStore == null) {
            true -> null
            false -> {
                val trustStore = KeyStore.getInstance(KeyStore.getDefaultType())
                trustStore.load(
                    ByteArrayInputStream(cryptoStore.trustStoreBytes),
                    cryptoStore.trustStorePassword.toCharArray()
                )
                ClientTrustManager(trustStore)
            }
        }


    private fun keysFromPemStreams(
        userKeyStream: InputStream,
        userCertStream: InputStream,
        caCertStream: InputStream,
        userKeyPassword: String
    ): Result<CryptoKeys> = runCatching {
        Security.addProvider(BouncyCastleProvider())
        val userKeyFile = userKeyStream.readBytes()
        val userCertFile = userCertStream.readBytes()
        val caCertFile = caCertStream.readBytes()
        val certificateFactory = CertificateFactory.getInstance("X.509")
        CryptoKeys(
            userKey = readPrivateKey(userKeyFile, userKeyPassword).getOrThrow(),
            userCert = certificateFactory.generateCertificate(ByteArrayInputStream(userCertFile)) as X509Certificate,
            caCert = ByteArrayInputStream(caCertFile).let { bis ->
                mutableListOf<X509Certificate>().apply {
                    while (bis.available() > 0) {
                        add(certificateFactory.generateCertificate(bis) as X509Certificate)
                    }
                }
            }
        )
    }

    private fun readPrivateKey(key: ByteArray, password: String): Result<PrivateKey> = runCatching {
        val pemKey = PEMParser(BufferedReader(InputStreamReader(ByteArrayInputStream(key)))).use {
            it.readObject() as PKCS8EncryptedPrivateKeyInfo
        }
        val pkcs8Prov = JceOpenSSLPKCS8DecryptorProviderBuilder().build(password.toCharArray())
        val converter = JcaPEMKeyConverter().setProvider(BouncyCastleProvider())
        val decrypted = pemKey.decryptPrivateKeyInfo(pkcs8Prov)
        converter.getPrivateKey(decrypted)
    }

    /**
     * Loads [CryptoStore] from PEM files.
     *
     * todo verify function: https://gitlab.com/sdc-suite/protosdc-kt/-/issues/17
     *
     * @param userKeyStream path to PEM file containing the private key.
     * @param userCertStream path to PEM file containing the public key.
     * @param caCertStream path to PEM file containing trusted CA certificates.
     * @param userKeyPassword password to decrypt the private key.
     *
     * @return a crypto setting result.
     */
    public fun loadCryptoSettingsFromPemStreams(
        userKeyStream: InputStream,
        userCertStream: InputStream,
        caCertStream: InputStream,
        userKeyPassword: String
    ): Result<CryptoStore> = runCatching {
        val keys = keysFromPemStreams(
            userKeyStream,
            userCertStream,
            caCertStream,
            userKeyPassword
        ).getOrThrow()

        val keyStore: KeyStore
        val trustStore: KeyStore
        try {
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
            keyStore.load(null)
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType())
            trustStore.load(null)
        } catch (e: CertificateException) {
            logger.error("Error creating keystore instance", e)
            throw RuntimeException("Error creating keystore instance", e)
        } catch (e: NoSuchAlgorithmException) {
            logger.error("Error creating keystore instance", e)
            throw RuntimeException("Error creating keystore instance", e)
        } catch (e: IOException) {
            logger.error("Error creating keystore instance", e)
            throw RuntimeException("Error creating keystore instance", e)
        } catch (e: KeyStoreException) {
            logger.error("Error creating keystore instance", e)
            throw RuntimeException("Error creating keystore instance", e)
        }

        try {
            keyStore.setKeyEntry("key", keys.userKey, userKeyPassword.toCharArray(), arrayOf(keys.userCert))
            keys.caCert.withIndex().forEach {
                trustStore.setCertificateEntry("cert${it.index}", it.value)
            }
        } catch (e: KeyStoreException) {
            logger.error("Error loading certificate into keystore instance", e)
            throw RuntimeException("Error loading certificate into keystore instance", e)
        }

        val keyStoreOutputStream = ByteArrayOutputStream()
        val trustStoreOutputStream = ByteArrayOutputStream()
        try {
            keyStore.store(keyStoreOutputStream, userKeyPassword.toCharArray())
            trustStore.store(trustStoreOutputStream, userKeyPassword.toCharArray())
        } catch (e: KeyStoreException) {
            logger.error("Error converting keystore to stream", e)
            throw RuntimeException("Error converting keystore to stream", e)
        } catch (e: IOException) {
            logger.error("Error converting keystore to stream", e)
            throw RuntimeException("Error converting keystore to stream", e)
        } catch (e: NoSuchAlgorithmException) {
            logger.error("Error converting keystore to stream", e)
            throw RuntimeException("Error converting keystore to stream", e)
        } catch (e: CertificateException) {
            logger.error("Error converting keystore to stream", e)
            throw RuntimeException("Error converting keystore to stream", e)
        }

        CryptoStore(
            keyStoreOutputStream.toByteArray(),
            userKeyPassword,
            trustStoreOutputStream.toByteArray(),
            userKeyPassword
        )
    }

    private data class CryptoKeys(
        val userKey: PrivateKey,
        val userCert: X509Certificate,
        val caCert: List<X509Certificate>
    )
}