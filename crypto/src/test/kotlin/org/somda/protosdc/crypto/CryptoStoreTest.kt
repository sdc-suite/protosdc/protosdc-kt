package org.somda.protosdc.crypto

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import java.io.InputStream

class CryptoStoreTest {
    @Test
    fun testMultipleCertsInTrustStore() {
        val password = "dummypass"
        val userKey = loadResource("user_private_key_encrypted.pem")
        val userCert = loadResource("user_certificate_root_signed.pem")
        val trustedCertificates = loadResource("three_trusted_certificates.pem")
        val trusted = CryptoUtil.loadCryptoSettingsFromPemStreams(
            userKey,
            userCert,
            trustedCertificates,
            password
        ).getOrThrow()

        assertDoesNotThrow {
            requireNotNull(CryptoUtil.loadTrustStore(trusted))
        }
    }

    @Test
    fun testOneCertInTrustStore() {
        val password = "dummypass"
        val userKey = loadResource("user_private_key_encrypted.pem")
        val userCert = loadResource("user_certificate_root_signed.pem")
        val trustedCertificates = loadResource("one_trusted_certificate.pem")
        val trusted = CryptoUtil.loadCryptoSettingsFromPemStreams(
            userKey,
            userCert,
            trustedCertificates,
            password
        ).getOrThrow()

        assertDoesNotThrow {
            requireNotNull(CryptoUtil.loadTrustStore(trusted))
        }
    }

    private fun loadResource(resourcePath: String): InputStream {
        return javaClass.classLoader.getResourceAsStream(resourcePath)
            ?: error("Resource $resourcePath not found")
    }
}