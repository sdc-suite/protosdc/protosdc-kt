package org.protosdc.example.discoproxy

import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.kotlin.Logging
import org.protosdc.example.common.ProtoSdcApp

fun main(args: Array<String>) = DiscoveryProxy().main(args)

class DiscoveryProxy : ProtoSdcApp(name = "discovery-proxy") {
    companion object : Logging

    override fun runApp() = runBlocking {
        logger.info { "Boot discovery proxy..." }

        val discoveryProxy = protoComponent.createDiscoveryProxy().apply {
            start()
        }

        protoFramework.start()
        protoComponent.start()

        logger.info { "Discovery proxy is running, press Enter to exit application" }
        logger.info { "Discovery proxy URI: ${runBlocking { discoveryProxy.endpointUri() }}" }
        readlnOrNull()
        logger.info { "Received exit signal, shutting down..." }
        discoveryProxy.stop()
        protoComponent.stop()
        protoFramework.stop()
        logger.info { "Application has stopped" }
    }
}