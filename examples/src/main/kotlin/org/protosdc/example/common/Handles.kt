package org.protosdc.example.common

import org.somda.protosdc.model.biceps.Handle

object Handles {
    val MDS = Handle("mds")
    val VMD = Handle("vmd")
    val NUMERIC_CHANNEL = Handle("numeric_channel")
    val RTSA_CHANNEL = Handle("rtsa_channel")
    val NUMERIC_METRIC = Handle("numeric_metric")
    val RTSA_METRIC = Handle("rtsa_metric")
    val ALERT_SYSTEM = Handle("alert_system")
    val SCO = Handle("sco")
    val ACTIVATE_OPERATION = Handle("activate_operation")
    val SET_STRING_OPERATION = Handle("set_string_operation")
    val SET_VALUE_OPERATION = Handle("set_value_operation")
    val SYSTEM_CONTEXT = Handle("system_context")
    val PATIENT_CONTEXT = Handle("patient_context")
    val PATIENT_CONTEXT_STATE = Handle("patient_context_state")
    val LOCATION_CONTEXT = Handle("location_context")
    val LOCATION_CONTEXT_STATE = Handle("location_context_state")
    val ENSEMBLE_CONTEXT = Handle("ensemble_context")

    fun condition(handle: Handle) = Handle("${handle.string}_condition")
    fun signal(handle: Handle) = Handle("${handle.string}_signal")
    fun withIndex(handle: Handle, index: Long) = Handle("${handle.string}_$index")
}