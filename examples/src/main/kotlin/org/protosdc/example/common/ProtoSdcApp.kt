package org.protosdc.example.common

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.int
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.Filter
import org.apache.logging.log4j.core.appender.ConsoleAppender
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration
import org.somda.protosdc.CryptoConfig
import org.somda.protosdc.GrpcConfig
import org.somda.protosdc.ProtoFramework
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.option.EnableAdhocDiscovery
import org.somda.protosdc.option.EnableManagedDiscovery
import org.somda.protosdc.setupProtoFramework
import java.net.InetSocketAddress
import java.net.NetworkInterface
import java.net.URI
import kotlin.system.exitProcess

abstract class ProtoSdcApp(name: String) : CliktCommand(name = name) {
    private companion object {
        const val CONTEXT_INSTANCE_ID = "instanceId"
        const val DEVICE_ID = "deviceId"
        const val CUSTOM_PATTERN = ("%d{HH:mm:ss.SSS}"
                + " [%thread]" // only include the space if we have a variable
                + " %notEmpty{[$CONTEXT_INSTANCE_ID=%.4X{$CONTEXT_INSTANCE_ID}] }"
                + " %notEmpty{[$DEVICE_ID=%.4X{$DEVICE_ID}] }"
                + "%-5level"
                + " %logger{36}"
                + " - %msg%n")
    }

    protected val insecure by option("--insecure", help = "Enable unsecure transmission")
        .flag(default = false)

    protected val userKeyFile by option("--userkey", "-k", help = "Path to private key PEM file")
        .file()
        .validate {
            if (!insecure) {
                require(it.exists()) { "'$it' does not exist." }
                require(it.isFile) { "'$it' is not a file." }
            }
        }

    protected val userCertFile by option("--usercert", "-u", help = "Path to public key PEM file")
        .file()
        .validate {
            if (!insecure) {
                require(it.exists()) { "'$it' does not exist." }
                require(it.isFile) { "'$it' is not a file." }
            }
        }

    protected val caCertFile by option("--cacert", "-c", help = "Path to certificate authority PEM file")
        .file()
        .validate {
            if (!insecure) {
                require(it.exists()) { "'$it' does not exist." }
                require(it.isFile) { "'$it' is not a file." }
            }
        }

    protected val userKeyPassword by option("--userkeypassword", "-w", help = "Private key PEM file password")
        .default("")

    protected val ipAddress by option("--address", "-a", help = "IP address to bind to")
        .default("127.0.0.1")

    protected val port by option("--port", "-p", help = "Port to bind to")
        .int()
        .default(0)

    protected val logLevel by option(
        "--loglevel",
        "-l",
        help = "Define log level: ${Level.values().joinToString(", ")}"
    ).default(Level.INFO.toString()).validate { context ->
        require(
            Level.values().any { it.toString() == context }) {
            "Invalid log level defined: $context. Choose one of ${
                Level.values().joinToString(", ")
            }"
        }
    }

    protected val discoveryProxyAddress by option("--discoveryproxy", help = "Discovery proxy URL")

    protected lateinit var socketAddress: InetSocketAddress

    protected lateinit var networkInterface: NetworkInterface

    protected lateinit var protoFramework: ProtoFramework

    protected lateinit var protoComponent: ProtoFramework.Component

    override fun run() {
        Configurator.initialize(loggerConfig(Level.values().first { it.toString() == logLevel }))

        val cryptoConfig = if (insecure) {
            null
        } else {
            kotlin.runCatching {
                requireNotNull(userKeyFile) { "User key file needed, but none provided" }
                requireNotNull(userCertFile) { "User cert file needed, but none provided" }
                requireNotNull(caCertFile) { "CA cert file needed, but none provided" }
            }.onFailure {
                System.err.println(it.message)
                exitProcess(1)
            }
            CryptoConfig(
                userKeyFilePath = userKeyFile!!.absolutePath,
                userCertFilePath = userCertFile!!.absolutePath,
                caCertFilePath = caCertFile!!.absolutePath,
                userKeyPassword = userKeyPassword
            )
        }

        val protoSdcConfig = ProtoSdcConfig(
            cryptography = cryptoConfig,
            grpc = GrpcConfig(insecure)
        )

        socketAddress = InetSocketAddress(ipAddress, port)
        networkInterface = NetworkInterface.getByInetAddress(socketAddress.address)

        protoFramework = setupProtoFramework(protoSdcConfig) {
            addOption(EnableAdhocDiscovery(networkInterface))
            discoveryProxyAddress?.also {
                addOption(EnableManagedDiscovery(URI.create(it)))
            }
        }

        protoComponent = protoFramework.createComponentFor(socketAddress)

        runApp()
    }

    private fun loggerConfig(consoleLevel: Level): BuiltConfiguration {
        val builder = ConfigurationBuilderFactory.newConfigurationBuilder()
        builder.setStatusLevel(consoleLevel)
        builder.setConfigurationName("LocalLogging")
        val layoutBuilder = builder
            .newLayout("PatternLayout")
            .addAttribute("pattern", CUSTOM_PATTERN)
        val rootLogger = builder.newRootLogger(consoleLevel)
        run {

            // create a console appender
            val appenderBuilder = builder
                .newAppender("console_logger", ConsoleAppender.PLUGIN_NAME)
                .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
            appenderBuilder.add(layoutBuilder)
            // only log WARN or worse to console
            appenderBuilder.addComponent(
                builder.newFilter("ThresholdFilter", Filter.Result.ACCEPT, Filter.Result.DENY)
                    .addAttribute("level", consoleLevel)
            )
            builder.add(appenderBuilder)
            rootLogger.add(builder.newAppenderRef(appenderBuilder.name))
        }
        builder.add(rootLogger)
        return builder.build()
    }

    abstract fun runApp()
}