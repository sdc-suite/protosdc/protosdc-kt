package org.protosdc.example.provider

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.apache.logging.log4j.kotlin.Logging
import org.protosdc.example.common.Handles
import org.somda.protosdc.biceps.common.*
import org.somda.protosdc.biceps.common.access.WriteDescriptionResult
import org.somda.protosdc.biceps.common.access.WriteStateResult
import org.somda.protosdc.model.biceps.*
import org.somda.protosdc.biceps.common.MdibFactory
import org.somda.protosdc.biceps.provider.access.LocalMdibAccess
import org.somda.protosdc.device.provider.Statistics
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.round
import kotlin.math.sin
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

class ExampleProviderMdib(
    private val mdibAccess: LocalMdibAccess,
    private val config: ProviderMdibConfig,
) {
    private companion object : Logging {
        var selfCheckCount = 0L
    }

    private val sentUpdatesCount = AtomicInteger(0)
    val consumerCount = AtomicInteger(0)

    // todo currently unused due to design problem - reactivate later
    fun statisticsTrigger(): (Statistics) -> Unit = { statistics ->
        consumerCount.set(statistics.consumerCount)
    }

    suspend fun writeDescription(): WriteDescriptionResult {
        val mods = MdibDescriptionModifications().apply {
            insert(
                DescriptionItem(
                    MdibFactory.mdsDescriptor(handleAttr = Handles.MDS),
                    MdibFactory.mdsState(descriptorHandleAttr = Handles.MDS.ref())
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.vmdDescriptor(handleAttr = Handles.VMD),
                    MdibFactory.vmdState(descriptorHandleAttr = Handles.VMD.ref()),
                    Handles.MDS.ref()
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.channelDescriptor(handleAttr = Handles.NUMERIC_CHANNEL),
                    MdibFactory.channelState(descriptorHandleAttr = Handles.NUMERIC_CHANNEL.ref()),
                    Handles.VMD.ref()
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.channelDescriptor(handleAttr = Handles.RTSA_CHANNEL),
                    MdibFactory.channelState(descriptorHandleAttr = Handles.RTSA_CHANNEL.ref()),
                    Handles.VMD.ref()
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.alertSystemDescriptor(
                        handleAttr = Handles.ALERT_SYSTEM,
                        selfCheckPeriodAttr = 2.seconds
                    ),
                    MdibFactory.alertSystemState(
                        descriptorHandleAttr = Handles.ALERT_SYSTEM.ref(),
                        activationStateAttr = AlertActivation(AlertActivation.EnumType.On)
                    ),
                    Handles.MDS.ref()
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.scoDescriptor(
                        handleAttr = Handles.SCO
                    ),
                    MdibFactory.scoState(
                        descriptorHandleAttr = Handles.SCO.ref()
                    ),
                    Handles.MDS.ref()
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.systemContextDescriptor(
                        handleAttr = Handles.SYSTEM_CONTEXT
                    ),
                    MdibFactory.systemContextState(
                        descriptorHandleAttr = Handles.SYSTEM_CONTEXT.ref()
                    ),
                    Handles.MDS.ref()
                )
            )
        }

        insertMetricsAndAlerts(mods)

        insertOperations(mods)

        insertContexts(mods)

        return mdibAccess.writeDescription(mods)
    }

    private fun insertContexts(mods: MdibDescriptionModifications) {
        mods.apply {
            insert(
                DescriptionItem(
                    MdibFactory.patientContextDescriptor(
                        handleAttr = Handles.PATIENT_CONTEXT
                    ),
                    listOf(
                        MdibFactory.patientContextState(
                            descriptorHandleAttr = Handles.PATIENT_CONTEXT.ref(),
                            handleAttr = Handles.PATIENT_CONTEXT_STATE,
                            contextAssociationAttr = ContextAssociation(ContextAssociation.EnumType.Assoc)
                        )
                    ),
                    Handles.SYSTEM_CONTEXT.ref()
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.locationContextDescriptor(
                        handleAttr = Handles.LOCATION_CONTEXT
                    ),
                    listOf(
                        MdibFactory.locationContextState(
                            descriptorHandleAttr = Handles.LOCATION_CONTEXT.ref(),
                            handleAttr = Handles.LOCATION_CONTEXT_STATE,
                            contextAssociationAttr = ContextAssociation(ContextAssociation.EnumType.Assoc),
                            locationDetail = LocationDetail(
                                poCAttr = "PoC1"
                            )
                        )
                    ),
                    Handles.SYSTEM_CONTEXT.ref()
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.ensembleContextDescriptor(
                        handleAttr = Handles.ENSEMBLE_CONTEXT
                    ),
                    listOf(),
                    Handles.SYSTEM_CONTEXT.ref()
                )
            )
        }
    }

    private fun insertOperations(mods: MdibDescriptionModifications) {
        mods.apply {
            insert(
                DescriptionItem(
                    MdibFactory.activateOperationDescriptor(
                        handleAttr = Handles.ACTIVATE_OPERATION,
                        operationTargetAttr = Handles.MDS.ref()
                    ),
                    MdibFactory.activateOperationState(
                        descriptorHandleAttr = Handles.ACTIVATE_OPERATION.ref(),
                        operatingModeAttr = OperatingMode(OperatingMode.EnumType.En)
                    ),
                    Handles.SCO.ref()
                )
            )
            mods.insert(
                DescriptionItem(
                    MdibFactory.setStringOperationDescriptor(
                        handleAttr = Handles.SET_STRING_OPERATION,
                        operationTargetAttr = Handles.MDS.ref()
                    ),
                    MdibFactory.setStringOperationState(
                        descriptorHandleAttr = Handles.SET_STRING_OPERATION.ref(),
                        operatingModeAttr = OperatingMode(OperatingMode.EnumType.En)
                    ),
                    Handles.SCO.ref()
                )
            )
            insert(
                DescriptionItem(
                    MdibFactory.setValueOperationDescriptor(
                        handleAttr = Handles.SET_VALUE_OPERATION,
                        operationTargetAttr = Handles.MDS.ref()
                    ),
                    MdibFactory.setValueOperationState(
                        descriptorHandleAttr = Handles.SET_VALUE_OPERATION.ref(),
                        operatingModeAttr = OperatingMode(OperatingMode.EnumType.En)
                    ),
                    Handles.SCO.ref()
                )
            )
        }
    }

    private fun insertMetricsAndAlerts(mods: MdibDescriptionModifications) {
        for (i in 1..config.numericMetricCount) {
            val nextHandle = Handles.withIndex(Handles.NUMERIC_METRIC, i)
            mods.insert(
                DescriptionItem(
                    MdibFactory.numericMetricDescriptor(
                        handleAttr = nextHandle,
                        metricAvailabilityAttr = MetricAvailability(MetricAvailability.EnumType.Cont),
                        metricCategoryAttr = MetricCategory(MetricCategory.EnumType.Msrmt),
                        determinationPeriodAttr = 1.seconds,
                        resolutionAttr = BigDecimal.valueOf(0.01),
                        unit = MdibFactory.codedValue("262688"),
                        technicalRange = listOf(
                            MdibFactory.range(0.0, 100.0)
                        )
                    ),
                    MdibFactory.numericMetricState(
                        descriptorHandleAttr = nextHandle.ref()
                    ),
                    Handles.NUMERIC_CHANNEL.string
                )
            ).also {
                if (config.alertsForNumericMetrics) {
                    val conditionHandle = Handles.condition(nextHandle)
                    val signalHandle = Handles.signal(nextHandle)
                    it.insert(
                        DescriptionItem(
                            MdibFactory.alertConditionDescriptor(
                                handleAttr = conditionHandle,
                                kindAttr = AlertConditionKind(AlertConditionKind.EnumType.Tec),
                                source = listOf(nextHandle.ref()),
                                priorityAttr = AlertConditionPriority(AlertConditionPriority.EnumType.None)
                            ),
                            MdibFactory.alertConditionState(
                                descriptorHandleAttr = conditionHandle.ref(),
                                activationStateAttr = AlertActivation(AlertActivation.EnumType.On),
                                presenceAttr = false
                            ),
                            Handles.ALERT_SYSTEM.string
                        )
                    )
                    it.insert(
                        DescriptionItem(
                            MdibFactory.alertSignalDescriptor(
                                handleAttr = signalHandle,
                                manifestationAttr = AlertSignalManifestation(AlertSignalManifestation.EnumType.Vis),
                                latchingAttr = false,
                                conditionSignaledAttr = conditionHandle.ref(),
                                acknowledgementSupportedAttr = false
                            ),
                            MdibFactory.alertSignalState(
                                descriptorHandleAttr = signalHandle.ref(),
                                activationStateAttr = AlertActivation(AlertActivation.EnumType.On),
                                presenceAttr = AlertSignalPresence(AlertSignalPresence.EnumType.Off)
                            ),
                            Handles.ALERT_SYSTEM.string
                        )
                    )
                }
            }
        }

        for (i in 1..config.rtsaMetricCount) {
            val nextHandle = Handles.withIndex(Handles.RTSA_METRIC, i)
            mods.insert(
                DescriptionItem(
                    MdibFactory.realTimeSampleArrayMetricDescriptor(
                        handleAttr = nextHandle,
                        metricAvailabilityAttr = MetricAvailability(MetricAvailability.EnumType.Cont),
                        metricCategoryAttr = MetricCategory(MetricCategory.EnumType.Msrmt),
                        determinationPeriodAttr = 1.seconds,
                        resolutionAttr = BigDecimal.valueOf(0.01),
                        unit = MdibFactory.codedValue("262688"),
                        technicalRange = listOf(
                            MdibFactory.range(0.0, 100.0)
                        ),
                        samplePeriodAttr = 20.milliseconds
                    ),
                    MdibFactory.realTimeSampleArrayMetricState(
                        descriptorHandleAttr = nextHandle.ref()
                    ),
                    Handles.RTSA_CHANNEL.string
                )
            )
        }
    }

    suspend fun runStateUpdates() {
        coroutineScope {
            launch {
                if (config.throughputPeriod <= 0) {
                    logger.warn {
                        "Throughput period is ${config.throughputPeriod}ms, needs to be > 0. " +
                                "No statistics will be printed."
                    }
                    return@launch
                }

                var iterations = 0
                while (coroutineContext.isActive) {
                    delay(config.throughputPeriod)

                    iterations++
                    val throughput = sentUpdatesCount.getAndSet(0) / config.throughputPeriod.milliseconds.inWholeSeconds

                    logger.info {
                        "Update throughput at ${consumerCount.get()} consumers: $throughput"
                    }
                }
            }
            launch {
                if (config.numericMetricCount == 0L) {
                    return@launch
                }
                while (isActive) {
                    delay(config.numericMetricUpdatePeriod)
                    val stateChanges = MetricStates()
                    mdibAccess.withReadLock { lockedAccess ->
                        for (i in 1..config.numericMetricCount) {
                            lockedAccess.entity(
                                Handles.withIndex(Handles.NUMERIC_METRIC, i),
                                MdibEntity.NumericMetric::class
                            ).onSuccess {
                                val timestamp = MdibFactory.timestamp()
                                val newValue =
                                    round((sin(((timestamp.unsignedLong.toDouble() / 1000) - i) / i) * 30 + 50) * 100) / 100
                                it.state.copy(
                                    metricValue = MdibFactory.numericMetricValue(
                                        determinationTimeAttr = MdibFactory.timestamp(),
                                        metricQuality = MdibFactory.metricQuality(
                                            validityAttr = MeasurementValidity(MeasurementValidity.EnumType.Vld),
                                            modeAttr = GenerationMode(GenerationMode.EnumType.Demo)
                                        ),
                                        valueAttr = BigDecimal.valueOf(newValue)
                                    )
                                ).also { state -> stateChanges.add(state) }
                            }
                        }
                    }
                    mdibAccess.writeStates(MdibStateModifications.Metric(stateChanges)).also { result ->
                        logger.debug { writeResultLog("Written numeric metric states", result) }
                    }.also {
                        sentUpdatesCount.incrementAndGet()
                    }
                    stateChanges.metricStates
                        .asSequence()
                        .filterIsInstance(AbstractStateOneOf.ChoiceNumericMetricState::class.java)
                        .map { it.value }
                        .filter { it.metricValue?.valueAttr?.let { value -> value.toDouble() > 60.0 } ?: false }
                        .map { it.descriptorHandleString() to it }
                        .toMap()
                        .also { fireAlerts(it) }
                }
            }

            launch {
                if (config.rtsaMetricCount == 0L) {
                    return@launch
                }
                while (isActive) {
                    delay(config.rtsaMetricUpdatePeriod)
                    val stateChanges = WaveformStates()
                    mdibAccess.withReadLock { lockedAccess ->
                        for (i in 1..config.rtsaMetricCount) {
                            lockedAccess.entity(
                                Handles.withIndex(Handles.RTSA_METRIC, i),
                                MdibEntity.RealTimeSampleArrayMetric::class
                            ).onSuccess {
                                val timestamp = MdibFactory.timestamp()
                                val newValues = mutableListOf<BigDecimal>()
                                val time = timestamp.unsignedLong.toDouble() / 1000 // in seconds
                                val angularFrequency = 2 * Math.PI
                                val sampleDistance = 0.002
                                val round2Digits: (Double) -> Double = { dbl -> round(dbl * 100) / 100 }
                                for (j in 0..20) {
                                    newValues.add(
                                        BigDecimal.valueOf(
                                            round2Digits(sin(angularFrequency * (time + (j * sampleDistance))) * 50 + 50)
                                        )
                                    )
                                }
                                it.state.copy(
                                    metricValue = MdibFactory.sampleArrayMetricValue(
                                        determinationTimeAttr = MdibFactory.timestamp(),
                                        metricQuality = MdibFactory.metricQuality(
                                            validityAttr = MeasurementValidity(MeasurementValidity.EnumType.Vld),
                                            modeAttr = GenerationMode(GenerationMode.EnumType.Demo)
                                        ),
                                        samplesAttr = RealTimeValueType(newValues)
                                    )
                                ).also { state -> stateChanges.add(state) }
                            }
                        }
                    }
                    mdibAccess.writeStates(MdibStateModifications.Waveform(stateChanges)).also { result ->
                        logger.debug { writeResultLog("Written realtime sample array metric states", result) }
                    }.also {
                        sentUpdatesCount.incrementAndGet()
                    }
                }
            }

            launch {
                if (!config.selfChecks) {
                    logger.info { "Alert system selfhecks are deactivated" }
                    return@launch
                }
                while (isActive) {
                    delay(config.alertUpdatePeriod)
                    mdibAccess.withReadLock { lockedAccess ->
                        lockedAccess.entity(Handles.ALERT_SYSTEM, MdibEntity.AlertSystem::class).getOrNull()
                            ?.let { entity ->
                                lockedAccess.entitiesByType(MdibEntity.AlertCondition::class)
                                    .filter { it.state.presence() }
                                    .map { HandleRef(it.handle) }
                                    .toList()
                                    .let {
                                        entity.state.copy(
                                            presentTechnicalAlarmConditionsAttr = AlertConditionReference(it),
                                            selfCheckCountAttr = ++selfCheckCount,
                                            lastSelfCheckAttr = MdibFactory.timestamp()
                                        )
                                    }
                            }
                    }?.also {
                        mdibAccess.writeStates(MdibStateModifications.Alert(AlertStates().add(it)))
                            .also { result ->
                                logger.debug { writeResultLog("Written selfcheck", result) }
                            }.also {
                                sentUpdatesCount.incrementAndGet()
                            }
                    }
                }
            }
        }
    }

    private suspend fun fireAlerts(states: Map<String, NumericMetricState>) {
        if (!config.alertsForNumericMetrics) {
            return
        }

        val stateChanges = AlertStates()

        mdibAccess.withReadLock { lockedAccess ->
            val timestamp = MdibFactory.timestamp()
            for (i in 1..config.numericMetricCount) {
                val handle = Handles.withIndex(Handles.NUMERIC_METRIC, i)
                val conditionHandle = Handles.condition(handle)
                val signalHandle = Handles.signal(handle)

                lockedAccess.entity(conditionHandle, MdibEntity.AlertCondition::class).getOrNull()?.let {
                    if (states.containsKey(handle.string)) {
                        if (it.state.presence()) {
                            null
                        } else {
                            it.state.copy(
                                presenceAttr = true,
                                determinationTimeAttr = timestamp
                            )
                        }
                    } else {
                        if (it.state.presence()) {
                            it.state.copy(
                                presenceAttr = false,
                                determinationTimeAttr = timestamp
                            )
                        } else {
                            null
                        }
                    }
                }?.also { stateChanges.add(it) }

                lockedAccess.entity(signalHandle, MdibEntity.AlertSignal::class).getOrNull()?.let {
                    if (states.containsKey(it.handle)) {
                        if (it.state.presence() != AlertSignalPresence(AlertSignalPresence.EnumType.Off)) {
                            null
                        } else {
                            it.state.copy(
                                presenceAttr = AlertSignalPresence(AlertSignalPresence.EnumType.On)
                            )
                        }
                    } else {
                        if (it.state.presence() != AlertSignalPresence(AlertSignalPresence.EnumType.Off)) {
                            it.state.copy(
                                presenceAttr = AlertSignalPresence(AlertSignalPresence.EnumType.Off)
                            )
                        } else {
                            null
                        }
                    }
                }?.also { stateChanges.add(it) }
            }
        }

        if (stateChanges.alertStates.isNotEmpty()) {
            mdibAccess.writeStates(MdibStateModifications.Alert(stateChanges))
                .also { result ->
                    logger.debug { writeResultLog("Written alert states", result) }
                }.also {
                    sentUpdatesCount.incrementAndGet()
                }
        }
    }

    private fun writeResultLog(prefix: String, result: WriteStateResult) =
        "$prefix: ${result.mdibVersion} {\n${result.states.states.joinToString("\n") { "    $it" }}\n}"
}