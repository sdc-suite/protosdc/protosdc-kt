package org.protosdc.example.provider

import kotlinx.serialization.Serializable

@Serializable
data class ProviderMdibConfig(
    val numericMetricCount: Long = 1,
    val numericMetricUpdatePeriod: Long = 1000,
    val alertsForNumericMetrics: Boolean = false,
    val selfChecks: Boolean = true,
    val alertUpdatePeriod: Long = 2000,
    val rtsaMetricCount: Long = 1,
    val rtsaMetricUpdatePeriod: Long = 200,
    val throughputPeriod: Long = 1000
)
