package org.somda.protosdc.test.util

import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.*
import org.bouncycastle.cert.X509v3CertificateBuilder
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.crypto.util.PrivateKeyFactory
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder
import java.math.BigInteger
import java.security.*
import java.security.cert.X509Certificate
import java.util.*

/**
 * SSL metadata used for crypto related tests.
 *
 * Provides key stores and trust stores for client and device side in-memory.
 *
 * Code derived from http://www.bouncycastle.org/documentation.html and https://www.baeldung.com/java-keystore
 */
class SslMetadata {
    var providerKeySet: KeySet
        private set
    var consumerKeySet: KeySet
        private set
    var discoveryProxyKeySet: KeySet
        private set

    companion object {
        const val PROVIDER_ISSUER_NAME = "protosdc-provider.org"
        const val CONSUMER_ISSUER_NAME = "protosdc-consumer.org"
        const val DISCOVERY_PROXY_ISSUER_NAME = "protosdc-discovery-proxy.org"
        const val SUBJECT_ALTERNATIVE_NAME = "somda.org"

        private fun generateKeyPair(): KeyPair {
            val keyPairGenerator: KeyPairGenerator = KeyPairGenerator.getInstance("RSA")
            keyPairGenerator.initialize(1024)
            return keyPairGenerator.generateKeyPair()
        }

        private fun createKeyStore(
            alias: String,
            privateKey: PrivateKey,
            password: String,
            certificateChain: List<X509Certificate>
        ): KeyStore {
            val keyStore = KeyStore.getInstance("jks")
            keyStore.load(null, password.toCharArray())
            val certificates = certificateChain.toTypedArray()
            keyStore.setKeyEntry(alias, privateKey, password.toCharArray(), certificates)
            return keyStore
        }

        private fun createTrustStore(
            password: String,
            trustStoreEntries: Map<String, X509Certificate>
        ): KeyStore {
            val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
            keyStore.load(null, password.toCharArray())
            trustStoreEntries.forEach {
                keyStore.setCertificateEntry(it.key, it.value)
            }
            return keyStore
        }

        private fun generateCertificate(
            issuer: String,
            keyPair: KeyPair,
            extendedKeyUsage: ExtendedKeyUsage
        ): X509Certificate {
            val subPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.public.encoded)
            val sigAlgId =
                DefaultSignatureAlgorithmIdentifierFinder().find("SHA256WithRSAEncryption")
            val digAlgId = DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId)
            val privateKeyAsymKeyParam = PrivateKeyFactory.createKey(keyPair.private.encoded)
            val sigGen = BcRSAContentSignerBuilder(sigAlgId, digAlgId).build(privateKeyAsymKeyParam)

            // generate the certificate
            val certGen = X509v3CertificateBuilder(
                X500Name("CN=$issuer"),
                BigInteger.valueOf(System.currentTimeMillis()),
                Date(System.currentTimeMillis() - 500000),
                Date(System.currentTimeMillis() + 500000),
                X500Name("CN=$issuer"),
                subPubKeyInfo
            )
            certGen.addExtension(Extension.basicConstraints, true, BasicConstraints(false))
            certGen.addExtension(
                Extension.keyUsage,
                true,
                KeyUsage(KeyUsage.digitalSignature or KeyUsage.keyEncipherment)
            )
            certGen.addExtension(
                Extension.subjectAlternativeName,
                false,
                GeneralNames(
                    GeneralName(GeneralName.rfc822Name, SUBJECT_ALTERNATIVE_NAME)
                )
            )
            certGen.addExtension(Extension.extendedKeyUsage, true, extendedKeyUsage)
            val certificateHolder = certGen.build(sigGen)
            return JcaX509CertificateConverter().setProvider("BC").getCertificate(certificateHolder)
        }
    }

    init {
        Security.addProvider(BouncyCastleProvider())
        val commonPassword = "secret"
        val ekuId = arrayOf(KeyPurposeId.id_kp_serverAuth, KeyPurposeId.id_kp_clientAuth)
        val extendedKeyUsage = ExtendedKeyUsage(ekuId)
        val providerAlias = "test-provider"
        val consumerAlias = "test-consumer"
        val discoProxyAlias = "test-discovery-proxy"
        val providerKeyPair = generateKeyPair()
        val consumerKeyPair = generateKeyPair()
        val discoProxyKeyPair = generateKeyPair()
        val providerCert = generateCertificate(PROVIDER_ISSUER_NAME, providerKeyPair, extendedKeyUsage)
        val consumerCert = generateCertificate(CONSUMER_ISSUER_NAME, consumerKeyPair, extendedKeyUsage)
        val discoProxyCert = generateCertificate(DISCOVERY_PROXY_ISSUER_NAME, discoProxyKeyPair, extendedKeyUsage)
        val providerKeyStore =
            createKeyStore(providerAlias, providerKeyPair.private, commonPassword, listOf(providerCert))
        val consumerKeyStore =
            createKeyStore(consumerAlias, consumerKeyPair.private, commonPassword, listOf(consumerCert))
        val discoProxyKeyStore =
            createKeyStore(discoProxyAlias, discoProxyKeyPair.private, commonPassword, listOf(discoProxyCert))
        val providerTrustStore = createTrustStore(
            commonPassword, mapOf(
                consumerAlias to consumerCert,
                discoProxyAlias to discoProxyCert
            )
        )
        val consumerTrustStore = createTrustStore(
            commonPassword, mapOf(
                providerAlias to providerCert,
                discoProxyAlias to discoProxyCert
            )
        )

        val discoProxyTrustStore = createTrustStore(
            commonPassword, mapOf(
                providerAlias to providerCert,
                consumerAlias to consumerCert
            )
        )
        providerKeySet = KeySet(providerKeyStore, commonPassword, providerTrustStore, commonPassword)
        consumerKeySet = KeySet(consumerKeyStore, commonPassword, consumerTrustStore, commonPassword)
        discoveryProxyKeySet = KeySet(discoProxyKeyStore, commonPassword, discoProxyTrustStore, commonPassword)
    }

    data class KeySet(
        val keyStore: KeyStore,
        val keyStorePassword: String,
        val trustStore: KeyStore,
        val trustStorePassword: String
    )
}