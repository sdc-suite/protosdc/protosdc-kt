package org.somda.protosdc.network.udp

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.somda.protosdc.network.ip.IpHeader
import java.net.InetAddress

class UdpMessageTest {
    @Test
    fun `test udp message equality`() {
        run {
            val msg1 = createUdpMessage("123456", 4)
            val msg2 = createUdpMessage("123456", 4)
            Assertions.assertEquals(msg1, msg2)
        }

        run {
            val msg1 = createUdpMessage("123456aa", 6)
            val msg2 = createUdpMessage("123456bb", 6)
            Assertions.assertEquals(msg1, msg2)
        }

        run {
            val msg1 = createUdpMessage("123456", 6)
            val msg2 = createUdpMessage("123456", 6)
            Assertions.assertEquals(msg1, msg2)
        }

        run {
            val msg1 = createUdpMessage("123456aa", 7)
            val msg2 = createUdpMessage("123456bb", 7)
            Assertions.assertNotEquals(msg1, msg2)
        }

        run {
            val msg1 = createUdpMessage("123456", 5)
            val msg2 = createUdpMessage("123456", 6)
            Assertions.assertNotEquals(msg1, msg2)
        }

        run {
            val msg1 = createUdpMessage("123456", 6)
            val msg2 = createUdpMessage("123456", 5)
            Assertions.assertNotEquals(msg1, msg2)
        }
    }

    @Test
    fun `test udp message hash code`() {
        run {
            val msg1 = createUdpMessage("123456", 4)
            val msg2 = createUdpMessage("123456", 4)
            Assertions.assertEquals(msg1.hashCode(), msg2.hashCode())
        }

        run {
            val msg1 = createUdpMessage("123456aa", 6)
            val msg2 = createUdpMessage("123456bb", 6)
            Assertions.assertEquals(msg1.hashCode(), msg2.hashCode())
        }

        run {
            val msg1 = createUdpMessage("123456", 6)
            val msg2 = createUdpMessage("123456", 6)
            Assertions.assertEquals(msg1.hashCode(), msg2.hashCode())
        }

        run {
            val msg1 = createUdpMessage("123456aa", 7)
            val msg2 = createUdpMessage("123456bb", 7)
            Assertions.assertNotEquals(msg1.hashCode(), msg2.hashCode())
        }

        run {
            val msg1 = createUdpMessage("123456", 5)
            val msg2 = createUdpMessage("123456", 6)
            Assertions.assertNotEquals(msg1.hashCode(), msg2.hashCode())
        }

        run {
            val msg1 = createUdpMessage("123456", 6)
            val msg2 = createUdpMessage("123456", 5)
            Assertions.assertNotEquals(msg1.hashCode(), msg2.hashCode())
        }
    }

    private fun createUdpMessage(content: String, length: Int) =
        UdpMessage(
            content.toByteArray(),
            UdpHeader(1, 1, length, IpHeader.IpV4(InetAddress.getLoopbackAddress(), InetAddress.getLoopbackAddress()))
        )
}