package it.org.somda.protosdc.discovery

import it.org.somda.protosdc.device.dagger.DaggerTestProtoSdcComponent
import it.org.somda.protosdc.network.grpc.CryptoStoreGenerator
import it.org.somda.protosdc.network.udp.udpMock
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.somda.protosdc.dagger.DaggerProtoSdcComponent
import org.somda.protosdc.discovery.consumer.DiscoveryConsumer
import org.somda.protosdc.discovery.consumer.DiscoveryEvent
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.discovery.proxy.DiscoveryProxy
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.discovery.ScopeMatcher
import org.somda.protosdc.proto.model.discovery.SearchFilter
import java.net.InetAddress
import java.net.InetSocketAddress

class ManagedDiscoveryTest {
    companion object {
        private const val timeout = 2000L
    }

    private val channel = Channel<UdpMessage>(100)
    private val protoSdcComponent = DaggerTestProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupConsumer())
        .build()
    private val udpComponent = udpMock()
    private val udpMessageQueue = udpComponent.messageQueue
    private val endpointIdentifier = "endpoint-id"

    private val proxyProtoSdcComponent = DaggerProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupProvider())
        .build()
    private val server = proxyProtoSdcComponent.serverFactory()
        .create(InetSocketAddress(InetAddress.getLoopbackAddress(), 0))


    private lateinit var discoveryProxy: DiscoveryProxy
    private lateinit var discoveryProvider: DiscoveryProvider
    private lateinit var discoveryConsumer: DiscoveryConsumer

    // for tests that only use one discovery provider and consumer
    private suspend fun simpleStartUp() {
        udpMessageQueue.start()
        discoveryProxy = proxyProtoSdcComponent.discoveryProxyFactory()
            .create(server).apply { start() }
        server.start()
        discoveryConsumer = protoSdcComponent.discoveryConsumerFactory().create(
            udpMessageQueue,
            server.addressAsUri()
        ).apply { start() }

        discoveryProvider = protoSdcComponent.discoveryProviderFactory().create(
            endpointIdentifier,
            udpMessageQueue,
            server.addressAsUri()
        ).apply { start() }

        waitForEndpoints(discoveryProxy, 1)
    }

    // for tests that only use one discovery provider and consumer
    private suspend fun simpleShutDown() {
        udpMessageQueue.stop()
        discoveryConsumer.stop()
        discoveryProvider.stop()
        discoveryProxy.stop()
        server.stop()
    }

    @Test
    fun `test managed mode hello`(): Unit = runBlocking {
        simpleStartUp()
        try {
            val subscription = discoveryConsumer.subscribe()
            discoveryProvider.update(null, null)
            val event = withTimeout(timeout) { subscription.receive() }
            check(event is DiscoveryEvent.EndpointEntered)
            assertTrue(event.secure)
            assertEquals(discoveryProvider.endpoint(), event.endpoint)
        } finally {
            simpleShutDown()
        }
    }

    @Test
    fun `test managed mode searches with one discovery provider and consumer`(): Unit = runBlocking {
        simpleStartUp()
        try {
            run {
                val expectedScopes = listOf(
                    "http://host/path1",
                    "http://host/path2"
                ).map {
                    Uri.newBuilder().setValue(it).build()
                }
                val subscription = discoveryConsumer.subscribe()
                discoveryProvider.updateScopes(expectedScopes)
                // make sure the scopes have been updated at the proxy before continuing
                withTimeout(timeout) { subscription.receive() }
                // close as otherwise the next call to subscribe() will suspend forever
                subscription.cancel()
                val searchFilters = expectedScopes.map {
                    ScopeMatcher.newBuilder()
                        .setAlgorithm(ScopeMatcher.Algorithm.STRING_COMPARE)
                        .setScope(it)
                        .build()
                }.map { SearchFilter.newBuilder().setScopeMatcher(it).build() }
                val endpoints = withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
                assertEquals(1, endpoints.size)
                assertEquals(discoveryProvider.endpoint(), endpoints.first())
            }
            run {
                val expectedScopes = listOf(
                    "http://host/path1",
                    "http://host/path2"
                ).map { Uri.newBuilder().setValue(it).build() }
                val subscription = discoveryConsumer.subscribe()
                discoveryProvider.updateScopes(expectedScopes)
                // make sure the scopes have been updated at the proxy before continuing
                withTimeout(timeout) { subscription.receive() }
                // close as otherwise the next call to subscribe() will suspend forever
                subscription.cancel()
                val searchFilters = expectedScopes.map {
                    ScopeMatcher.newBuilder()
                        .setAlgorithm(ScopeMatcher.Algorithm.RFC_3986)
                        .setScope(it)
                        .build()
                }.map { SearchFilter.newBuilder().setScopeMatcher(it).build() }
                val endpoints = withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
                assertEquals(1, endpoints.size)
                assertEquals(discoveryProvider.endpoint(), endpoints.first())
            }
            run {
                val expectedScopes = listOf(
                    "http://host/path1/",
                    "HTTP://host/path2"
                ).map { Uri.newBuilder().setValue(it).build() }
                val modifiedExpectedScopes =
                    listOf(
                        "HTTP://host/path1/",
                        "http://host/path2"
                    ).map { Uri.newBuilder().setValue(it).build() }
                val subscription = discoveryConsumer.subscribe()
                discoveryProvider.updateScopes(modifiedExpectedScopes)
                // make sure the scopes have been updated at the proxy before continuing
                withTimeout(timeout) { subscription.receive() }
                // close as otherwise the next call to subscribe() will suspend forever
                subscription.cancel()
                val searchFilters = expectedScopes.map {
                    ScopeMatcher.newBuilder()
                        .setAlgorithm(ScopeMatcher.Algorithm.RFC_3986)
                        .setScope(it)
                        .build()
                }.map { SearchFilter.newBuilder().setScopeMatcher(it).build() }

                val endpoints = withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
                assertEquals(1, endpoints.size)
                assertEquals(discoveryProvider.endpoint(), endpoints.first())
            }
            run {
                val endpoints = withTimeout(timeout) {
                    discoveryConsumer.searchAsync(emptyList(), 1).await()
                }
                assertEquals(1, endpoints.size)
                assertEquals(discoveryProvider.endpoint(), endpoints.first())
            }
            run {
                val searchFilters = listOf(SearchFilter.newBuilder().build())
                val endpoints = withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
                assertEquals(0, endpoints.size)
            }
            run {
                val searchFilters = listOf(
                    SearchFilter.newBuilder().setScopeMatcher(
                        ScopeMatcher.newBuilder().setScope(Uri.newBuilder().setValue("http://unknown"))
                    ).build()
                )
                val endpoints = withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
                assertEquals(0, endpoints.size)
            }
            run {
                val searchFilters = listOf(SearchFilter.newBuilder().setEndpointIdentifier("<unknown>").build())
                val endpoints = withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
                assertEquals(0, endpoints.size)
            }
        } finally {
            simpleShutDown()
        }
    }

    @Test
    fun `test managed mode searches with multiple discovery providers and consumers`(): Unit = runBlocking {
        udpMessageQueue.start()
        discoveryProxy = proxyProtoSdcComponent.discoveryProxyFactory()
            .create(server).also { it.start() }
        server.start()

        val count = 10
        val providerConsumerPairs = mutableListOf<ProviderConsumerPair>()
        try {
            for (i in 0 until count) {
                val newPair = providerConsumerPair()
                providerConsumerPairs.add(newPair)
                newPair.consumer.start()
                newPair.provider.start()
                waitForEndpoints(discoveryProxy, i + 1)
            }
            run {
                val aConsumer = providerConsumerPairs[0].consumer
                val endpoints = withTimeout(timeout) {
                    aConsumer.searchAsync(emptyList(), 1).await()
                }
                assertEquals(count, endpoints.size)
            }
            run {
                val aConsumer: DiscoveryConsumer = providerConsumerPairs[0].consumer
                val halfCount = count / 2
                val searchFilters = providerConsumerPairs.subList(0, halfCount)
                    .map { providerConsumerPair: ProviderConsumerPair ->
                        SearchFilter.newBuilder().setEndpointIdentifier(
                            providerConsumerPair.endpointIdentifier
                        ).build()
                    }
                val endpoints = withTimeout(timeout) {
                    aConsumer.searchAsync(searchFilters, 1).await()
                }
                assertEquals(halfCount, endpoints.size)
            }
            run {
                for (providerConsumerPair in providerConsumerPairs) {
                    val searchFilter = SearchFilter.newBuilder().setEndpointIdentifier(
                        providerConsumerPair.endpointIdentifier
                    ).build()
                    val endpoints = withTimeout(timeout) {
                        providerConsumerPair.consumer.searchAsync(listOf(searchFilter), 1).await()
                    }
                    assertEquals(1, endpoints.size)
                    assertEquals(providerConsumerPair.provider.endpoint(), endpoints.first())
                }
            }
            run {
                var i = 0
                for (providerConsumerPair in providerConsumerPairs) {
                    println("Round " + ++i)
                    val searchFilters = providerConsumerPair.scopes.map {
                        SearchFilter.newBuilder()
                            .setScopeMatcher(
                                ScopeMatcher.newBuilder()
                                    .setScope(it).build()
                            ).build()
                    }
                    val endpoints = withTimeout(timeout) {
                        providerConsumerPair.consumer.searchAsync(searchFilters, 1).await()
                    }
                    assertEquals(1, endpoints.size)
                    assertEquals(providerConsumerPair.provider.endpoint(), endpoints.first())
                }
            }
        } finally {
            providerConsumerPairs.forEach {
                it.consumer.stop()
                it.provider.stop()
            }
            udpMessageQueue.stop()
            server.stop()
        }
    }

    private suspend fun providerConsumerPair(): ProviderConsumerPair {
        val endpointIdentifier = "endpointId=${ProviderConsumerPair.instanceCount}"
        val scopes = listOf(Uri.newBuilder().setValue("http://scope" + ProviderConsumerPair.instanceCount++).build())
        val consumer = createConsumer()
        val subscription = consumer.subscribe()
        return ProviderConsumerPair(
            endpointIdentifier,
            scopes,
            subscription,
            createProvider(endpointIdentifier, scopes),
            consumer
        )
    }

    private data class ProviderConsumerPair(
        val endpointIdentifier: String,
        val scopes: List<Uri>,
        val subscription: ReceiveChannel<DiscoveryEvent>,
        val provider: DiscoveryProvider,
        val consumer: DiscoveryConsumer
    ) {
        companion object {
            var instanceCount = 0
        }
    }

    private suspend fun createProvider(endpointIdentifier: String, scopes: List<Uri>) =
        protoSdcComponent.discoveryProviderFactory()
            .create(endpointIdentifier, udpMessageQueue, server.addressAsUri())
            .also { it.update(scopes, setOf(Uri.newBuilder().setValue(server.addressAsUri().toString()).build())) }


    private suspend fun createConsumer() = protoSdcComponent.discoveryConsumerFactory()
        .create(udpMessageQueue, server.addressAsUri())
}