package it.org.somda.protosdc.discovery

import kotlinx.coroutines.delay
import org.somda.protosdc.discovery.proxy.DiscoveryProxy

suspend fun waitForEndpoints(discoveryProxy: DiscoveryProxy, count: Int) {
    // wait 10 seconds max
    val delayInMs = 100L
    val loopCount = 100
    for (i in 0 until loopCount) {
        if (discoveryProxy.endpointCount() == count) {
            return
        }
        delay(delayInMs)
    }
    throw Exception("The expected endpoint number of $count was not met after ${delayInMs * loopCount} ms. " +
            "Actual: ${discoveryProxy.endpointCount()}")
}