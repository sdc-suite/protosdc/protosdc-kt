package it.org.somda.protosdc.extension

import com.google.protobuf.Any
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.somda.protosdc.proto.model.biceps.*
import org.somda.protosdc.test.extension.Extension1

class ModelExtensionTest {
    @Test
    fun `test biceps extension`() {
        val namespace = "urn:test"
        val number = 100
        val extSrc = Extension1.newBuilder().setTestNumber(number).build()
        val descrSrc = MdsDescriptorMsg.newBuilder()
            .setAbstractComplexDeviceComponentDescriptor(
                AbstractComplexDeviceComponentDescriptorMsg.newBuilder()
                    .setAbstractDeviceComponentDescriptor(
                        AbstractDeviceComponentDescriptorMsg.newBuilder()
                            .setAbstractDescriptor(
                                AbstractDescriptorMsg.newBuilder()
                                    .setExtensionElement(
                                        ExtensionMsg.newBuilder()
                                            .addItem(
                                                ExtensionMsg.ItemMsg.newBuilder()
                                                    .setMustUnderstand(false)
                                                    .setExtensionData(Any.pack(extSrc, namespace))
                                            )
                                    )
                            )
                    )
            ).build()

        val blob = descrSrc.toByteArray()

        val descrDest = MdsDescriptorMsg.parseFrom(blob)
        assertEquals(
            1,
            descrDest
                .abstractComplexDeviceComponentDescriptor
                .abstractDeviceComponentDescriptor
                .abstractDescriptor
                .extensionElement
                .itemCount
        )
        val anyData = descrDest
            .abstractComplexDeviceComponentDescriptor
            .abstractDeviceComponentDescriptor
            .abstractDescriptor
            .extensionElement
            .getItem(0)
            .extensionData
        assertTrue(anyData.typeUrl.startsWith(namespace))
        assertTrue(anyData.`is`(Extension1::class.java))
        val extDest = anyData.unpack(Extension1::class.java)
        assertEquals(number, extDest.testNumber)
    }
}