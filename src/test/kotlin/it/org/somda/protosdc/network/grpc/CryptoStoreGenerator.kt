package it.org.somda.protosdc.network.grpc

import org.somda.protosdc.crypto.CryptoStore
import org.somda.protosdc.test.util.SslMetadata
import java.io.ByteArrayOutputStream
import java.security.KeyStore
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLSession

/**
 * [CryptoStore] generator for integration tests using TLS.
 */
object CryptoStoreGenerator {
    private val sslMetadata = SslMetadata().also {
        HttpsURLConnection.setDefaultHostnameVerifier { _: String?, _: SSLSession? -> true }
    }

    fun setupProvider() = setup(sslMetadata.providerKeySet)

    fun setupConsumer() = setup(sslMetadata.consumerKeySet)

    fun setupDiscoveryProxy() = setup(sslMetadata.discoveryProxyKeySet)

    private fun setup(keySet: SslMetadata.KeySet) = CryptoStore(
        keyStoreBytes = toByteArray(keySet.keyStore, keySet.keyStorePassword),
        keyStorePassword = keySet.keyStorePassword,
        trustStoreBytes = toByteArray(keySet.trustStore, keySet.trustStorePassword),
        trustStorePassword = keySet.trustStorePassword
    )

    private fun toByteArray(keyStore: KeyStore, password: String): ByteArray {
        val bos = ByteArrayOutputStream()
        keyStore.store(bos, password.toCharArray())
        return bos.toByteArray()
    }
}