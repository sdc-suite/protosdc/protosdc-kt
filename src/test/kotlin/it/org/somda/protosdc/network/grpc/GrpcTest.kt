package it.org.somda.protosdc.network.grpc

import io.grpc.StatusException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toCollection
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.somda.protosdc.GrpcConfig
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.crypto.CryptoStore
import org.somda.protosdc.dagger.DaggerProtoSdcComponent
import org.somda.protosdc.network.grpc.server.SslSessionInterceptor
import org.somda.protosdc.test.grpc.*
import org.somda.protosdc.test.util.SslMetadata
import java.net.InetAddress
import java.net.InetSocketAddress
import java.security.cert.X509Certificate

class TestServiceImpl : TestServiceGrpcKt.TestServiceCoroutineImplBase() {
    val clientCertsFromLastCall = mutableListOf<X509Certificate>()

    override suspend fun add(request: AddRequest): AddResponse {
        clientCertsFromLastCall
            .also { it.clear() }
            .also {
                it.addAll(SslSessionInterceptor.x509Certificates())
            }

        return AddResponse.newBuilder().setSum(request.summand1 + request.summand2).build()
    }

    override fun integerSeries(request: IntegerSeriesRequest): Flow<IntegerSeriesResponse> {
        return flow {
            for (i in 1..request.count) {
                emit(IntegerSeriesResponse.newBuilder().setInteger(i).build())
            }
        }
    }
}

class GrpcTest {
    private val socketAddr = InetSocketAddress(InetAddress.getByName("127.0.0.1"), 0)
    private var serverComponent = DaggerProtoSdcComponent.builder().build()
    private var clientComponent = DaggerProtoSdcComponent.builder().build()

    @Test
    fun `test client against server without encryption`() {
        val serverConfig = ProtoSdcConfig(
            grpc = GrpcConfig(true)
        )
        val clientConfig = ProtoSdcConfig(
            grpc = GrpcConfig(true)
        )

        serverComponent = grpcComponent(serverConfig)
        clientComponent = grpcComponent(clientConfig)

        successfulTestSequence()
    }

    @Test
    fun `test client against server with encryption`() {
        val serverConfig = ProtoSdcConfig(
            grpc = GrpcConfig(false)
        )
        val clientConfig = ProtoSdcConfig(
            grpc = GrpcConfig(false)
        )

        serverComponent = grpcComponent(serverConfig, CryptoStoreGenerator.setupProvider())
        clientComponent = grpcComponent(clientConfig, CryptoStoreGenerator.setupConsumer())

        successfulTestSequence()
    }

    @Test
    fun `client without encryption shall fail against server with encryption only`(): Unit = runBlocking {
        val serverConfig = ProtoSdcConfig(
            grpc = GrpcConfig(false)
        )
        val clientConfig = ProtoSdcConfig(
            grpc = GrpcConfig(true)
        )

        serverComponent = grpcComponent(serverConfig, CryptoStoreGenerator.setupProvider())
        clientComponent = grpcComponent(clientConfig)

        val server = serverComponent.serverFactory().create(socketAddr)
        server.registerService(TestServiceImpl())
        server.start()
        val channel = clientComponent.channelFactory().create(server.address())
        val stub = TestServiceGrpcKt.TestServiceCoroutineStub(channel)

        assertThrows<StatusException> {
            stub.add(AddRequest.newBuilder().setSummand1(50).setSummand2(50).build())
        }
    }

    @Test
    fun `test server interceptor`(): Unit = runBlocking {
        val serverConfig = ProtoSdcConfig(
            grpc = GrpcConfig(false)
        )
        val clientConfig = ProtoSdcConfig(
            grpc = GrpcConfig(false)
        )

        serverComponent = grpcComponent(serverConfig, CryptoStoreGenerator.setupProvider())
        clientComponent = grpcComponent(clientConfig, CryptoStoreGenerator.setupConsumer())

        val server = serverComponent.serverFactory().create(socketAddr)
        val testServiceImpl = TestServiceImpl()
        server.registerService(testServiceImpl)
        server.registerInterceptor(SslSessionInterceptor())
        server.start()
        val channel = clientComponent.channelFactory().create(server.address())
        val stub = TestServiceGrpcKt.TestServiceCoroutineStub(channel)
        assertEquals(0, testServiceImpl.clientCertsFromLastCall.size)
        stub.add(AddRequest.getDefaultInstance())
        assertEquals(1, testServiceImpl.clientCertsFromLastCall.size)
        assertTrue(testServiceImpl.clientCertsFromLastCall.first().issuerX500Principal.name.contains(SslMetadata.CONSUMER_ISSUER_NAME))
    }

    private fun successfulTestSequence(): Unit = runBlocking {
        val server = serverComponent.serverFactory().create(socketAddr)
        server.registerService(TestServiceImpl())
        server.start()
        val channel = clientComponent.channelFactory().create(server.address())
        val stub = TestServiceGrpcKt.TestServiceCoroutineStub(channel)

        run {
            val n1 = 50
            val n2 = 60
            val sum = stub.add(AddRequest.newBuilder().setSummand1(n1).setSummand2(n2).build())
            assertEquals(sum.sum, n1 + n2)
        }

        run {
            val n = 10
            val intSeries = stub.integerSeries(IntegerSeriesRequest.newBuilder().setCount(n).build())
            val sum = intSeries.toCollection(mutableListOf()).fold(0) { acc, elem -> acc.plus(elem.integer) }
            assertEquals(sum, (n * (n + 1) / 2))
        }
    }

    private fun grpcComponent(protoSdcConfig: ProtoSdcConfig, cryptoStore: CryptoStore? = null) =
        DaggerProtoSdcComponent.builder().bind(protoSdcConfig).bind(cryptoStore).build()
}