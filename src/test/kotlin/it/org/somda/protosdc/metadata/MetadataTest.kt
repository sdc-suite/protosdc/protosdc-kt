package it.org.somda.protosdc.metadata

import it.org.somda.protosdc.device.dagger.DaggerTestProtoSdcComponent
import it.org.somda.protosdc.network.grpc.CryptoStoreGenerator
import it.org.somda.protosdc.network.udp.udpMock
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.somda.protosdc.dagger.DaggerProtoSdcComponent
import org.somda.protosdc.metadata.provider.ServiceMetadata
import org.somda.protosdc.proto.model.common.LocalizedString
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import java.net.InetAddress
import java.net.InetSocketAddress

class MetadataTest {
    companion object {
        private const val timeout = 2000L
    }

    private val protoSdcServerComponent = DaggerTestProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupProvider())
        .build()

    private val protoSdcClientComponent = DaggerProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupConsumer())
        .build()

    private val udp = udpMock()
    private val udpMessageQueue = udp.messageQueue
    private val server = protoSdcServerComponent.serverFactory()
        .create(InetSocketAddress(InetAddress.getLoopbackAddress(), 0))

    @Test
    fun `test get metadata`(): Unit = runBlocking {
        val expectedMetadata = EndpointMetadata.newBuilder()
            .addAllFriendlyName(
                listOf(
                    LocalizedString.newBuilder()
                        .setLocale("en")
                        .setValue("A fancy endpoint")
                        .build()
                )
            )
            .build()

        udpMessageQueue.start()
        val discoveryProvider = protoSdcServerComponent.discoveryProviderFactory().create(
            "endpoint-id",
            udpMessageQueue
        )

        val metadataProvider = protoSdcServerComponent.metadataProviderFactory()
            .create(
                server,
                discoveryProvider,
                expectedMetadata,
                listOf(
                    ServiceMetadata(
                        listOf("Service1", "Service2"),
                        "ServiceId",
                        server
                    )
                )
            )

        server.start()

        metadataProvider.start()
        discoveryProvider.updatePhysicalAddresses(
            setOf(
                Uri.newBuilder().setValue(server.addressAsUri().toString()).build()
            )
        )
        discoveryProvider.start()

        val metadataConsumer = protoSdcClientComponent.metadataConsumerFactory()
            .create(server.addressAsUri())
        metadataConsumer.start()

        val metadata = withTimeout(timeout) { metadataConsumer.metadata() }
        assertEquals(discoveryProvider.endpoint(), metadata.endpoint)
        assertEquals(expectedMetadata, metadata.endpointMetadata)
        val expectedScopes = listOf(
            Uri.newBuilder()
                .setValue("http://brand-new-scope")
                .build()
        )
        discoveryProvider.updateScopes(expectedScopes)
        val updatedMetadata = withTimeout(timeout) { metadataConsumer.metadata() }
        assertEquals(expectedScopes, updatedMetadata.endpoint.scopeList)

        metadataConsumer.stop()
        metadataProvider.stop()
        discoveryProvider.stop()
        server.stop()
    }
}

