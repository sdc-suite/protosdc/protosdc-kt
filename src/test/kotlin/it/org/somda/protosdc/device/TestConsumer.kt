package it.org.somda.protosdc.device

import it.org.somda.protosdc.network.grpc.CryptoStoreGenerator
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.dagger.DaggerProtoSdcComponent
import org.somda.protosdc.device.consumer.SdcDeviceProxy
import org.somda.protosdc.discovery.consumer.DiscoveryEvent
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.test.util.DefaultWait
import java.net.NetworkInterface
import java.net.URI

class TestConsumer(
    udp: Udp,
    discoveryProxyAddress: URI? = null,
    private val endpointIdentifier: String,
    private val onConnected: (suspend (deviceProxy: SdcDeviceProxy) -> Unit) = {}
) : ServiceDelegate by ServiceDelegateImpl() {
    private val awaitChannel = Channel<SdcDeviceProxy>()

    private val protoSdcClientComponent = DaggerProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupConsumer())
        .build()
    private val sdcDeviceDiscovery = protoSdcClientComponent.sdcDeviceDiscoveryFactory()
        .create(udp, discoveryProxyAddress)

    private val mutex = Mutex()
    private var deviceProxy: SdcDeviceProxy? = null
    private lateinit var discoverySubscription: ReceiveChannel<DiscoveryEvent>

    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)

    init {
        onStartUp {
            sdcDeviceDiscovery.start()
            discoverySubscription = sdcDeviceDiscovery.subscribe()
            coroutineScopeBinding.launch {
                while (coroutineContext.isActive) {
                    discoverySubscription.receive().let {
                        when (it) {
                            is DiscoveryEvent.EndpointEntered -> if (it.endpoint.endpointIdentifier == endpointIdentifier) {
                                notifyConnected(sdcDeviceDiscovery.connect(it.endpoint))
                            }
                            else -> Unit
                        }
                    }
                }
            }
            coroutineScopeBinding.launch {
                try {
                    withTimeout(DefaultWait.MILLIS) {
                        sdcDeviceDiscovery.searchByEndpointIdentifierAsync(endpointIdentifier).await()
                    }.let { endpoint ->
                        notifyConnected(sdcDeviceDiscovery.connect(endpoint.getOrThrow()))
                    }
                } catch (e: Exception) {
                    // that's ok, hello/bye subscription is still listening
                }
            }
        }

        onShutDown {
            discoverySubscription.cancel()
            sdcDeviceDiscovery.stop()
            coroutineScopeBinding.cancel()
        }
    }

    suspend fun await() {
        awaitChannel.receive()
    }

    private suspend fun notifyConnected(connectedProxy: SdcDeviceProxy) {
        mutex.withLock {
            if (deviceProxy == null) {
                deviceProxy = connectedProxy
                deviceProxy
            } else {
                null
            }
        }?.let {
            onConnected(it)
            awaitChannel.trySend(it)
        }
    }
}