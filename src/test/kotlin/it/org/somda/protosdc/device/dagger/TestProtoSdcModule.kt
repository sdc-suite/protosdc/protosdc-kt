package it.org.somda.protosdc.device.dagger

import dagger.Module
import dagger.Provides
import it.org.somda.protosdc.network.udp.dagger.TestUdpMockComponent
import org.somda.protosdc.common.InstanceId
import java.util.*
import javax.inject.Singleton


@Module
internal abstract class TestProtoSdcModule {
    companion object {
        @Provides
        @Singleton
        fun instanceId() = InstanceId(UUID.randomUUID().toString())
    }
}