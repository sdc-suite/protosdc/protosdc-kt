package it.org.somda.protosdc.device.dagger

import dagger.BindsInstance
import dagger.Component
import it.org.somda.protosdc.network.udp.dagger.TestUdpMockComponent
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.common.ComponentConfig
import org.somda.protosdc.crypto.CryptoStore
import org.somda.protosdc.dagger.ProtoSdcComponent
import org.somda.protosdc.dagger.ProtoSdcModule
import org.somda.protosdc.device.dagger.DeviceModule
import org.somda.protosdc.discovery.dagger.DiscoveryModule
import org.somda.protosdc.metadata.dagger.MetadataModule
import javax.annotation.Nullable
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ProtoSdcModule::class,
        DeviceModule::class,
        DiscoveryModule::class,
        MetadataModule::class
    ]
)
internal interface TestProtoSdcComponent : ProtoSdcComponent {
    @Component.Builder
    interface Builder {
        fun bind(@BindsInstance @ComponentConfig @Nullable protoSdcConfig: ProtoSdcConfig?): Builder
        fun bind(@BindsInstance @ComponentConfig @Nullable cryptoStoreOverride: CryptoStore?): Builder
        fun build(): TestProtoSdcComponent
    }
}