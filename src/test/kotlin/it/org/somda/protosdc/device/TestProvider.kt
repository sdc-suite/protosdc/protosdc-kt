package it.org.somda.protosdc.device

import it.org.somda.protosdc.network.grpc.CryptoStoreGenerator
import org.somda.protosdc.biceps.test.MdibModificationsPresets
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.dagger.DaggerProtoSdcComponent
import org.somda.protosdc.device.provider.SdcDevice
import org.somda.protosdc.device.provider.sco.OperationInvocationReceiver
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import java.net.InetSocketAddress
import java.net.NetworkInterface
import java.net.URI

class TestProvider(
    endpointIdentifier: String,
    udp: Udp,
    socketAddress: InetSocketAddress,
    metadata: EndpointMetadata = EndpointMetadata.getDefaultInstance(),
    operationInvocationReceiver: OperationInvocationReceiver? = null,
    discoveryProxyAddress: URI? = null
) : ServiceDelegate by ServiceDelegateImpl() {

    private val protoSdcComponent = DaggerProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupProvider())
        .build()

    private val server = protoSdcComponent.serverFactory().create(socketAddress)

    private val sdcDeviceFactory = protoSdcComponent.sdcDeviceFactory()
    private var sdcDevice = sdcDeviceFactory.create(
        udp,
        server,
        endpointIdentifier,
        metadata,
        operationInvocationReceiver,
        discoveryProxyAddress
    )

    init {
        onStartUp {
            server.start()
            sdcDevice.mdibAccess().writeDescription(MdibModificationsPresets().fullTree())
            sdcDevice.start()

        }

        onShutDown {
            sdcDevice.stop()
            server.stop()
        }
    }

    fun device(): SdcDevice = sdcDevice
}