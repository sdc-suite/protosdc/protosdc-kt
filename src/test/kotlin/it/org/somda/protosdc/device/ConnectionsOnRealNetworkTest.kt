package it.org.somda.protosdc.device

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import org.somda.protosdc.biceps.common.DescriptionItem
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.WaveformStates
import org.somda.protosdc.biceps.common.access.MdibAccessEvent
import org.somda.protosdc.biceps.common.storage.DefaultInstantiation
import org.somda.protosdc.model.biceps.*
import org.somda.protosdc.biceps.test.Handles
import org.somda.protosdc.device.provider.SdcDevice
import org.somda.protosdc.network.ip.ipVersion
import org.somda.protosdc.network.udp.SocketInfo
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.udp
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import org.somda.protosdc.test.util.DefaultWait
import org.somda.protosdc.test.util.LongWait
import org.somda.protosdc.test.util.NetworkInterfaceWithMulticastSupport
import java.math.BigDecimal
import java.net.InetAddress
import java.net.InetSocketAddress

class ConnectionsOnRealNetworkTest {
    private val host = InetSocketAddress("localhost", 0)
    private val ipV4Address = InetAddress.getByName("239.255.255.250")
    private val ipV4NetworkInterface = NetworkInterfaceWithMulticastSupport.findFor(ipV4Address.hostAddress).let {
        check(it != null)
        it
    }

    private var udp = createUdp()

    private fun createUdp() = udp {
        binding(
            UdpBindingConfig(
                ipV4NetworkInterface,
                ipV4Address.ipVersion(),
                SocketInfo(ipV4Address, 6464)
            )
        )
    }

    @BeforeEach
    fun beforeEach(): Unit = runBlocking {
        udp = createUdp().apply { start() }
    }

    @AfterEach
    fun afterEach(): Unit = runBlocking {
        udp.stop()
    }

    @Test
    fun `connect one consumer to provider by using ad hoc mode`(): Unit = runBlocking {
        run {
            val instanceIdentifier = "ConnectOneConsumerToProvider1"
            val testProvider = TestProvider(instanceIdentifier, udp, host).apply { start() }
            runCatching {
                withTimeout(LongWait.MILLIS * 10) {
                    TestConsumer(udp, null, instanceIdentifier) {
                        exchangeSomeData(testProvider.device())
                    }.apply {
                        start()
                        await()
                    }
                }
            }.onFailure { fail("Test failed because a connection could not be established", it) }
        }
        run {
            val instanceIdentifier = "ConnectOneConsumerToProvider2"
            val testProvider = TestProvider(instanceIdentifier, udp, host)
            runCatching {
                withTimeout(LongWait.MILLIS * 2) {
                    val testConsumer = TestConsumer(udp, null, instanceIdentifier) {
                        exchangeSomeData(testProvider.device())
                    }.apply {
                        start()
                    }
                    delay(1000)
                    testProvider.start()
                    testConsumer.await()
                    testProvider.stop()
                    testConsumer.stop()
                }
            }.onFailure { fail("Test failed because a connection could not be established", it) }
        }
    }

    @Test
    fun `connect one consumer to provider by using managed mode`(): Unit = runBlocking {
        val discoveryProxy = TestDiscoveryProxy(host).apply { start() }
        run {
            val instanceIdentifier = "ConnectOneConsumerToProvider1"
            val testProvider = TestProvider(
                instanceIdentifier,
                udp,
                host,
                EndpointMetadata.getDefaultInstance(),
                null,
                discoveryProxy.address()
            ).apply { start() }
            runCatching {
                withTimeout(LongWait.MILLIS * 2) {
                    TestConsumer(udp, discoveryProxy.address(), instanceIdentifier) {
                        exchangeSomeData(testProvider.device())
                    }.also {
                        it.start()
                        it.await()
                        testProvider.stop()
                        it.stop()
                    }
                }
            }.onFailure { fail("Test failed because a connection could not be established", it) }
        }
        run {
            val instanceIdentifier = "ConnectOneConsumerToProvider2"
            val testProvider = TestProvider(
                instanceIdentifier, udp, host,
                EndpointMetadata.getDefaultInstance(),
                null,
                discoveryProxy.address()
            )
            runCatching {
                withTimeout(DefaultWait.MILLIS * 2) {
                    val testConsumer =
                        TestConsumer(udp, discoveryProxy.address(), instanceIdentifier) {
                            exchangeSomeData(testProvider.device())
                        }.also {
                            it.start()
                        }
                    delay(1000)
                    testProvider.start()
                    testConsumer.await()
                    testProvider.stop()
                    testConsumer.stop()
                }
            }.onFailure { fail("Test failed because a connection could not be established", it) }
        }

        discoveryProxy.stop()
    }

    @Test
    fun `test multiple consumers against one provider`(): Unit = runBlocking {
        val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)
        val consumerCount = 100
        val discoveryProxy = TestDiscoveryProxy(host).apply { start() }
        val instanceIdentifier = "ConnectMultipleConsumersToProvider"
        val testProvider = TestProvider(
            instanceIdentifier,
            udp,
            host,
            EndpointMetadata.getDefaultInstance(),
            null,
            discoveryProxy.address()
        ).apply { start() }
        // waitForEndpoints(discoveryProxy.discoveryProxy(), 1)
        withTimeout(LongWait.MILLIS * consumerCount) {
            val consumers = mutableListOf<TestConsumer>()
            val subscriptions = mutableListOf<ReceiveChannel<MdibAccessEvent>>()
            for (i in 1..consumerCount) {
                consumers.add(
                    TestConsumer(udp, discoveryProxy.address(), instanceIdentifier) {
                        subscriptions.add(it.mdibAccess().subscribe())
                    }.apply {
                        start()
                        await()
                    }
                )
            }

            val parentJob = coroutineScopeBinding.launch {
                subscriptions.forEach {
                    launch {
                        receiveSomeData(it)
                    }
                }
            }

            writeSomeData(testProvider.device())

            parentJob.join()

            consumers.forEach { it.stop() }
        }

        testProvider.stop()
        discoveryProxy.stop()
    }

    private suspend fun writeSomeData(sdcDevice: SdcDevice) {
        val instances = DefaultInstantiation()

        val rtsaState = AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState(
            instances.defaultRealTimeSampleArrayMetricState(Handles.METRIC_2)
        )

        sdcDevice.mdibAccess().writeDescription(
            MdibDescriptionModifications().insert(
                DescriptionItem(
                    instances.descriptorOf(rtsaState),
                    listOf(rtsaState),
                    Handles.CHANNEL_1
                )
            )
        )

        for (i in 0..99) {
            val samples = mutableListOf<BigDecimal>().also {
                for (j in 1L..10L) {
                    it.add(BigDecimal.valueOf(i * 10 + j))
                }
            }

            sdcDevice.mdibAccess().writeStates(
                MdibStateModifications.Waveform(WaveformStates().apply {
                    add(
                        sdcDevice.mdibAccess()
                            .entity(Handles.METRIC_2, MdibEntity.RealTimeSampleArrayMetric::class)
                            .getOrThrow().state.let {
                                it.copy(
                                    abstractMetricState = it.abstractMetricState,
                                    metricValue = SampleArrayValue(
                                        abstractMetricValue = AbstractMetricValue(
                                            metricQuality = AbstractMetricValue.MetricQuality(
                                                validityAttr = MeasurementValidity(MeasurementValidity.EnumType.Vld)
                                            )
                                        ),
                                        samplesAttr = RealTimeValueType(samples)
                                    )
                                )
                            }
                    )
                })
            )
        }
    }

    private suspend fun exchangeSomeData(sdcDevice: SdcDevice) {
        // change local MDIB concurrently
        coroutineScope {
            val mdibSubscription = sdcDevice.mdibAccess().subscribe()
            val job = launch(Dispatchers.Default) {
                receiveSomeData(mdibSubscription)
            }
            launch(Dispatchers.Default) {
                writeSomeData(sdcDevice)
            }

            job.join()
        }
    }

    private suspend fun receiveSomeData(mdibSubscription: ReceiveChannel<MdibAccessEvent>) {
        // discard description modification report
        mdibSubscription.receive()

        // collect changes
        for (i in 0..99) {
            val event = mdibSubscription.receive()
            assertEquals(1, event.modifiedStates.size)
            val modification = event.modifiedStates.first()
            val state = modification.state
            require(state is AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState)
            requireNotNull(state.value.metricValue)
            requireNotNull(state.value.metricValue!!.samplesAttr)
            val decimals = state.value.metricValue!!.samplesAttr!!.decimal
            assertEquals(10, decimals.size)
            decimals.forEachIndexed { index, number ->
                assertEquals(BigDecimal.valueOf(i * 10L + index + 1L), number)
            }
        }
    }
}