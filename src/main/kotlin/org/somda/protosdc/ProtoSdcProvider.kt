package org.somda.protosdc

import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.common.UriUtil
import org.somda.protosdc.dagger.ProtoSdcComponent
import org.somda.protosdc.device.provider.SdcDevice
import org.somda.protosdc.device.provider.sco.OperationInvocationReceiver
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import java.net.URI

public class ProtoSdcProvider internal constructor(
    udp: Udp,
    server: Server,
    discoveryProxyUrl: URI?,
    daggerComponent: ProtoSdcComponent,
    init: Init.() -> Unit = {}
) : ServiceDelegate by ServiceDelegateImpl() {
    private val config = Init().apply(init)

    public val sdcDevice: SdcDevice = daggerComponent.sdcDeviceFactory().create(
        udp,
        server,
        config.endpointIdentifier,
        config.endpointMetadata,
        config.operationInvocationReceiver,
        discoveryProxyUrl
    )

    init {
        onStartUp {
            sdcDevice.discoveryProvider()
                .updateScopes(config.staticScopes.map { Uri.newBuilder().setValue(it.toString()).build() })
            sdcDevice.start()
        }

        onShutDown {
            sdcDevice.start()
        }
    }

    public class Init internal constructor() {
        internal var endpointIdentifier: String = UriUtil.randomUuid().toString()
        internal var endpointMetadata: EndpointMetadata = EndpointMetadata.newBuilder().build()
        internal var operationInvocationReceiver: OperationInvocationReceiver? = null
        internal val staticScopes: MutableList<URI> = mutableListOf()

        public fun endpointIdentifier(init: String) {
            endpointIdentifier = init
        }

        public fun endpointMetadata(init: EndpointMetadata) {
            endpointMetadata = init
        }

        public fun operationInvocationReceiver(init: OperationInvocationReceiver) {
            this.operationInvocationReceiver = init
        }

        public fun staticScope(vararg init: URI) {
            staticScopes.addAll(init)
        }

        public fun staticScope(init: Iterable<URI>) {
            staticScopes.addAll(init)
        }
    }
}

