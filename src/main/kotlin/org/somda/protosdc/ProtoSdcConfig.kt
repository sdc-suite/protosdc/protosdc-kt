package org.somda.protosdc

import kotlinx.serialization.Serializable
import org.somda.protosdc.biceps.BicepsConfig
import org.somda.protosdc.network.udp.UdpConfig

/**
 * Crypto configuration.
 *
 * @property userKeyFilePath Either an absolute file path or a relative file path (starting from the working directory)
 *                           to a PEM file containing the private key for the app.
 * @property userKeyPassword If the user key is password-protected, the password used to decrypt the PEM file.
 *                           Be careful to not expose the password to third parties.
 * @property userCertFilePath Either an absolute file path or a relative file path (starting from the working directory)
 *                            to a PEM file containing the public key for the app.
 * @property caCertFilePath Either an absolute file path or a relative file path (starting from the working directory)
 *                          to a PEM file containing trusted CAs / certificate chains.
 */
@Serializable
public data class CryptoConfig(
    val userKeyFilePath: String,
    val userKeyPassword: String,
    val userCertFilePath: String,
    val caCertFilePath: String
)

/**
 * gRPC configuration.
 *
 * @property serverInsecure Flag to allow servers accepting insecure (non-TLS) connections.
 *                          Clients will try to connect via unsecure channels.
 *                          Set to true to allow insecure connections, otherwise false.
 */
@Serializable
public data class GrpcConfig(
    val serverInsecure: Boolean = false
)

/**
 * Discovery configuration.
 *
 * @property maxWaitForSearchInMillis Defines the maximum waiting time for search requests until timeout.
 */
@Serializable
public data class DiscoveryConfig(
    val maxWaitForSearchInMillis: Long = 10_000
)

/**
 * Addressing configuration.
 *
 * @property messageIdCacheSize Defines the cache size for message id buffers.
 */
@Serializable
public data class AddressingConfig(
    val messageIdCacheSize: Int = 200
)

/**
 * Device configuration.
 *
 * @property awaitingTransactionDurationInMillis Defines the default maximum waiting time for a transaction to finish.
 */
@Serializable
public data class DeviceConfig(
    val awaitingTransactionDurationInMillis: Long = 10_000
)

/**
 * ProtoSDC-kt configuration.
 *
 * @property cryptography Settings that control cryptography (keystore, truststore, passwords).
 *                        Required if [GrpcConfig.serverInsecure] evaluates to false and no crypto store is provided
 *                        by other means.
 * @property maxStartupWaitInMillis Defines the maximum waiting time for services to startup in milliseconds.
 *                                  This is a general timeout when waiting for signals to be fired.
 *                                  The value should be adjusted based on the computational power and expected
 *                                  resource consumption of the CPU running the app.
 */
@Serializable
public data class ProtoSdcConfig(
    val cryptography: CryptoConfig? = null,
    val addressing: AddressingConfig = AddressingConfig(),
    val discovery: DiscoveryConfig = DiscoveryConfig(),
    val device: DeviceConfig = DeviceConfig(),
    val grpc: GrpcConfig = GrpcConfig(),
    val udp: UdpConfig = UdpConfig(),
    val bicepsConfig: BicepsConfig = BicepsConfig(),
    val maxStartupWaitInMillis: Long = 5_000
)