package org.somda.protosdc.discovery.common


private fun actionPrefix(): String = "org.somda.protosdc.discovery.action."

/**
 * Action qualifiers for protoSDC discovery messages.
 */
internal enum class Action(val stringRepresentation: String) {
    /**
     * Indication of a search request message.
     */
    SEARCH_REQUEST(actionPrefix() + "SearchRequest"),

    /**
     * Indication of a search response message.
     */
    SEARCH_RESPONSE(actionPrefix() + "SearchResponse"),

    /**
     * Indication of a hello message.
     */
    HELLO(actionPrefix() + "Hello"),

    /**
     * Indication of a bye message.
     */
    BYE(actionPrefix() + "Bye");
}