package org.somda.protosdc.discovery.common

import com.google.protobuf.AbstractMessageLite
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.network.udp.UdpMessageQueue

internal class UdpProtobufUtil constructor(private val messageQueue: UdpMessageQueue) {
    suspend fun sendMulticast(message: AbstractMessageLite<*, *>) {
        val bytes = message.toByteArray()
        messageQueue.sendMulticastMessage(bytes)
    }

    suspend fun sendResponse(message: AbstractMessageLite<*, *>, requestMessage: UdpMessage) =
        messageQueue.sendMessage(
            message.toByteArray(),
            requestMessage.header.ipHeader.sourceAddress(),
            requestMessage.header.sourcePort
        )
}