package org.somda.protosdc.discovery.common

import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.discovery.Endpoint
import org.somda.protosdc.proto.model.discovery.ScopeMatcher
import org.somda.protosdc.proto.model.discovery.SearchFilter
import java.net.URI

/**
 * Matches an endpoint against search filters.
 *
 * @param endpoint the endpoint to match.
 * @param searchFilters the search filters to apply.
 * @return true if the search filter collection is empty, or if the search filter includes the endpoint's
 * identifier, or if all scopes of the search filter are included in the endpoint's list of scopes;
 * otherwise false.
 */
internal fun matchSearch(endpoint: Endpoint, searchFilters: Collection<SearchFilter>): Boolean {
    if (searchFilters.isEmpty()) {
        return true
    }

    val endpointIdentifierFilters = searchFilters.filter { it.typeCase == SearchFilter.TypeCase.ENDPOINT_IDENTIFIER }
    if (endpointIdentifierFilters.any { it.endpointIdentifier == endpoint.endpointIdentifier }) {
        return true
    }

    val scopeFilters = searchFilters.filter { it.typeCase == SearchFilter.TypeCase.SCOPE_MATCHER }
    val matchedScopesCount = scopeFilters.filter { searchFilter ->
        endpoint.scopeList.any { uri: Uri ->
            return when (searchFilter.scopeMatcher.algorithm) {
                ScopeMatcher.Algorithm.RFC_3986 ->
                    URI.create(searchFilter.scopeMatcher.scope.value) == URI.create(uri.value)
                ScopeMatcher.Algorithm.STRING_COMPARE ->
                    searchFilter.scopeMatcher.scope.value == uri.value
                else -> false
            }
        }
    }.count()

    return scopeFilters.isNotEmpty() && matchedScopesCount == scopeFilters.size
}
