package org.somda.protosdc.discovery.proxy

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc.addressing.AddressingValidator
import org.somda.protosdc.addressing.createFor
import org.somda.protosdc.addressing.discloseValidationError
import org.somda.protosdc.addressing.replyTo
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.discovery.common.Action
import org.somda.protosdc.discovery.common.matchSearch
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.proto.model.discovery.*
import java.net.URI

internal class DiscoveryProxyImpl @AssistedInject constructor(
    @Assisted private val server: Server,
    instanceId: InstanceId,
    private val addressingValidator: AddressingValidator
) : DiscoveryProxy, ServiceDelegate by ServiceDelegateImpl() {
    @AssistedFactory
    interface Factory {
        fun create(
            server: Server
        ): DiscoveryProxyImpl
    }

    private companion object : Logging

    private val mutex = Mutex()
    private val endpointRepository: MutableMap<String, Endpoint> = mutableMapOf()

    private val helloAndByePublisher = ChannelPublisher<ProxySubscribeHelloAndByeResponse>(
        "$instanceId@DiscoveryProxyHelloAndByePublisher"
    )

    init {
        onStartUp {
            logger.info { "Starting discovery proxy" }
            server.registerService(ConsumerProxyService())
            server.registerService(ProviderProxyService())
            logger.info { "Discovery proxy has started" }
        }

        onShutDown {
            logger.info { "Stopping discovery proxy" }
            helloAndByePublisher.unsubscribeAll()
            logger.info { "Discovery proxy shut down" }
        }
    }

    override suspend fun endpointCount(): Int = endpointRepository.size

    override suspend fun endpointUri(): URI {
        return server.addressAsUri()
    }

    private inner class ConsumerProxyService : ConsumerProxyServiceGrpcKt.ConsumerProxyServiceCoroutineImplBase() {
        override suspend fun search(request: ProxySearchRequest): ProxySearchResponse {
            addressingValidator.create(request.addressing)
                .validateMessageId()
                .validateAction(Action.SEARCH_REQUEST.stringRepresentation)

            val filteredEndpoints = mutex.withLock {
                endpointRepository.values.filter { endpoint ->
                    matchSearch(
                        endpoint,
                        request.searchFilterList
                    )
                }
            }

            logger.debug { "Incoming search request" }
            logger.trace {
                "Incoming search request: $request; " +
                        "filtered endpoints for result: ${filteredEndpoints.joinToString()}"
            }

            return ProxySearchResponse.newBuilder()
                .setAddressing(
                    replyTo(
                        request.addressing,
                        Action.SEARCH_RESPONSE.stringRepresentation
                    )
                )
                .addAllEndpoint(filteredEndpoints).build()
        }

        override fun subscribeHelloAndBye(request: ProxySubscribeHelloAndByeRequest) = flow {
            val subscription = helloAndByePublisher.subscribe()
            while (currentCoroutineContext().isActive) {
                try {
                    subscription.receive().also {
                        logger.debug { "Outgoing hello/bye to consumer" }
                        emit(it)
                    }
                } catch (e: Exception) {
                    logger.info { "Hello/bye subscription ended: ${e.message}" }
                    subscription.cancel()
                    break
                }
            }
        }
    }

    private suspend fun distributeHello(endpoint: Endpoint) {
        ProxySubscribeHelloAndByeResponse.newBuilder()
            .setAddressing(createFor(Action.HELLO.stringRepresentation))
            .setHello(Hello.newBuilder().setEndpoint(endpoint))
            .build().also {
                logger.debug { "Distribute hello" }
                logger.trace { "Distribute hello: $it" }
                helloAndByePublisher.send(it)
            }
    }

    private suspend fun distributeBye(endpoint: Endpoint) {
        ProxySubscribeHelloAndByeResponse.newBuilder()
            .setAddressing(createFor(Action.BYE.stringRepresentation))
            .setBye(Bye.newBuilder().setEndpoint(endpoint))
            .build().also {
                logger.debug { "Distribute bye" }
                logger.trace { "Distribute bye: $it" }
                helloAndByePublisher.send(it)
            }
    }

    private inner class ProviderProxyService : ProviderProxyServiceGrpcKt.ProviderProxyServiceCoroutineImplBase() {
        override suspend fun notifyHelloAndBye(requests: Flow<ProxyNotifyHelloAndByeRequest>): ProxyNotifyHelloAndByeResponse {
            requests.collect { notifyHelloAndByeRequest ->
                logger.info("Incoming hello/bye from provider")
                when (notifyHelloAndByeRequest.typeCase) {
                    ProxyNotifyHelloAndByeRequest.TypeCase.HELLO -> {
                        discloseValidationError {
                            addressingValidator.create(notifyHelloAndByeRequest.addressing)
                                .validateMessageId()
                                .validateAction(Action.HELLO.stringRepresentation)
                        }
                        notifyHelloAndByeRequest.hello.endpoint
                            .also { mutex.withLock { endpointRepository[it.endpointIdentifier] = it } }
                            .also { distributeHello(it) }
                    }

                    ProxyNotifyHelloAndByeRequest.TypeCase.BYE -> {
                        discloseValidationError {
                            addressingValidator.create(notifyHelloAndByeRequest.addressing)
                                .validateMessageId()
                                .validateAction(Action.BYE.stringRepresentation)
                        }
                        notifyHelloAndByeRequest.bye.endpoint
                            .also { mutex.withLock { endpointRepository.remove(it.endpointIdentifier) } }
                            .also { distributeBye(notifyHelloAndByeRequest.bye.endpoint) }
                    }

                    else -> Unit
                }
                logger.info("Incoming hello/bye has been notified to consumers")
            }
            return ProxyNotifyHelloAndByeResponse.getDefaultInstance()
        }
    }
}