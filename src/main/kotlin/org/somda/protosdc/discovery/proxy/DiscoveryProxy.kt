package org.somda.protosdc.discovery.proxy

import org.somda.protosdc.common.Service
import org.somda.protosdc.network.grpc.server.Server
import java.net.URI

/**
 * Discovery proxy interface.
 *
 * The discovery proxy operates without any public API.
 */
public interface DiscoveryProxy : Service {
    /**
     * Exposes the current number of registered endpoints.
     */
    public suspend fun endpointCount(): Int

    public suspend fun endpointUri(): URI
}