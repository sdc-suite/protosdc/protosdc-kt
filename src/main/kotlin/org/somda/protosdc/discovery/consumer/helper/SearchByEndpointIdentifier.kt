package org.somda.protosdc.discovery.consumer.helper

import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.withTimeout
import org.somda.protosdc.discovery.consumer.EndpointNotFoundException
import org.somda.protosdc.discovery.consumer.common.DiscoveryLogger
import org.somda.protosdc.proto.model.discovery.DiscoveryMessage
import org.somda.protosdc.proto.model.discovery.Endpoint
import kotlin.time.Duration

internal class SearchByEndpointIdentifier(
    private val endpointIdentifier: String,
    private val requestMessageId: String,
    private val maxWait: Duration,
    private val incomingChannel: ReceiveChannel<DiscoveryMessage>,
    private val logger: DiscoveryLogger
) {
    suspend operator fun invoke(): Result<Endpoint> {
        try {
            var wait = maxWait.inWholeMilliseconds
            val tStartInMillis = System.currentTimeMillis()
            while (wait > 0) {
                try {
                    val result = withTimeout(wait) {
                        logger.debugAdhoc({ "SearchByEndpointIdentifier($requestMessageId) is waiting for an incoming message" })
                        val event = incomingChannel.receive()
                        logger.debugAdhoc({ "SearchByEndpointIdentifier($requestMessageId) received a message" })
                        wait -= System.currentTimeMillis() - tStartInMillis
                        if (!event.hasSearchResponse() ||
                            !event.addressing.hasRelatesId() ||
                            event.addressing.relatesId.value != requestMessageId ||
                            event.searchResponse.endpointList.size != 1
                        ) {
                            return@withTimeout null
                        } else {
                            logger.debugAdhoc(
                                { "SearchByEndpointIdentifier($requestMessageId) received a result" },
                                { "SearchByEndpointIdentifier($requestMessageId) received a result: ${event.searchResponse}" }
                            )
                            return@withTimeout event.searchResponse.endpointList.first()
                        }
                    }
                    if (result != null) {
                        return Result.success(result)
                    }
                } catch (e: TimeoutCancellationException) {
                    break
                }
            }
            logger.debugAdhoc({ "SearchByEndpointIdentifier($requestMessageId) ended without a result" })
            return Result.failure(EndpointNotFoundException(endpointIdentifier))
        } finally {
            incomingChannel.cancel()
        }
    }
}