package org.somda.protosdc.discovery.consumer

import com.google.protobuf.InvalidProtocolBufferException
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.*
import org.somda.protosdc.DiscoveryConfig
import org.somda.protosdc.addressing.AddressingValidator
import org.somda.protosdc.addressing.ValidationException
import org.somda.protosdc.addressing.createFor
import org.somda.protosdc.common.*
import org.somda.protosdc.discovery.common.Action
import org.somda.protosdc.discovery.common.UdpProtobufUtil
import org.somda.protosdc.discovery.consumer.common.DiscoveryLogger
import org.somda.protosdc.discovery.consumer.helper.Search
import org.somda.protosdc.discovery.consumer.helper.SearchByEndpointIdentifier
import org.somda.protosdc.network.grpc.client.ChannelFactory
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.network.udp.UdpMessageQueue
import org.somda.protosdc.proto.model.addressing.Addressing
import org.somda.protosdc.proto.model.discovery.*
import java.io.ByteArrayInputStream
import java.net.InetSocketAddress
import java.net.URI
import java.time.Duration
import java.util.concurrent.atomic.AtomicInteger
import kotlin.time.Duration.Companion.milliseconds

internal class DiscoveryConsumerImpl @AssistedInject constructor(
    @Assisted private val udpMessageQueue: UdpMessageQueue,
    @Assisted private val discoveryProxyAddress: URI?,
    discoveryConfig: DiscoveryConfig,
    instanceId: InstanceId,
    private val grpcChannelFactory: ChannelFactory,
    private val addressingValidator: AddressingValidator
) : DiscoveryConsumer, ServiceDelegate by ServiceDelegateImpl() {
    private val maxWaitForSearch = Duration.ofMillis(discoveryConfig.maxWaitForSearchInMillis)

    @AssistedFactory
    interface Factory {
        fun create(
            udpMessageQueue: UdpMessageQueue,
            discoveryProxyAddress: URI?
        ): DiscoveryConsumerImpl
    }

    companion object {
        private val discoveryIdCounter = AtomicInteger(0)
    }

    private lateinit var incomingUdpMessages: ReceiveChannel<UdpMessage>
    private val logger by InstanceLogger(instanceId())
    private val prefixedLogger = DiscoveryLogger(logger)

    private val publisher = ChannelPublisher<DiscoveryEvent>("$instanceId@DiscoveryPublisher")

    private val searchResponsePublisher = ChannelPublisher<DiscoveryMessage>("$instanceId@SearchResponsePublisher")

    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)

    private val udpUtil = UdpProtobufUtil(udpMessageQueue)

    private lateinit var discoveryProxyService: ConsumerProxyServiceGrpcKt.ConsumerProxyServiceCoroutineStub

    init {
        onStartUp {
            if (discoveryProxyAddress == null) {
                logger.info { "No Discovery Proxy selected. Switch to ad-hoc mode" }
                runAdhocMode()
            } else {
                logger.info { "Discovery Proxy selected: $discoveryProxyAddress. Switch to managed mode" }
                runManagedMode()
            }

            logger.info { "Discovery consumer is running" }
        }

        onShutDown {
            if (discoveryProxyAddress == null) {
                incomingUdpMessages.cancel()
                coroutineScopeBinding.cancel()
            }
            logger.info { "Discovery consumer shut down" }
        }
    }

    private suspend fun runAdhocMode() {
        if (discoveryProxyAddress != null) {
            return
        }
        incomingUdpMessages = udpMessageQueue.subscribe()
        coroutineScopeBinding.launch(CoroutineName("Incoming discovery consumer messages")) {
            while (isActive) {
                try {
                    val discoveryMessage = incomingUdpMessages.receive()
                        .let {
                            awaitWithIoDispatcher {
                                DiscoveryMessage.parseFrom(ByteArrayInputStream(it.data, 0, it.header.length))
                            }.getOrThrow()
                        }

                    try {
                        addressingValidator.create(getAddressing(discoveryMessage)).validateMessageId()
                    } catch (e: ValidationException) {
                        // throw away message duplicates which occur due to UDP message retransmission
                    } catch (e: IllegalArgumentException) {
                        // ignore messages that are not processed by the consumer
                        continue
                    }

                    when (discoveryMessage.typeCase) {
                        DiscoveryMessage.TypeCase.HELLO -> {
                            prefixedLogger.debugAdhoc(
                                { "Received hello message" },
                                { "Received hello message: $discoveryMessage" }
                            )
                            validateHello(discoveryMessage.addressing)
                            publisher.send(
                                DiscoveryEvent.EndpointEntered(
                                    discoveryMessage.hello.endpoint,
                                    false
                                )
                            )
                        }
                        DiscoveryMessage.TypeCase.SEARCH_RESPONSE -> {
                            prefixedLogger.debugAdhoc(
                                { "Received search response message" },
                                { "Received search response message: $discoveryMessage" }
                            )
                            validateSearchResponse(discoveryMessage.addressing)
                            searchResponsePublisher.send(discoveryMessage)
                        }
                        else -> {
                            prefixedLogger.debugAdhoc(
                                { "Received unsupported discovery message over UDP" },
                                { "Received unsupported discovery message over UDP: ${discoveryMessage.typeCase}" }
                            )
                        }
                    }
                } catch (e: ClosedReceiveChannelException) {
                    prefixedLogger.warnAdhoc(e) { "Outgoing UdpMessageQueue ended: ${e.message}" }
                    break
                } catch (e: InvalidProtocolBufferException) {
                    prefixedLogger.debugAdhoc(
                        { "Protocol Buffers message processing error" },
                        { "Protocol Buffers message processing error: ${e.message}" }
                    )
                } catch (e: ValidationException) {
                    prefixedLogger.debugAdhoc(
                        { "Validation exception" },
                        { "Validation exception: ${e.message}" }
                    )
                } catch (e: Exception) {
                    prefixedLogger.warnAdhoc(e) { "Exception thrown: ${e.message}" }
                }
            }
        }
    }

    override suspend fun subscribe() = publisher.subscribe()

    override suspend fun searchAsync(
        searchFilters: Collection<SearchFilter>,
        maxResults: Int
    ): DeferredSearchResult<List<Endpoint>> {
        checkRunning()
        return if (discoveryProxyAddress == null) {
            val addressing = createFor(Action.SEARCH_REQUEST.stringRepresentation)
            val search: SearchRequest.Builder = SearchRequest.newBuilder()
                .addAllSearchFilter(searchFilters)
            val discoveryMessage = DiscoveryMessage.newBuilder()
                .setAddressing(addressing)
                .setSearchRequest(search)
                .build()
            prefixedLogger.debugAdhoc(
                { "Search endpoints" },
                { "Search endpoints: $discoveryMessage" }
            )
            val searchResponseSubscription = searchResponsePublisher.subscribe()
            val async = asyncSearch {
                Search(
                    it,
                    addressing.messageId,
                    maxResults,
                    maxWaitForSearch.toMillis().milliseconds,
                    publisher,
                    searchResponseSubscription,
                    prefixedLogger
                ).invoke()
            }
            udpUtil.sendMulticast(discoveryMessage)
            async
        } else {
            val discoveryMessage = ProxySearchRequest.newBuilder()
                .setAddressing(createFor(Action.SEARCH_REQUEST.stringRepresentation))
                .addAllSearchFilter(searchFilters).build()
            prefixedLogger.debugManaged(
                { "Search endpoints" },
                { "Search endpoints: $discoveryMessage" }
            )
            asyncSearch {
                discoveryProxyService.search(discoveryMessage).endpointList
            }
        }
    }

    override suspend fun searchAsync(searchFilter: SearchFilter, maxResults: Int) =
        searchAsync(listOf(searchFilter), maxResults)

    override suspend fun searchAsync(searchFilter: SearchFilter): DeferredSearchResult<Result<Endpoint>> =
        checkRunning {
            asyncSearch {
                prefixedLogger.debugManaged(
                    { "Search endpoint by filter" },
                    { "Search endpoint by filter: $searchFilter" }
                )
                try {
                    Result.success(searchAsync(searchFilter, 1).await().first())
                } catch (e: NoSuchElementException) {
                    Result.failure(EndpointNotFoundException("Did not found a matching endpoint for search $searchFilter"))
                } catch (e: Exception) {
                    Result.failure(e)
                }
            }
        }

    override suspend fun searchByEndpointIdentifierAsync(endpointIdentifier: String): DeferredSearchResult<Result<Endpoint>> {
        checkRunning()
        return if (discoveryProxyAddress == null) {
            prefixedLogger.debugAdhoc(
                { "Search endpoint by endpoint identifier" },
                { "Search endpoint by endpoint identifier: $endpointIdentifier" }
            )
            val addressing = createFor(Action.SEARCH_REQUEST.stringRepresentation)
            val search = SearchRequest.newBuilder()
                .addSearchFilter(SearchFilter.newBuilder().setEndpointIdentifier(endpointIdentifier))
            val discoveryMessage = DiscoveryMessage.newBuilder()
                .setAddressing(addressing)
                .setSearchRequest(search)
                .build()
            SearchByEndpointIdentifier(
                endpointIdentifier,
                addressing.messageId,
                maxWaitForSearch.toMillis().milliseconds,
                searchResponsePublisher.subscribe(),
                prefixedLogger
            ).let { runSearch -> asyncSearch { runSearch() } }.also { udpUtil.sendMulticast(discoveryMessage) }
        } else {
            prefixedLogger.debugManaged(
                { "Search endpoint by endpoint identifier" },
                { "Search endpoint by endpoint identifier: $endpointIdentifier" }
            )
            asyncSearch {
                val searchResponse = discoveryProxyService.search(
                    ProxySearchRequest.newBuilder()
                        .setAddressing(createFor(Action.SEARCH_REQUEST.stringRepresentation))
                        .addSearchFilter(SearchFilter.newBuilder().setEndpointIdentifier(endpointIdentifier)).build()
                )
                if (searchResponse.endpointCount != 1) {
                    return@asyncSearch Result.failure(EndpointNotFoundException(endpointIdentifier))
                }
                Result.success(searchResponse.getEndpoint(0))
            }
        }
    }

    @Throws(IllegalArgumentException::class)
    private fun getAddressing(discoveryMessage: DiscoveryMessage): Addressing {
        return when (discoveryMessage.typeCase) {
            DiscoveryMessage.TypeCase.HELLO -> discoveryMessage.addressing
            DiscoveryMessage.TypeCase.SEARCH_RESPONSE -> discoveryMessage.addressing
            else -> throw IllegalArgumentException(
                "Discovery message of type ${discoveryMessage.typeCase} is ignored",
            )
        }
    }

    private suspend fun runManagedMode() {
        requireNotNull(discoveryProxyAddress)
        InetSocketAddress(discoveryProxyAddress.host, discoveryProxyAddress.port).also { socketAddr ->
            grpcChannelFactory.create(socketAddr).also { channel ->
                discoveryProxyService = ConsumerProxyServiceGrpcKt.ConsumerProxyServiceCoroutineStub(channel).apply {
                    subscribeHelloAndBye(ProxySubscribeHelloAndByeRequest.getDefaultInstance())
                        .onStart { prefixedLogger.infoManaged { "Subscribe for hello/bye at $discoveryProxyAddress" } }
                        .onEach { handleHelloAndBye(it) }
                        .onCompletion {
                            logger.info {
                                when (it == null) {
                                    true -> "Managed mode run completed successfully"
                                    false -> "Managed mode run completed with message:${it.message}"
                                }
                            }
                        }
                        .catch { prefixedLogger.warnManaged(it) { "Exception thrown while being subscribed to hello/bye: ${it.message}" } }
                        .launchIn(coroutineScopeBinding)
                }
            }
        }
    }

    private suspend fun handleHelloAndBye(subscribeHelloAndByeResponse: ProxySubscribeHelloAndByeResponse) {
        when (subscribeHelloAndByeResponse.typeCase) {
            ProxySubscribeHelloAndByeResponse.TypeCase.HELLO -> {
                prefixedLogger.debugManaged({
                    "Device entered the network. Device identifier: " +
                            subscribeHelloAndByeResponse.hello.endpoint.endpointIdentifier
                })
                publisher.send(
                    DiscoveryEvent.EndpointEntered(
                        subscribeHelloAndByeResponse.hello.endpoint,
                        grpcChannelFactory.secured
                    )
                )
            }
            ProxySubscribeHelloAndByeResponse.TypeCase.BYE -> {
                prefixedLogger.debugManaged({
                    "Device entered the network. Device identifier: " +
                            subscribeHelloAndByeResponse.hello.endpoint.endpointIdentifier
                })
                publisher.send(
                    DiscoveryEvent.EndpointLeft(
                        subscribeHelloAndByeResponse.bye.endpoint,
                        grpcChannelFactory.secured
                    )
                )
            }
            else -> {
                prefixedLogger.warnManaged {
                    "Received unknown hello-and-bye subscribe request: ${subscribeHelloAndByeResponse.typeCase}"
                }
            }
        }
    }

    @Throws(ValidationException::class)
    private suspend fun validateSearchResponse(addressing: Addressing) {
        addressingValidator.create(addressing).validateAction(Action.SEARCH_RESPONSE.stringRepresentation)
    }

    @Throws(ValidationException::class)
    private suspend fun validateHello(addressing: Addressing) {
        addressingValidator.create(addressing).validateAction(Action.HELLO.stringRepresentation)
    }

    private fun <T> asyncSearch(block: suspend (searchId: String) -> T) =
        "searchId(${discoveryIdCounter.incrementAndGet()}@$this)".let {
            DeferredSearchResult(it, coroutineScopeBinding.async {
                block(it)
            })
        }
}