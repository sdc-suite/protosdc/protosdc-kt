package org.somda.protosdc.discovery.consumer.helper

import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.withTimeout
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.discovery.consumer.DiscoveryEvent
import org.somda.protosdc.discovery.consumer.common.DiscoveryLogger
import org.somda.protosdc.proto.model.discovery.DiscoveryMessage
import org.somda.protosdc.proto.model.discovery.Endpoint
import kotlin.time.Duration

internal class Search(
    private val searchId: String,
    private val requestMessageId: String,
    private val maxResults: Int,
    private val maxWait: Duration,
    private val discoveryEventPublisher: ChannelPublisher<DiscoveryEvent>,
    private val incomingChannel: ReceiveChannel<DiscoveryMessage>,
    private val logger: DiscoveryLogger
) {
    private val endpoints = ArrayList<Endpoint>()

    suspend operator fun invoke(): List<Endpoint> {
        var searchMatchesCount = 0
        var wait = maxWait.inWholeMilliseconds
        val tStartInMillis = System.currentTimeMillis()
        try {
            logger.debugAdhoc({ "Search($requestMessageId) started" })
            while (wait > 0) {
                try {
                    if (withTimeout(wait) {
                            val event = incomingChannel.receive()
                            wait -= System.currentTimeMillis() - tStartInMillis
                            if (!event.hasSearchResponse() ||
                                !event.addressing.hasRelatesId() ||
                                event.addressing.relatesId.value != requestMessageId
                            ) {
                                return@withTimeout false
                            }

                            logger.debugAdhoc(
                                { "Search($requestMessageId) received a result" },
                                { "Search($requestMessageId) received a result: ${event.searchResponse}" }
                            )

                            discoveryEventPublisher.send(
                                DiscoveryEvent.SearchedEndpointFound(
                                    event.searchResponse.endpointList,
                                    false,
                                    searchId
                                )
                            )
                            endpoints.addAll(event.searchResponse.endpointList)
                            return@withTimeout ++searchMatchesCount == maxResults
                        }) {
                        break
                    }
                } catch (e: TimeoutCancellationException) {
                    break
                }
            }
            logger.debugAdhoc({ "Search($requestMessageId) has ended" })
            discoveryEventPublisher.send(DiscoveryEvent.EndpointSearchTimeout(searchMatchesCount, searchId))
            return endpoints
        } finally {
            incomingChannel.cancel()
        }
    }
}