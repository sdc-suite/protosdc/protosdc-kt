package org.somda.protosdc.network.grpc.server

import io.grpc.*
import java.security.cert.X509Certificate
import javax.net.ssl.SSLSession

internal class SslSessionInterceptor : ServerInterceptor {
    companion object {
        val SSL_SESSION_CONTEXT: Context.Key<SSLSession> = Context.key("SSLSession")
        fun x509Certificates() = kotlin.runCatching {
            SSL_SESSION_CONTEXT.get().peerCertificates.filterIsInstance(
                X509Certificate::class.java
            ).toList()
        }.getOrDefault(listOf())
    }

    override fun <ReqT, RespT> interceptCall(
        call: ServerCall<ReqT, RespT>,
        headers: Metadata, next: ServerCallHandler<ReqT, RespT>
    ): ServerCall.Listener<ReqT> {
        val sslSession = call.attributes.get(Grpc.TRANSPORT_ATTR_SSL_SESSION)
            ?: return next.startCall(call, headers)
        return Contexts.interceptCall(
            Context.current().withValue(SSL_SESSION_CONTEXT, sslSession), call, headers, next
        )
    }
}