package org.somda.protosdc.network.grpc.server

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.grpc.BindableService
import io.grpc.ServerInterceptor
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder
import io.grpc.netty.shaded.io.netty.handler.ssl.ClientAuth
import io.grpc.netty.shaded.io.netty.handler.ssl.SslContextBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.somda.protosdc.GrpcConfig
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.common.awaitWithIoDispatcher
import org.somda.protosdc.common.checkRunning
import org.somda.protosdc.crypto.CryptoStore
import org.somda.protosdc.crypto.CryptoUtil
import java.net.InetSocketAddress
import java.net.URI
import javax.annotation.Nullable

/**
 * A gRPC server implementation that supports TLS.
 */
internal class Server @AssistedInject constructor(
    @Assisted private val networkAddress: InetSocketAddress,
    @Nullable private val cryptoStore: CryptoStore?,
    grpcConfig: GrpcConfig,
    instanceId: InstanceId
) : ServiceDelegate by ServiceDelegateImpl() {
    private val insecure = grpcConfig.serverInsecure
    private val logger by InstanceLogger(instanceId())
    private val services = mutableListOf<BindableService>()
    private val interceptors = mutableListOf<ServerInterceptor>()
    private var server: io.grpc.Server? = null

    private lateinit var address: InetSocketAddress
    private lateinit var addressAsUri: URI

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted networkAddress: InetSocketAddress
        ): Server
    }

    init {
        onStartUp {
            awaitWithIoDispatcher {
                val serverBuilder = NettyServerBuilder.forAddress(networkAddress)
                if (!insecure) {
                    require(cryptoStore != null) { "Cannot start server, no crypto settings available" }

                    // load crypto stuff
                    val keyStoreManagerFactory = try {
                        CryptoUtil.loadKeyStore(cryptoStore)
                    } catch (e: Exception) {
                        logger.error("Keystore could not be loaded", e)
                        throw e
                    }
                    val trustStoreManagerFactory = try {
                        CryptoUtil.loadTrustStore(cryptoStore)
                    } catch (e: Exception) {
                        logger.error("Truststore could not be loaded", e)
                        throw e
                    }
                    serverBuilder.sslContext(
                        GrpcSslContexts.configure(
                            SslContextBuilder.forServer(keyStoreManagerFactory)
                                .trustManager(trustStoreManagerFactory)
                        )
                            .clientAuth(ClientAuth.REQUIRE)
                            .build()
                    )
                }

                services.forEach(serverBuilder::addService)
                interceptors.forEach(serverBuilder::intercept)

                val server = serverBuilder.build().also {
                    this.server = it
                    it.start()
                }

                address = server.listenSockets
                    .filterIsInstance<InetSocketAddress>()
                    .firstOrNull() ?: InetSocketAddress(networkAddress.hostName, server.port)
                val scheme = when (insecure) {
                    true -> "http"
                    false -> "https"
                }
                val address = networkAddress.address.hostAddress
                val port = server.port
                addressAsUri = URI.create("$scheme://$address:$port")
            }
        }

        onShutDown {
            awaitWithIoDispatcher {
                server?.shutdownNow()?.awaitTermination()
            }
        }
    }

    /**
     * Registers a service, only allowed before startup.
     *
     * @param service to register with the server.
     * @throws IllegalStateException if registering a service on a running server.
     */
    fun registerService(service: BindableService) {
        check(server == null) { "Adding services during runtime is unsupported" }
        logger.info("Adding service ${service.javaClass.simpleName} to gRPC server")
        services.add(service)
    }

    /**
     * Registers a server interceptor, only allowed before startup.
     *
     * @param interceptor to register with the server.
     * @throws IllegalStateException if registering an interceptor on a running server.
     */
    fun registerInterceptor(interceptor: ServerInterceptor) {
        check(server == null) { "Adding interceptors during runtime is unsupported" }
        logger.info("Adding interceptor ${interceptor.javaClass.simpleName} to gRPC server")
        interceptors.add(interceptor)
    }

    /**
     * Retrieves the address the server is running on.
     *
     * @return the address.
     * @throws IllegalStateException if the server isn't running and therefore has no address.
     */
    suspend fun address() = checkRunning { address }

    /**
     * Retrieves the address the server is running on as [java.net.URI].
     *
     * @return the address.
     * @throws IllegalStateException if the server isn't running and therefore has no address.
     */
    suspend fun addressAsUri() = checkRunning { addressAsUri }
}