package org.somda.protosdc.addressing

import com.google.protobuf.StringValue
import io.grpc.Status
import io.grpc.StatusException
import org.somda.protosdc.proto.model.addressing.Addressing
import java.util.*

/**
 * Creates an addressing header for the specified action including a random UUID as message id.
 *
 * @param action the action to be put to the header.
 * @return the builder to build the header.
 */
public fun createFor(action: String): Addressing.Builder {
    return Addressing.newBuilder()
        .setMessageId(UUID.randomUUID().toString())
        .setAction(action)
}

/**
 * Creates an addressing reply header for the specified addressing header and action with a random UUID message id.
 *
 * @param action the action to be put to the header.
 * @return the builder to build the header.
 */
public fun replyTo(request: Addressing, action: String): Addressing.Builder {
    return Addressing.newBuilder()
        .setMessageId(UUID.randomUUID().toString())
        .setRelatesId(StringValue.newBuilder().setValue(request.messageId).build())
        .setAction(action)
}

/**
 * Executes the given code block, catches [ValidationException] instances and transforms them into [StatusException]
 * to be communicated via gRPC.
 */
public suspend fun discloseValidationError(block: suspend () -> Unit) {
    try {
        block()
    } catch (e: ValidationException) {
        throw StatusException(
            Status.INVALID_ARGUMENT.withDescription(
                "Addressing validation failed: ${e.message}"
            )
        )
    }
}