package org.somda.protosdc.addressing

import com.google.common.collect.EvictingQueue
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.AddressingConfig
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Message id buffer to detect duplicates message ids.
 */
@Singleton
internal class MessageDuplicateDetection @Inject constructor(
    addressingConfig: AddressingConfig
) {
    private val messageIdCache: EvictingQueue<String> = EvictingQueue.create(addressingConfig.messageIdCacheSize)

    private val mutex = Mutex()

    /**
     * Accepts a message id, checks if it already exists in the buffer and adds it to the buffer if not.
     *
     * @param messageId the message if to check.
     * @return true if the message already exists, false otherwise.
     */
    suspend fun isDuplicate(messageId: String): Boolean {
        mutex.withLock {
            val foundMessageId = messageIdCache.stream()
                .filter { anObject -> messageId == anObject }
                .findFirst()
            return if (foundMessageId.isPresent) {
                true
            } else {
                messageIdCache.add(messageId)
                false
            }
        }
    }
}
