package org.somda.protosdc.addressing

import org.somda.protosdc.proto.model.addressing.Addressing
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Helper class to validate addressing header.
 */
@Singleton
internal class AddressingValidator @Inject constructor(
    private val messageDuplicateDetection: MessageDuplicateDetection
) {
    /**
     * Creates a validator based on the given addressing header.
     *
     * @param addressing addressing to be validated.
     * @return a validator object on which validation can be performed.
     */
    fun create(addressing: Addressing): Validator {
        return object : Validator {
            @Throws(ValidationException::class)
            override suspend fun validateAction(action: String): Validator {
                // todo bypass until actions are aligned; see https://gitlab.com/sdc-suite/protosdc-kt/-/issues/18
                if (action != addressing.action && false) {
                    throw ValidationException(
                        String.format(
                            "Expected action differs from actual action: %s != %s",
                            addressing.action,
                            action
                        )
                    )
                }
                return this
            }

            @Throws(ValidationException::class)
            override suspend fun validateMessageId(): Validator {
                val messageId = addressing.messageId
                if (messageDuplicateDetection.isDuplicate(messageId)) {
                    throw ValidationException(String.format("Message ID %s already seen", messageId))
                }
                return this
            }
        }
    }

    /**
     * A validator object that supports validation of addressing headers.
     */
    interface Validator {
        /**
         * Checks if the action matches the action of the validated addressing header.
         *
         * @param action the action to check.
         * @return fluent interface.
         * @throws ValidationException if the action does not match the addressing header action.
         */
        @Throws(ValidationException::class)
        suspend fun validateAction(action: String): Validator

        /**
         * Checks if the message id of the validated addressing header is not a duplicate.
         *
         * @return fluent interface.
         * @throws ValidationException if the message id of the addressing header is found in an internal message id
         *                             cache.
         */
        @Throws(ValidationException::class)
        suspend fun validateMessageId(): Validator
    }
}