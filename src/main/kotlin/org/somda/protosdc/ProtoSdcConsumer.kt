package org.somda.protosdc

import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.dagger.ProtoSdcComponent
import org.somda.protosdc.device.consumer.SdcDeviceDiscovery
import org.somda.protosdc.network.udp.Udp
import java.net.URI

public class ProtoSdcConsumer internal constructor(
    udp: Udp,
    discoveryProxyUrl: URI?,
    daggerComponent: ProtoSdcComponent,
) : ServiceDelegate by ServiceDelegateImpl() {
    public val discovery: SdcDeviceDiscovery = daggerComponent.sdcDeviceDiscoveryFactory().create(udp, discoveryProxyUrl)

    init {
        onStartUp {
            discovery.start()
        }

        onShutDown {
            discovery.stop()
        }
    }
}