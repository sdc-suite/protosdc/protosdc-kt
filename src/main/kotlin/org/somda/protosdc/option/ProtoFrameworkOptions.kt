package org.somda.protosdc.option

import org.somda.protosdc.common.Option
import org.somda.protosdc.common.Options
import org.somda.protosdc.crypto.CryptoStore
import org.somda.protosdc.network.udp.UdpConfig
import org.somda.protosdc.proto.mapping.BicepsExtensionsProtoHandler
import java.net.NetworkInterface
import java.net.URI
import java.net.URL

public interface ProtoFrameworkOption : Option

public data class EnableAdhocDiscovery(
    public val networkInterface: NetworkInterface,
    public val udpConfig: UdpConfig = UdpConfig()
) : ProtoFrameworkOption

public class EnableManagedDiscovery(
    public val discoveryProxyUrl: URI
) : ProtoFrameworkOption

// todo: EnableCommunicationLog currently not implemented
//public class EnableCommunicationLog(
//    public val commlogConfig: CommlogConfig
//) : ProtoFrameworkOption

public class BicepsExtensionsHandlers : ProtoFrameworkOption {
    private val _handlers: MutableSet<BicepsExtensionsProtoHandler> = mutableSetOf()
    public val handlers: Set<BicepsExtensionsProtoHandler> = _handlers

    public fun register(handler: BicepsExtensionsProtoHandler) {
        _handlers.add(handler)
    }
}

public fun bicepsExtensionHandlers(init: BicepsExtensionsHandlers.() -> Unit): BicepsExtensionsHandlers {
    return BicepsExtensionsHandlers().apply(init)
}

public fun bicepsExtensionHandlers(vararg handler: BicepsExtensionsProtoHandler): BicepsExtensionsHandlers {
    return bicepsExtensionHandlers {
        for (item in handler) {
            register(item)
        }
    }
}

public class ProtoFrameworkOptions(option: Set<ProtoFrameworkOption?>) : Options<ProtoFrameworkOption>(option) {
    public constructor(vararg option: ProtoFrameworkOption?) : this(option.toSet())
}