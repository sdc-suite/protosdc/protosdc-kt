package org.somda.protosdc.option

import org.somda.protosdc.common.Option
import org.somda.protosdc.common.Options
import org.somda.protosdc.crypto.CryptoStore
import javax.net.ssl.HostnameVerifier


public sealed interface ProtoComponentOption: Option

public class CustomHostnameVerifier(
    public val hostnameVerifier: HostnameVerifier
) : ProtoComponentOption

public class CryptoStoreOverride(
    public val cryptoStore: CryptoStore
): ProtoComponentOption

public class ProtoComponentOptions(option: Set<ProtoComponentOption?>) : Options<ProtoComponentOption>(option) {
    public constructor(vararg option: ProtoComponentOption?) : this(option.toSet())
}