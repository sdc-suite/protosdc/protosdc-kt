package org.somda.protosdc

import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.dagger.DaggerProtoSdcComponent
import org.somda.protosdc.dagger.ProtoSdcComponent
import org.somda.protosdc.discovery.proxy.DiscoveryProxy
import org.somda.protosdc.network.ip.IpVersion.IP_V4
import org.somda.protosdc.network.udp.SocketInfo
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.UdpConfig
import org.somda.protosdc.network.udp.udp
import org.somda.protosdc.option.CryptoStoreOverride
import org.somda.protosdc.option.EnableAdhocDiscovery
import org.somda.protosdc.option.EnableManagedDiscovery
import org.somda.protosdc.option.ProtoComponentOption
import org.somda.protosdc.option.ProtoComponentOptions
import org.somda.protosdc.option.ProtoFrameworkOption
import org.somda.protosdc.option.ProtoFrameworkOptions
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.NetworkInterface
import java.net.URI

/**
 * todo: finish proto framework before enabling kotlin explicit api
 */
public class ProtoFramework internal constructor(
    private val config: ProtoSdcConfig,
    options: ProtoFrameworkOptions = ProtoFrameworkOptions()
) :
    ServiceDelegate by ServiceDelegateImpl() {

    public constructor(config: ProtoSdcConfig, vararg option: ProtoFrameworkOption?) : this(
        config,
        ProtoFrameworkOptions(option.toSet())
    )


    private val udp = options.get<EnableAdhocDiscovery>().let {
        udp {
            config(it?.udpConfig ?: UdpConfig())
            // todo: assign actual instance id
            instanceId(InstanceId("tbd"))
            binding(
                UdpBindingConfig(
                    networkInterface = it?.networkInterface
                        ?: NetworkInterface.getByInetAddress(InetAddress.getLoopbackAddress()),
                    ipVersion = IP_V4,
                    multicastSocket = SocketInfo(InetAddress.getByName("239.255.255.250"), 3702)
                )
            )
        }
    }

    //    private val adhocDiscoveryOption = udpComponent?.let { AdhocDiscoveryOption(it.udpMessageQueue()) }
//
    private val managedDiscoveryOptionUrl = options.get<EnableManagedDiscovery>()?.discoveryProxyUrl

    init {
        if (++instanceCounter > 1) {
            error("${ProtoFramework::class} can only be instantiated once")
        }

//        options.get<BicepsExtensionsHandlers>()?.also { option ->
//            KotlinToXmlBaseTypes.registerAnyHandler { kotlinObject, parent ->
//                val extensionItem = kotlinObject as Extension.Item
//                option.handlers
//                    .mapNotNull { it.mapKnownToXml(extensionItem.extensionData, parent.ownerDocument) }
//                    .filterIsInstance<Element>()
//                    .firstOrNull() ?: error("No mapper from ${kotlinObject::class} to XML found")
//
//            }
//
//            XmlToKotlinBaseTypes.registerAnyHandler { node ->
//                val n = option.handlers.firstNotNullOfOrNull {
//                    it.mapKnownToKotlin(node)
//                } ?: node
//                n
//            }
//        }


        onStartUp {
            udp.start()
        }

        onShutDown {
            udp.stop()
        }
    }

    public fun createComponentFor(
        networkAddress: InetSocketAddress,
        init: ProtoComponentOptions.() -> Unit
    ): Component {
        return createComponentFor(
            networkAddress,
            ProtoComponentOptions().apply(init)
        )
    }

    public fun createComponentFor(
        networkAddress: InetSocketAddress,
        vararg option: ProtoComponentOption?
    ): Component {
        return createComponentFor(
            networkAddress,
            ProtoComponentOptions(option.toSet())
        )
    }

    public fun createComponentFor(
        networkAddress: InetSocketAddress,
        options: ProtoComponentOptions
    ): Component {
        val daggerComponent = DaggerProtoSdcComponent.builder().let { builder ->
            builder.bind(config)
            options.get<CryptoStoreOverride>()?.also {
                builder.bind(it.cryptoStore)
            }

//        options.get<EnableCommunicationLog>()?.also {
//            builder.bind(it.commlogConfig)
//        }
            builder.build()
        }
        return Component(
            udp,
            managedDiscoveryOptionUrl,
            daggerComponent,
            networkAddress
        )
    }

    public class Component internal constructor(
        private val udp: Udp,
        private val managedDiscoveryOptionUrl: URI?,
        private val daggerComponent: ProtoSdcComponent,
        networkAddress: InetSocketAddress
    ) : ServiceDelegate by ServiceDelegateImpl() {
        private val server = daggerComponent.serverFactory().create(networkAddress)

        init {
            onStartUp {
                server.start()
            }

            onShutDown {
                server.stop()
            }
        }

        public fun createSdcProvider(init: ProtoSdcProvider.Init.() -> Unit = {}): ProtoSdcProvider {
            return ProtoSdcProvider(udp, server, managedDiscoveryOptionUrl, daggerComponent, init)
        }

        public fun createSdcConsumer(): ProtoSdcConsumer {
            return ProtoSdcConsumer(udp, managedDiscoveryOptionUrl, daggerComponent)
        }

        public fun createDiscoveryProxy(): DiscoveryProxy {
            return daggerComponent.discoveryProxyFactory().create(server)
        }
    }

    public companion object {
        private var instanceCounter = 0
    }
}

public fun setupProtoFramework(config: ProtoSdcConfig, init: ProtoFrameworkOptions): ProtoFramework {
    return ProtoFramework(config, init)
}

public fun setupProtoFramework(config: ProtoSdcConfig, vararg init: ProtoFrameworkOption?): ProtoFramework {
    return ProtoFramework(config, ProtoFrameworkOptions(init.toSet()))
}

public fun setupProtoFramework(config: ProtoSdcConfig, init: ProtoFrameworkOptions.() -> Unit): ProtoFramework {
    return ProtoFramework(config, ProtoFrameworkOptions().apply(init))
}