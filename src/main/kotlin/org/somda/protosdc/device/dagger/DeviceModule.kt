package org.somda.protosdc.device.dagger

import dagger.Module
import dagger.Provides
import org.somda.protosdc.device.consumer.*
import org.somda.protosdc.device.provider.SdcDevice
import org.somda.protosdc.device.provider.SdcDeviceImpl
import org.somda.protosdc.device.provider.StatisticsCallback
import org.somda.protosdc.device.provider.sco.OperationInvocationReceiver
import org.somda.protosdc.metadata.consumer.Metadata
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import java.net.InetSocketAddress
import java.net.NetworkInterface
import java.net.URI
import javax.inject.Singleton

@Module
internal abstract class DeviceModule {
    companion object {
        @Singleton
        @Provides
        fun sdcDeviceProxyFactory(implFactory: SdcDeviceProxyImpl.Factory) = object : SdcDeviceProxy.Factory {
            override fun create(metadata: Metadata) =
                implFactory.create(metadata)
        }

        @Singleton
        @Provides
        fun sdcDeviceDiscoveryFactory(implFactory: SdcDeviceDiscoveryImpl.Factory) =
            object : SdcDeviceDiscovery.Factory {
                override fun create(udp: Udp, discoveryProxyAddress: URI?) =
                    implFactory.create(udp, discoveryProxyAddress)
            }
    }
}