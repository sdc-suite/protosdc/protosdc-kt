package org.somda.protosdc.device.common

/**
 * Constants that are used by the protoSDC device implementation (provider and consumer).
 */
internal object Constants {
    /**
     * IP address that is used for multicast discovery transmission.
     */
    const val DISCOVERY_IP_ADDRESS = "239.255.255.250"

    /**
     * Port that is used for multicast discovery transmission.
     */
    const val DISCOVERY_PORT = 6464

    /**
     * If used as root of an instance identifier, this string specifies an extension to include a PEM string to
     * represent a certificate.
     *
     * This is a workaround as SDC Glue lacks an appropriate definition and solves
     * https://sourceforge.net/p/opensdc/ieee11073-20701/7/.
     */
    const val ROOT_PEM_STRING = "http://www.somda.org/cert/subject/rfc2253"
}