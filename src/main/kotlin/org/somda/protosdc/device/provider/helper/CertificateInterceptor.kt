package org.somda.protosdc.device.provider.helper

import io.grpc.*
import javax.net.ssl.SSLSession

internal class CertificateInterceptor : ServerInterceptor {
    companion object {
        val SSL_SESSION_CONTEXT: Context.Key<SSLSession> = Context.key("SSLSession")
    }

    override fun <ReqT, RespT> interceptCall(
        call: ServerCall<ReqT, RespT>,
        headers: Metadata, next: ServerCallHandler<ReqT, RespT>
    ): ServerCall.Listener<ReqT> {
        val sslSession = call.attributes.get(Grpc.TRANSPORT_ATTR_SSL_SESSION)
            ?: return next.startCall(call, headers)
        return Contexts.interceptCall(
            Context.current().withValue(SSL_SESSION_CONTEXT, sslSession), call, headers, next
        )
    }
}