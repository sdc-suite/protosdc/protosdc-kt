package org.somda.protosdc.device.provider.service

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.isActive
import org.somda.protosdc.addressing.createFor
import org.somda.protosdc.biceps.common.access.MdibAccessEvent
import org.somda.protosdc.biceps.common.access.ObservableMdibAccess
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.device.common.Action
import org.somda.protosdc.proto.model.*
import java.util.concurrent.atomic.AtomicInteger

internal data class EpisodicReportSet(
    val action: String,
    val generatedReport: EpisodicReport
)

internal typealias SubscriberCountCallback = (Int) -> Unit

internal class MdibReportingService @AssistedInject constructor(
    @Assisted private val mdibAccess: ObservableMdibAccess,
    @Assisted private val episodicReportPublisher: ChannelPublisher<EpisodicReportSet>,
    @Assisted private val subscriberCountCallback: SubscriberCountCallback,
    instanceId: InstanceId
) : MdibReportingServiceGrpcKt.MdibReportingServiceCoroutineImplBase() {

    private val logger by InstanceLogger(instanceId())

    private var subscriberCount = AtomicInteger(0)

    @AssistedFactory
    interface Factory {
        fun create(
            mdibAccess: ObservableMdibAccess,
            episodicReportPublisher: ChannelPublisher<EpisodicReportSet>,
            subscriberCountCallback: SubscriberCountCallback
        ): MdibReportingService
    }

    override fun episodicReport(request: EpisodicReportRequest) = flow<EpisodicReportStream> {
        val subscription = episodicReportPublisher.subscribe()
        incrementSubscribers()
        while (currentCoroutineContext().isActive) {
            try {
                subscription.receive().let {
                    emit(
                        EpisodicReportStream.newBuilder()
                            .setAddressing(createFor(it.action))
                            .setReport(it.generatedReport)
                            .build()
                    )
                }
            } catch (e: Exception) {
                when (e) {
                    is ClosedReceiveChannelException, is CancellationException -> {
                        logger.info { "Channel is closed for reception. Exit episodic report processing." }
                    }

                    else -> {
                        logger.warn { "Reception of a report failed: ${e.message}" }
                        logger.trace(e) { "Reception of a report failed: ${e.message}" }
                    }
                }
                break
            }
        }
        subscription.cancel()
        decrementSubscribers()
    }

    override fun periodicReport(request: PeriodicReportRequest) = flow<PeriodicReportStream> { }

    private fun incrementSubscribers() {
        subscriberCountCallback(subscriberCount.incrementAndGet())
    }

    private fun decrementSubscribers() {
        subscriberCountCallback(subscriberCount.decrementAndGet())
    }

    suspend fun runEventDistributionInCurrentScope() {
        coroutineScope {
            val subscription = mdibAccess.subscribe()
            runCatching {
                while (isActive) {
                    val event = subscription.receive()
                    episodicReportPublisher.send(
                        when (event) {
                            is MdibAccessEvent.AlertStateModification -> EpisodicReportSet(
                                Action.EPISODIC_ALERT_REPORT.stringRepresentation,
                                MdibReportingServiceHelper.makeEpisodicAlertReport(mdibAccess, event)
                            )

                            is MdibAccessEvent.ComponentStateModification -> EpisodicReportSet(
                                Action.EPISODIC_COMPONENT_REPORT.stringRepresentation,
                                MdibReportingServiceHelper.makeEpisodicComponentReport(mdibAccess, event)
                            )

                            is MdibAccessEvent.ContextStateModification -> EpisodicReportSet(
                                Action.EPISODIC_CONTEXT_REPORT.stringRepresentation,
                                MdibReportingServiceHelper.makeEpisodicContextReport(mdibAccess, event)
                            )

                            is MdibAccessEvent.MetricStateModification -> EpisodicReportSet(
                                Action.EPISODIC_METRIC_REPORT.stringRepresentation,
                                MdibReportingServiceHelper.makeEpisodicMetricReport(mdibAccess, event)
                            )

                            is MdibAccessEvent.OperationStateModification -> EpisodicReportSet(
                                Action.EPISODIC_OPERATIONAL_STATE_REPORT.stringRepresentation,
                                MdibReportingServiceHelper.makeEpisodicOperationReport(mdibAccess, event)
                            )

                            is MdibAccessEvent.WaveformModification -> EpisodicReportSet(
                                Action.WAVEFORM_STREAM.stringRepresentation,
                                MdibReportingServiceHelper.makeWaveformStreamReport(mdibAccess, event)
                            )

                            is MdibAccessEvent.DescriptionModification -> {
                                EpisodicReportSet(
                                    Action.DESCRIPTION_MODIFICATION_REPORT.stringRepresentation,
                                    MdibReportingServiceHelper.makeDescriptionModificationReport(mdibAccess, event)
                                )
                            }
                        }
                    )
                }
            }.onFailure { logger.warn(it) { "Event distribution ended" } }
            subscription.cancel()
        }
    }
}