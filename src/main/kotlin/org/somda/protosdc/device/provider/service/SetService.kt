package org.somda.protosdc.device.provider.service

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.somda.protosdc.addressing.AddressingValidator
import org.somda.protosdc.addressing.createFor
import org.somda.protosdc.addressing.discloseValidationError
import org.somda.protosdc.addressing.replyTo
import org.somda.protosdc.model.biceps.InstanceIdentifier
import org.somda.protosdc.model.biceps.OperationInvokedReport
import org.somda.protosdc.biceps.provider.access.LocalMdibAccess
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.device.common.Action
import org.somda.protosdc.device.common.Constants
import org.somda.protosdc.device.provider.sco.OperationInvocationReceiver
import org.somda.protosdc.device.provider.sco.ScoController
import org.somda.protosdc.network.grpc.server.SslSessionInterceptor
import org.somda.protosdc.proto.mapping.biceps.KotlinToProto
import org.somda.protosdc.proto.mapping.biceps.ProtoToKotlin
import org.somda.protosdc.proto.model.*
import javax.security.auth.x500.X500Principal
import kotlin.time.Duration.Companion.milliseconds

internal class SetService @AssistedInject constructor(
    @Assisted private val mdibAccess: LocalMdibAccess,
    @Assisted private val operationInvocationReceiver: OperationInvocationReceiver,
    @Assisted private val operationInvokedReportChannel: Channel<OperationInvokedReport>,
    private val addressingValidator: AddressingValidator,
    scoControllerFactory: ScoController.Factory
) : SetServiceGrpcKt.SetServiceCoroutineImplBase(), ServiceDelegate by ServiceDelegateImpl() {
    @AssistedFactory
    interface Factory {
        fun create(
            mdibAccess: LocalMdibAccess,
            operationInvocationReceiver: OperationInvocationReceiver,
            operationInvokedReportChannel: Channel<OperationInvokedReport>
        ): SetService
    }

    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)
    private val reportPublisher = ChannelPublisher<OperationInvokedReport>("operationInvokedReportPublisher")

    private val scoController = scoControllerFactory.create(operationInvokedReportChannel, mdibAccess)

    init {
        onStartUp {
            coroutineScopeBinding.launch {
                while (isActive) {
                    reportPublisher.send(operationInvokedReportChannel.receive())
                }
            }
        }

        onShutDown {
            coroutineScopeBinding.cancel()
        }
    }

    override fun operationInvokedReport(request: OperationInvokedReportRequest) =
        flow<OperationInvokedReportStream> {
            val subscription = reportPublisher.subscribe()
            while (currentCoroutineContext().isActive) {
                try {
                    emit(
                        OperationInvokedReportStream.newBuilder()
                            .setAddressing(createFor(Action.OPERATION_INVOKED_REPORT.stringRepresentation))
                            .setOperationInvoked(
                                KotlinToProto.map_org_somda_protosdc_model_biceps_OperationInvokedReport(
                                    subscription.receive()
                                )
                            )
                            .build()
                    )
                } catch (e: Exception) {
                    break
                }
            }
            subscription.cancel()
        }

    override suspend fun activate(request: ActivateRequest): ActivateResponse =
        ActivateResponse.newBuilder()
            .setAddressing(replyTo(request.addressing, Action.ACTIVATE_RESPONSE.stringRepresentation))
            .setPayload(
                KotlinToProto.map_org_somda_protosdc_model_biceps_ActivateResponse(
                    org.somda.protosdc.model.biceps.ActivateResponse(
                        scoController.processIncomingSetOperation(
                            request.payload.abstractSet.operationHandleRef.string,
                            theSource()
                        ) { context ->
                            operationInvocationReceiver.activate(
                                request.payload.abstractSet.operationHandleRef.string,
                                context,
                                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_ActivateMsg(request.payload)
                            )
                        }.toAbstractSetResponse()
                    )
                )
            ).build().also {
                discloseValidationError {
                    addressingValidator.create(request.addressing)
                        .validateMessageId()
                        .validateAction(Action.ACTIVATE.stringRepresentation)
                }
            }

    override suspend fun setAlertState(request: SetAlertStateRequest): SetAlertStateResponse =
        SetAlertStateResponse.newBuilder()
            .setAddressing(replyTo(request.addressing, Action.SET_ALERT_STATE_RESPONSE.stringRepresentation))
            .setPayload(
                KotlinToProto.map_org_somda_protosdc_model_biceps_SetAlertStateResponse(
                    org.somda.protosdc.model.biceps.SetAlertStateResponse(
                        scoController.processIncomingSetOperation(
                            request.payload.abstractSet.operationHandleRef.string,
                            theSource()
                        ) { context ->
                            operationInvocationReceiver.setAlertState(
                                request.payload.abstractSet.operationHandleRef.string,
                                context,
                                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetAlertStateMsg(request.payload)
                            )
                        }.toAbstractSetResponse()
                    )
                )
            ).build().also {
                discloseValidationError {
                    addressingValidator.create(request.addressing)
                        .validateMessageId()
                        .validateAction(Action.SET_ALERT_STATE.stringRepresentation)
                }
            }

    override suspend fun setComponentState(request: SetComponentStateRequest): SetComponentStateResponse =
        SetComponentStateResponse.newBuilder()
            .setAddressing(replyTo(request.addressing, Action.SET_COMPONENT_STATE_RESPONSE.stringRepresentation))
            .setPayload(
                KotlinToProto.map_org_somda_protosdc_model_biceps_SetComponentStateResponse(
                    org.somda.protosdc.model.biceps.SetComponentStateResponse(
                        scoController.processIncomingSetOperation(
                            request.payload.abstractSet.operationHandleRef.string,
                            theSource()
                        ) { context ->
                            operationInvocationReceiver.setComponentState(
                                request.payload.abstractSet.operationHandleRef.string,
                                context,
                                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetComponentStateMsg(request.payload)
                            )
                        }.toAbstractSetResponse()
                    )
                )
            ).build().also {
                discloseValidationError {
                    addressingValidator.create(request.addressing)
                        .validateMessageId()
                        .validateAction(Action.SET_COMPONENT_STATE.stringRepresentation)
                }
            }

    override suspend fun setContextState(request: SetContextStateRequest): SetContextStateResponse =
        SetContextStateResponse.newBuilder()
            .setAddressing(replyTo(request.addressing, Action.SET_CONTEXT_STATE_RESPONSE.stringRepresentation))
            .setPayload(
                KotlinToProto.map_org_somda_protosdc_model_biceps_SetContextStateResponse(
                    org.somda.protosdc.model.biceps.SetContextStateResponse(
                        scoController.processIncomingSetOperation(
                            request.payload.abstractSet.operationHandleRef.string,
                            theSource()
                        ) { context ->
                            operationInvocationReceiver.setContextState(
                                request.payload.abstractSet.operationHandleRef.string,
                                context,
                                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetContextStateMsg(request.payload)
                            )
                        }.toAbstractSetResponse()
                    )
                )
            ).build().also {
                discloseValidationError {
                    addressingValidator.create(request.addressing)
                        .validateMessageId()
                        .validateAction(Action.SET_CONTEXT_STATE.stringRepresentation)
                }
            }

    override suspend fun setMetricState(request: SetMetricStateRequest): SetMetricStateResponse =
        SetMetricStateResponse.newBuilder()
            .setAddressing(replyTo(request.addressing, Action.SET_METRIC_STATE_RESPONSE.stringRepresentation))
            .setPayload(
                KotlinToProto.map_org_somda_protosdc_model_biceps_SetMetricStateResponse(
                    org.somda.protosdc.model.biceps.SetMetricStateResponse(
                        scoController.processIncomingSetOperation(
                            request.payload.abstractSet.operationHandleRef.string,
                            theSource()
                        ) { context ->
                            operationInvocationReceiver.setMetricState(
                                request.payload.abstractSet.operationHandleRef.string,
                                context,
                                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetMetricStateMsg(request.payload)
                            )
                        }.toAbstractSetResponse()
                    )
                )
            ).build().also {
                discloseValidationError {
                    addressingValidator.create(request.addressing)
                        .validateMessageId()
                        .validateAction(Action.SET_METRIC_STATE.stringRepresentation)
                }
            }

    override suspend fun setString(request: SetStringRequest): SetStringResponse =
        SetStringResponse.newBuilder()
            .setAddressing(replyTo(request.addressing, Action.SET_STRING_RESPONSE.stringRepresentation))
            .setPayload(
                KotlinToProto.map_org_somda_protosdc_model_biceps_SetStringResponse(
                    org.somda.protosdc.model.biceps.SetStringResponse(
                        scoController.processIncomingSetOperation(
                            request.payload.abstractSet.operationHandleRef.string,
                            theSource()
                        ) { context ->
                            operationInvocationReceiver.setString(
                                request.payload.abstractSet.operationHandleRef.string,
                                context,
                                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetStringMsg(request.payload)
                            )
                        }.toAbstractSetResponse()
                    )
                )
            ).build().also {
                discloseValidationError {
                    addressingValidator.create(request.addressing)
                        .validateMessageId()
                        .validateAction(Action.SET_STRING.stringRepresentation)
                }
            }

    override suspend fun setValue(request: SetValueRequest): SetValueResponse =
        SetValueResponse.newBuilder()
            .setAddressing(replyTo(request.addressing, Action.SET_VALUE_RESPONSE.stringRepresentation))
            .setPayload(
                KotlinToProto.map_org_somda_protosdc_model_biceps_SetValueResponse(
                    org.somda.protosdc.model.biceps.SetValueResponse(
                        scoController.processIncomingSetOperation(
                            request.payload.abstractSet.operationHandleRef.string,
                            theSource()
                        ) { context ->
                            operationInvocationReceiver.setValue(
                                request.payload.abstractSet.operationHandleRef.string,
                                context,
                                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetValueMsg(request.payload)
                            )
                        }.toAbstractSetResponse()
                    )
                )
            ).build().also {
                discloseValidationError {
                    addressingValidator.create(request.addressing)
                        .validateMessageId()
                        .validateAction(Action.SET_VALUE.stringRepresentation)
                }
            }

    private fun theSource() = SslSessionInterceptor.x509Certificates().firstOrNull()
        ?.let {
            InstanceIdentifier(
                rootAttr = InstanceIdentifier.RootAttr(Constants.ROOT_PEM_STRING),
                extensionAttr = InstanceIdentifier.ExtensionAttr(it.subjectX500Principal.getName(X500Principal.RFC2253))
            )
        }
        ?: anonymousSource()

    private fun anonymousSource() = InstanceIdentifier()

    private fun ms() = 500.milliseconds
}