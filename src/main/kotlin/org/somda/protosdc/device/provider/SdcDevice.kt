package org.somda.protosdc.device.provider

import org.somda.protosdc.biceps.provider.access.LocalMdibAccess
import org.somda.protosdc.common.Service
import org.somda.protosdc.device.provider.sco.OperationInvocationReceiver
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.metadata.provider.MetadataProvider
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import java.net.InetSocketAddress
import java.net.NetworkInterface
import java.net.URI

public typealias StatisticsCallback = (Statistics) -> Unit

public interface SdcDevice : Service {
    public suspend fun endpointIdentifier(): String

    public suspend fun mdibAccess(): LocalMdibAccess

    public suspend fun discoveryProvider(): DiscoveryProvider

    public suspend fun metadataProvider(): MetadataProvider
}