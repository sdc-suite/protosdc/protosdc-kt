package org.somda.protosdc.device.provider

public data class Statistics(
    val consumerCount: Int
)