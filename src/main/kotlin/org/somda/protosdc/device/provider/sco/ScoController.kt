package org.somda.protosdc.device.provider.sco

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.channels.SendChannel
import org.somda.protosdc.biceps.provider.access.LocalMdibAccess
import org.somda.protosdc.common.DeviceId
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.model.biceps.*

/**
 * Manages callbacks for incoming set service requests.
 */
internal class ScoController @AssistedInject constructor(
    @Assisted private val operationInvokedReportChannel: SendChannel<OperationInvokedReport>,
    @Assisted private val mdibAccess: LocalMdibAccess,
    private val instanceId: InstanceId,
) {
    @AssistedFactory
    interface Factory {
        fun create(
            operationInvokedReportChannel: SendChannel<OperationInvokedReport>,
            mdibAccess: LocalMdibAccess,
        ): ScoController
    }

    private var transactionCounter = 0

    /**
     * Invokes processing of an incoming network set service call.
     *
     * @param handle  the handle of the operation that was called.
     * @param source  the instance identifier that represents the calling client.
     * @param process the function that handles the particular SCO request.
     * @return the initial invocation info required for the response message. The initial invocation info is
     * requested by the callback that is going to be invoked. In case that no callback can be found, a fail state is
     * returned.
     */
    suspend fun processIncomingSetOperation(
        handle: String,
        source: InstanceIdentifier,
        process: suspend (context: Context) -> InvocationResponse
    ) = Context(
        TransactionId(transactionCounter++),
        handle,
        source,
        operationInvokedReportChannel,
        mdibAccess,
        instanceId,
        DeviceId("") // todo populate device id (see https://gitlab.com/sdc-suite/protosdc/protosdc-kt/-/issues/33)
    ).let { context ->
        runCatching {
            process(context)
        }.getOrDefault(
            context.createUnsuccessfulResponse(
                mdibAccess.mdibVersion(),
                InvocationState(InvocationState.EnumType.Fail),
                InvocationError(InvocationError.EnumType.Unspec),
                listOf(
                    LocalizedText(
                        langAttr = "en",
                        localizedTextContent = LocalizedTextContent(
                            "There is no ultimate invocation processor available for operation $handle"
                        )
                    )
                )
            )
        )
    }
}