package org.somda.protosdc.device.provider.helper

import com.google.protobuf.UInt64Value
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.access.MdibAccess
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.proto.mapping.biceps.KotlinToProto
import org.somda.protosdc.proto.model.biceps.*

internal fun mapToMdibVersionGroup(mdibVersion: MdibVersion): MdibVersionGroupMsg = MdibVersionGroupMsg.newBuilder()
    .setInstanceIdAttr(UInt64Value.of(mdibVersion.instanceId))
    .setSequenceIdAttr(mdibVersion.sequenceId)
    .setMdibVersionAttr(VersionCounterMsg.newBuilder().setUnsignedLong(mdibVersion.version).build())
    .build()

internal suspend fun createAbstractGetResponse(mdibAccess: MdibAccess): AbstractGetResponseMsg.Builder =
    AbstractGetResponseMsg.newBuilder().setMdibVersionGroupAttr(mapToMdibVersionGroup(mdibAccess.mdibVersion()))

internal suspend fun createAbstractReport(mdibAccess: MdibAccess): AbstractReportMsg.Builder = AbstractReportMsg.newBuilder()
    .setMdibVersionGroupAttr(mapToMdibVersionGroup(mdibAccess.mdibVersion()))

internal fun mapStatesFromEntity(entity: MdibEntity) = when (entity) {
    is MdibEntity.ActivateOperation -> listOf(
        AbstractStateOneOfMsg.newBuilder().setActivateOperationState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_ActivateOperationState(entity.state)
        ).build()
    )
    is MdibEntity.AlertCondition -> listOf(
        AbstractStateOneOfMsg.newBuilder().setAlertConditionState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_AlertConditionState(entity.state)
        ).build()
    )
    is MdibEntity.AlertSignal -> listOf(
        AbstractStateOneOfMsg.newBuilder().setAlertSignalState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_AlertSignalState(entity.state)
        ).build()
    )
    is MdibEntity.AlertSystem -> listOf(
        AbstractStateOneOfMsg.newBuilder().setAlertSystemState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_AlertSystemState(entity.state)
        ).build()
    )
    is MdibEntity.Battery -> listOf(
        AbstractStateOneOfMsg.newBuilder().setBatteryState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_BatteryState(entity.state)
        ).build()
    )
    is MdibEntity.Channel -> listOf(
        AbstractStateOneOfMsg.newBuilder().setChannelState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_ChannelState(entity.state)
        ).build()
    )
    is MdibEntity.Clock -> listOf(
        AbstractStateOneOfMsg.newBuilder().setClockState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_ClockState(entity.state)
        ).build()
    )
    is MdibEntity.DistributionSampleArrayMetric -> listOf(
        AbstractStateOneOfMsg.newBuilder().setDistributionSampleArrayMetricState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_DistributionSampleArrayMetricState(entity.state)
        ).build()
    )
    is MdibEntity.EnsembleContext -> entity.states.map {
        AbstractStateOneOfMsg.newBuilder().setEnsembleContextState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_EnsembleContextState(it)
        ).build()
    }
    is MdibEntity.EnumStringMetric -> listOf(
        AbstractStateOneOfMsg.newBuilder().setEnumStringMetricState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_EnumStringMetricState(entity.state)
        ).build()
    )
    is MdibEntity.LimitAlertCondition -> listOf(
        AbstractStateOneOfMsg.newBuilder().setLimitAlertConditionState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_LimitAlertConditionState(entity.state)
        ).build()
    )
    is MdibEntity.LocationContext -> entity.states.map {
        AbstractStateOneOfMsg.newBuilder().setLocationContextState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_LocationContextState(it)
        ).build()
    }
    is MdibEntity.Mds -> listOf(
        AbstractStateOneOfMsg.newBuilder().setMdsState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_MdsState(entity.state)
        ).build()
    )
    is MdibEntity.MeansContext -> entity.states.map {
        AbstractStateOneOfMsg.newBuilder().setMeansContextState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_MeansContextState(it)
        ).build()
    }
    is MdibEntity.NumericMetric -> listOf(
        AbstractStateOneOfMsg.newBuilder().setNumericMetricState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_NumericMetricState(entity.state)
        ).build()
    )
    is MdibEntity.OperatorContext -> entity.states.map {
        AbstractStateOneOfMsg.newBuilder().setOperatorContextState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_OperatorContextState(it)
        ).build()
    }
    is MdibEntity.PatientContext -> entity.states.map {
        AbstractStateOneOfMsg.newBuilder().setPatientContextState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_PatientContextState(it)
        ).build()
    }
    is MdibEntity.RealTimeSampleArrayMetric -> listOf(
        AbstractStateOneOfMsg.newBuilder().setRealTimeSampleArrayMetricState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_RealTimeSampleArrayMetricState(entity.state)
        ).build()
    )
    is MdibEntity.Sco -> listOf(
        AbstractStateOneOfMsg.newBuilder().setScoState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_ScoState(entity.state)
        ).build()
    )
    is MdibEntity.SetAlertStateOperation -> listOf(
        AbstractStateOneOfMsg.newBuilder().setSetAlertStateOperationState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_SetAlertStateOperationState(entity.state)
        ).build()
    )
    is MdibEntity.SetComponentStateOperation -> listOf(
        AbstractStateOneOfMsg.newBuilder().setSetComponentStateOperationState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_SetComponentStateOperationState(entity.state)
        ).build()
    )
    is MdibEntity.SetContextStateOperation -> listOf(
        AbstractStateOneOfMsg.newBuilder().setSetContextStateOperationState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_SetContextStateOperationState(entity.state)
        ).build()
    )
    is MdibEntity.SetMetricStateOperation -> listOf(
        AbstractStateOneOfMsg.newBuilder().setSetMetricStateOperationState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_SetMetricStateOperationState(entity.state)
        ).build()
    )
    is MdibEntity.SetStringOperation -> listOf(
        AbstractStateOneOfMsg.newBuilder().setSetStringOperationState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_SetStringOperationState(entity.state)
        ).build()
    )
    is MdibEntity.SetValueOperation -> listOf(
        AbstractStateOneOfMsg.newBuilder().setSetValueOperationState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_SetValueOperationState(entity.state)
        ).build()
    )
    is MdibEntity.StringMetric -> listOf(
        AbstractStateOneOfMsg.newBuilder().setStringMetricState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_StringMetricState(entity.state)
        ).build()
    )
    is MdibEntity.SystemContext -> listOf(
        AbstractStateOneOfMsg.newBuilder().setSystemContextState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_SystemContextState(entity.state)
        ).build()
    )
    is MdibEntity.Vmd -> listOf(
        AbstractStateOneOfMsg.newBuilder().setVmdState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_VmdState(entity.state)
        ).build()
    )
    is MdibEntity.WorkflowContext -> entity.states.map {
        AbstractStateOneOfMsg.newBuilder().setWorkflowContextState(
            KotlinToProto.map_org_somda_protosdc_model_biceps_WorkflowContextState(it)
        ).build()
    }
}

internal fun mapContextStateFromEntity(entity: MdibEntity, contextStateHandle: String) = when (entity) {
    is MdibEntity.EnsembleContext -> entity.states.first { it.abstractContextState.handleString() == contextStateHandle }
        .let {
            AbstractStateOneOfMsg.newBuilder().setEnsembleContextState(
                KotlinToProto.map_org_somda_protosdc_model_biceps_EnsembleContextState(it)
            ).build()
        }
    is MdibEntity.LocationContext -> entity.states.first { it.abstractContextState.handleString() == contextStateHandle }
        .let {
            AbstractStateOneOfMsg.newBuilder().setLocationContextState(
                KotlinToProto.map_org_somda_protosdc_model_biceps_LocationContextState(it)
            ).build()
        }
    is MdibEntity.MeansContext -> entity.states.first { it.abstractContextState.handleString() == contextStateHandle }
        .let {
            AbstractStateOneOfMsg.newBuilder().setMeansContextState(
                KotlinToProto.map_org_somda_protosdc_model_biceps_MeansContextState(it)
            ).build()
        }
    is MdibEntity.OperatorContext -> entity.states.first { it.abstractContextState.handleString() == contextStateHandle }
        .let {
            AbstractStateOneOfMsg.newBuilder().setOperatorContextState(
                KotlinToProto.map_org_somda_protosdc_model_biceps_OperatorContextState(it)
            ).build()
        }
    is MdibEntity.PatientContext -> entity.states.first { it.abstractContextState.handleString() == contextStateHandle }
        .let {
            AbstractStateOneOfMsg.newBuilder().setPatientContextState(
                KotlinToProto.map_org_somda_protosdc_model_biceps_PatientContextState(it)
            ).build()
        }
    is MdibEntity.WorkflowContext -> entity.states.first { it.abstractContextState.handleString() == contextStateHandle }
        .let {
            AbstractStateOneOfMsg.newBuilder().setWorkflowContextState(
                KotlinToProto.map_org_somda_protosdc_model_biceps_WorkflowContextState(it)
            ).build()
        }
    else -> throw Exception("Given entity with handle '${entity.handle}' is not a context: ${entity::class.java}")
}