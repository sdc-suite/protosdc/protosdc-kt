package org.somda.protosdc.device.provider.helper

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.somda.protosdc.biceps.common.InvalidWhenBranchException
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.access.MdibAccess
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.proto.mapping.biceps.KotlinToProto
import org.somda.protosdc.proto.model.biceps.*

internal class TreeMapper @AssistedInject constructor(
    @Assisted private val mdibAccess: MdibAccess,
    instanceId: InstanceId
) {
    private val logger by InstanceLogger(instanceId())

    @AssistedFactory
    interface Factory {
        fun create(@Assisted mdibAccess: MdibAccess): TreeMapper
    }

    /**
     * Maps to an [MdibMsg] instance.
     *
     * @return a fully populated [MdibMsg] instance.
     */
    suspend fun mapMdib(): MdibMsg {
        val mdib = MdibMsg.newBuilder()
        mdib.mdibVersionGroupAttr = mapToMdibVersionGroup(mdibAccess.mdibVersion())
        mdib.mdDescription = mapMdDescription(emptyList())
        mdib.mdState = mapMdState(emptyList())
        return mdib.build()
    }

    /**
     * Maps to an [MdStateMsg] instance.
     *
     * @param handleFilter a filter to limit the result:
     *
     *  * If the handle reference list is empty,
     * all states in the MDIB are included in the result list.
     *  * If a handle reference does match a multi-state handle,
     * the corresponding multi-state is included in the result list.
     *  * If a handle reference does match a descriptor handle,
     * all states that belong to the corresponding descriptor are included in the result list.
     *
     * @return the mapped instance.
     */
    suspend fun mapMdState(handleFilter: List<String>): MdStateMsg {
        val mdState = MdStateMsg.newBuilder()
        mdState.stateVersionAttr = VersionCounterMsg.newBuilder().setUnsignedLong(mdibAccess.mdStateVersion()).build()
        if (handleFilter.isEmpty()) {
            for (rootEntity in mdibAccess.rootEntities()) {
                appendStates(mdState, rootEntity)
            }
        } else {
            for (rootEntity in mdibAccess.rootEntities()) {
                appendStatesIfMatch(mdState, rootEntity, handleFilter.toMutableSet())
            }
        }
        return mdState.build()
    }

    /**
     * Maps to an [MdDescriptionMsg] instance.
     *
     * @param handleFilter a filter to limit the result:
     *
     *  * If the handle reference list is empty,
     * all MDS descriptors are included in the result list.
     *  * If a handle reference does match an MDS descriptor,
     * it is included in the result list.
     *  * If a handle reference does not match an MDS descriptor (i.e., any other descriptor),
     * the MDS descriptor that is in the parent tree of the handle reference
     * is included in the result list.
     *
     * @return the mapped instance.
     */
    suspend fun mapMdDescription(handleFilter: List<String>): MdDescriptionMsg {
        val mdDescription = MdDescriptionMsg.newBuilder()
        val handleFilterCopy = HashSet(handleFilter)
        mdDescription.descriptionVersionAttr =
            VersionCounterMsg.newBuilder().setUnsignedLong(mdibAccess.mdDescriptionVersion()).build()
        val rootEntities: MutableList<MdibEntity>
        if (handleFilter.isEmpty()) {
            rootEntities = mdibAccess.rootEntities().toMutableList()
        } else {
            val allRootEntities = mdibAccess.rootEntities().toMutableList()
            rootEntities = allRootEntities.filter { mdibEntity ->
                handleFilterCopy.any { handle ->
                    mdibEntity.handle == handle
                }
            }.toMutableList()

            rootEntities.forEach { mdibEntity ->
                handleFilterCopy.remove(mdibEntity.handle)
                allRootEntities.remove(mdibEntity)
            }

            for (entity in allRootEntities) {
                for (handle in handleFilterCopy) {
                    if (findHandleInSubtree(handle, entity)) {
                        rootEntities.add(entity)
                        handleFilterCopy.remove(handle)
                        break
                    }
                }
            }
        }
        for (rootEntity in rootEntities) {
            mapMds(mdDescription, rootEntity)
        }
        return mdDescription.build()
    }

    private suspend fun appendStates(mdState: MdStateMsg.Builder, entity: MdibEntity) {
        mdState.addAllState(mapStatesFromEntity(entity))
        for (childHandle in entity.resolveChildren()) {
            mdibAccess.anyEntity(childHandle).getOrNull()?.let { childEntity -> appendStates(mdState, childEntity) }
        }
    }

    private suspend fun appendStatesIfMatch(
        mdState: MdStateMsg.Builder,
        entity: MdibEntity,
        filterSet: MutableSet<String>
    ) {
        if (filterSet.contains(entity.handle)) {
            filterSet.remove(entity.handle)
            entity.resolveContextStates().forEach { filterSet.remove(it.handleString()) }
            mdState.addAllState(mapStatesFromEntity(entity))
        } else {
            entity.resolveContextStates().forEach {
                if (filterSet.contains(it.handleString())) {
                    filterSet.remove(it.handleString())
                    mdState.addState(mapContextStateFromEntity(entity, it.handleString()))
                }
            }
        }
        for (childHandle in entity.resolveChildren()) {
            mdibAccess.anyEntity(childHandle).getOrNull()?.let { childEntity ->
                appendStatesIfMatch(mdState, childEntity, filterSet)
            }
        }
    }

    private suspend fun findHandleInSubtree(handle: String, mdibEntity: MdibEntity): Boolean {
        for (childHandle in mdibEntity.resolveChildren()) {
            val childEntity = mdibAccess.anyEntity(childHandle).getOrNull()
            if (childEntity != null) {
                if (childEntity.handle == handle) {
                    return true
                }
                if (findHandleInSubtree(handle, childEntity)) {
                    return true
                }
            }
        }
        return false
    }

    private suspend fun mapMds(mdDescription: MdDescriptionMsg.Builder, mds: MdibEntity) {
        if (mds !is MdibEntity.Mds) {
            return
        }

        val builder = MdsDescriptorMsg.newBuilder(
            KotlinToProto.map_org_somda_protosdc_model_biceps_MdsDescriptor(mds.descriptor)
        )
        val compBuilder = builder.abstractComplexDeviceComponentDescriptorBuilder

        mdibAccess.childrenByType(mds.handle, MdibEntity.Battery::class).forEach {
            builder.addBattery(KotlinToProto.map_org_somda_protosdc_model_biceps_BatteryDescriptor(it.descriptor))
        }

        mdibAccess.childrenByType(mds.handle, MdibEntity.Clock::class).firstOrNull()?.let {
            builder.setClock(KotlinToProto.map_org_somda_protosdc_model_biceps_ClockDescriptor(it.descriptor))
        }

        mapSco(compBuilder, mdibAccess.childrenByType(mds.handle, MdibEntity.Sco::class))
        mapAlertSystem(compBuilder, mdibAccess.childrenByType(mds.handle, MdibEntity.AlertSystem::class))
        mapSystemContext(builder, mdibAccess.childrenByType(mds.handle, MdibEntity.SystemContext::class))
        mapVmds(builder, mdibAccess.childrenByType(mds.handle, MdibEntity.Vmd::class))
        mdDescription.addMds(builder.build())
    }

    private suspend fun mapVmds(parent: MdsDescriptorMsg.Builder, vmds: List<MdibEntity>) {
        for (vmd in vmds) {
            if (vmd !is MdibEntity.Vmd) {
                continue
            }

            val builder = VmdDescriptorMsg
                .newBuilder(KotlinToProto.map_org_somda_protosdc_model_biceps_VmdDescriptor(vmd.descriptor))
            val compBuilder = builder.abstractComplexDeviceComponentDescriptorBuilder
            mapSco(compBuilder, mdibAccess.childrenByType(vmd.handle, MdibEntity.Sco::class))
            mapAlertSystem(compBuilder, mdibAccess.childrenByType(vmd.handle, MdibEntity.AlertSystem::class))
            mapChannels(builder, mdibAccess.childrenByType(vmd.handle, MdibEntity.Channel::class))
            parent.addVmd(builder.build())
        }
    }

    private suspend fun mapChannels(parent: VmdDescriptorMsg.Builder, channels: List<MdibEntity>) {
        for (channel in channels) {
            if (channel !is MdibEntity.Channel) {
                continue
            }

            val builder = ChannelDescriptorMsg
                .newBuilder(KotlinToProto.map_org_somda_protosdc_model_biceps_ChannelDescriptor(channel.descriptor))

            channel.children.forEach { child ->
                mdibAccess.anyEntity(child).getOrNull()?.let {
                    builder.addMetric(
                        when (it) {
                            is MdibEntity.NumericMetric ->
                                AbstractMetricDescriptorOneOfMsg.newBuilder().setNumericMetricDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_NumericMetricDescriptor(it.descriptor)
                                )
                            is MdibEntity.EnumStringMetric ->
                                AbstractMetricDescriptorOneOfMsg.newBuilder().setEnumStringMetricDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_EnumStringMetricDescriptor(it.descriptor)
                                )
                            is MdibEntity.StringMetric ->
                                AbstractMetricDescriptorOneOfMsg.newBuilder().setStringMetricDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_StringMetricDescriptor(it.descriptor)
                                )
                            is MdibEntity.RealTimeSampleArrayMetric ->
                                AbstractMetricDescriptorOneOfMsg.newBuilder().setRealTimeSampleArrayMetricDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_RealTimeSampleArrayMetricDescriptor(
                                        it.descriptor
                                    )
                                )
                            is MdibEntity.DistributionSampleArrayMetric ->
                                AbstractMetricDescriptorOneOfMsg.newBuilder()
                                    .setDistributionSampleArrayMetricDescriptor(
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_DistributionSampleArrayMetricDescriptor(
                                            it.descriptor
                                        )
                                    )
                            else -> throw InvalidWhenBranchException(this)
                        }
                    )
                }
            }

            parent.addChannel(builder.build())
        }
    }

    private suspend fun mapSco(parent: AbstractComplexDeviceComponentDescriptorMsg.Builder, scos: List<MdibEntity>) {
        scos.firstOrNull()?.let { sco ->
            if (sco !is MdibEntity.Sco) {
                return
            }

            val builder = ScoDescriptorMsg
                .newBuilder(KotlinToProto.map_org_somda_protosdc_model_biceps_ScoDescriptor(sco.descriptor))

            sco.children.forEach { child ->
                mdibAccess.anyEntity(child).getOrNull()?.let {
                    builder.addOperation(
                        when (it) {
                            is MdibEntity.ActivateOperation ->
                                AbstractOperationDescriptorOneOfMsg.newBuilder().setActivateOperationDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_ActivateOperationDescriptor(it.descriptor)
                                )
                            is MdibEntity.SetStringOperation ->
                                AbstractOperationDescriptorOneOfMsg.newBuilder().setSetStringOperationDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_SetStringOperationDescriptor(it.descriptor)
                                )
                            is MdibEntity.SetValueOperation ->
                                AbstractOperationDescriptorOneOfMsg.newBuilder().setSetValueOperationDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_SetValueOperationDescriptor(it.descriptor)
                                )
                            is MdibEntity.SetContextStateOperation ->
                                AbstractOperationDescriptorOneOfMsg.newBuilder().setSetContextStateOperationDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_SetContextStateOperationDescriptor(
                                        it.descriptor
                                    )
                                )
                            is MdibEntity.SetMetricStateOperation ->
                                AbstractOperationDescriptorOneOfMsg.newBuilder().setSetMetricStateOperationDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_SetMetricStateOperationDescriptor(
                                        it.descriptor
                                    )
                                )
                            is MdibEntity.SetAlertStateOperation ->
                                AbstractOperationDescriptorOneOfMsg.newBuilder().setSetAlertStateOperationDescriptor(
                                    KotlinToProto.map_org_somda_protosdc_model_biceps_SetAlertStateOperationDescriptor(
                                        it.descriptor
                                    )
                                )
                            is MdibEntity.SetComponentStateOperation ->
                                AbstractOperationDescriptorOneOfMsg.newBuilder()
                                    .setSetComponentStateOperationDescriptor(
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_SetComponentStateOperationDescriptor(
                                            it.descriptor
                                        )
                                    )
                            else -> throw InvalidWhenBranchException(this)
                        }
                    )
                }
            }

            parent.sco = builder.build()
        }
    }

    private suspend fun mapSystemContext(parent: MdsDescriptorMsg.Builder, systemContexts: List<MdibEntity>) {
        systemContexts.firstOrNull()?.let { systemContext ->
            if (systemContext !is MdibEntity.SystemContext) {
                return
            }

            val builder = SystemContextDescriptorMsg
                .newBuilder(KotlinToProto.map_org_somda_protosdc_model_biceps_SystemContextDescriptor(systemContext.descriptor))

            systemContext.children.forEach { child ->
                mdibAccess.anyEntity(child).getOrNull()?.let {
                    when (it) {
                        is MdibEntity.PatientContext ->
                            builder.patientContext =
                                KotlinToProto.map_org_somda_protosdc_model_biceps_PatientContextDescriptor(it.descriptor)
                        is MdibEntity.LocationContext ->
                            builder.locationContext =
                                KotlinToProto.map_org_somda_protosdc_model_biceps_LocationContextDescriptor(it.descriptor)
                        is MdibEntity.EnsembleContext ->
                            builder.addEnsembleContext(
                                KotlinToProto.map_org_somda_protosdc_model_biceps_EnsembleContextDescriptor(
                                    it.descriptor
                                )
                            )
                        is MdibEntity.OperatorContext ->
                            builder.addOperatorContext(
                                KotlinToProto.map_org_somda_protosdc_model_biceps_OperatorContextDescriptor(
                                    it.descriptor
                                )
                            )
                        is MdibEntity.WorkflowContext ->
                            builder.addWorkflowContext(
                                KotlinToProto.map_org_somda_protosdc_model_biceps_WorkflowContextDescriptor(
                                    it.descriptor
                                )
                            )
                        is MdibEntity.MeansContext ->
                            builder.addMeansContext(
                                KotlinToProto.map_org_somda_protosdc_model_biceps_MeansContextDescriptor(
                                    it.descriptor
                                )
                            )
                        else -> throw InvalidWhenBranchException(this)
                    }
                }
            }

            parent.systemContext = builder.build()
        }
    }

    private suspend fun mapAlertSystem(
        parent: AbstractComplexDeviceComponentDescriptorMsg.Builder,
        alertSystems: List<MdibEntity>
    ) {
        alertSystems.firstOrNull()?.let { alertSystem ->
            if (alertSystem !is MdibEntity.AlertSystem) {
                return
            }

            val builder = AlertSystemDescriptorMsg
                .newBuilder(KotlinToProto.map_org_somda_protosdc_model_biceps_AlertSystemDescriptor(alertSystem.descriptor))

            alertSystem.children.forEach { child ->
                mdibAccess.anyEntity(child).getOrNull()?.let {
                    if (it is MdibEntity.AlertSignal) {
                        builder.addAlertSignal(
                            KotlinToProto.map_org_somda_protosdc_model_biceps_AlertSignalDescriptor(it.descriptor)
                        )
                    } else {
                        builder.addAlertCondition(
                            when (it) {
                                is MdibEntity.AlertCondition ->
                                    AlertConditionDescriptorOneOfMsg.newBuilder().setAlertConditionDescriptor(
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_AlertConditionDescriptor(it.descriptor)
                                    )

                                is MdibEntity.LimitAlertCondition ->
                                    AlertConditionDescriptorOneOfMsg.newBuilder().setLimitAlertConditionDescriptor(
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_LimitAlertConditionDescriptor(
                                            it.descriptor
                                        )
                                    )

                                else -> throw InvalidWhenBranchException(this)
                            }
                        )
                    }
                }
            }

            parent.alertSystem = builder.build()

        }
    }
}