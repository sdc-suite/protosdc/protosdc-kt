package org.somda.protosdc.device.consumer

import org.somda.protosdc.model.biceps.*
import java.math.BigDecimal

public object SetServiceFactory {
    public fun activate(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        argument: List<Activate.Argument> = listOf()
    ): Activate = Activate(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        argument = argument
    )

    public fun setString(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        requestedStringValue: String
    ): SetString = SetString(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        requestedStringValue = requestedStringValue
    )

    public fun setValue(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        requestedNumericValue: BigDecimal
    ): SetValue = SetValue(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        requestedNumericValue = requestedNumericValue
    )

    public fun setMetricState(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        proposedMetricState: List<AbstractMetricStateOneOf>
    ): SetMetricState = SetMetricState(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        proposedMetricState = proposedMetricState
    )

    public fun setMetricState(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        proposedMetricState: AbstractMetricStateOneOf
    ): SetMetricState = SetMetricState(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        proposedMetricState = listOf(proposedMetricState)
    )

    public fun setAlertState(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        proposedAlertState: AbstractAlertStateOneOf
    ): SetAlertState = SetAlertState(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        proposedAlertState = proposedAlertState
    )

    public fun setComponentState(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        proposedComponentState: List<AbstractDeviceComponentStateOneOf>
    ): SetComponentState = SetComponentState(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        proposedComponentState = proposedComponentState
    )

    public fun setComponentState(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        proposedComponentState: AbstractDeviceComponentStateOneOf
    ): SetComponentState = SetComponentState(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        proposedComponentState = listOf(proposedComponentState)
    )

    public fun setContextState(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        proposedContextState: List<AbstractContextStateOneOf>
    ): SetContextState = SetContextState(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        proposedContextState = proposedContextState
    )

    public fun setContextState(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef,
        proposedContextState: AbstractContextStateOneOf
    ): SetContextState = SetContextState(
        abstractSet = abstractSet(
            extensionElement = extensionElement,
            operationHandleRef = operationHandleRef
        ),
        proposedContextState = listOf(proposedContextState)
    )

    private fun abstractSet(
        extensionElement: Extension? = null,
        operationHandleRef: HandleRef
    ) = AbstractSet(
        extensionElement = extensionElement,
        operationHandleRef = operationHandleRef
    )
}