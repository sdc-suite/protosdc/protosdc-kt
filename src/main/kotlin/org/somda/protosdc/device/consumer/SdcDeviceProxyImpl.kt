package org.somda.protosdc.device.consumer

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.grpc.Channel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.addressing.createFor
import org.somda.protosdc.biceps.common.access.ObservableMdibAccess
import org.somda.protosdc.biceps.common.toMdibVersion
import org.somda.protosdc.biceps.consumer.access.RemoteMdibAccess
import org.somda.protosdc.common.*
import org.somda.protosdc.device.common.Action
import org.somda.protosdc.device.common.ServiceType
import org.somda.protosdc.biceps.common.ModificationsBuilder
import org.somda.protosdc.biceps.mdibAccessFactory
import org.somda.protosdc.device.consumer.helper.ReportProcessor
import org.somda.protosdc.device.consumer.helper.ReportWriter
import org.somda.protosdc.device.consumer.sco.ScoController
import org.somda.protosdc.metadata.consumer.Metadata
import org.somda.protosdc.network.grpc.client.ChannelFactory
import org.somda.protosdc.proto.mapping.biceps.ProtoToKotlin
import org.somda.protosdc.proto.model.*
import org.somda.protosdc.proto.model.biceps.GetMdibMsg
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.URI
import java.time.Duration
import kotlin.time.Duration.Companion.milliseconds

internal class SdcDeviceProxyImpl @AssistedInject constructor(
    @Assisted private val metadata: Metadata,
    private val channelFactory: ChannelFactory,
    private val reportWriterFactory: ReportWriter.Factory,
    private val reportProcessor: ReportProcessor,
    scoControllerFactory: ScoController.Factory,
    instanceId: InstanceId,
    protoSdcConfig: ProtoSdcConfig
) : SdcDeviceProxy, ServiceDelegate by ServiceDelegateImpl() {
    private val startupWait = Duration.ofMillis(protoSdcConfig.maxStartupWaitInMillis)

    @AssistedFactory
    interface Factory {
        fun create(metadata: Metadata): SdcDeviceProxyImpl
    }

    private val mdibAccessFactory = mdibAccessFactory {
        config(protoSdcConfig.bicepsConfig)
    }

    private val stopMutex = Mutex()
    private val mdibReportingReady = WakeUpSignal()
    private val setServiceReady = WakeUpSignal()

    private val logger by InstanceLogger(instanceId())
    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)
    private var scoController: ScoController? = null

    private val channels = mutableMapOf<URI, Channel>()
    private val shutDownPublisher = ChannelPublisher<ShutDownInfo>("ShutDownPublisher", 1)
    private lateinit var mdibAccess: RemoteMdibAccess

    init {
        onStartUp {
            metadata.services.forEach { service ->
                service.physicalAddressList.firstOrNull()?.let { uriString ->
                    URI.create(uriString.value).also { uri ->
                        if (!channels.containsKey(uri)) {
                            channels[uri] = awaitWithIoDispatcher { InetAddress.getByName(uri.host) }
                                .getOrThrow()
                                .let { addr -> channelFactory.create(InetSocketAddress(addr, uri.port)) }
                        }
                    }
                }
            }

            val setServiceStub = runCatching {
                val setServiceUri =
                    URI(metadata.services.first { it.typeList.contains(ServiceType.SET_SERVICE.typeName) }
                        .physicalAddressList.first().value)

                channels[setServiceUri]?.let {
                    SetServiceGrpcKt.SetServiceCoroutineStub(it)
                } ?: throw Exception("No matching channel found for ${ServiceType.SET_SERVICE.typeName}")
            }

            setServiceStub.onSuccess { result ->
                scoController = scoControllerFactory.create(result)
                result.operationInvokedReport(
                    OperationInvokedReportRequest.newBuilder()
                        .setAddressing(createFor(Action.SUBSCRIBE_OPERATION_INVOKED_REPORT.stringRepresentation))
                        .build()
                ).onStart {
                    logger.info { "Subscribe for operation invoked reports" }
                    setServiceReady.sendSignal()
                }.onEach { scoController!!.processOperationInvokedReport(it) }
                    .onCompletion {
                        logger.info {
                            when (it == null) {
                                true -> "Collecting operation invoked reports completed successfully"
                                false -> "Collecting operation invoked reports failed with message: ${it.message}"
                            }
                        }
                        if (it != null) {
                            shutDownWithError(it)
                        }
                    }
                    .catch { logger.warn(it) { "Exception while collecting operation invoked reports: ${it.message}" } }
                    .launchIn(coroutineScopeBinding)
            }

            val mdibReportingServiceStub = try {
                val reportingServiceUri =
                    URI(metadata.services.first { it.typeList.contains(ServiceType.MDIB_REPORTING_SERVICE.typeName) }
                        .physicalAddressList.first().value)

                channels[reportingServiceUri]?.let {
                    MdibReportingServiceGrpcKt.MdibReportingServiceCoroutineStub(it)
                } ?: throw Exception("No matching channel found for ${ServiceType.MDIB_REPORTING_SERVICE.typeName}")
            } catch (e: Exception) {
                logger.warn(e) { "No MdibReportingService at ${metadata.endpoint.endpointIdentifier}" }
                shutDownWithError(e)
                return@onStartUp
            }

            val getServiceStub = try {
                val getServiceUri =
                    URI(metadata.services.first { it.typeList.contains(ServiceType.GET_SERVICE.typeName) }
                        .physicalAddressList.first().value)

                channels[getServiceUri]?.let {
                    GetServiceGrpcKt.GetServiceCoroutineStub(it)
                } ?: throw Exception("No matching channel found for ${ServiceType.GET_SERVICE.typeName}")
            } catch (e: Exception) {
                logger.warn(e) { "No GetService at ${metadata.endpoint.endpointIdentifier}" }
                shutDownWithError(e)
                return@onStartUp
            }

            reportProcessor.start()

            mdibReportingServiceStub.episodicReport(
                EpisodicReportRequest.newBuilder()
                    .setAddressing(createFor(Action.SUBSCRIBE_EPISODIC_REPORT.stringRepresentation))
                    .build()
            ).onStart {
                logger.info { "Subscribe for episodic MDIB reports" }
                mdibReportingReady.sendSignal()
            }.onEach { reportProcessor.processReport(it.report) }
                .onCompletion {
                    logger.info {
                        when (it == null) {
                            true -> "Collecting episodic MDIB reports completed successfully"
                            false -> "Collecting episodic MDIB reports failed with message: ${it.message}"
                        }
                    }
                    if (it != null) {
                        shutDownWithError(it)
                    }
                }
                .catch { logger.warn(it) { "Exception while collecting episodic MDIB reports: ${it.message}" } }
                .launchIn(coroutineScopeBinding)

            val getMdibResponseMsg = getServiceStub.getMdib(
                GetMdibRequest.newBuilder()
                    .setAddressing(createFor(Action.GET_MDIB.stringRepresentation))
                    .setPayload(GetMdibMsg.getDefaultInstance())
                    .build()
            )
            val getMdibResponse =
                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_GetMdibResponseMsg(getMdibResponseMsg.payload)
            val mdibVersion = getMdibResponse.abstractGetResponse.mdibVersionGroupAttr.toMdibVersion()
            mdibAccess = mdibAccessFactory.createRemoteMdibAccess(mdibVersion)
            mdibAccess.writeDescription(
                ModificationsBuilder(getMdibResponse.mdib).get(),
                mdibVersion.version
            )
            reportProcessor.startApplyingReportsOnMdib(reportWriterFactory.create(mdibAccess))

            runCatching {
                // wait for mdib reporting and set service to be ready
                mdibReportingReady.awaitSignal(startupWait.toMillis().milliseconds)
                setServiceReady.awaitSignal(startupWait.toMillis().milliseconds)
            }.onFailure {
                throw Exception("SDC device proxy could not be started", it)
            }
        }

        onShutDown {
            reportProcessor.stop()
            mdibAccess.unsubscribeAll()
            coroutineScopeBinding.cancel()
        }
    }

    private suspend fun shutDownWithError(reason: Throwable) {
        coroutineScope {
            awaitWithIoDispatcher {
                stopMutex.withLock {
                    when (isRunning()) {
                        true -> stop().let { reason }
                        false -> null
                    }
                }?.let { shutDownPublisher.offer(ShutDownInfo(metadata.endpoint, it)) }
            }
        }
    }

    override suspend fun metadata() = checkRunning { metadata }

    override suspend fun endpointIdentifier(): String = checkRunning { metadata.endpoint.endpointIdentifier }

    override suspend fun mdibAccess(): ObservableMdibAccess = checkRunning { mdibAccess }

    override suspend fun setService() = checkRunning { scoController }

    override suspend fun subscribeShutDown() = shutDownPublisher.subscribe()
}