package org.somda.protosdc.device.consumer.helper

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.somda.protosdc.biceps.common.*
import org.somda.protosdc.biceps.consumer.access.RemoteMdibAccess
import org.somda.protosdc.model.biceps.*
import org.somda.protosdc.proto.mapping.biceps.ProtoToKotlin
import org.somda.protosdc.proto.model.EpisodicReport

/**
 * Helper class that accepts any state reports and writes them to a [RemoteMdibAccess] instance.
 *
 * The [ReportWriter] acts as a dispatcher for all reports SDC generates and which have to be transformed
 * to modifications a [RemoteMdibAccess] instance understands.
 */
internal class ReportWriter @AssistedInject constructor(
    @Assisted
    val mdibAccess: RemoteMdibAccess
) {
    @AssistedFactory
    interface Factory {
        fun create(mdibAccess: RemoteMdibAccess): ReportWriter
    }

    /**
     * Transforms the given report to a modifications set and writes it to the [RemoteMdibAccess] instance.
     *
     * @param report     the report to write.
     * @param mdibAccess the MDIB access to write to.
     * @throws ReportProcessingException in case the report cannot be transformed or dispatched correctly.
     * @throws PreprocessingException    corresponds to the exception that [RemoteMdibAccess.writeDescription] or
     * [RemoteMdibAccess.writeStates]
     * throws.
     */
    suspend fun write(report: EpisodicReport) {
        when (report.typeCase) {
            EpisodicReport.TypeCase.ALERT ->
                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_EpisodicAlertReportMsg(report.alert).also {
                    write(it.abstractAlertReport.abstractReport, makeModifications(it))
                }

            EpisodicReport.TypeCase.COMPONENT ->
                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_EpisodicComponentReportMsg(report.component)
                    .also {
                        write(it.abstractComponentReport.abstractReport, makeModifications(it))
                    }

            EpisodicReport.TypeCase.METRIC ->
                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_EpisodicMetricReportMsg(report.metric).also {
                    write(it.abstractMetricReport.abstractReport, makeModifications(it))
                }

            EpisodicReport.TypeCase.OPERATIONAL_STATE ->
                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_EpisodicOperationalStateReportMsg(report.operationalState)
                    .also {
                        write(it.abstractOperationalStateReport.abstractReport, makeModifications(it))
                    }

            EpisodicReport.TypeCase.CONTEXT ->
                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_EpisodicContextReportMsg(report.context).also {
                    write(it.abstractContextReport.abstractReport, makeModifications(it))
                }

            EpisodicReport.TypeCase.WAVEFORM ->
                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_WaveformStreamMsg(report.waveform).also {
                    write(it.abstractReport, makeModifications(it))
                }

            EpisodicReport.TypeCase.DESCRIPTION ->
                ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_DescriptionModificationReportMsg(report.description)
                    .also {
                        write(it.abstractReport, makeModifications(it))
                    }

            else -> throw Exception("Receive unexpected report message")
        }
    }

    private suspend fun write(report: AbstractReport, modifications: MdibStateModifications) =
        mdibAccess.writeStates(modifications, report.mdibVersionGroupAttr.version())

    private suspend fun write(report: AbstractReport, modifications: MdibDescriptionModifications) =
        mdibAccess.writeDescription(modifications, report.mdibVersionGroupAttr.version())

    private fun makeModifications(report: DescriptionModificationReport) =
        MdibDescriptionModifications().also { modifications ->
            report.reportPart.forEach { reportPart ->
                reportPart.descriptor.forEach { descriptor ->
                    DescriptionItem(
                        descriptor,
                        reportPart.state.filter { it.descriptorHandleString() == descriptor.handleString() },
                        reportPart.parentDescriptorAttr?.string
                    ).also { descriptorItem ->
                        modifications.add(
                            when (reportPart.modificationTypeAttr?.enumType
                                ?: DescriptionModificationType.EnumType.Upt) {
                                DescriptionModificationType.EnumType.Crt ->
                                    MdibDescriptionModification.Insert(descriptorItem)

                                DescriptionModificationType.EnumType.Del ->
                                    MdibDescriptionModification.Delete(descriptorItem)

                                DescriptionModificationType.EnumType.Upt ->
                                    MdibDescriptionModification.Update(descriptorItem)
                            }
                        )
                    }
                }
            }
        }

    private fun makeModifications(report: WaveformStream) = MdibStateModifications.Waveform(
        report.state.map {
            StateModification.Waveform(it)
        }
    )

    private fun makeModifications(report: EpisodicAlertReport) = MdibStateModifications.Alert(
        report.abstractAlertReport.reportPart.fold(mutableListOf()) { acc, item ->
            acc.apply {
                addAll(item.alertState.map { StateModification.Alert(it) })
            }
        }
    )

    private fun makeModifications(report: EpisodicComponentReport) = MdibStateModifications.Component(
        report.abstractComponentReport.reportPart.fold(mutableListOf()) { acc, item ->
            acc.apply {
                addAll(item.componentState.map { StateModification.Component(it) })
            }
        }
    )

    private fun makeModifications(report: EpisodicMetricReport) = MdibStateModifications.Metric(
        report.abstractMetricReport.reportPart.fold(mutableListOf()) { acc, item ->
            acc.apply {
                addAll(item.metricState.map { StateModification.Metric(it) })
            }
        }
    )

    private fun makeModifications(report: EpisodicContextReport) = MdibStateModifications.Context(
        report.abstractContextReport.reportPart.fold(mutableListOf()) { acc, item ->
            acc.apply {
                addAll(item.contextState.map { StateModification.Context(it) })
            }
        }
    )

    private fun makeModifications(report: EpisodicOperationalStateReport) = MdibStateModifications.Operation(
        report.abstractOperationalStateReport.reportPart.fold(mutableListOf()) { acc, item ->
            acc.apply {
                addAll(item.operationState.map { StateModification.Operation(it) })
            }
        }
    )
}