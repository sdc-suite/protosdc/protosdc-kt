package org.somda.protosdc.device.consumer.helper

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.biceps.common.toMdibVersion
import org.somda.protosdc.biceps.consumer.access.RemoteMdibAccess
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.proto.mapping.biceps.ProtoToKotlin
import org.somda.protosdc.proto.model.EpisodicReport
import org.somda.protosdc.proto.model.biceps.AbstractReportMsg
import javax.inject.Inject

/**
 * Class that is responsible for buffering and processing of incoming reports.
 *
 *
 * As long as there is no MDIB (plus optional context states) available, the report processor buffers incoming reports
 * and only applies them once an MDIB (plus optional context states) was set.
 * In case every report type is received through one subscription, the [ReportProcessor] ensures data
 * coherency.
 */
internal class ReportProcessor @Inject constructor(
    instanceId: InstanceId
) : ServiceDelegate by ServiceDelegateImpl() {
    private val logger by InstanceLogger(instanceId())

    private val mdibReadyMutex = Mutex()
    private var reportWriter: ReportWriter? = null
    private val bufferedReportsChannel = Channel<EpisodicReport>(10000, BufferOverflow.DROP_OLDEST)
    private val mdibAccessChannel = Channel<EpisodicReport>()

    private var writeChannel = bufferedReportsChannel

    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)

    init {
        onStartUp {
            coroutineScopeBinding.launch {
                runCatching {
                    while (coroutineScopeBinding.isActive) {
                        mdibAccessChannel.receive().let { report ->
                            reportWriter?.let { writer ->
                                applyReportsFromBuffer(writer)
                                writer.write(report)
                            }
                        }
                    }
                }.onFailure {
                    logger.warn(it) { "Exception while writing reports: ${it.message}" }
                }
            }
        }

        onShutDown {
            coroutineScopeBinding.cancel()
        }
    }

    /**
     * Queues or processes a report.
     *
     * In case no MDIB was set via [.startApplyingReportsOnMdib] yet,
     * this function queues incoming reports.
     * Once [.startApplyingReportsOnMdib] was called, reports are
     * directly applied on the injected [RemoteMdibAccess] instance.
     *
     * As soon as the [ReportProcessor] is shut down, no reports will be processed henceforth.
     *
     * @param report the report to process.
     */
    suspend fun processReport(report: EpisodicReport) {
        writeChannel.send(report)
    }

    /**
     * Accepts an MDIB and starts applying reports on it.
     *
     * @param reportWriter the [RemoteMdibAccess] writer.
     */
    suspend fun startApplyingReportsOnMdib(reportWriter: ReportWriter) {
        mdibReadyMutex.withLock {
            if (this.reportWriter != null) {
                logger.warn {
                    "Tried to invoke startApplyingReportsOnMdib() multiple times. " +
                            "Make sure to call it only once. " +
                            "Invocation ignored."
                }
                return
            }
            this.reportWriter = reportWriter
        }

        applyReportsFromBuffer(reportWriter)
        this.writeChannel = mdibAccessChannel
    }

    private suspend fun applyReportsFromBuffer(reportWriter: ReportWriter) {
        while (true) {
            bufferedReportsChannel.tryReceive().getOrNull()?.let {
                applyReportOnMdib(bufferedReportsChannel.receive(), reportWriter)
            } ?: break
        }
    }

    private fun mdibVersionFromReport(report: AbstractReportMsg) =
        ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_MdibVersionGroupMsg(
            report.mdibVersionGroupAttr
        ).toMdibVersion()

    private suspend fun applyReportOnMdib(report: EpisodicReport, reportWriter: ReportWriter) {
        val mdibAccessMdibVersion = reportWriter.mdibAccess.mdibVersion()
        val reportMdibVersion = when (report.typeCase) {
            EpisodicReport.TypeCase.ALERT -> mdibVersionFromReport(report.alert.abstractAlertReport.abstractReport)
            EpisodicReport.TypeCase.COMPONENT -> mdibVersionFromReport(report.component.abstractComponentReport.abstractReport)
            EpisodicReport.TypeCase.METRIC -> mdibVersionFromReport(report.metric.abstractMetricReport.abstractReport)
            EpisodicReport.TypeCase.OPERATIONAL_STATE -> mdibVersionFromReport(report.operationalState.abstractOperationalStateReport.abstractReport)
            EpisodicReport.TypeCase.CONTEXT -> mdibVersionFromReport(report.context.abstractContextReport.abstractReport)
            EpisodicReport.TypeCase.WAVEFORM -> mdibVersionFromReport(report.waveform.abstractReport)
            EpisodicReport.TypeCase.DESCRIPTION -> mdibVersionFromReport(report.description.abstractReport)
            else -> throw IllegalStateException("Episodic report contains unsupported enumeration value: ${report.typeCase}")
        }
        if (mdibAccessMdibVersion.sequenceId != reportMdibVersion.sequenceId ||
            mdibAccessMdibVersion.instanceId != reportMdibVersion.instanceId
        ) {
            throw Exception(
                "MDIB version from MDIB ($mdibAccessMdibVersion) and " +
                        "MDIB version from report ($reportMdibVersion) do not match"
            )
        }
        if (mdibAccessMdibVersion.version >= reportMdibVersion.version) {
            logger.warn {
                "Received outdated report from provider. Latest known version: " +
                        "${mdibAccessMdibVersion.version}, received version: ${reportMdibVersion.version}"
            }
            return
        }
        reportWriter.write(report)
    }
}