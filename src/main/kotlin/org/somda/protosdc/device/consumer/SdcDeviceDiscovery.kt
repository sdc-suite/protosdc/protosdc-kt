package org.somda.protosdc.device.consumer

import org.somda.protosdc.discovery.consumer.DiscoveryConsumer
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.proto.model.discovery.Endpoint
import java.net.URI

public interface SdcDeviceDiscovery : DiscoveryConsumer {
    public interface Factory {
        public fun create(
            udp: Udp,
            discoveryProxyAddress: URI?
        ): SdcDeviceDiscovery

        public fun create(
            udp: Udp
        ): SdcDeviceDiscovery = create(udp, null)
    }

    public suspend fun connect(endpoint: Endpoint): SdcDeviceProxy
}