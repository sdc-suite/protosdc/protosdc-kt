package org.somda.protosdc.dagger

import dagger.Module
import dagger.Provides
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.common.ComponentConfig
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.crypto.CryptoStore
import org.somda.protosdc.crypto.CryptoUtil
import java.io.File
import java.io.FileInputStream
import java.util.*
import javax.annotation.Nullable
import javax.inject.Singleton


@Module
internal abstract class ProtoSdcModule {
    companion object {
        @Provides
        @Singleton
        fun protoSdcConfig(@Nullable @ComponentConfig config: ProtoSdcConfig?) = config ?: ProtoSdcConfig()

        @Provides
        @Singleton
        fun grpcConfig(protoSdcConfig: ProtoSdcConfig) = protoSdcConfig.grpc

        @Provides
        @Singleton
        fun discoveryConfig(protoSdcConfig: ProtoSdcConfig) = protoSdcConfig.discovery

        @Provides
        @Singleton
        fun deviceConfig(protoSdcConfig: ProtoSdcConfig) = protoSdcConfig.device

        @Provides
        @Singleton
        fun addressingConfig(protoSdcConfig: ProtoSdcConfig) = protoSdcConfig.addressing

        @Provides
        @Singleton
        fun cryptoStore(@Nullable @ComponentConfig cryptoStore: CryptoStore?, protoSdcConfig: ProtoSdcConfig) =
            when (cryptoStore) {
                null -> when (protoSdcConfig.grpc.serverInsecure) {
                    true -> null // no crypto store / cryptography config, which is ok as we are running insecure mode
                    false -> when (val cryptography = protoSdcConfig.cryptography) {
                        null -> throw Exception(
                            "No crypto store and no cryptography section in config, but gRPC " +
                                    "is running in secure mode"
                        )

                        else -> when (protoSdcConfig.grpc.serverInsecure) {
                            true -> null
                            false -> CryptoUtil.loadCryptoSettingsFromPemStreams(
                                userKeyInputStream(cryptography.userKeyFilePath),
                                userCertInputStream(cryptography.userCertFilePath),
                                caCertInputStream(cryptography.caCertFilePath),
                                cryptography.userKeyPassword
                            ).getOrThrow()
                        }
                    }
                }

                else -> cryptoStore
            }

        @Provides
        @Singleton
        fun instanceId() = InstanceId(UUID.randomUUID().toString())

        private fun userKeyInputStream(path: String) = FileInputStream(File(path).absoluteFile.also {
            require(it.exists()) {
                "Private key PEM at '${it.absolutePath}' does not exists but is required as " +
                        "gRPC is running in secure mode."
            }
        })

        private fun userCertInputStream(path: String) = FileInputStream(File(path).absoluteFile.also {
            require(it.exists()) {
                "Public key PEM at '${it.absolutePath}' does not exists but is required as " +
                        "gRPC is running in secure mode."
            }
        })

        private fun caCertInputStream(path: String) = FileInputStream(File(path).absoluteFile.also {
            require(it.exists()) {
                "CA certificate PEM at '${it.absolutePath}' does not exists but is required as " +
                        "gRPC is running in secure mode."
            }
        })
    }
}