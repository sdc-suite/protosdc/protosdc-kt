package org.somda.protosdc.dagger

import dagger.BindsInstance
import dagger.Component
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.common.ComponentConfig
import org.somda.protosdc.crypto.CryptoStore
import org.somda.protosdc.device.consumer.SdcDeviceDiscovery
import org.somda.protosdc.device.consumer.SdcDeviceProxy
import org.somda.protosdc.device.dagger.DeviceModule
import org.somda.protosdc.device.provider.SdcDeviceImpl
import org.somda.protosdc.discovery.consumer.DiscoveryConsumer
import org.somda.protosdc.discovery.dagger.DiscoveryModule
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.discovery.proxy.DiscoveryProxy
import org.somda.protosdc.discovery.proxy.DiscoveryProxyImpl
import org.somda.protosdc.metadata.consumer.MetadataConsumer
import org.somda.protosdc.metadata.dagger.MetadataModule
import org.somda.protosdc.metadata.provider.MetadataProvider
import org.somda.protosdc.metadata.provider.MetadataProviderImpl
import org.somda.protosdc.network.grpc.client.ChannelFactory
import org.somda.protosdc.network.grpc.server.Server
import javax.annotation.Nullable
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ProtoSdcModule::class,
        DeviceModule::class,
        DiscoveryModule::class,
        MetadataModule::class
    ]
)
internal interface ProtoSdcComponent {
    @Component.Builder
    interface Builder {
        fun bind(@BindsInstance @ComponentConfig @Nullable protoSdcConfig: ProtoSdcConfig?): Builder
        fun bind(@BindsInstance @ComponentConfig @Nullable cryptoStoreOverride: CryptoStore?): Builder
        fun build(): ProtoSdcComponent
    }

    fun serverFactory(): Server.Factory
    fun channelFactory(): ChannelFactory
    fun discoveryProviderFactory(): DiscoveryProvider.Factory
    fun discoveryConsumerFactory(): DiscoveryConsumer.Factory
    fun discoveryProxyFactory(): DiscoveryProxyImpl.Factory
    fun metadataProviderFactory(): MetadataProviderImpl.Factory
    fun metadataConsumerFactory(): MetadataConsumer.Factory

    fun sdcDeviceFactory(): SdcDeviceImpl.Factory
    fun sdcDeviceProxyFactory(): SdcDeviceProxy.Factory
    fun sdcDeviceDiscoveryFactory(): SdcDeviceDiscovery.Factory
}