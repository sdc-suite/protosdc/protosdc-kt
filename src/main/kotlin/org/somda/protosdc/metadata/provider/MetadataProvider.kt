package org.somda.protosdc.metadata.provider

import org.somda.protosdc.common.Service
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.proto.model.metadata.EndpointMetadata

/**
 * Metadata provider service interface.
 *
 * There is currently nothing to be settable or gettable by the service except for the interface induced by
 * [Service].
 *
 * To keep things simple, endpoint metadata and services cannot be changed after instantiation / while the service
 * is running.
 */
public interface MetadataProvider : Service