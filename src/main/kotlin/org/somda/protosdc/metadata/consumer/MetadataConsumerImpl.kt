package org.somda.protosdc.metadata.consumer

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.grpc.Channel
import org.somda.protosdc.addressing.AddressingValidator
import org.somda.protosdc.addressing.createFor
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.common.checkRunning
import org.somda.protosdc.metadata.common.Action
import org.somda.protosdc.network.grpc.client.ChannelFactory
import org.somda.protosdc.proto.model.metadata.GetMetadataRequest
import org.somda.protosdc.proto.model.metadata.MetadataServiceGrpcKt
import java.net.InetSocketAddress
import java.net.URI

internal class MetadataConsumerImpl @AssistedInject internal constructor(
    @Assisted private val physicalAddress: URI?,
    @Assisted private var channel: Channel?,
    private val grpcChannelFactory: ChannelFactory,
    private val addressingValidator: AddressingValidator
) : MetadataConsumer, ServiceDelegate by ServiceDelegateImpl() {
    private lateinit var metadataService: MetadataServiceGrpcKt.MetadataServiceCoroutineStub

    @AssistedFactory
    interface Factory {
        fun create(physicalAddress: URI?, channel: Channel?): MetadataConsumerImpl
    }

    init {
        onStartUp {
            metadataService = when (physicalAddress) {
                null -> channel?.let { MetadataServiceGrpcKt.MetadataServiceCoroutineStub(it) }
                    ?: throw IllegalArgumentException("Expecting a non-null channel, but was null")
                else -> InetSocketAddress(physicalAddress.host, physicalAddress.port).let { socketAddr ->
                    grpcChannelFactory.create(socketAddr).let { channel ->
                        MetadataServiceGrpcKt.MetadataServiceCoroutineStub(channel)
                    }
                }
            }
        }
    }

    override suspend fun metadata() = checkRunning {
        val metadataRequest = GetMetadataRequest.newBuilder()
            .setAddressing(createFor(Action.GET_METADATA_REQUEST.stringRepresentation))
            .build()
        metadataService.getMetadata(metadataRequest).let {
            addressingValidator.create(it.addressing)
                .validateMessageId()
                .validateAction(Action.GET_METADATA_RESPONSE.stringRepresentation)

            Metadata(it.endpoint, it.metadata, it.serviceList)
        }
    }
}
