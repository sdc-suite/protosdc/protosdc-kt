package org.somda.protosdc.metadata.dagger

import dagger.Module
import dagger.Provides
import io.grpc.Channel
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.metadata.consumer.MetadataConsumer
import org.somda.protosdc.metadata.consumer.MetadataConsumerImpl
import org.somda.protosdc.metadata.provider.MetadataProvider
import org.somda.protosdc.metadata.provider.MetadataProviderImpl
import org.somda.protosdc.metadata.provider.ServiceMetadata
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import java.net.URI
import javax.inject.Singleton

@Module(subcomponents = [])
internal abstract class MetadataModule {
    companion object {
        @Singleton
        @Provides
        fun metadataConsumerFactory(implFactory: MetadataConsumerImpl.Factory) = object : MetadataConsumer.Factory {
            override fun create(physicalAddress: URI) = implFactory.create(physicalAddress, null)
            override fun create(channel: Channel) = implFactory.create(null, channel)
        }
    }
}