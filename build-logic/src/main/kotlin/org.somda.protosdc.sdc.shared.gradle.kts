import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.somda.withDokkaJar

plugins {
    idea
    java
    `java-test-fixtures`

    kotlin("jvm")
    kotlin("kapt")
    kotlin("plugin.serialization")

    id("org.somda.repository_collection")
    id("org.jetbrains.dokka")
    id("org.somda.with_dokka_jar")
    id("org.somda.append_snapshot_id")
}

version = "1.0.0"
group = "org.somda.protosdc"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))

    api(libs.kotlinx.coroutines)

    implementation(libs.kotlinx.serialization.json)

    implementation(libs.bundles.protosdc)
    implementation(libs.bundles.logging)

    implementation(libs.google.guava)
    implementation(libs.apache.commons.io)
    implementation(libs.google.dagger)
    implementation(libs.ktoml.core)
    implementation(libs.mdib.dsl)

    testImplementation(kotlin("test"))
    testImplementation(libs.bundles.logging)
    testImplementation(libs.bundles.testing)
    testImplementation(libs.google.dagger)
    testImplementation(libs.mockito.kotlin)
    testImplementation(libs.slf4j.simple)
    testImplementation(libs.kotlinx.coroutines)

    testFixturesApi(libs.bundles.bouncycastle)
    testFixturesApi(libs.bundles.logging)
    testFixturesApi(libs.bundles.testing)
    testFixturesApi(libs.google.dagger)
    testFixturesApi(libs.kotlinx.coroutines)

    if (JavaVersion.current().isJava9Compatible) {
        // Workaround for @javax.annotation.Generated
        // see: https://github.com/grpc/grpc-java/issues/3633
        testFixturesApi(libs.javax.annotation)
    }

    kapt(libs.google.dagger.compiler)
    kaptTest(libs.google.dagger.compiler)
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

// read in a packages.md for package descriptions
tasks.dokkaHtml.configure {
    dokkaSourceSets {
        configureEach {
            includes.from("packages.md")
        }
    }
}

val javaSource: Int = 17
val jdkVersion: JvmTarget = JvmTarget.JVM_17

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(javaSource)
    }
    withSourcesJar()
    withDokkaJar()
}

kotlin {
    compilerOptions {
        jvmTarget = jdkVersion
    }
}

kapt {
    // allegedly, improves error output
    correctErrorTypes = true
}
