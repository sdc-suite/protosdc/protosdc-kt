import org.gradle.api.Project
import org.gradle.kotlin.dsl.the

// workaround to make libs available in pre-compiled plugins
val Project.libs
    get() = the<org.gradle.accessors.dm.LibrariesForLibs>()
