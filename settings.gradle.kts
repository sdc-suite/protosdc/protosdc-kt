pluginManagement {
    // include 'plugins build' to define convention plugins.
    includeBuild("build-logic")

    repositories {
        gradlePluginPortal()
        maven("https://gitlab.com/api/v4/projects/60445622/packages/maven")
    }
}

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

rootProject.name = "protosdc-kt"
include("common")
include("udp")
include("biceps")
include("examples")
include("udp")
include("crypto")
include("sco")
